// <!-- Copyright (c) 2014 GTECH Canada ULC and its affiliates -->
// Author: Patrice Richard (patrice.richard@GTECH.com)
var projIDTxt = ""; 
var projIDVal = "";
var projCompSelVal = "";
var projCompSelTxt = "";
var reportCategory = "3";
var reportNumber = "";
$(document).ready(function()
{
    google.load('visualization', '1', {'packages':['corechart']});
    $("#dms_showRep").hide();
    $("#dms_ComponentTitle").hide();

    //Loads the Setup Modal    
    $("#dms_setupModalBtn").click(function(){
        $projList = $("#dms_projList");
        $.ajax({
            url:'../ajax/ac_gen_loadProjList.php',
            dataType:'JSON',
            success:function(data){
                $projList.html('');
                $projList.append('<option id="all">--Choose a Project--</option>')
                $.each(data.proj, function(index, val){
                    //console.log(val.projID),
                    $projList.append('<option id="' + val.projID + '">' + val.projName + '(' + val.projCode + ')</option>');
                });
            }
        });
        $("#dms_setupModal").modal("show");
    }); 

    document.getElementById('dms_projList').onchange = function(){
        $compList = $("#dms_compList");
        var projListVal = $("#dms_projList option:selected").attr("id");
        console.log(projListVal)
        $.ajax({
            url: "../ajax/ac_gen_loadCompList.php",
            type:"POST",
            dataType:"json",
            data: {"projID" : projListVal},
            success:function(data){
                if(data.length == undefined)
                {
                    $compList.html('');
                    $compList.prop('disabled', false);
                    $compList.append('<option id="all">--All Components--</option>');
                    $.each(data.comp, function(key, val)
                    {
                        $compList.append('<option id="' + val.ID + '">' + val.cname + '</option>')
                    });
                }
                else
                {
                    $compList.html('');
                    $compList.prop('disabled', true);
                    $compList.append('<option id="all">--No Components--</option>');
                }
            }
        });
    };

    $("#dms_SetupCompleteBtn").click(function()
    {
        $.ajax
        ({
            url:"../ajax/ac_gen_createReport.php",
            type:"POST",
            dataType:"json",
            data:{"category":reportCategory},
            success:function(data)
            {
                //console.log(data.repID);
                reportNumber = data.repID;
                var dt = new Date();
                var time = dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
                $("#dms_repDate").text($.datepicker.formatDate('dd M yy', dt)+" "+time);
                projIDTxt = $("#dms_projList option:selected").text();
                projIDVal = $("#dms_projList option:selected").attr("id");
                projCompSelVal = $("#dms_compList option:selected").attr("id");
                projCompSelTxt = $("#dms_compList option:selected").text();
                if(projCompSelVal !== 'all'){
                    $("#dms_ComponentTitle").show();
                    $("#dms_ComponentTitle").html('<h4><strong>Component:</strong>'+projCompSelTxt+'</h4>');
                }
                $("#dms_showRep").show();
                $("#dms_ReportTile").text(projIDTxt);
                $("#dms_setupModal").modal("hide");
                $("#dms_runDateOnScreen").text($("#dmsSetupStartDate").val()+ '  -  ' +$("#dmsSetupEndDate").val());
                $("#dms_setupModalBtn").hide();
                load1_1OustandingDefectsTbl();
                load1_2DefectFindRateGrph();
                load1_3DefectResRateGrph();
                load1_3DefectResRateTbl();
//                load1_4DefectBuildRateTbl();
//                load1_5DefectDensityTbl();
//                load1_6CompDefDistGrph();
//                load1_7DefectReopenRateTbl();
//                load1_8DefectSeverityTbl();
            }
        });
    });
});
function load1_1OustandingDefectsTbl(){
$("#dms_outDefTbl").jqGrid({
        url:'../ajax/ac_dms_loadOutDefectsTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal, "repID":reportNumber},
        mtype: 'POST',
        height:'auto',
        width:380,
        pager:false,
        colModel:[
            {label:'Severity', name:'defSev', width:60 , key: true},
            {label:'Total', name:'defTotal', width: 40},
            {label:'Unresolved', name:'defUnres', width: 40},
            {label:'Resolved', name:'defResolved', width: 40},
            {label:'In Test', name:'defInTest', width: 40}
        ]
    });
};
function load1_2DefectFindRateGrph(){
    var findRateJSON = $.ajax({
          url: "../ajax/ac_dms_loadFindRateGrph.php",
          type:"POST",
          dataType:"json",
          data: {"projID":projIDVal, "compVal":projCompSelVal, "repID":reportNumber},
          async: false
          }).responseText;
          
      var findRateData = new google.visualization.DataTable(findRateJSON);
      var view = new google.visualization.DataView(findRateData);
      view.setColumns([0,1,{
              calc:"stringify",
              sourceColumn:1,
              type:"string",
              role:"annotation"
      },2,{
              calc:"stringify",
              sourceColumn:2,
              type:"string",
              role:"annotation"
      }]);

      var chart = new google.visualization.ColumnChart(document.getElementById('dms_defFindRateGrph'));
      var options = {
          width:1000,
          height:340,
          isStacked:true
      };
      chart.draw(view, options);
};
function load1_3DefectResRateGrph(){
    var resRateJSON = $.ajax({
          url: "../ajax/ac_dms_loadResRateGrph.php",
          type:"POST",
          dataType:"json",
          data: {"projID":projIDVal, "compVal":projCompSelVal, "repID":reportNumber},
          async: false
          }).responseText;
          
      var resRateData = new google.visualization.DataTable(resRateJSON); 

      var chart = new google.visualization.AreaChart(document.getElementById('dms_defResRateGrph'));
      var options = {
          width:1000,
          height:340
      };
      chart.draw(resRateData, options);
};
function load1_3DefectResRateTbl(){
$("#dms_defResRateTbl").jqGrid({
        url:'../ajax/ac_dms_loadResRateTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal, "repID":reportNumber},
        mtype: 'POST',
        height:'auto',
        width:280,
        pager:false,
        colModel:[
            {label:'Information', name:'infTxt', width:60 , key: true},
            {label:'Value', name:'infVal', width: 40}
        ]
    });
};
function load1_4DefectBuildRateTbl(){
$("#dms_buildRateTbl").jqGrid({
        url:'../ajax/ac_dms_loadBuildRateTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal},
        mtype: 'POST',
        height:'auto',
        width:380,
        pager:false,
        colModel:[
            {label:'Version Name', name:'versName', width:60 , key: true},
            {label:'Release Date', name:'relDate', width: 40},
            {label:'Issue Count', name:'issKey', width: 40}
        ]
    });
};
function load1_5DefectDensityTbl(){
$("#dms_defDensityTbl").jqGrid({
        url:'../ajax/ac_dms_loadDefectDensityTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal},
        mtype: 'POST',
        height:'auto',
        width:380,
        pager:false,
        colModel:[
            {label:'Group', name:'defSev', width:60 , key: true},
            {label:'Confirmed Defects', name:'defConf', width: 40},
            {label:'Components', name:'defComp', width: 40}, 
            {label:'Density', name:'defDensity', width: 40}
        ]
    });
};
function load1_6CompDefDistGrph(){

   var compDistJSON = $.ajax({
          url: "../ajax/ac_dms_loadCompDefDistGrph.php?chof=validate",
          type:"POST",
          dataType:"json",
          data: {"projID":projIDVal, "compVal":projCompSelVal},
          async: false
          }).responseText;
          
      var compDistData = new google.visualization.DataTable(compDistJSON);

      var chartCompDist = new google.visualization.PieChart(document.getElementById('dms_compDistGrph'));
      var options = {
          width:1000,
          height:340
      };
      chartCompDist.draw(compDistData, options);
};
function load1_7DefectReopenRateTbl(){
$("#dms_defReopTbl").jqGrid({
        url:'../ajax/ac_dms_loadReopRateTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal},
        mtype: 'POST',
        height:'auto',
        width:380,
        pager:false,
        colModel:[
            {label:'Category', name:'defSev', width:60 , key: true},
            {label:'Closed', name:'defClosed', width: 40},
            {label:'Reopened', name:'defReopened', width: 40},
            {label:'% Reopened', name:'perReop', width: 40}
        ]
    });
};
function load1_8DefectSeverityTbl(){
$("#dms_defSevTbl").jqGrid({
        url:'../ajax/ac_dms_loadDefSevTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal},
        mtype: 'POST',
        height:'auto',
        width:680,
        pager:false,
        colModel:[
            {label:'Category', name:'defSev', width:60 , key: true},
            {label:'GIT', name:'defGIT', width: 40},
            {label:'QA', name:'defQA', width: 40},
            {label:'Total', name:'defTotal', width: 40}
        ]
    });
};