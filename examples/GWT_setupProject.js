// <!-- Copyright (c) 2014 GTECH Canada ULC and its affiliates -->
// Author: Patrice Richard (patrice.richard@GTECH.com)
var projIDTxt = ""; 
var projIDVal = "";
var projCompSelVal = "";
var projCompSelTxt = "";
var reportCategory = "1";
var reportNumber = "";
$(document).ready(function(){
    google.load('visualization', '1', {'packages':['corechart']});
    $("#gwt_showRep").hide();

    //Loads the Setup Modal    
    $("#gwt_setupModalBtn").click(function(){
        $projList = $("#gwt_projList");
        $.ajax({
            url:'../ajax/ac_gen_loadProjList.php',
            dataType:'JSON',
            success:function(data){
                $projList.html('');
                $projList.append('<option id="all">--Choose a Project--</option>')
                $.each(data.proj, function(index, val){
                    //console.log(val.projID),
                    $projList.append('<option id="' + val.projID + '">' + val.projName + '(' + val.projCode + ')</option>');
                });
            }
        });
        $("#gwt_setupModal").modal("show");
    }); 
    
    document.getElementById('gwt_projList').onchange = function(){
        $compList = $("#gwt_compList");
        var projListVal = $("#gwt_projList option:selected").attr("id");
        console.log(projListVal)
        $.ajax({
            url: "../ajax/ac_gen_loadCompList.php",
            type:"POST",
            dataType:"json",
            data: {"projID" : projListVal},
            success:function(data){
                if(data.length == undefined)
                {
                    $compList.html('');
                    $compList.prop('disabled', false);
                    $compList.append('<option id="all">--All Components--</option>');
                    $.each(data.comp, function(key, val)
                    {
                        $compList.append('<option id="' + val.ID + '">' + val.cname + '</option>')
                    });
                }
                else
                {
                    $compList.html('');
                    $compList.prop('disabled', true);
                    $compList.append('<option id="all">--No Components--</option>');
                }
            }
        });
    };
    
  $("#gwt_SetupCompleteBtn").click(function(){
      $.ajax({
            url:"../ajax/ac_gen_createReport.php",
            type:"POST",
            dataType:"json",
            data:{category:reportCategory},
            success:function(data)
                {    
                reportNumber = data.repID;
                var dt = new Date();
                var time = dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
                $("#gwt_repDate").text($.datepicker.formatDate('dd M yy', dt)+" "+time);
                projIDTxt = $("#gwt_projList option:selected").text();
                projIDVal = $("#gwt_projList option:selected").attr("id");
                projCompSelVal = $("#gwt_compList option:selected").attr("id");
                projCompSelTxt = $("#gwt_compList option:selected").text();
                if(projCompSelVal !== 'all')
                    {
                        $("#gwt_ComponentTitle").show();
                        $("#gwt_ComponentTitle").html('<h4><strong>Component:</strong>'+projCompSelTxt+'</h4>');
                    }
                $("#gwt_showRep").show();
                $("#gwt_ReportTile").text($("#gwt_projList option:selected").text());
                $("#gwt_setupModal").modal("hide");
        //        loadProjRateTbl();
        //        loadFindRateGrph();
        //        loadFindRateTbl();
        //        loadGITSevOverviewTbl();
                $("#gwt_setupModalBtn").hide();
                }
      });   
   });
});

function loadProjRateTbl (){
    $("#gtw_defectProjRatebl").jqGrid({
        url:'../ajax/ac_gwt_loadProjRateTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal},
        mtype: 'POST',
        height:'auto',
        width:800,
        pager:false,
        colModel:[
            {label:'Issue Type', name:'defName', width:145 , key: true},
            {label:'Total', name:'defTotal', width: 65},
            {label:'Unresolved', name:'defUnres', width: 65},
            {label:'Resolved', name:'defResolved', width: 65},
            {label:'in Test', name:'defInTest', width: 65},
            {label:'Confirmed', name:'defConf', width: 65},
            {label:'Non-Issues', name:'defNotIss', width: 65},
            {label:'Deffered', name:'defDeff', width: 65},
            {label:'Estimated', name:'projAmount', width: 65},
            {label:'Percent Complete', name:'perComp', width: 65},
        ],
        footerrow: true,
        gridComplete: function(){
            var $grr_defectOverviewTblGrid = $("#gtw_defectProjRatebl");
            var defTotalSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defTotal', false, 'sum');
            var defUnresSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defUnres', false, 'sum');
            var defResolvedSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defResolved', false, 'sum');
            var defInTestSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defInTest', false, 'sum');
            var defConfSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defConf', false, 'sum');
            var defNotIssSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defNotIss', false, 'sum');
            var defDeffSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defDeff', false, 'sum');
            $grr_defectOverviewTblGrid.jqGrid('footerData','set',{'defName':'TOTAL', 'defTotal':defTotalSum, 'defUnres':defUnresSum, 'defResolved':defResolvedSum, 'defInTest':defInTestSum, 'defConf':defConfSum,'defNotIss':defNotIssSum, 'defDeff':defDeffSum });
        }
    })
    $('#gtw_defectProjRatebl').setGroupHeaders(
        {
            useColSpanStyle: true,
            groupHeaders: [
                { "numberOfColumns": 2, "titleText": "Closed", "startColumnName": "defConf" }]
        });
};

function loadFindRateGrph(){
    var findRateJSON = $.ajax({
          url: "../ajax/ac_gwt_loadFindRateGrph.php",
          type:"POST",
          dataType:"json",
          data: {"projID":$("#gwt_projList option:selected").val()},
          //data: {"projID":14520},
          async: false
          }).responseText;
          
      var findRateData = new google.visualization.DataTable(findRateJSON);
      var view = new google.visualization.DataView(findRateData);
      view.setColumns([0,1,{
              calc:"stringify",
              sourceColumn:1,
              type:"string",
              role:"annotation"
      },2,{
              calc:"stringify",
              sourceColumn:2,
              type:"string",
              role:"annotation"
      }])

      var chart = new google.visualization.ColumnChart(document.getElementById('defectFindRateGrph'));
      var options = {
          width:1000,
          height:340,
          isStacked:true
      }
      chart.draw(view, options);
};

function loadFindRateTbl(){
    $("#gtw_defectFindRatetbl").jqGrid({
        url:'../ajax/ac_gwt_loadFindRateTbl.php',
        datatype:'json',
        postData: {"projID":$("#gwt_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:800,
        pager:false,
        colModel:[
            {label:'Date', name:'dispDate', width:145 , key: true},
            {label:'Unresolved', name:'unres', width: 65},
            {label:'Resolved', name:'res', width: 65},
            {label:'Total', name:'tot', width: 65}
        ],
        footerrow: false
    })
};

function loadDeferredDefectsTbl (){
    $("#gwt_dfrdDefTbl").jqGrid({
        url:'../ajax/ac_gwt_loadDeferredDefectsTbl.php',
        datatype:'json',
        postData: {"projID":$("#gwt_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:780,
        pager:false,
        colModel:[
            {label:'Issue', name:'pkey', width:75 , key: true},
            {label:'Summary', name:'SUMMARY', width: 145}
        ]
    })
};

function loadGITSevOverviewTbl(){
    $("#gwt_GITSevOverviewTbl").jqGrid({
        url:'../ajax/ac_gwt_loadGITSevOverviewTbl.php',
        datatype:'json',
        postData: {"projID":$("#gwt_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:800,
        pager:false,
        colModel:[
            {label:'Issue Type', name:'defSev', width:145 , key: true},
            {label:'Total', name:'defTotal', width: 65},
            {label:'Unresolved', name:'defUnres', width: 65},
            {label:'Resolved', name:'defResolved', width: 65},
            {label:'in Test', name:'defInTest', width: 65},
            {label:'Confirmed', name:'defConf', width: 65},
            {label:'Non-Issues', name:'defNotIss', width: 65},
            {label:'Deffered', name:'defDeff', width: 65},
            {label:'Percent Complete', name:'perComp', width: 65},
        ],
        footerrow: true,
        gridComplete: function(){
            var $grr_defectOverviewTblGrid = $("#gwt_GITSevOverviewTbl");
            var defTotalSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defTotal', false, 'sum');
            var defUnresSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defUnres', false, 'sum');
            var defResolvedSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defResolved', false, 'sum');
            var defInTestSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defInTest', false, 'sum');
            var defConfSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defConf', false, 'sum');
            var defNotIssSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defNotIss', false, 'sum');
            var defDeffSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defDeff', false, 'sum');
            var defPerCompSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'perComp', false, 'sum');
            $grr_defectOverviewTblGrid.jqGrid('footerData','set',{'defSev':'TOTAL', 'defTotal':defTotalSum, 'defUnres':defUnresSum, 'defResolved':defResolvedSum, 'defInTest':defInTestSum, 'defConf':defConfSum,'defNotIss':defNotIssSum, 'defDeff':defDeffSum, 'perComp':defPerCompSum+'%'});
        }
    })
    $('#gwt_GITSevOverviewTbl').setGroupHeaders(
        {
            useColSpanStyle: true,
            groupHeaders: [
                { "numberOfColumns": 2, "titleText": "Closed", "startColumnName": "defConf" }]
        });
};
