// <!-- Copyright (c) 2014 GTECH Canada ULC and its affiliates -->
// Author: Patrice Richard (patrice.richard@GTECH.com)
var projIDTxt = ""; 
var projIDVal = "";
var projCompSelVal = "";
var projCompSelTxt = "";
$(document).ready(function(){
    $("#grr_showRep").hide();
    $("#grr_ComponentTitle").hide();

    //Loads the Setup Modal    
   $("#grr_setupModalBtn").click(function(){
       $projList = $("#grr_projList");
        $.ajax({
            url:'../ajax/ac_gen_loadProjList.php',
            dataType:'JSON',
            success:function(data){
                $projList.html('');
                $projList.append('<option id="all">--Choose a Project--</option>')
                $.each(data.proj, function(index, val){
                    //console.log(val.projID),
                    $projList.append('<option id="' + val.projID + '">' + val.projName + '(' + val.projCode + ')</option>');
                });
            }
        });
       $("#grr_setupModal").modal("show");
   }); 
   
   document.getElementById('grr_projList').onchange = function(){
        $compList = $("#grr_compList");
        //var e = ;
        var projListVal = $("#grr_projList option:selected").attr("id");
        console.log(projListVal)
        $.ajax({
            url: "../ajax/ac_gen_loadCompList.php",
            type:"POST",
            dataType:"json",
            data: {"projID" : projListVal},
            success:function(data){
                if(data.length == undefined)
                {
                    $compList.html('');
                    $compList.prop('disabled', false);
                    $compList.append('<option id="all">--All Components--</option>');
                    $.each(data.comp, function(key, val)
                    {
                        $compList.append('<option id="' + val.ID + '">' + val.cname + '</option>')
                    });
                }
                else
                {
                    $compList.html('');
                    $compList.prop('disabled', true);
                    $compList.append('<option id="all">--No Components--</option>');
                }
            }
        });
    };
     
  $("#grrSetupCompleteBtn").click(function(){
        var dt = new Date();
        var time = dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
        $("#grr_repDate").text($.datepicker.formatDate('dd M yy', dt)+" "+time);
        projIDTxt = $("#grr_projList option:selected").text();
        projIDVal = $("#grr_projList option:selected").attr("id");
        projCompSelVal = $("#grr_compList option:selected").attr("id");
        projCompSelTxt = $("#grr_compList option:selected").text();
        if(projCompSelVal !== 'all'){
            $("#grr_ComponentTitle").show();
            $("#grr_ComponentTitle").html('<h4><strong>Component:</strong>'+projCompSelTxt+'</h4>');
        }
        $("#grr_showRep").show();
        $("#grr_ReportTile").text(projIDTxt);
        $("#grr_setupModal").modal("hide");
        $("#grr_runDateOnScreen").text($("#grrSetupStartDate").val()+ '  -  ' +$("#grrSetupEndDate").val());
        $("#grr_setupModalBtn").hide();
        //loadDefectOverviewTbl();
        //loadDeferredDefectsTbl();
        //loadSeverityOverviewTbl();
        //loadSeverityOverviewPercTbl();
        //loadGITOverviewTbl();
        //loadGITOverviewPercTbl();
   });
   
   
});

function loadDefectOverviewTbl (){
    $("#grr_defectOverviewTbl").jqGrid({
        url:'../ajax/ac_grr_loadDefectOverviewTbl.php',
        datatype:'json',
        postData: {"projID":projIDVal, "compVal":projCompSelVal},
        mtype: 'POST',
        height:'auto',
        width:785,
        //pager:"#grr_defectOverviewTblPgr",
        pager:false,
        colModel:[
            {label:'Issue Type', name:'defName', width:85 , key: true},
            {label:'Total', name:'defTotal', width: 65},
            {label:'Confirmed', name:'defConf', width: 65},
            {label:'Non-Issues', name:'defNotIss', width: 65},
            {label:'Deffered', name:'defDeff', width: 65}
        ],
        footerrow: true,
        gridComplete: function(){
            var $grr_defectOverviewTblGrid = $("#grr_defectOverviewTbl");
            var defTotalSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defTotal', false, 'sum');
            var defConfSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defConf', false, 'sum');
            var defNotIssSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defNotIss', false, 'sum');
            var defDeffSum = $grr_defectOverviewTblGrid.jqGrid('getCol', 'defDeff', false, 'sum');
            $grr_defectOverviewTblGrid.jqGrid('footerData','set',{'defName':'TOTAL', 'defTotal':defTotalSum, 'defConf':defConfSum,'defNotIss':defNotIssSum, 'defDeff':defDeffSum });
        }
    })
};
function loadDeferredDefectsTbl (){
    $("#grr_dfrdDefTbl").jqGrid({
        url:'../ajax/ac_grr_loadDeferredDefectsTbl.php',
        datatype:'json',
        postData: {"projID":$("#grr_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:780,
        pager:false,
        colModel:[
            {label:'Issue', name:'pkey', width:75 , key: true},
            {label:'Summary', name:'SUMMARY', width: 145}
        ]
    })
};
function loadSeverityOverviewTbl (){
     $("#grr_sevOVTbl").jqGrid({
        url:'../ajax/ac_grr_loadSevOverviewTbl.php',
        datatype:'json',
        postData: {"projID":$("#grr_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:780,
        //pager:"#grr_defectOverviewTblPgr",
        pager:false,
        colModel:[
            {label:'Issue Type', name:'issType', width:85 , key: true},
            {label:'High', name:'sevHigh', width: 75},
            {label:'Medium', name:'sevMed', width: 75},
            {label:'Low', name:'sevLow', width: 75},
            {label:'Informational', name:'sevInf', width: 75},
            {label:'TOTAL', name:'sevTotal', width: 75}
        ],
        footerrow: true,
        gridComplete: function(){
            var $grr_sevOVTblgrid = $("#grr_sevOVTbl")
            var sevHighSum = $grr_sevOVTblgrid.jqGrid('getCol', 'sevHigh', false, 'sum');
            var sevMedSum = $grr_sevOVTblgrid.jqGrid('getCol', 'sevMed', false, 'sum');
            var sevLowSum = $grr_sevOVTblgrid.jqGrid('getCol', 'sevLow', false, 'sum');
            var sevInfSum = $grr_sevOVTblgrid.jqGrid('getCol', 'sevInf', false, 'sum');
            var sevTotalSum = $grr_sevOVTblgrid.jqGrid('getCol', 'sevTotal', false, 'sum');
            $grr_sevOVTblgrid.jqGrid('footerData','set',{'issType':'TOTAL:',sevHigh:sevHighSum,sevMed:sevMedSum, sevLow:sevLowSum,sevInf:sevInfSum,sevTotal:sevTotalSum})
        }
    })
};
function loadSeverityOverviewPercTbl (){
     $("#grr_sevOVPercTbl").jqGrid({
        url:'../ajax/ac_grr_loadSevOverviewPercTbl.php',
        datatype:'json',
        postData: {"projID":$("#grr_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:180,
        //pager:"#grr_defectOverviewTblPgr",
        pager:false,
        colModel:[
            {label:'Issue Type', name:'issType', width:85 , key: true},
            {label:'Percentage', name:'perc', width: 75},
        ]
    })
};

function loadGITOverviewTbl(){
$("#grr_gitOVTbl").jqGrid({
        url:'../ajax/ac_grr_loadGITOverviewTbl.php',
        datatype:'json',
        postData: {"projID":$("#grr_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:180,
        //pager:"#grr_defectOverviewTblPgr",
        pager:false,
        colModel:[
            {label:'Issue Type', name:'defName', width:85 , key: true},
            {label:'Issues', name:'defNum', width: 75},
        ],
        footerrow:true,
        gridComplete: function () {
            var $grr_gitOVTblGrid = $("#grr_gitOVTbl");
            var totIssues = $grr_gitOVTblGrid.jqGrid('getCol', 'defNum', false, 'sum');
            $grr_gitOVTblGrid.jqGrid('footerData','set',{defName:'TOTAL:',defNum:totIssues})
        }
    })
};

function loadGITOverviewPercTbl(){
$("#grr_gitOVPercTbl").jqGrid({
        url:'../ajax/ac_grr_loadGITOverviewPercTbl.php',
        datatype:'json',
        postData: {"projID":$("#grr_projList option:selected").val()},
        mtype: 'POST',
        height:'auto',
        width:180,
        //pager:"#grr_defectOverviewTblPgr",
        pager:false,
        colModel:[
            {label:'Issue Type', name:'defName', width:85 , key: true},
            {label:'Issues', name:'defNum', width: 75},
        ],
        footerrow:false
    })
};