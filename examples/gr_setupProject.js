// <!-- Copyright (c) 2014 GTECH Canada ULC and its affiliates -->
// Author: Patrice Richard (patrice.richard@GTECH.com)
var gv_resRateDays;
    var gv_resRateOuput;
$(document).ready(function(){
    google.load('visualization', '1', {'packages':['corechart']});
    
    //Loads the Setup Modal    
   $("#gr_setupProjBtn").click(function(){
       $("#gr_setupProjModal").load('../modals/gr_SetupProjModal.php');
       $("#gr_setupProjModal").modal("show");
   }); 
   
   //Actions the Setup Modal (Actions the functions)
   $("#grSetupBtnComplete").click(function(){
       $("#gr_ReportTile").text($("#projList option:selected").text());
       $("#gr_setupProjModal").modal("hide");
        loadOpenDefectTbl();
        loadOpenDefectGraph();
        loadResRateTbl('99','all');
        loadResRateGraph('99');
    });
    
    //Loads that Resolution Rate Modal and fills in the fields
    $("#gr_resRateOptBtn").click(function(){
        $("#gr_resRateOptModal").load('../modals/gr_resRateOptModal.php');
        $("#gr_resRateOptModal").modal("show");
        setTimeout(function(){
            $("#gr_ResRateDays").val(gv_resRateDays);
        $("#gr_ResRateOutput").val(gv_resRateOuput);},500
        );
    });
    
    // Actions the Resolution Rate Modal
    $("#gr_ResRateOptSetBtn").click(function(){
        $("#gr_resRateOptModal").modal("hide");
        var daysVal = $("#gr_ResRateDays").val();
        var outTypeVal = $("#gr_ResRateOutput option:selected").val();
        loadResRateTbl(daysVal,outTypeVal);
        loadResRateGraph(daysVal);
    });
});


function loadOpenDefectTbl (){
    $.ajax({
        url:"../ajax/fctn_loadOpenDefectTbl.php",
        dataType:"json",
        type:"POST",
        data : {"projID":$("#projList option:selected").val()},
        success:function(data){
//            console.log(data);
            var dataSet = data;
            $("#tblOpenAssigned").dataTable({
               "destroy": true,
                "paging":   false,
                "info":     false,
                "searching":false,
                "columnDefs":[
                    {"width":"30%", "targets":0},
                    {"width":"30%", "targets":1},
                    {"title":"Full Name", "targets":0},
                    {"title":"Tot. Open Issue", "targets":1}
                ],
                data:dataSet,
                columns:[
                    {"data":"fullName"},
                    {"data":"totalIssue"}
                    ]
            }); 
        }
    });
};

function loadOpenDefectGraph(){
    var openDefectJSON = $.ajax({
          url: "../ajax/fctn_loadOpenDefectGraph.php",
          type:"POST",
          dataType:"json",
          data: {"projID":$("#projList option:selected").val()},
          async: false
          }).responseText;
          
      var openDefectData = new google.visualization.DataTable(openDefectJSON);

      var chart = new google.visualization.BarChart(document.getElementById('divOpenDefect'));
      chart.draw(openDefectData, {width: 500, height: 240});

}

function loadResRateTbl(days, dispOption){
    $.ajax({
        url:"../ajax/fctn_loadResRateTbl.php",
        dataType:"json",
        type:"POST",
        data : {"projID":$("#projList option:selected").val(),"runDays":days, "outType":dispOption},
        success:function(data){
            console.log(data);
            var dataSet = data;
            $("#tblResRate").dataTable({
               "destroy": true,
                "paging":   false,
                "info":     false,
                "orderable": false,
                "bSort": false,
                "searching":false,
                "columnDefs":[
                    {"width":"30%", "targets":0},
                    {"width":"30%", "targets":1},
                    {"width":"30%", "targets":2},
                    {"width":"30%", "targets":3},
                    {"title":"Date", "targets":0},
                    {"title":"Opened", "targets":1},
                    {"title":"Resovled", "targets":2},
                    {"title":"Res Rate", "targets":3}
                ],
                data:dataSet,
                columns:[
                    {"data":"selDate"},
                    {"data":"issueOpen"},
                    {"data":"issueRes"},
                    {"data":"resRate"}
                    ]
            });
            gv_resRateDays = days;
            gv_resRateOuput = dispOption;
            
        }
    });
}

function loadResRateGraph(days){
    var resRateJSON = $.ajax({
          url: "../ajax/fctn_loadResRateGraph.php",
          type:"POST",
          dataType:"json",
          data: {"projID":$("#projList option:selected").val(),"runDays":days},
          async: false
          }).responseText;
          
      var resRateData = new google.visualization.DataTable(resRateJSON);

      var chart = new google.visualization.LineChart(document.getElementById('divResRateGraph'));
      chart.draw(resRateData);

}
