// <!-- Copyright (c) 2014 GTECH Canada ULC and its affiliates -->
// Author: Patrice Richard (patrice.richard@GTECH.com)
$(document).ready(function(){
    if (document.getElementById('dms_setupModalBtn') !== null){
        $projListID = $("#dms_projList")
        $modalID = $("#dms_setupModal")
    }
    
    $("#dms_setupModalBtn").click(function(){
        $projList = $("#dms_projList");
        $.ajax({
            url:'../ajax/ac_dms_loadProjList.php',
            dataType:'JSON',
            success:function(data){
                $projList.html('');
                //$projList.append('<option id="all">--All Components--</option>');
                $.each(data.proj, function(key, val){
                    $projList.append('<option id="' + val.projID + '">' + val.projName + '(' + val.projCode + ')</option>');
                });
            }
        });
        $("#dms_setupModal").modal("show");
    }); 
});