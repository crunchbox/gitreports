DECLARE @projID INT = 27320

INSERT INTO dbo.jiraissue SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[jiraissue] 
WHERE [project] = @projID

INSERT INTO dbo.component SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[component] 
WHERE project = @projID

INSERT INTO dbo.nodeassociation SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[nodeassociation] 
WHERE SINK_NODE_ID IN (SELECT ID FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[component] WHERE project = @projID) AND SINK_NODE_ENTITY = 'Component'