

TRUNCATE TABLE dbo.project

INSERT INTO dbo.project SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[project] 

TRUNCATE TABLE dbo.issuestatus

INSERT INTO dbo.issuestatus SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[issuestatus] 

TRUNCATE TABLE dbo.issuetype

INSERT INTO dbo.issuetype SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[issuetype] 

TRUNCATE TABLE dbo.component
TRUNCATE TABLE dbo.jiraissue
TRUNCATE TABLE dbo.nodeassociation
