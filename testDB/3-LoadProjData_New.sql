DECLARE @ProjTable TABLE (id INT, projIDTbl INT)
DECLARE @currStart INT = 1
DECLARE @projID INT

-- ====================ADD NEW LINE FOR NEW PROJECT=============================
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 28922) --Sorcerers Riches
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 28921) --Disco Dolphin	
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 27920) --ThroneOfZinon
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 27320) --FortuneRooster
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 14520) --MoneyDrop Blue
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 19120) --MoneyDrop Red
INSERT INTO @ProjTable VALUES (((SELECT COUNT(projIDTbl) FROM @ProjTable) + 1), 28923) --WLE HD
-- ============================END==============================================
SELECT * FROM @ProjTable


-- Truncate Tables
TRUNCATE TABLE dbo.project
TRUNCATE TABLE dbo.issuestatus
TRUNCATE TABLE dbo.issuetype
TRUNCATE TABLE dbo.component
TRUNCATE TABLE dbo.jiraissue
TRUNCATE TABLE dbo.nodeassociation

-- Full Table Dumps
INSERT INTO dbo.issuestatus SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[issuestatus] 
INSERT INTO dbo.issuetype SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[issuetype] 

-- Project Dumps

WHILE (@currStart <= (SELECT COUNT(projIDTbl) FROM @ProjTable))
	BEGIN
		SET @projID = (SELECT projIDTbl FROM @ProjTable WHERE id = @currStart)

		
		INSERT INTO dbo.project SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[project]
		WHERE [ID] = @projID
		
		INSERT INTO dbo.jiraissue SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[jiraissue] 
		WHERE [project] = @projID

		INSERT INTO dbo.component SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[component] 
		WHERE project = @projID

		INSERT INTO dbo.nodeassociation SELECT * FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[nodeassociation] 
		WHERE SINK_NODE_ID IN (SELECT ID FROM [CAMONSQLDB4].[CSPJIRADB].[dbo].[component] WHERE project = @projID) AND SINK_NODE_ENTITY = 'Component'

		SET @currStart = @currStart + 1
	END

