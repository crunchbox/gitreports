var dashboardData;

var kitCarouselOrder = ["kitsReleased", "newThemeOther", "newThemeFirstPass", "QuartersFirstPass", "kitreleaselist", "kittestlist"]; //"AllSitesFirstPass", "AllSitesNewOther"
var defectCarouselOrder = ["defectsForKitsReleased", "defectsPhaseFound", "ContainmentRate", "QuartersPhaseFound", "avgDefects"];
var edaCarouselOrder = ["edas", "cn_in", "cn_inEntered", "edasEntered", "edaNewOnly"];
var otherCarouselOrder = ["BVATestCasesPassFail", "UTPTestCasesPassFail", "GameTracker", "KitCountForTools", "EQEScoreByPushDate"];
var mathCarouselOrder = ["themesReleased", "currentMathStates", "mathEdas", "mathDefectsFound"];
//var myCarousels = ["kitsReleased", "newThemeOther", "newThemeFirstPass", "AllSitesFirstPass", "AllSitesNewOther", "QuartersFirstPass", "defectsForKitsReleased", "defectsPhaseFound", "defectsClosedbyTesterType", "edas", "ContainmentRate", "edasEntered", "BVATestCasesPassFail", "UTPTestCasesPassFail", "GameTracker"];
var myCarousels = [kitCarouselOrder, defectCarouselOrder, otherCarouselOrder, mathCarouselOrder, edaCarouselOrder];
var filter = false;
var toolname = "Dashboard Preferences";
var username = null;
var selectedStudio = "All";
var dashboardTheme = "light";
var mboDashboardTheme = "black";
var dashboardChartConfigs = {};
var dashboardCharts = [];
var dashboardMainColor = "#5b5b5b";
var dashboardColors = ["#FF671F", "#1B9DDB", "#8DC8E8", "#FFA300", "#0C51A1", "#F0AD4E", "#a7a737", "#86a965", "#8aabb0", "#d8854f", "#cfd27e",
                  "#9d9888", "#916b8a", "#724887", "#7256bc", // end defaults 
                  "#FF0000", "#008000", "#FFC0CB", "#FFA500", "#FFF0F5", "#FFFF00", "#0000FF", "#CD853F",
                  "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#FFFFE0", "#ADD8E6", "#D2B48C",
			      "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#EEE8AA", "#00BFFF", "#FFDEAD",
			      "#8FBC8B", "#00FF7F", "#FFF0F5", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
			      "#40E0D0", "#66CDAA"];



function loadDashboard(studio) {

	// REST API CALL TO GET DATA JSON
	toggleFullScreen();
    //get_DashboardData();

	selectedStudio = studio; // set selected studio

    //drawDashboard();
}

function updateDashboardConfig() {
    for (var item in dashboardChartConfigs) {
        if (item.indexOf("MBO") === -1) {
            if (dashboardChartConfigs[item].config !== null || dashboardChartConfigs[item].config !== undefined) {
                dashboardChartConfigs[item].config.theme = dashboardTheme;
                dashboardChartConfigs[item].config.color = dashboardMainColor;
                if (dashboardChartConfigs[item].config.valueAxes !== undefined) {
                    dashboardChartConfigs[item].config.valueAxes.gridColor = dashboardMainColor;
                    dashboardChartConfigs[item].config.valueAxes.axisColor = dashboardMainColor;
                    dashboardChartConfigs[item].config.valueAxes.color = dashboardMainColor;
                }

                if (dashboardChartConfigs[item].config.categoryAxis !== undefined) {
                    dashboardChartConfigs[item].config.categoryAxis.gridColor = dashboardMainColor;
                    dashboardChartConfigs[item].config.categoryAxis.axisColor = dashboardMainColor;
                    dashboardChartConfigs[item].config.categoryAxis.color = dashboardMainColor;
                }
                AmCharts.makeChart(item, dashboardChartConfigs[item].config, 100);
            } else {
                console.log(item + " doesn't have config");
            }
        }
    }
}

$(window).load(function () {
    GetUserName();   
    
    if (showDarkTheme)
        $('#theme_selector').val('Dark');
    else $('#theme_selector').val('Light');

    if ($("#dashboardContainer").is(":visible"))
        initiateDataRequests();

	if (selectedStudio != "All") {
	    $("#viewDataBtnText").text("Viewing " + selectedStudio + " Data (Click to change)");
	}

    if (!$.isEmptyObject(studio_alias)) {
	    DashboardUpdateStudioList(studio_alias);
	}
	else {
		get_StudioList(get_DashboardStudioList_callBack);
	}

	$("#studioList").change(function () {
	    $("#waitMsg").text("");
	    selectedStudio = $(this).val();
	    $('#myModal').modal('toggle');
	    $("#viewDataBtnText").text("Viewing " + selectedStudio + " Data (Click to change)");
        myCarousels = [kitCarouselOrder, defectCarouselOrder, otherCarouselOrder, mathCarouselOrder, edaCarouselOrder];
	    for (var i = 0; i < myCarousels.length; i++) {
	        for (var j = 0; j < myCarousels[i].length; j++) {
	            $("#" + (myCarousels[i])[j] + " .figBody").addClass("loading");
	            $("#" + (myCarousels[i])[j] + " .figBody").removeClass("doneLoading");
	        }
	    }
	    initiateDataRequests();
	});
});


function checkIfStillLoading() {
    var canChange = true;
    for (var metrics in savedCallbacks) {
        if (!savedCallbacks[metrics])
            canChange = false;
    }

    if (canChange) {
        $('#studioList').prop('disabled', false);
        $("#waitMsg").text("");
    }

    return canChange;
}

function DashboardUpdateStudioList(studioList) {
    var unique = [];
    for (var studio in studioList) {
        if (unique.indexOf(studioList[studio]) === -1)
            unique.push(studioList[studio]);
    }

    unique.push("All");
    unique.sort();

	for (var i = 0; i < unique.length; i++) {
		$('#studioList').append('<option value="' + unique[i] + '">' + unique[i] + '</option>');
	}
	//$('#studioList').append('<option value="Ignite Australia" >Ignite Australia</option>');
	
	if (unique.indexOf(selectedStudio) == -1) {
	    selectedStudio = "All";
	    $("#viewDataBtnText").text("Viewing " + selectedStudio + " Data (Click to change)");
	}
}

function getUserPreferences_callback(r) {
    if (r.reply.DashboardPersonalPref !== null && r.reply.DashboardPersonalPref[0] != null) {
        var userPrefs = JSON.parse(r.reply.DashboardPersonalPref[0].value);

        //get id of first element in original list
        kitCarouselOrder = rearrangeCarousel(kitCarouselOrder, checkCarousel(userPrefs.kitCarouselOrder));
        defectCarouselOrder = rearrangeCarousel(defectCarouselOrder, checkCarousel(userPrefs.defectCarouselOrder));
        otherCarouselOrder = rearrangeCarousel(otherCarouselOrder, checkCarousel(userPrefs.otherCarouselOrder));
        mathCarouselOrder = rearrangeCarousel(mathCarouselOrder, checkCarousel(userPrefs.mathCarouselOrder));
        edaCarouselOrder = rearrangeCarousel(edaCarouselOrder, checkCarousel(userPrefs.edaCarouselOrder));

        sendUpdateCardQuery();
    }
   // resetCurrentHash();
}

function checkCarousel(carousel) {
    if (carousel === undefined || carousel=== null || carousel.length === 0)
        return [];

    return carousel; 
}

function rearrangeCarousel(originalCarousel, newCarousel) {
    var newOrdering = []; //jQuery.extend(true, [], originalCarousel);
    var i = 0,
		j = 0;
    var idPointer = originalCarousel[0];
    for (i = 0; i < newCarousel.length; i++) {
        if (originalCarousel.indexOf(newCarousel[i]) !== -1) {
            idPointer = originalCarousel[0];

            if (idPointer !== newCarousel[i]) {
                $("#" + newCarousel[i]).insertBefore(("#" + idPointer));
                newOrdering.push(newCarousel[i]);
            }
            else {
                newOrdering.push(newCarousel[i]);
            }

            var index = originalCarousel.indexOf(newCarousel[i]);
            if (index > -1) {
                originalCarousel.splice(index, 1);
            }
        }
    }


    newOrdering = newOrdering.concat(originalCarousel);
    return newOrdering;
}


// to store call back for cache response if error
var dashboardCacheBackupCallBack = {};

function initiateDataRequests() {

    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10)
        month = "0" + month;
    var day = date.getDate();
    if (day < 10)
        day = "0" + day;
    var prevYear = year - 1;
    var start = prevYear.toString() + month.toString() + "01"

    var twoYearPrev = year - 2;
   

    //var end = getTomorrowsISODate();//year + month + day;
//    var yearStart = year + "0101";
//    
//    var start2Year_Date = getStartOfPrevQuarter(date, 7); // twoYearPrev + month + "01";
//    var start2Year = start2Year_Date.toString("yyyyMMdd");
//
//
//    var yearStart2Year = prevYear + "0101";

    var skipList = ["UTPTestCasesPassFail", "GameTracker", "themesReleased", "currentMathStates",
        "mathEdas", "mathDefectsFound"];


    //savedCallbacks = {};
    //$('#studioList').prop('disabled', true);
    $("#waitMsg").text("");

    for (var i = 0; i < myCarousels.length; i++) {
        var metrics = myCarousels[i];

        for (var j = 0; j < metrics.length; j++) {
            if (skipList.indexOf(metrics[j]) === -1) {
                var mycallback = window[metrics[j] + "Callback"];
                
                
                if (typeof mycallback === 'function' && $("#dashboardContainer").is(":visible")) {
                    //savedCallbacks[metrics[j]+"_"+ selectedStudio] = false; 

                    if (metrics[j].search("Quarters") === -1)
                    {
                        dashboardCacheBackupCallBack[[metrics[j], start]] = mycallback;
                        //get_DashboardOverviewData(metrics[j], start, end, yearStart, selectedStudio.replaceAll(" ", "_"), "", mycallback);
                    }
                else {
                        if (metrics[j] === "QuartersFirstPass") {
                          //  dashboardCacheBackupCallBack[["newThemeFirstPass", start2Year]] = mycallback;
                            //get_DashboardOverviewData("newThemeFirstPass", start2Year, end, yearStart2Year, selectedStudio.replaceAll(" ", "_"), "", mycallback);
                        }
                        else if (metrics[j] === "QuartersPhaseFound") {
                            //dashboardCacheBackupCallBack[["defectsPhaseFound", start2Year]] = mycallback;
                       //     get_DashboardOverviewData("defectsPhaseFound", start2Year, end, yearStart2Year, selectedStudio.replaceAll(" ", "_"), "", mycallback);
                        }
                }
                }
                    
            }
        }
    }

    get_MathDefectsFound_dashboard();
    get_MathEdas_dashboard();
    get_MathCurrentStates_dashboard();
    get_MathThemesReleased_dashboard(); 
    get_GameTrackerList(get_GameTrackerList_dashboardCallback);
    get_UTPList(get_UTPList_dashboardCallback);
};

function get_MathThemesReleased_dashboard() {
    var client = new $.RestClient("/rest/",
               {
                   stringifyData: true
               });
    client.add("mathThemesReleased");
    var restRequest = client.mathThemesReleased.create({}, {
    });

    restRequest.done(mathThemesReleasedCallback);
}

function get_MathCurrentStates_dashboard() {
    var client = new $.RestClient("/rest/",
        {
            stringifyData: true
        });
    client.add("mathCurrentStates");
    var restRequest = client.mathCurrentStates.create({}, {
    });

    restRequest.done(mathCurrentStatesCallback);
}

function get_MathEdas_dashboard() {
    var client = new $.RestClient("/rest/",
        {
            stringifyData: true
        });
    client.add("mathEdas");
    var restRequest = client.mathEdas.create({}, {
    });

    restRequest.done(mathEdasCallback);
}

function get_MathDefectsFound_dashboard() {
    var client = new $.RestClient("/rest/",
        {
            stringifyData: true
        });
    client.add("mathDefectsFound");
    var restRequest = client.mathDefectsFound.create({}, {
    });

    restRequest.done(mathDefectsFoundCallback);
}

function get_GameTrackerList_dashboardCallback(r) {
    if (r != null) 
    {
        var today = new Date();
        var lastYear = new Date();
        lastYear.setFullYear(today.getFullYear() - 1);

        var count = {
            utpEnabled: 0,
            utpDisabled: 0,
            noUtp: 0
        };

        for (var i = 0; i < r.length; i++) {
            var latestBuild = r[i].Builds[r[i].Builds.length - 1]; //r[i].Builds[0];
            if (latestBuild !== undefined && latestBuild !== null) {
                var gameDate = new Date(latestBuild.Locations[0].Created);
                if (gameDate >= lastYear) {
                    if ((r[i].Studio).toLowerCase() === selectedStudio.toLowerCase() || selectedStudio === "All") {
                        if (r[i].Builds[r[i].Builds.length - 1].UtpEnabled === "1") {
                            count.utpEnabled++;
                        } else {
                            count.utpDisabled++;
                        }
                    }
                }
            }
        }

        GameTrackerCallback(count);
    }
    else get_GameTrackerList(get_GameTrackerList_dashboardCallback);
    resetCurrentHash();
}

function get_UTPList_dashboardCallback(r) {
    if (r != null)
    {
        if (r["UTP"]) {
            if (restJsonDict.hasOwnProperty("UTPTestCasesListRaw") == false) {
                restJsonDict["UTPTestCasesListRaw"] = r["UTP"];
            }
            var uniqueCasesNames = [];
            for (var i = 0; i < r["UTP"].length; i++) {
                uniqueCasesNames.push(r["UTP"][i].Name);
            }
            restJsonDict["UTPTestCasesList"] = uniqueCasesNames;
            utp_list = uniqueCasesNames;
        }

        if (r["UTPCases_Month0"]) {
            dashboardRemoveNonUTPTestCases(r);

            var count = {
                passed: 0,
                failed: 0,
                other: 0
            };

            for (var i = 0; i <= 12; i++) {
                var dataStr = "UTPCases_Month" + i;
                var data = restJsonDict[dataStr];

                for (var j = 0; j < data.length; j++) {
                    if ((data[j].StudioName).toLowerCase() === selectedStudio.toLowerCase() || selectedStudio === "All") {
                        if (data[j].Result === "Passed") {
                            count.passed++;
                        } else if (data[j].Result === "Failed") {
                            count.failed++;
                        } else {
                            count.other++;
                        }
                    }
                }
            }

            UTPTestCasesPassFailCallback(count);
        }
    }
    else get_UTPList(get_UTPList_dashboardCallback);
    resetCurrentHash();
}

function dashboardRemoveNonUTPTestCases(r) {
    for (var i = 0; i <= 12; i++) {
        var dataStr = "UTPCases_Month" + i;
        var data = r[dataStr];

        var newItems = [];
        for (var j = 0; j < data.length; j++) {
            var caseName = data[j].CaseName;
            if (utp_list.indexOf(caseName) !== -1) {
                newItems.push(data[j]);
            }
        }

        restJsonDict[dataStr] = newItems;
    }

}

function get_DashboardOverviewData(metric, startDate, endDate, yearStart, studio, paGroup, mycallback) {
    
    var rest_params = {
        metric: metric,
        startDate: startDate,
        endDate: endDate,
        yearStart: yearStart,
        studio: studio,
        paGroup: paGroup
    };        

    var cache_filename = compose_cache_filename("dashboardOverviewData", rest_params);

    get_CachedData(cache_filename, mycallback, cacheData_errorHandler);
};
    
function get_DashboardOverviewData_RestRequest(metric, startDate, endDate, yearStart, studio, paGroup, mycallback) {
    
    var client = new $.RestClient('/rest/');
    client.add("dashboardOverviewData");
    var restRequest;
    
    restRequest = client.dashboardOverviewData.create({}, {
        metric: metric,
        startDate: startDate,
        endDate: endDate,
        yearStart: yearStart,
        studio: studio,
        paGroup: paGroup
    });
    restRequest.done(function (r) {
        var found = false;
        for (var params in r) {
            if (params.search(selectedStudio) !== -1) {
                found = true;
            }
        }
        if (found && $("#dashboardContainer").is(":visible"))
            mycallback(r);
    });
};

function generateEmptyChart(chartDiv, type) {
    var msg = "No data is available";
    if (type === "error" && chartDiv.search("MBO") === -1) {
        msg = "Oops, something went wrong. :(";
    } else if (type === "noMBO" || chartDiv.search("MBO") !== -1) {
        msg = "No MBO available";
    }

    var config = {
        "type": "serial",
        "theme": "black",
        "dataProvider": [],
        "valueAxes": [
            {
                "title": "",
                "color": dashboardMainColor,
                "titleColor": dashboardMainColor
            }
        ],
        "graphs": [
            {
                "balloonText": "",
                "fillAlphas": 1,
                "lineAlpha": 0.5,
                "title": "Income",
                "type": "column",
                "valueField": "income",
                "labelText": "[[value]]",
                "labelPosition": "middle",
                "lineColor": dashboardMainColor,
                "labelColorField": dashboardMainColor
            }
        ],
        "depth3D": 20,
        "angle": 30,
        "rotate": true,
        "categoryField": "year",
        "categoryAxis": {
            "gridPosition": "start",
            "fillAlpha": 1,
            "position": "left"
        }
    };
    var chart = AmCharts.makeChart(chartDiv,
    config);

    chart.valueAxes[0].minimum = 0;
    chart.valueAxes[0].maximum = 100;

    // add dummy data point
    var dataPoint = {
        dummyValue: 0
    };
    dataPoint[chart.categoryField] = '';
    chart.dataProvider = [dataPoint];

    // add label
    try {
        chart.addLabel(0, '50%', msg, 'center', 12, dashboardMainColor);
    } catch (err) {

    }

    // set opacity of the chart div
    chart.chartDiv.style.opacity = 1;

    // redraw it
    chart.validateNow();

    dashboardChartConfigs[chartDiv] = {chart:chart, config:config};
}

function kitsReleasedCallback(r) {
    if (r != null && r !== undefined && (r["kitsReleased_" + selectedStudio]) !== undefined) {
        $("#kitsReleased .figBody").removeClass("loading");
        $("#kitsReleased .figBody").addClass("doneLoading");

        var labelName = "kitsReleased_";
        var ytd = undefined, rolling = undefined;
        for (var i = 0; i < r[labelName + selectedStudio].length; i++) {
            if ((r[labelName + selectedStudio])[i].Label.indexOf("YTD") !== -1) {
                ytd = (r[labelName + selectedStudio])[i];
            }
            else
                rolling = (r[labelName + selectedStudio])[i];

        }

        if (ytd !== undefined && ytd.Value !== null)
            $("#dashKitsReleasedYTD .num").text(ytd.Value);
        else
            $("#dashKitsReleasedYTD .num").text(0);

        if (rolling !== undefined && rolling.Value !== null)
            $("#dashKitsReleasedRolling .num").text(rolling.Value);
        else
            $("#dashKitsReleasedRolling .num").text(0);

    } else {
        $("#dashKitsReleasedYTD .num").text(0);
        $("#dashKitsReleasedRolling .num").text(0);
    }
    //savedCallbacks["kitsReleased_" +selectedStudio]= true;
    resetCurrentHash();
};

function mathThemesReleasedCallback(r) {
    $("#themesReleased .figBody").removeClass("loading");
    $("#themesReleased .figBody").addClass("doneLoading");

    if (r == null) {
        $("#dashThemesReleasedMtd .num").text(0);
        $("#dashThemesReleasedQtd .num").text(0);
        $("#dashThemesReleasedYTD .num").text(0);
        $("#dashThemesReleasedRolling .num").text(0);
    } else {
        $("#mathThemesReleased .figBody").removeClass("loading");
        $("#mathThemesReleased .figBody").addClass("doneLoading");

        var ytd = r["Ytd"];
        var qtd = r["Qtd"];
        var mtd = r["Mtd"];
        var rol = r["Rol"];

        $("#dashmathThemesReleasedMtd .num").text(mtd);
        $("#dashmathThemesReleasedQtd .num").text(qtd);
        $("#dashmathThemesReleasedYtd .num").text(ytd);
        $("#dashmathThemesReleasedRol .num").text(rol);

    }
    resetCurrentHash();
};

function mathCurrentStatesCallback(r) {

    $("#currentMathStates .figBody").removeClass("loading");
    $("#currentMathStates .figBody").addClass("doneLoading");

    if (r == null) {
        generateEmptyChart("dashcurrentMathStates", "");
    } else {
        // draw Current States chart
        var config = generateDashConfigPie(true);
        var queue, waiting, gameDesign, working;
        
        if (r["Queue"] == null) { queue = 0 } else { queue = r["Queue"] }
        if (r["Waiting"] == null) { waiting = 0 } else { waiting = r["Waiting"] }
        if (r["With Game Design"] == null) { gameDesign = 0 } else { gameDesign = r["With Game Design"] }
        if (r["Working"] == null) { working = 0 } else { working = r["Working"] }

        config.dataProvider = [
            {
                "title": "Queue",
                "value": queue,
                "color": dashboardColors[0]
            },
            {
                "title": "Waiting",
                "value": waiting,
                "color": dashboardColors[1]
            },
            {
                "title": "With Game Design",
                "value": gameDesign,
                "color": dashboardColors[2]
            },
            {
                "title": "Working",
                "value": working,
                "color": dashboardColors[3]
            }
        ];

        var tot = 0;
        for (var i = 0; i < config.dataProvider.length; i++) {
            tot += config.dataProvider[i].value;
        }

        config.allLabels = [
            {
                "text": ((config.dataProvider[2].value / tot) * 100).toFixed(2) + "%",
                "align": "center",
                "bold": true,
                "y": 70,
                "size": 25
            }, {
                "text": config.dataProvider[2].title,
                "align": "center",
                "y": 110,
                "size": 18
            }
        ];

        dashboardChartConfigs["dashcurrentMathStates"] = { chart: null, config: config };
        var chart = AmCharts.makeChart("dashcurrentMathStates", config, 100);

    }
    resetCurrentHash();
};

function mathEdasCallback(r) {
    $("#mathEdas .figBody").removeClass("loading");
    $("#mathEdas .figBody").addClass("doneLoading");

    if (r == null) {
        $("#dashMathEdasYTD .num").text(0);
        $("#dashMathEdasRolling .num").text(0);
    } else {
        $("#dashMathEdasYTD .num").text(r["Ytd"]);
        $("#dashMathEdasRolling .num").text(r["Rol"]);
    }
    resetCurrentHash();
};

function mathDefectsFoundCallback(r) {
    $("#mathDefectsFound .figBody").removeClass("loading");
    $("#mathDefectsFound .figBody").addClass("doneLoading");

    if (r == null) {
        $("#dashMathDefectsFoundMtd .num").text(0);
        $("#dashMathDefectsFoundQtd .num").text(0);
        $("#dashMathDefectsFoundYtd .num").text(0);
        $("#dashMathDefectsFoundRol .num").text(0);
    } else {
        $("#dashMathDefectsFoundMtd .num").text(r["Mtd"]);
        $("#dashMathDefectsFoundQtd .num").text(r["Qtd"]);
        $("#dashMathDefectsFoundYtd .num").text(r["Ytd"]);
        $("#dashMathDefectsFoundRol .num").text(r["Rol"]);
    }
    resetCurrentHash();
};

function newThemeOtherCallback(r) {

    $("#newThemeOther .figBody").removeClass("loading");
    $("#newThemeOther .figBody").addClass("doneLoading");

    if (r !== undefined && r != null && r["newThemeOther_" + selectedStudio] !== undefined && (r["newThemeOther_" + selectedStudio]).length > 0) {

        if ((r["newThemeOther_" + selectedStudio])[0] !== undefined &&
        (r["newThemeOther_" + selectedStudio])[1] !== undefined) {

            var labelName = "newThemeOther_";
            var newThemeYTD = undefined, otherYTD = undefined, newThemeRolling=undefined, otherRolling=undefined;
            for (var i = 0; i < r[labelName + selectedStudio].length; i++) {
                if ((r[labelName + selectedStudio])[i].Label.indexOf("New Theme") !== -1 && (r[labelName + selectedStudio])[i].Label.indexOf("YTD") !== -1) {
                    newThemeYTD = (r[labelName + selectedStudio])[i];
                }
                else if ((r[labelName + selectedStudio])[i].Label.indexOf("New Theme") !== -1 && (r[labelName + selectedStudio])[i].Label.indexOf("Rolling") !== -1) {
                    newThemeRolling = (r[labelName + selectedStudio])[i];
                }
                else if ((r[labelName + selectedStudio])[i].Label.indexOf("Other") !== -1 && (r[labelName + selectedStudio])[i].Label.indexOf("YTD") !== -1) {
                    otherYTD = (r[labelName + selectedStudio])[i];
                }
                else if ((r[labelName + selectedStudio])[i].Label.indexOf("Other") !== -1 && (r[labelName + selectedStudio])[i].Label.indexOf("Rolling") !== -1) {
                    otherRolling = (r[labelName + selectedStudio])[i];
                }

            }

            if (newThemeYTD === undefined || newThemeYTD.Value === null)
                newThemeYTD = { Value: 0 };
            if (otherYTD === undefined || otherYTD.Value === null)
                otherYTD = { Value: 0 };
            if (newThemeRolling === undefined || newThemeRolling.Value === null)
                newThemeRolling = { Value: 0 };
            if (otherRolling === undefined || otherRolling.Value === null)
                otherRolling = { Value: 0 };

            var config = generateDashConfigPie();
            config.dataProvider = [
                {
                    "title": "New Theme",
                    "value": newThemeYTD.Value,
                    "color": dashboardColors[0]
                }, {
                    "title": "Other",
                    "value": otherYTD.Value,
                    "color": dashboardColors[1]
                }
            ];
            config.allLabels = [
                {
                    "text": config.dataProvider[0].value,
                    "align": "center",
                    "bold": true,
                    "y": 70,
                    "size": 25
                }, {
                    "text": config.dataProvider[0].title,
                    "align": "center",
                    "y": 110,
                    "size": 18
                }, {
                    "text": "YTD",
                    "align": "center",
                    "y": 135,
                    "size": 12
                }
            ];
            dashboardChartConfigs["dashKitsNewThemeOtherYTD_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("dashKitsNewThemeOtherYTD_chartdiv", config, 100);
            


            var config2 = generateDashConfigPie();
            config2.dataProvider = [
                {
                    "title": "New Theme",
                    "value": newThemeRolling.Value,
                    "color": dashboardColors[0]
                }, {
                    "title": "Other",
                    "value": otherRolling.Value,
                    "color": dashboardColors[1]
                }
            ];
            config2.allLabels = [
                {
                    "text": config2.dataProvider[0].value,
                    "align": "center",
                    "bold": true,
                    "y": 70,
                    "size": 25
                }, {
                    "text": config2.dataProvider[0].title,
                    "align": "center",
                    "y": 110,
                    "size": 18
                }, {
                    "text": "Rolling",
                    "align": "center",
                    "y": 135,
                    "size": 12
                }
            ];
            dashboardChartConfigs["dashKitsNewThemeOtherRolling_chartdiv"] = { chart: null, config: config2 };
            AmCharts.makeChart("dashKitsNewThemeOtherRolling_chartdiv", config2, 100);
            

        } else {
            generateEmptyChart("dashKitsNewThemeOtherYTD_chartdiv", "error");
            generateEmptyChart("dashKitsNewThemeOtherRolling_chartdiv", "error");
        }

    } else {
        generateEmptyChart("dashKitsNewThemeOtherYTD_chartdiv", "");
        generateEmptyChart("dashKitsNewThemeOtherRolling_chartdiv", "error");
    }
    //savedCallbacks["newThemeOther_" +selectedStudio]= true;

    resetCurrentHash();
}

function newThemeFirstPassCallback(r) {
    $("#newThemeFirstPass .figBody").removeClass("loading");
    $("#newThemeFirstPass .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["newThemeFirstPass_" + selectedStudio] !== undefined && (r["newThemeFirstPass_" + selectedStudio]).length > 0) {
        var rolling = undefined, ytd = undefined, mbo = undefined;
        for (var i = 0; i < r["newThemeFirstPass_" + selectedStudio].length; i++) {
            if ((r["newThemeFirstPass_" + selectedStudio])[i].Label.indexOf("Rolling") !== -1) {
                rolling = (r["newThemeFirstPass_" + selectedStudio])[i];
            }
            else if ((r["newThemeFirstPass_" + selectedStudio])[i].Label.indexOf("YTD") !== -1) {
                ytd = (r["newThemeFirstPass_" + selectedStudio])[i];

            }
            else if ((r["newThemeFirstPass_" + selectedStudio])[i].Label.indexOf("MBO") !== -1) {
                mbo = (r["newThemeFirstPass_" + selectedStudio])[i];
            }
        }

        if (ytd !== undefined && ytd.Value !== null) {
            $("#firstPassNewThemeYTD .num")
            .text(ytd.Value.toFixed(2));
        }
        else
            $("#firstPassNewThemeYTD .num").text("0.00%");

        if (rolling !== undefined && rolling.Value !== null) {
            $("#firstPassNewThemeRolling .num")
            .text(rolling.Value.toFixed(2));
        }
        else
            $("#firstPassNewThemeRolling .num").text("0.00%");


        if (mbo !== undefined && mbo !== null) {
            var myVal = 0;
            if (ytd !== undefined && ytd.Value !== null)
                myVal = parseFloat(ytd.Value.toFixed(2));

            var config = generateDashConfiMboGreenToRed();
            config.dataProvider = [
                {
                    "category": "MBO",
                    "excelent": 33.33,
                    "average": 33.33,
                    "bad": 33.33,
                    "limit": parseFloat(mbo.Value.toFixed(2)),
                    "full": 100,
                    "bullet": myVal
                }
            ];
            dashboardChartConfigs["firstPassNewThemeMBO_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("firstPassNewThemeMBO_chartdiv", config, 100);

        } else
            generateEmptyChart("firstPassNewThemeMBO_chartdiv", "noMBO");

    } else {
        generateEmptyChart("firstPassNewThemeMBO_chartdiv", "");
        $("#firstPassNewThemeYTD .num").text("0.00%");
        $("#firstPassNewThemeRolling .num").text("0.00%");
    }
    //savedCallbacks["newThemeFirstPass_" + selectedStudio] = true;
    resetCurrentHash();
}

function kitreleaselistCallback(r) {
    if (r != null && r !== undefined && (r["kitreleaselist_" +selectedStudio]) !== undefined) {
        $("#kitreleaselist .figBody").removeClass("loading");
        $("#kitreleaselist .figBody").addClass("doneLoading");

        var labelName = "kitreleaselist_";
        var week = undefined, month = undefined;
        for (var i = 0; i < r[labelName + selectedStudio].length; i++) {
            if ((r[labelName + selectedStudio])[i].Label.indexOf("Week") !== -1) {
                week = (r[labelName +selectedStudio])[i];
            }
            else
                month = (r[labelName + selectedStudio])[i];

        }

        if (week !== undefined && week.Value !== null)
            $("#kitreleaselistWeek .num").text(week.Value);
        else
            $("#kitreleaselistWeek .num").text(0);

        if (month !== undefined && month.Value !== null)
            $("#kitreleaselistMonth .num").text(month.Value);
        else
            $("#kitreleaselistMonth .num").text(0);

    } else {
        $("#kitreleaselistWeek .num").text(0);
        $("#kitreleaselistMonth .num").text(0);
    }

    resetCurrentHash();
}

function kittestlistCallback(r) {
    if (r != null && r !== undefined && (r["kittestlist_" + selectedStudio]) !== undefined) {
        $("#kittestlist .figBody").removeClass("loading");
        $("#kittestlist .figBody").addClass("doneLoading");

        var design_kits = 0;
        var queue_kits = 0;
        var testing_kits = 0;
        var total_kits = 0;
        for (var index = 0; index < (r["kittestlist_" + selectedStudio]).length; index++) {
            var item = r["kittestlist_" + selectedStudio][index];
            if (item.Label == "Design")
                design_kits = item.Value;
            else if (item.Label == "Queue")
                queue_kits = item.Value;
            else if (item.Label == "Testing")
                testing_kits = item.Value;
        }
        total_kits = design_kits + queue_kits + testing_kits;
        $("#kittestlistCount .num").text(total_kits);
        $("#kitdesignCount .num").text(design_kits);
        $("#kitqueueCount .num").text(queue_kits);
        $("#kittestCount .num").text(testing_kits);
    } else {
        var design_kits = 0;
        var queue_kits = 0;
        var testing_kits = 0;
        var total_kits = 0;
        $("#kittestlistCount .num").text(total_kits);
        $("#kitdesignCount .num").text(design_kits);
        $("#kitqueueCount .num").text(queue_kits);
        $("#kittestCount .num").text(testing_kits);
    }
    resetCurrentHash();
}


function QuartersFirstPassCallback(r) {
    $("#QuartersFirstPass .figBody").removeClass("loading");
    $("#QuartersFirstPass .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["newThemeFirstPass_" + selectedStudio] !== undefined && (r["newThemeFirstPass_" + selectedStudio]).length > 0) {
        var labelName = "newThemeFirstPass_";
        var rolling = undefined, ytd = undefined, mbo = undefined;
        for (var i = 0; i < r[labelName + selectedStudio].length; i++) {
            if ((r[labelName + selectedStudio])[i].Label.indexOf("Rolling") !== -1) {
                rolling = (r[labelName + selectedStudio])[i];
            }
            else if ((r[labelName + selectedStudio])[i].Label.indexOf("YTD") !== -1) {
                ytd = (r[labelName + selectedStudio])[i];

            }
            else if ((r[labelName + selectedStudio])[i].Label.indexOf("MBO") !== -1) {
                mbo = (r[labelName + selectedStudio])[i];
            }
        }

        if (ytd !== undefined && ytd.Value !== null) {
            $("#QuartersFirstPassYTD .num").text(ytd.Value.toFixed(2));
        }
        else
            $("#QuartersFirstPassYTD .num").text("0.00%");

        if (rolling !== undefined && rolling.Value !== null) {
            $("#QuartersFirstPassRolling .num").text(rolling.Value.toFixed(2));
        }
        else
            $("#QuartersFirstPassRolling .num").text("0.00%");

        if (mbo !== undefined && mbo.Value !== null) {
            var myVal = 0;
            if (ytd !== undefined && ytd.Value !== null) {
                myVal = parseFloat(ytd.Value.toFixed(2));
            }

            var config = generateDashConfiMboGreenToRed();
            config.dataProvider = [
                {
                    "category": "MBO",
                    "excelent": 33.33,
                    "average": 33.33,
                    "bad": 33.33,
                    "limit": parseFloat(mbo.Value.toFixed(2)),
                    "full": 100,
                    "bullet": myVal
                }
            ];
            dashboardChartConfigs["QuartersFirstPassMBO_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("QuartersFirstPassMBO_chartdiv", config, 100);

        } else
            generateEmptyChart("QuartersFirstPassMBO_chartdiv", "");

    } else {
        generateEmptyChart("QuartersFirstPassMBO_chartdiv", "");
    }
    //savedCallbacks["QuartersFirstPass_" + selectedStudio] = true;
    resetCurrentHash();
}

function defectsForKitsReleasedCallback(r) {
    $("#defectsForKitsReleased .figBody").removeClass("loading");
    $("#defectsForKitsReleased .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["defectsForKitsReleased_" + selectedStudio] !== undefined && (r["defectsForKitsReleased_" + selectedStudio]).length > 0) {
        var config = generateDashConfigColumn();

        var data = _.sortBy((r["defectsForKitsReleased_" +selectedStudio]), 'Label');
        if (data.length > 4) {
            var start = data.length - 4;
            data = data.slice(start, data.length);
        }

        var formattedData = [];
        var sum = 0;
        var max = 0; 
        for (var i = 0; i < data.length; i++) {
            var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);
            sum += data[i].Value;
            formattedData.push({
                "quarter": label,
                "value": data[i].Value
            });

            if (data[i].Value > max)
                max = data[i].Value;
        }


        config.valueAxes = [
				{
				    "maximum": max
				}
        ];

        var title = "Total: " + sum;
        config.titles = [
            {
                "text": title,
                "size": 15
            }
        ];
        config.dataProvider = formattedData;
        config.marginLeft = "40";

       
        var chart = AmCharts.makeChart("dashDefectsForKitsReleased_chartdiv", config, 100);
        dashboardChartConfigs["dashDefectsForKitsReleased_chartdiv"] = { chart: null, config: config };
    } else {
        generateEmptyChart("dashDefectsForKitsReleased_chartdiv", "");
    }
    //savedCallbacks["defectsForKitsReleased_" + selectedStudio] = true;
    resetCurrentHash();
}




function avgDefectsCallback(r) {
    $("#avgDefects .figBody").removeClass("loading");
    $("#avgDefects .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["avgDefects_" + selectedStudio] !== undefined && (r["avgDefects_" + selectedStudio]).length > 0) {
        var config = generateDashConfigColumn();

        var data = _.sortBy((r["avgDefects_" + selectedStudio]), 'Quarter');
        if (data.length > 4) {
            var start = data.length - 4;
            data = data.slice(start, data.length);
        }

        var formattedData = [];
        var sum = 0;

        var max = 0; 
        for (var i = 0; i < data.length; i++) {
            var label = data[i].Quarter.slice(data[i].Quarter.length - 2, data[i].Quarter.length) + " '" + data[i].Quarter.slice(data[i].Quarter.length - 4, data[i].Quarter.length - 2);
            sum += data[i].Value;
            formattedData.push({
                "quarter": label,
                "value": parseFloat(data[i].Avg.toFixed(2)),
                "kitCount": data[i].KitCount
            });

            if (parseFloat(data[i].Avg.toFixed(2)) > max)
                max = parseFloat(data[i].Avg.toFixed(2));
        }



        config.valueAxes = [
				{
				    "maximum": max
				}
        ];

        var title = "Total: " + sum;
        config.graphs[0].balloonText = "[[category]]: <b>[[value]]</b>\nKitCount: [[kitCount]]";
        config.titles = [
            {
                "text": title,
                "size": 15
            }
        ];


        config.dataProvider = formattedData;
        config.marginLeft = "40";

        dashboardChartConfigs["dashAvgDefects_chartdiv"] = { chart: null, config: config };
        var chart = AmCharts.makeChart("dashAvgDefects_chartdiv", config, 100);

    } else {
        generateEmptyChart("dashAvgDefects_chartdiv", "");
    }
    //savedCallbacks["defectsForKitsReleased_" + selectedStudio] = true;
    resetCurrentHash();
}


function avgDefectsCallback(r) {
    $("#avgDefects .figBody").removeClass("loading");
    $("#avgDefects .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["avgDefects_" + selectedStudio] !== undefined && (r["avgDefects_" + selectedStudio]).length > 0) {
        var config = generateDashConfigColumn();

        var data = _.sortBy((r["avgDefects_" + selectedStudio]), 'Quarter');
        if (data.length > 4) {
            var start = data.length - 4;
            data = data.slice(start, data.length);
        }

        var formattedData = [];
        var sum = 0;
        for (var i = 0; i < data.length; i++) {
            var label = data[i].Quarter.slice(data[i].Quarter.length - 2, data[i].Quarter.length) + " '" + data[i].Quarter.slice(data[i].Quarter.length - 4, data[i].Quarter.length - 2);
            sum += data[i].Value;
            formattedData.push({
                "quarter": label,
                "value": parseFloat(data[i].Avg.toFixed(2)),
                "kitCount": data[i].KitCount
            });
        }

        var title = "Total: " + sum;
        config.graphs[0].balloonText = "[[category]]: <b>[[value]]</b>\nKitCount: [[kitCount]]";
        config.titles = [
            {
                "text": title,
                "size": 15
            }
        ];
        config.dataProvider = formattedData;
        config.marginLeft = "40";


        var chart = AmCharts.makeChart("dashAvgDefects_chartdiv", config, 100);
    } else {
        generateEmptyChart("dashAvgDefects_chartdiv", "");
    }
    //savedCallbacks["defectsForKitsReleased_" + selectedStudio] = true;
    resetCurrentHash();
}

function defectsPhaseFoundCallback(r) {
    $("#defectsPhaseFound .figBody").removeClass("loading");
    $("#defectsPhaseFound .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["defectsPhaseFound_" + selectedStudio] !== undefined && (r["defectsPhaseFound_" + selectedStudio]).length > 5) {

        // draw Defects by Phase Found chart
        var config = generateDashConfigPie(true);
        config.dataProvider = [
            {
                "title": "Early Testing",
                "value": (r["defectsPhaseFound_" + selectedStudio])[0].Value,
                "color": dashboardColors[0]
            },
            {
                "title": "Math",
                "value": (r["defectsPhaseFound_" + selectedStudio])[1].Value,
                "color": dashboardColors[1]
            },
            {
                "title": "PA",
                "value": (r["defectsPhaseFound_" + selectedStudio])[2].Value,
                "color": dashboardColors[2]
            },
            {
                "title": "Paytable",
                "value": (r["defectsPhaseFound_" + selectedStudio])[3].Value,
                "color": dashboardColors[3]
            },
            {
                "title": "Post PA",
                "value": (r["defectsPhaseFound_" + selectedStudio])[4].Value,
                "color": dashboardColors[4]
            },
            {
                "title": "Pre PA",
                "value": (r["defectsPhaseFound_" + selectedStudio])[5].Value,
                "color": dashboardColors[5]
            }
        ];

        var tot = 0;
        for (var i = 0; i < config.dataProvider.length; i++) {
            tot += config.dataProvider[i].value;
        }

        config.allLabels = [
            {
                "text": ((config.dataProvider[2].value / tot) * 100).toFixed(2) + "%",
                "align": "center",
                "bold": true,
                "y": 70,
                "size": 25
            }, {
                "text": config.dataProvider[2].title,
                "align": "center",
                "y": 110,
                "size": 18
            }, {
                "text": "Rolling",
                "align": "center",
                "y": 135,
                "size": 12
            }
        ];

        dashboardChartConfigs["dashPhaseFound_chartdiv"] = { chart: null, config: config };
        var chart = AmCharts.makeChart("dashPhaseFound_chartdiv", config, 100);

    } else {
        generateEmptyChart("dashPhaseFound_chartdiv", "");
    }
    //savedCallbacks["defectsPhaseFound_" + selectedStudio] = true;
    resetCurrentHash();
}

function edasCallback(r) {
    $("#edas .figBody").removeClass("loading");
    $("#edas .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["edas_" + selectedStudio] !== undefined && (r["edas_" + selectedStudio]).length > 0) {

        var config = generateDashConfigLine(false);

        var data = _.sortBy((r["edas_" + selectedStudio]), 'Label');
        var mbo = {}, mboLoc = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].Label.search("MBO") !== -1) {
                mbo = data[i];
                mboLoc = i;
            }
        }

        if (mboLoc !== -1)
            data.splice(mboLoc, 1);

        if (data.length > 0) {
            if (data.length > 4) {
                var start = data.length - 4;
                data = data.slice(start, data.length);
            }

            var formattedData = [];
            var sum = 0;
            for (var i = 0; i < data.length; i++) {
                var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);
                sum += data[i].Value;
                formattedData.push({
                    "quarter": label,
                    "value": data[i].Value.toFixed(3)
                });
            }


            config.dataProvider = formattedData;
            if (mbo.Value !== null) {
                var balloonText = "<span style='font-size:110x;'>MBO: <b>" + mbo.Value + "</b></span>";
                config.valueAxes = [
                    {
                        "guides": [
                            {
                                "value": mbo.Value,
                                "balloonText": balloonText,
                                "lineAlpha": 0.9,
                                "lineColor": "#EE4266",
                                "label": "MBO",
                                "position": "right",
                                "boldLabel": true,
                                "lineThickness": 2,
                                "inside": true
                            }
                        ]
                    }
                ];
            }

            dashboardChartConfigs["dashEDAs_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("dashEDAs_chartdiv", config, 100);

        } else {
            generateEmptyChart("dashEDAs_chartdiv", "");
        }
    } else {
        generateEmptyChart("dashEDAs_chartdiv", "");
    }
    //savedCallbacks["edas_" + selectedStudio] = true;
    resetCurrentHash();
}

function cn_inCallback(r) {
    $("#cn_in .figBody").removeClass("loading");
    $("#cn_in .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["cn_in_" + selectedStudio] !== undefined && (r["cn_in_" + selectedStudio]).length > 0) {

        var config = generateDashConfigColumn();

        var data = _.sortBy((r["cn_in_" +selectedStudio]), 'Label');
        var mbo = {}, mboLoc = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].Label.search("MBO") !== -1) {
                mbo = data[i];
                mboLoc = i;
            }
        }

        if (mboLoc !== -1)
            data.splice(mboLoc, 1);

        if (data.length > 0) {
            if (data.length > 4) {
                var start = data.length - 4;
                data = data.slice(start, data.length);
            }

            var formattedData = [];
            var sum = 0;
            var prevYear = data[0].Label.substring(data[0].Label.length - 5, data[0].Label.length - 3);
            var tot = 0;
            for (var i = 0; i < data.length; i++) {
                var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);

                var currYear = data[i].Label.substring(data[i].Label.length - 5, data[i].Label.length - 3);

                if (prevYear !== currYear) {
                    if (sum > tot)
                        tot = sum;
                    sum = 0;
                    prevYear = currYear;
                }
                sum += data[i].Value;
                formattedData.push({
                    "total": sum,
                    "quarter": label,
                    "value": data[i].Value
                });
            }


            config.graphs.push({
                "id": "graph2",
                "balloonText":
                    "<span style='font-size:10px;'>[[title]] in [[category]]: <b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "lineThickness": 2,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#ffffff",
                "lineColor":dashboardColors[1],
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Total",
                "valueField": "total",
                "dashLengthField": "dashLengthLine"
            });

            if (mbo.Value !== null) {
                var max = tot;
                if (max < mbo.Value) {
                    max = mbo.Value;
                }
                var balloonText = "<span style='font-size:10px;'>MBO: <b>" + mbo.Value + "</b></span>";
                config.valueAxes = [
                    {
                        "maximum": max,
                        "guides": [
                            {
                                "value": mbo.Value,
                                "balloonText": balloonText,
                                "lineAlpha": 0.9,
                                "lineColor": "#EE4266",
                                "label": "MBO",
                                "position": "right",
                                "boldLabel": true,
                                "lineThickness": 2,
                                "inside": true
                            }
                        ]
                    }
                ];
            }

                var title = "Total: " + sum;
                config.titles = [
                    {
                        "text": title,
                        "size": 15
                    }
                ];
                config.dataProvider = formattedData;

                dashboardChartConfigs["dashcn_in_chartdiv"] = { chart: null, config: config };
                var chart = AmCharts.makeChart("dashcn_in_chartdiv", config, 100);

        } else
                generateEmptyChart("dashcn_in_chartdiv", "");
        } else {
            generateEmptyChart("dashcn_in_chartdiv", "");
        }
        //savedCallbacks["cn_in_" + selectedStudio] = true;
    resetCurrentHash();
}

function cn_inEnteredCallback(r) {
    $("#cn_inEntered .figBody").removeClass("loading");
    $("#cn_inEntered .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["cn_inEntered_" + selectedStudio] !== undefined && (r["cn_inEntered_" + selectedStudio]).length > 0) {

        var config = generateDashConfigColumn();

        var data = _.sortBy((r["cn_inEntered_" + selectedStudio]), 'Label');
        var mbo = {}, mboLoc = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].Label.search("MBO") !== -1) {
                mbo = data[i];
                mboLoc = i;
            }
        }

        if (mboLoc !== -1)
            data.splice(mboLoc, 1);

        if (data.length > 0) {
            if (data.length > 4) {
                var start = data.length - 4;
                data = data.slice(start, data.length);
            }

            var formattedData = [];
            var sum = 0;
            var prevYear = data[0].Label.substring(data[0].Label.length - 5, data[0].Label.length - 3);
            var tot = 0;
            for (var i = 0; i < data.length; i++) {
                var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);

                var currYear = data[i].Label.substring(data[i].Label.length - 5, data[i].Label.length - 3);

                if (prevYear !== currYear) {
                    if (sum > tot)
                        tot = sum;
                    sum = 0;
                    prevYear = currYear;
                }
                sum += data[i].Value;
                formattedData.push({
                    "total": sum,
                    "quarter": label,
                    "value": data[i].Value
                });
            }


            config.graphs.push({
                "id": "graph2",
                "balloonText":
                    "<span style='font-size:10px;'>[[title]] in [[category]]: <b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "lineThickness": 2,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#ffffff",
                "lineColor": dashboardColors[1],
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Total",
                "valueField": "total",
                "dashLengthField": "dashLengthLine"
            });

            if (mbo.Value !== null) {
                var max = tot;
                if (max < mbo.Value) {
                    max = mbo.Value;
                }
                var balloonText = "<span style='font-size:10px;'>MBO: <b>" + mbo.Value + "</b></span>";
                config.valueAxes = [
                    {
                        "maximum": max,
                        "guides": [
                            {
                                "value": mbo.Value,
                                "balloonText": balloonText,
                                "lineAlpha": 0.9,
                                "lineColor": "#EE4266",
                                "label": "MBO",
                                "position": "right",
                                "boldLabel": true,
                                "lineThickness": 2,
                                "inside": true
                            }
                        ]
                    }
                ];
            }

            var title = "Total: " + sum;
            config.titles = [
                {
                    "text": title,
                    "size": 15
                }
            ];
            config.dataProvider = formattedData;

            dashboardChartConfigs["dashcn_inEntered_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("dashcn_inEntered_chartdiv", config, 100);

        } else
            generateEmptyChart("dashcn_inEntered_chartdiv", "");
    } else {
        generateEmptyChart("dashcn_inEntered_chartdiv", "");
    }
    //savedCallbacks["cn_inEntered_" + selectedStudio] = true;
    resetCurrentHash();
}

function ContainmentRateCallback(r) {
    //draw DCR
    $("#ContainmentRate .figBody").removeClass("loading");
    $("#ContainmentRate .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["ContainmentRate_" + selectedStudio] !== undefined && (r["ContainmentRate_" + selectedStudio]).length) {
        var config = generateDashConfigLine(true);
        var formattedData = [];

        var data = _.sortBy((r["ContainmentRate_" +selectedStudio]), 'Label');
        var mbo = {},
			mboLoc = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].Label.search("MBO") !== -1) {
                mbo = data[i];
                mboLoc = i;
            }
        }

        if (mboLoc !== -1)
            data.splice(mboLoc, 1);

        if (data.length > 0) {
            if (data.length > 4) {
                var start = data.length - 4;
                data = data.slice(start, data.length);
            }

            for (var i = 0; i < data.length; i++) {
                var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);

                formattedData.push({
                    "quarter": label,
                    "value": parseFloat((data[i].Value).toFixed(2))
                });
            }


            config.dataProvider = formattedData;

            if (mbo.Value != null) {
                var balloonText = "<span style='font-size:10px;'>MBO: <b>" +
					parseFloat((mbo.Value).toFixed(2)) +
					"</b></span>";
                config.valueAxes = [
					{
					    "guides": [
							{
							    "value": parseFloat((mbo.Value).toFixed(2)),
							    "ballonText": balloonText,
							    "lineAlpha": 0.9,
							    "lineColor": "#EE4266",
							    "label": "MBO: " + parseFloat((mbo.Value).toFixed(2)) + "%",
							    "position": "right",
							    "boldLabel": true,
							    "lineThickness": 2,
							    "inside": true
							}
					    ]
					}
                ];
            }

            dashboardChartConfigs["dashDCR_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("dashDCR_chartdiv", config, 100);

        }
        else
            generateEmptyChart("dashDCR_chartdiv", "");
    }
    else {
        generateEmptyChart("dashDCR_chartdiv", "");
    }
    //savedCallbacks["ContainmentRate_" + selectedStudio] = true;
    resetCurrentHash();
}

function edasEnteredCallback(r) {
    $("#edasEntered .figBody").removeClass("loading");
    $("#edasEntered .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["edasEntered_" + selectedStudio] !== undefined && (r["edasEntered_" + selectedStudio]).length > 0) {
        var config = generateDashConfigColumn();

        var data = (r["edasEntered_" + selectedStudio]);
        var mbo = {},
			mboLoc = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].Label.search("MBO") !== -1) {
                mbo = data[i];
                mboLoc = i;
            }
        }

        if (mboLoc !== -1)
            data.splice(mboLoc, 1);

        if (data.length > 0) {
            if (data.length > 4) {
                var start = data.length - 4;
                data = data.slice(start, data.length);
            }

            var formattedData = [];
            var sum = 0;
            var prevYear = data[0].Label.substring(data[0].Label.length - 5, data[0].Label.length - 3);
            var tot = 0;
            for (var i = 0; i < data.length; i++) {
                var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);
                var currYear = data[i].Label.substring(data[i].Label.length - 5, data[i].Label.length - 3);

                if (prevYear !== currYear) {
                    if (sum > tot)
                        tot = sum;
                    sum = 0;
                    prevYear = currYear;
                }

                sum += (data[i].Value);
                formattedData.push({
                    "total": parseFloat(sum.toFixed(2)),
                    "quarter": label,
                    "value": parseFloat((data[i].Value).toFixed(2))
                });
            }


            config.graphs.push({
                "id": "graph2",
                "balloonText": "<span style='font-size:10px;'>[[title]] in [[category]]: <b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "lineThickness": 2,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "lineColor": dashboardColors[1],
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Total",
                "valueField": "total",
                "dashLengthField": "dashLengthLine"
            });


            var max = tot;
            if (max < data[data.length - 1].Value) {
                max = data[data.length - 1].Value;
            }
            var balloonText = "<span style='font-size:10px;'>MBO: <b>" + max + "</b></span>";
            config.valueAxes = [
				{
				    "maximum": max
				    /*
                    "guides": [
                        {
                            "value": max,
                            "balloonText": balloonText,
                            "lineAlpha": 0.9,
                            "lineColor": "#EE4266",
                            "label": "MBO",
                                    "position": "right",
                                    "boldLabel": true,
                                    "lineThickness": 2,
                                    "inside": true
                                }
                            ]*/
				}
            ];
            var title = "Total: " + sum.toFixed(2);
            config.titles = [
				{
				    "text": title,
				    "size": 15
				}
            ];
            config.dataProvider = formattedData;

            dashboardChartConfigs["edaEntered_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("edaEntered_chartdiv", config, 100);

        }
        else
            generateEmptyChart("edaEntered_chartdiv", "");
    }
    else {
        generateEmptyChart("edaEntered_chartdiv", "");
    }
    //savedCallbacks["edasEntered_" + selectedStudio] = true;
    resetCurrentHash();
}

function QuartersPhaseFoundCallback(r) {
    $("#QuartersPhaseFound .figBody").removeClass("loading");
    $("#QuartersPhaseFound .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["defectsPhaseFound_" + selectedStudio] !== undefined && (r["defectsPhaseFound_" + selectedStudio]).length > 5) {

        // draw Defects by Phase Found chart
        var config = generateDashConfigPie(true);
        config.dataProvider = [
			{
			    "title": "Early Testing",
			    "value": (r["defectsPhaseFound_" + selectedStudio])[0].Value,
			    "color": dashboardColors[0]
			},
			{
			    "title": "Math",
			    "value": (r["defectsPhaseFound_" + selectedStudio])[1].Value,
			    "color": dashboardColors[1]
			},
			{
			    "title": "PA",
			    "value": (r["defectsPhaseFound_" + selectedStudio])[2].Value,
			    "color": dashboardColors[2]
			},
			{
			    "title": "Paytable",
			    "value": (r["defectsPhaseFound_" + selectedStudio])[3].Value,
			    "color": dashboardColors[3]
			},
			{
			    "title": "Post PA",
			    "value": (r["defectsPhaseFound_" + selectedStudio])[4].Value,
			    "color": dashboardColors[4]
			},
			{
			    "title": "Pre PA",
			    "value": (r["defectsPhaseFound_" + selectedStudio])[5].Value,
			    "color": dashboardColors[5]
			}
        ];

        var tot = 0;
        for (var i = 0; i < config.dataProvider.length; i++) {
            tot += config.dataProvider[i].value;
        }

        config.allLabels = [
			{
			    "text": ((config.dataProvider[2].value / tot) * 100).toFixed(2) + "%",
			    "align": "center",
			    "bold": true,
			    "y": 70,
			    "size": 25
			}, {
			    "text": config.dataProvider[2].title,
			    "align": "center",
			    "y": 110,
			    "size": 18
			}, {
			    "text": "Rolling",
			    "align": "center",
			    "y": 135,
			    "size": 12
			}
        ];

        dashboardChartConfigs["dashPhaseFoundQuarters_chartdiv"] = { chart: null, config: config };
        var chart = AmCharts.makeChart("dashPhaseFoundQuarters_chartdiv", config, 100);

    }
    else {
        generateEmptyChart("dashPhaseFoundQuarters_chartdiv", "");
    }
    //savedCallbacks["QuartersPhaseFound_" + selectedStudio] = true;
    resetCurrentHash();
}

function BVATestCasesPassFailCallback(r) {
    $("#BVATestCasesPassFail .figBody").removeClass("loading");
    $("#BVATestCasesPassFail .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["BVATestCasesPassFail_" + selectedStudio] !== undefined && (r["BVATestCasesPassFail_" + selectedStudio]).length > 0) {

        // draw BVA Test Cases pie chart
        var config = generateDashConfigPie(true);
        config.dataProvider = [
			{
			    "title": "Pass",
			    "value": (r["BVATestCasesPassFail_" + selectedStudio])[3].Value,
			    "color": dashboardColors[0]
			}, {
			    "title": "Fail",
			    "value": (r["BVATestCasesPassFail_" + selectedStudio])[1].Value,
			    "color": dashboardColors[1]
			}, {
			    "title": "Other",
			    "value": (r["BVATestCasesPassFail_" + selectedStudio])[2].Value,
			    "color": dashboardColors[2]
			}
        ];

        var tot = 0;
        for (var i = 0; i < config.dataProvider.length; i++) {
            tot += config.dataProvider[i].value;
        }

        config.allLabels = [
			{
			    "text": ((config.dataProvider[0].value / tot) * 100).toFixed(2) + "%",
			    "align": "center",
			    "bold": true,
			    "y": 70,
			    "size": 25
			}, {
			    "text": config.dataProvider[0].title,
			    "align": "center",
			    "y": 110,
			    "size": 18
			}, {
			    "text": "Rolling",
			    "align": "center",
			    "y": 135,
			    "size": 12
			}
        ];

        dashboardChartConfigs["dashBVATestCases_chartdiv"] = { chart: null, config: config };
        chart = AmCharts.makeChart("dashBVATestCases_chartdiv", config, 100);

    }
    else {
        generateEmptyChart("dashBVATestCases_chartdiv", "");
    }
    //savedCallbacks["BVATestCasesPassFail_" + selectedStudio] = true;
    resetCurrentHash();
}

function UTPTestCasesPassFailCallback(r) {
    $("#UTPTestCasesPassFail .figBody").removeClass("loading");
    $("#UTPTestCasesPassFail .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && (r.passed + r.failed + r.other) > 0) {

        var config = generateDashConfigPie(true);
        config.dataProvider = [
			{
			    "title": "Pass",
			    "value": r.passed,
			    "color": dashboardColors[0]
			}, {
			    "title": "Fail",
			    "value": r.failed,
			    "color": dashboardColors[1]
			}, {
			    "title": "Other",
			    "value": r.other,
			    "color": dashboardColors[2]
			}
        ];

        var tot = 0;
        for (var i = 0; i < config.dataProvider.length; i++) {
            tot += config.dataProvider[i].value;
        }

        config.allLabels = [
			{
			    "text": ((config.dataProvider[0].value / tot) * 100).toFixed(2) + "%",
			    "align": "center",
			    "bold": true,
			    "y": 70,
			    "size": 25
			}, {
			    "text": config.dataProvider[0].title,
			    "align": "center",
			    "y": 110,
			    "size": 18
			}, {
			    "text": "Rolling",
			    "align": "center",
			    "y": 135,
			    "size": 12
			}
        ];

        dashboardChartConfigs["dashUTPTestCases_chartdiv"] = { chart: null, config: config };
        var chart = AmCharts.makeChart("dashUTPTestCases_chartdiv", config, 100);

    }
    else {
        generateEmptyChart("dashUTPTestCases_chartdiv", "");
    }
    //savedCallbacks["UTPTestCasesPassFail_" + selectedStudio] = true;
    resetCurrentHash();
}

function GameTrackerCallback(r) {
    $("#GameTracker .figBody").removeClass("loading");
    $("#GameTracker .figBody").addClass("doneLoading");
    if (r != null && r !== undefined && (r.utpDisabled + r.utpEnabled) > 0) {

        var config = generateDashConfigPie(true);
        config.dataProvider = [
			{
			    "title": "Disabled",
			    "value": r.utpDisabled,
			    "color": dashboardColors[0]
			},
			{
			    "title": "Enabled",
			    "value": r.utpEnabled,
			    "color": dashboardColors[1]
			}
        ];

        var tot = 0;
        for (var i = 0; i < config.dataProvider.length; i++) {
            tot += config.dataProvider[i].value;
        }

        config.allLabels = [
			{
			    "text": ((config.dataProvider[1].value / tot) * 100).toFixed(2) + "%",
			    "align": "center",
			    "bold": true,
			    "y": 70,
			    "size": 25
			}, {
			    "text": config.dataProvider[1].title,
			    "align": "center",
			    "y": 110,
			    "size": 18
			}, {
			    "text": "Rolling",
			    "align": "center",
			    "y": 135,
			    "size": 12
			}
        ];

        dashboardChartConfigs["dashGameTracker_chartdiv"] = { chart: null, config: config };
        var chart = AmCharts.makeChart("dashGameTracker_chartdiv", config, 100);

    }
    else {
        generateEmptyChart("dashGameTracker_chartdiv", "");
    }
    //savedCallbacks["GameTracker_" +selectedStudio]= true;
    resetCurrentHash();
}

function edaNewOnlyCallback(r) {
    $("#edaNewOnly .figBody").removeClass("loading");
    $("#edaNewOnly .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && r["edaNewOnly_" + selectedStudio] !== undefined && (r["edaNewOnly_" + selectedStudio]).length > 0) {

        var config = generateDashConfigLine(false);

        var data = _.sortBy((r["edaNewOnly_" + selectedStudio]), 'Label');
        var mbo = {}, mboLoc = -1;
        for (var i = 0; i < data.length; i++) {
            if (data[i].Label.search("MBO") !== -1) {
                mbo = data[i];
                mboLoc = i;
            }
        }

        if (mboLoc !== -1)
            data.splice(mboLoc, 1);

        if (data.length > 0) {
            if (data.length > 4) {
                var start = data.length - 4;
                data = data.slice(start, data.length);
            }

            var formattedData = [];
            var sum = 0;
            for (var i = 0; i < data.length; i++) {
                var label = data[i].Label.slice(data[i].Label.length - 2, data[i].Label.length) + " '" + data[i].Label.slice(data[i].Label.length - 5, data[i].Label.length - 3);
                sum += data[i].Value;
                formattedData.push({
                    "quarter": label,
                    "value": data[i].Value.toFixed(3)
                });
            }


            config.dataProvider = formattedData;
            if (mbo.Value !== null) {
                var balloonText = "<span style='font-size:110x;'>MBO: <b>" + mbo.Value + "</b></span>";
                config.valueAxes = [
                    {
                        "guides": [
                            {
                                "value": mbo.Value,
                                "balloonText": balloonText,
                                "lineAlpha": 0.9,
                                "lineColor": "#EE4266",
                                "label": "MBO",
                                "position": "right",
                                "boldLabel": true,
                                "lineThickness": 2,
                                "inside": true
                            }
                        ]
                    }
                ];
            }

            dashboardChartConfigs["dashEdaNewOnly_chartdiv"] = { chart: null, config: config };
            var chart = AmCharts.makeChart("dashEdaNewOnly_chartdiv", config, 100);

        } else {
            generateEmptyChart("dashEdaNewOnly_chartdiv", "");
        }
    } else {
        generateEmptyChart("dashEdaNewOnly_chartdiv", "");
    }
    //savedCallbacks["edas_" + selectedStudio] = true;
    resetCurrentHash();
}

function KitCountForToolsCallback(r) {
    $("#KitCountForTools .figBody").removeClass("loading");
    $("#KitCountForTools .figBody").addClass("doneLoading");

    if (r != null && r !== undefined && (r["KitCountForTools_" + selectedStudio])!==undefined && (r["KitCountForTools_" + selectedStudio]).length > 0) {
        var data = (r["KitCountForTools_" + selectedStudio]);

        var max = data.length;
        if (max > 5)
            max = 5;

        for (var i = 0; i < max; i++) {
            var toolName = data[i].Label;
            if (toolName.length > 18)
                toolName = (data[i].Label).substring(0, 18) + "...";

            $("#KitCountForTools_item" +(i + 1) + " .tool").text((i +1) + ". " +toolName);
        }


    } else {
        $("#KitCountForTools_item1 .tool").text("No Tools Found");
    }
    // savedCallbacks["KitCountForTools_" + selectedStudio] = true;
    resetCurrentHash();
}

function EQEScoreByPushDateCallback(r) {
    if (r != null && r !== undefined && (r["EQEScoreByPushDate_" + selectedStudio])!==undefined) {
        $("#EQEScoreByPushDate .figBody").removeClass("loading");
        $("#EQEScoreByPushDate .figBody").addClass("doneLoading");

        if ((r["EQEScoreByPushDate_" + selectedStudio]).length > 0)
        {
            $("#dashEQEScoreRolling .num").text((r["EQEScoreByPushDate_" + selectedStudio])[0].Value);
            //$("#dashEQEScoreYTD .num").text((r["EQEScoreByPushDate_" + selectedStudio])[1].Value);
        }
    }
    resetCurrentHash();
}
/*
function drawDashboard() {
    //update kits released div
    //document.getElementById("dashKitsReleasedYTD").innerHTML = dashboardData[4].Value;

    // draw kits released new theme/other pie and number

    // draw First Pass pie chart
    var passRate = 76.45;
    var config = generateDashConfigGauge(passRate);
    var chart = AmCharts.makeChart("dashFirstPass_chartdiv", config, 100);

}*/


function GetUserName() {
    var client = new $.RestClient('/rest/');
    client.add("currentUser");
    var restRequest = client.currentUser.create(); // post request
    // POST /rest/currentUser/
    restRequest.done(updateUserFirstName);
}

function updateUserFirstName(r) {
    var user = r["user"];
    username = user.split('\\')[1];
    $('#welcomeUser').text(r["firstname"]);

    if (username !== null) {
        var getQuery = {
            "Get": {
                "nothing": "nothing"
            }
        };
        getWebToolData(username, toolname, getQuery, getUserPreferences_callback);
    }
}

function loadTheMetric(metricName) {
    var $activediv = $("#dashboardContainer");
    $activediv.removeClass('active');
    $activediv.addClass('hide');

    loadMetricView(metricName);
}

function generateDashConfiMboGreenToRed() {
    var config = {
        "type": "serial",
        "theme": mboDashboardTheme,
        "autoMargins": false,
        "marginTop": 15,
        "marginLeft": 40,
        "marginBottom": 30,
        "marginRight": 20,
        "valueAxes": [
			{
			    "maximum": 100,
			    "stackType": "regular",
			    "gridAlpha": 0
			}
        ],
        "startDuration": 1,
        "graphs": [{
            "fillAlphas": 0.9,
            "lineColor": "#C41E3D",
            "showBalloon": false,
            "type": "column",
            "valueField": "excelent"
        }, {
            "fillAlphas": 0.9,
            "lineColor": "#E4B363",
            "showBalloon": false,
            "type": "column",
            "valueField": "average"
        }, {
            "fillAlphas": 0.9,
            "lineColor": "#21A179",
            "showBalloon": false,
            "type": "column",
            "valueField": "bad"
        },
			/*
			            {
			                "valueField": "full",
			                "showBalloon": false,
			                "type": "column",
			                "lineAlpha": 0,
			                "fillAlphas": 0.8,
			                "fillColors": ["#e84c3d", "#f6d32b", "#2DCC70"],
			                "gradientOrientation": "horizontal"
			            },*/
			{
			    "clustered": false,
			    "columnWidth": 0.3,
			    "fillAlphas": 1,
			    "lineColor": "#F6F5AE",
			    "stackable": false,
			    "type": "column",
			    "valueField": "bullet"
			}, {
			    "columnWidth": .30,
			    "lineColor": "#FFFFFF",
			    "lineThickness": 3,
			    "noStepRisers": true,
			    "stackable": false,
			    "type": "step",
			    "valueField": "limit",
			    "labelText": "[[value]]%",
			    "color": "#FFFFFF",
			    "fontSize": 28,
			    "labelPosition": "top"

			}
        ],
        "rotate": true,
        "columnWidth": .55,
        "categoryField": "category",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 0
        }
    };

    return config;
}

function generateDashConfiMboPieNumVert() {
    var config = {
        "type": "serial",
        "theme": mboDashboardTheme,
        "autoMargins": false,
        "marginTop": 20,
        "marginLeft": 40,
        "marginBottom": 25,
        "marginRight": 20,
        "valueAxes": [
			{
			    "maximum": 100,
			    "stackType": "regular",
			    "gridAlpha": 0
			}
        ],
        "startDuration": 1,
        "graphs": [
			{
			    "valueField": "full",
			    "showBalloon": false,
			    "type": "column",
			    "lineAlpha": 0,
			    "fillAlphas": 0.8,
			    "fillColors": ["#2DCC70", "#f6d32b", "#e84c3d"],
			    "gradientOrientation": "vertical"
			}, {
			    "clustered": false,
			    "columnWidth": 0.3,
			    "fillAlphas": 1,
			    "lineColor": "#000000",
			    "stackable": false,
			    "type": "column",
			    "valueField": "bullet"
			}, {
			    "columnWidth": .35,
			    "lineColor": dashboardMainColor,
			    "lineThickness": 3,
			    "noStepRisers": true,
			    "stackable": false,
			    "type": "step",
			    "valueField": "limit",
			    "labelText": "[[value]]",
			    "color": dashboardMainColor,
			    "fontSize": 28,
			    "labelPosition": "top"

			}
        ],

        "columnWidth": .65,
        "categoryField": "category",
        "categoryAxis": {
            "gridPosition": "start"
        }
    };

    return config;
}

function generateDashConfigLine(showPercentage) {
    var balloonText = "[[category]]: <b><span style='font-size:11px;'>[[value]]</span></b>";
    var marginLeft = 32;
    if (showPercentage) {
        balloonText = "[[category]]: <b><span style='font-size:11px;'>[[value]]</span></b>%";
        marginLeft = 40;
    }

    var config = {
        "type": "serial",
        "theme": dashboardTheme,
        "color": dashboardMainColor,
        "categoryField": "quarter",
        "autoMargins": false, 
        "fontSize": 10,
        "marginTop": 20,
        "marginLeft": marginLeft,
        "marginBottom": 25,
        "marginRight": 10,
        "graphs": [
			{
			    "id": "g1",
			    "balloonText": balloonText,
			    "bullet": "round",
			    "bulletSize": 8,
			    "lineColor": dashboardColors[0],
			    "lineThickness": 2,
			    "type": "smoothedLine",
			    "valueField": "value"
			}
        ],
        "valueAxis": {
            "gridColor": dashboardMainColor,
            "axisColor": dashboardMainColor,
            "color": dashboardMainColor,
            "dashLength": 1,
            "axisThickness":2
        },
        "categoryAxis": {
            "autoGridCount": true,
            "gridPosition": "start",
            "gridColor": dashboardMainColor,
            "axisColor": dashboardMainColor,
            "color": dashboardMainColor,
            "dashLength": 1
        }
    };

    return config;
}

function generateDashConfigPie(showPercent) {
    var rollOverfunction = handlePieHover;
    if (showPercent) {
        rollOverfunction = handlePiePercentHover;
    }

    var config = {
        "type": "pie",
        "theme": dashboardTheme,
        "color": dashboardMainColor,
        "colorField": "color",
        "titleField": "title",
        "valueField": "value",
        "labelsEnabled": false,
        "autoMargins": false,
        "marginTop": 15,
        "marginBottom": 0,
        "marginLeft": 0,
        "marginRight": 0,
        "pullOutRadius": 0,
        "innerRadius": "75%",
        "labelText": "[[title]]",
        "outlineThickness": 0,
        "balloon": {
            "fixedPosition": false
        },
        "export": {
            "enabled": false
        },
        "listeners": [{
            "event": "rollOverSlice",
            "method": rollOverfunction
        }]
    };
    return config;
}

function handlePiePercentHover(event) {
    if (event.dataItem && event.chart.allLabels[1].text !== event.dataItem.title) {
        event.chart.allLabels[0].text = event.dataItem.percents.toFixed(2) + "%";
        event.chart.allLabels[1].text = event.dataItem.title;
        event.chart.validateNow();
    }
}

function handlePieHover(event) {
    if (event.dataItem && event.chart.allLabels[1].text !== event.dataItem.title) {
        event.chart.allLabels[0].text = event.dataItem.value;
        event.chart.allLabels[1].text = event.dataItem.title;
        event.chart.validateNow();
    }
}

function generateDashConfigColumn(showPercent) {
    var balloonText = "[[category]]: <b>[[value]]</b>";
    if (showPercent)
        balloonText = "[[category]]: <b>[[percent]]</b>";

    var config = {
        "type": "serial",
        "fontSize": 10,
        "theme": dashboardTheme,
        "color": dashboardMainColor,
        "autoMargins": false,
        "marginTop": 15,
        "marginLeft": 35,
        "marginBottom": 25,
        "marginRight": 10,
        "graphs": [{
            "id": "g1",
            "balloonText": balloonText,
            "type": "column",
            "valueField": "value",
            "fillAlphas": 1,
            "fillColors": dashboardColors[0],
            "lineColor": dashboardColors[0]
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "valueAxis": {
            "gridColor": dashboardMainColor,
            "axisColor": dashboardMainColor,
            "color": dashboardMainColor,
            "dashLength": 1,
            "axisThickness": 2
        },
        "categoryAxis": {
            "autoGridCount": true,
            "gridPosition": "start",
            "gridColor": dashboardMainColor,
            "axisColor": dashboardMainColor,
            "color": dashboardMainColor,
            "dashLength":1
        },
        "categoryField": "quarter"
    };
    return config;
}

function getDashQuarterList() {
    var now = new Date();
    var quarter = getQuarter(now.getMonth());
    var year = now.getFullYear();
    var quarters = [];
    var prev;

    for (var i = 0; i < 3; i++) {
        prev = getPreviousQuarter(quarter, year);
        quarter = prev.quarter;
        year = prev.year.toString();
        quarters.unshift("Q" + quarter + "-" + year.substr(2, 2));
    }
    quarterList = quarters;
    quarterList[quarterList.length] = "CF";
    return quarters;
}


/*
function get_DashboardData(studio) {
    var client = new $.RestClient('/rest/');
    client.add("dashboardData");
    var restRequest;

    if (!studio) {
        restRequest = client.dashboardData.create();
    } else {
        restRequest = client.dashboardData.create({}, {
            studio: studio
        });
    }
    restRequest.done(get_DashboardData_callBack);
};

function get_DashboardData_callBack(r) {
    if (r != null && r != undefined) {
        var studio = r["studio"];
        dashboardData = r.DashboardData;
        drawDashboard();
    }
    resetCurrentHash();
};*/

function callBack(slide) {
    var target = slide.currentTarget.id;
    if (target && target != "")
        loadTheMetric(target);
};

function moveCard(array, element, offset) {
    var index = array.indexOf(element);
    var newIndex = index + offset;

    if (newIndex > -1 && newIndex < array.length) {
        //Remove the element from the array
        removedElement = array.splice(index, 1)[0];

        // At "newIndex", remove 0 elements, insert the removed element
        array.splice(newIndex, 0, removedElement);
    }
};

function updateCardOrder(elem, direction) {
    var id = elem[0].id;
    var offset;
    switch (direction) {
        case "left":
            offset = -1;
            break;
        case "right":
            offset = 1;
            break;
    }
    var array = getMetricCarousel(id);
    console.log(array);
    moveCard(array, id, offset);
    console.log(array);

    sendUpdateCardQuery();
};

function sendUpdateCardQuery() {
    // SAVE USER SETTINGS (Order Arrays) db webtools
    var query = {
        "Update": {
            "kitCarouselOrder": kitCarouselOrder,
            "defectCarouselOrder": defectCarouselOrder,
            "otherCarouselOrder": otherCarouselOrder,
            "mathCarouselOrder": mathCarouselOrder,
            "edaCarouselOrder": edaCarouselOrder,
            "studio": ""
        }
    };
    getWebToolData(username, toolname, query, function () { });
}

function getMetricCarousel(metricId) {
    if (kitCarouselOrder.indexOf(metricId) > -1)
        return kitCarouselOrder;
    else if (defectCarouselOrder.indexOf(metricId) > -1)
        return defectCarouselOrder;
    else if (otherCarouselOrder.indexOf(metricId) > -1)
        return otherCarouselOrder;
    else if (mathCarouselOrder.indexOf(metricId) > -1)
        return mathCarouselOrder;
    else if (edaCarouselOrder.indexOf(metricId) > -1)
        return edaCarouselOrder;
};