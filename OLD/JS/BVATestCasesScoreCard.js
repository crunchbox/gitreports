var testCaseName = "";
var BVATestCase_processedData = {};
var BVATestCase_ChartData = {};
var BVATestCase_Charts = {};


var bvaSC_data = {
	testCaseName: "",
	count: "",
	suite_count: "",
	suite_sudio: "",
	data: []
}





function loadBVATestCaseSCData(prefix, testCase, parenturl) {
	testCaseName = testCase;
	parent_prefix = prefix;

	if (parenturl == undefined)
		parenturl = "#/metric/studio/all";

	$("#gobacklink_a").attr("title", parenturl);

	get_bvaTestCaseSCData(); // get Data


}


function removeBVATestCaseSC() {
	$.when($("#bva_testCaseSC").remove()).then(setTimeout(resetCurrentHash, 100));
	if (parent_prefix.endsWith("_L2") || parent_prefix.endsWith("_L3")) {
		$("#" + parent_prefix + "_chartdiv").show();
		var tempStr = parent_prefix;
		tempStr = parent_prefix.substr(0, parent_prefix.length - 3);
		$("#" + tempStr + "_dataTable_group").show();
		$("#" + tempStr + "_dataTable_link").show();
		$("#link_" + tempStr + "_4").remove();
		level--;
		$("#link_" + tempStr + "_" + level).addClass('active');
	} else displayTopLevelMetric(parent_prefix);
}


function get_bvaTestCaseSCData() {
	if (bvaSC_jsonData.hasOwnProperty(testCaseName) == false) {
		var client = new $.RestClient('/rest/');
		client.add("bvaTestCasesByCaseName");
		var restRequest = client.bvaTestCasesByCaseName.create({}, {
			caseName: testCaseName
		});
		// POST /rest/scorecard/?kit={kitNumber}
		restRequest.done(get_BVATestCaseSCData_callBack);

	} else get_BVATestCaseSCData_callBack(bvaSC_jsonData[testCaseName]);
}


function get_BVATestCaseSCData_callBack(r) {
	if (bvaSC_jsonData.hasOwnProperty(testCaseName) == false) {
		bvaSC_jsonData[testCaseName] = r;
	}
	var currentData = r["BVATestCases_" + testCaseName];

	BVASC_dataTables["testCaseData"] = currentData;

	configureBVATestCasesCharts();

	return currentData;
}


function configureBVATestCasesCharts() {
	var testCaseData = BVASC_dataTables["testCaseData"];
	if (testCaseData.length < 0) {
		alert("No BVA Test Case Found!");
		removeBVAScoreCard();
		return;
	}

	removeLoadingDiv();

	bvaSC_data.testCaseName = testCaseName;
	bvaSC_data.count = testCaseData.length;
    bvaSC_data.data = testCaseData;/*_.sortBy(testCaseData, function (testCase) {
	    return (testCase.StartTime);
	});*/

    if (testCaseData.length > 0)
        bvaSC_data.description = testCaseData[0].Description;
    else
        bvaSC_data.description = "No description available.";

	var date = new Date(bvaSC_data.data[0].StartTime);
	bvaSC_data.earlyRun = date.toString().substring(0, 33);

	var today = new Date();
	today.setDate(today.getDate() + 1);
	var date2 = new Date(bvaSC_data.data[(bvaSC_data.data).length - 1].StartTime);
	var k = (bvaSC_data.data).length - 1;
	while (date2 > today) {
		date2 = new Date(bvaSC_data.data[k].StartTime);
		k--;
	}

	bvaSC_data.lastRun = date2.toString().substring(0, 33);

	$("#bvaData").ready(function () {
		if ($(window).width() < 750) {
			$(".tableLabel").css('text-align', 'left');
		} else
			$(".tableLabel").css('text-align', 'right');

		$("#caseName_td").text(bvaSC_data.testCaseName);
		$("#usage_td").text(bvaSC_data.count);
		var date = new Date(bvaSC_data.data[0].StartTime);
		$("#earlyRun_td").text(bvaSC_data.earlyRun);
		$("#lastRun_td").text(bvaSC_data.lastRun);
		$("#desc_td").text(bvaSC_data.description);
		$("#atp_td").text(bvaSC_data.testCaseName + " in ATP")
		$("#atp_a").attr("href", "http://acloud.insideigt.com/ATP_dev/client/#/test/" + bvaSC_data.testCaseName);
		$("#helpicon").unbind('click');
		$("#helpicon").click(function () {
		    var URL = "HelpPage/helpPage.html#bvaTestCase";
		    window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
		});
	});


	var config = getBVATestCaseTimelineConfig("timelineChart", "Test Case Timeline", "black", bvaSC_data.data);

	//add balloon function
	for (i = 3; i < 6; i++)
		config.graphs[i].balloonFunction = adjustTimelineBallonText;

	//make timeline chart
	BVASC_chartConfigDict["timelineChart"] = config;
	var chart = AmCharts.makeChart("timeline_chartdiv", config, 100);
	BVATestCase_Charts["timelineChart"] = chart;
	chart.addListener("rendered", zoomTimeLineChart);
	chart.handleResize();

	var studioConfig = getStudioConfig("studioChart", "Test Case Usage / Studio", "black", bvaSC_data.data);
	BVASC_chartConfigDict["studioChart"] = studioConfig;
	var studioChart = AmCharts.makeChart("studioChart_chartdiv", studioConfig, 100);
	BVATestCase_Charts["studioChart"] = studioChart;
	studioChart.addListener("rendered", zoomStudioChart);
	studioChart.handleResize();

	var pieConfig = getPassFailConfig("passFailPie", "Pass/Fail Overall", "black", bvaSC_data.data);
	BVASC_chartConfigDict["passFailPie"] = pieConfig;
	var pieChart = AmCharts.makeChart("passFailPie_chartdiv", pieConfig, 100);
	BVATestCase_Charts["passFailPie"] = pieChart;
	pieChart.handleResize();


	var suiteConfig = getSuiteConfig("suiteChart", "Test Case Usage / Suite", "black", bvaSC_data.data);
	BVASC_chartConfigDict["suiteChart"] = suiteConfig;
	var suiteChart = AmCharts.makeChart("suiteChart_chartdiv", suiteConfig, 100);
	BVATestCase_Charts["suiteChart"] = suiteChart;
	suiteChart.addListener("rendered", zoomSuiteChart);
	suiteChart.handleResize();

	var runtimeConfig = getRuntimeConfig("runtime", "Runtime Overview", "black", bvaSC_data.data);
	BVASC_chartConfigDict["runtime"] = runtimeConfig;
	var runtimeChart = AmCharts.makeChart("runtime_chartdiv", runtimeConfig, 100);
	BVATestCase_Charts["runtime"] = runtimeChart;
	runtimeChart.handleResize();

	removeLoadingDiv();
}



function parseRuntimeData(chart_id, data) {
	var tempChartData = [];
	var results = _.sortBy(data, function (testCase) {
		return (testCase.ElapsedTime)
	});
	var min = results[0].ElapsedTime;
	var max = results[results.length - 1].ElapsedTime;
	var interval = 6;
	var interLen = Math.ceil((max - min) / interval);

	var count = {
		0: 0
	};
	var labels = [0];
	var time = min;
	for (var i = 0; i < interval; i++) {
		time = Math.ceil(time + interLen);
		count[time] = 0;
		labels.push(time);
	}

	for (var i = 0; i < results.length; i++) {
		var caseTime = results[i].ElapsedTime;

		var found = false;
		for (var maxTime in count) {
			if (caseTime <= maxTime && found === false) {
				count[maxTime] += 1;
				found = true;
			} else if (found) {
				break;
			}
		}
	}

	var prev = 0;
	for (var i = 0; i < labels.length; i++) {
		var str = "";
		if (i !== 0) {
			prev = labels[i - 1];
			str = prev + "-" + labels[i] + "ms";
		} else
			str = labels[i] + "ms";

		tempChartData.push({
			type: str,
			Count: count[labels[i]]
		});
	}



	BVATestCase_processedData[chart_id] = results;
	return tempChartData;
};

function getRuntimeConfig(chart_id, chartTitle, theme, data) {
	var chartData = parseRuntimeData(chart_id, data);

	var config = {
		"type": "pie",
		"theme": "black",
		"dataProvider": chartData,
		"valueField": "Count",
		"titleField": "type",
		"colorField": "color",
		"balloon": {
			"fixedPosition": true
		},
		"titles": [
			{
				"id": chart_id,
				"size": 15,
				"text": chartTitle
            }
        ],
		"export": {
			"enabled": true
		}
	};


	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});


	return config;
}


function zoomSuiteChart() {
    var chartData = BVATestCase_ChartData["suiteChart"];
    var max = chartData.length;
    if (max > 6)
        max = 6;
    BVATestCase_Charts["suiteChart"].zoomToIndexes(0, max);
}


function parseSuiteData(chart_id, data) {
	var tempChartData = [];
	var grouped = _.groupBy(data, function (testCase) {
		return (testCase.SuiteName)
	});

	for (var type in grouped) {
		var results = _.groupBy(grouped[type], function (result) {
			return (result.Result)
		});
		var passed = 0,
			failed = 0,
			other = 0;

		if (results.Passed !== undefined)
			passed = results.Passed.length;
		if (results.Failed !== undefined)
			failed = results.Failed.length;

		for (var result in results) {
			if (result !== "Passed" && result !== "Failed")
				other += results[result].length;
		}

		var passedPercent = ((passed / (passed + failed + other)) * 100).toFixed(2);
		var failedPercent = ((failed / (passed + failed + other)) * 100).toFixed(2);
		var otherPercent = ((other / (passed + failed + other)) * 100).toFixed(2);

		tempChartData.push({
			suite: type,
			Passed: passed,
			Failed: failed,
			Other: other,
			passed_percent: passedPercent,
			failed_percent: failedPercent,
			other_percent: otherPercent
		});
	}

	BVATestCase_processedData[chart_id] = grouped;
	return tempChartData;
}


function getSuiteConfig(chart_id, chartTitle, theme, data) {
	var chartData = parseSuiteData(chart_id, data);
	BVATestCase_ChartData[chart_id] = chartData;
	var ballonText = "<b><span style='font-size:13px'>[[category]]</span></b><br>[[title]]: <b>[[value]]</b>";

	var graphsData = [
		{
			"id": "g1",
			"fillColors": "#2ECC71",
			"lineColor": "#2ECC71",
			"color": "#ffffff",
			"labelText": "[[percents]]%",
			"title": "Passed %",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "passed_percent",
			"newStack": true,
			"showBalloon": false
            },
		{
			"id": "g2",
			"fillColors": "#E74C3C",
			"lineColor": "#E74C3C",
			"color": "#ffffff",
			"labelText": "[[percents]]%",
			"title": "Failed %",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "failed_percent",
			"newStack": false,
			"showBalloon": false
            },
		{
			"id": "g3",
			"fillColors": "#3498DB",
			"lineColor": "#3498DB",
			"color": "#ffffff",
			"labelText": "[[percents]]%",
			"title": "Other %",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "other_percent",
			"newStack": false,
			"showBalloon": false
            },
		{
			"fillColors": "#239c56",
			"lineColor": "#239c56",
			"color": "#ffffff",
			"title": "Passed",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Passed",
			"newStack": true,
			"balloonText": ballonText,
			"showBalloon": true
            },
		{
			"fillColors": "#ac3326",
			"lineColor": "#ac3326",
			"color": "#ffffff",
			"title": "Failed",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Failed",
			"newStack": false,
			"balloonText": ballonText,
			"showBalloon": true
            },
		{
			"fillColors": "#2473a6",
			"lineColor": "#2473a6",
			"color": "#ffffff",
			"title": "Other",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Other",
			"newStack": false,
			"balloonText": ballonText,
			"showBalloon": true
            }
    ];



	var graphs = [];
	var graph = {};
	var i;

	for (i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["id"] = graphsData[i]["id"];
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["showBalloon"] = graphsData[i]["showBalloon"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}

	var config = {
		"type": "serial",
		"theme": "black",
		"categoryField": "suite",
		"marginRight": 40,
		"marginLeft": 10,
		"marginBottom": 100,
		"mouseWheelZoomEnabled": true,
		"mouseWheelScrollEnabled": true,
		"legend": {
			"enabled": true,
			"useGraphSettings": true
		},
		"dataProvider": chartData,
		"valueAxes": [{
				"id": "v1",
				"axisColor": "#88D498",
				"axisThickness": 2,
				"axisAlpha": 1,
				"gridAlpha": 0,
				"title": "Percent",
				"stackType": "100%",
				"position": "left",
				"autoGridCount": false
        },
			{
				"id": "v2",
				"axisColor": "#6ca979",
				"axisThickness": 2,
				"axisAlpha": 1,
				"gridAlpha": 0,
				"stackType": "regular",
				"position": "right",
				"title": "Test Cases",
				"autoGridCount": false
        }],
		"graphs": graphs,
		"titles": [
			{
				"id": chart_id,
				"size": 15,
				"text": chartTitle
            }
        ],
		"chartScrollbar": {
			"oppositeAxis": true,
			"scrollbarHeight": 20,
			"backgroundAlpha": 0,
			"selectedBackgroundAlpha": 0.1,
			"selectedBackgroundColor": "#888888",
			"graphFillAlpha": 0,
			"graphLineAlpha": 0.5,
			"selectedGraphFillAlpha": 0,
			"selectedGraphLineAlpha": 1,
			"autoGridCount": true,
			"color": "#AAAAAA"
		},
		"categoryAxis": {
			"gridPosition": "start",
			"gridAlpha": 0,
			"autoWrap": true
		},
		"chartCursor": {
			"pan": false,
			"fullWidth": true,
			"valueLineBalloonEnabled": false,
			"cursorAlpha": .1,
			"zoomable": true
		},
		"export": {
			"enabled": true
		},
		"responsive": {
			"enabled": true,
			"rules": [
				{
					"maxWidth": 1000,
					"overrides": {
						"legend": {
							"enabled": false
						}
					}
                },
				{
					"maxWidth": 500,
					"overrides": {
						"valueAxes": {
							"inside": true
						}
					}
                }
            ]
		}
	};


	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});


	return config;
}



function parsePassPieData(chart_id, data) {
	var tempChartData = [];
	var results = _.groupBy(data, function (testCase) {
		return (testCase.Result)
	});

	var passed = 0,
		failed = 0,
		other = 0;

	if (results.Passed !== undefined)
		passed = results.Passed.length;
	if (results.Failed !== undefined)
		failed = results.Failed.length;

	for (var result in results) {
		if (result !== "Passed" && result !== "Failed")
			other += results[result].length;
	}

	tempChartData.push({
		type: "Passed",
		Count: passed,
		"color": "#2ECC71"
	});

	tempChartData.push({
		type: "Failed",
		Count: failed,
		"color": "#E74C3C"
	});

	tempChartData.push({
		type: "Other",
		Count: other,
		"color": "#3498DB"
	});

	BVATestCase_processedData[chart_id] = results;
	return tempChartData;
};

function getPassFailConfig(chart_id, chartTitle, theme, data) {
	var chartData = parsePassPieData(chart_id, data);

	var config = {
		"type": "pie",
		"theme": "black",
		"dataProvider": chartData,
		"valueField": "Count",
		"titleField": "type",
		"colorField": "color",
		"balloon": {
			"fixedPosition": true
		},
		"titles": [
			{
				"id": chart_id,
				"size": 15,
				"text": chartTitle
            }
        ],
		"export": {
			"enabled": true
		}
	};


	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});


	return config;
}


function zoomStudioChart() {
    var chartData = BVATestCase_ChartData["studioChart"];
    var max = chartData.length;
    if (max > 4)
        max = 4;
    BVATestCase_Charts["studioChart"].zoomToIndexes(0, max);
}


function parseStudioData(chart_id, data) {
	var tempChartData = [];
	var grouped = _.groupBy(data, function (testCase) {
		return (testCase.StudioName)
	});

	for (var type in grouped) {
		var results = _.groupBy(grouped[type], function (result) {
			return (result.Result)
		});
		var passed = 0,
			failed = 0,
			other = 0;

		if (results.Passed !== undefined)
			passed = results.Passed.length;
		if (results.Failed !== undefined)
			failed = results.Failed.length;

		for (var result in results) {
			if (result !== "Passed" && result !== "Failed")
				other += results[result].length;
		}

		var passedPercent = ((passed / (passed + failed + other)) * 100).toFixed(2);
		var failedPercent = ((failed / (passed + failed + other)) * 100).toFixed(2);
		var otherPercent = ((other / (passed + failed + other)) * 100).toFixed(2);

		tempChartData.push({
			studio: type,
			Passed: passed,
			Failed: failed,
			Other: other,
			passed_percent: passedPercent,
			failed_percent: failedPercent,
			other_percent: otherPercent
		});
	}

	BVATestCase_processedData[chart_id] = grouped;
	return tempChartData;
}


function getStudioConfig(chart_id, chartTitle, theme, data) {
    var chartData = parseStudioData(chart_id, data);
    BVATestCase_ChartData[chart_id] = chartData; 
	var ballonText = "<b><span style='font-size:13px'>[[category]]</span></b><br>[[title]]: <b>[[value]]</b>";

	var graphsData = [
		{
			"id": "g1",
			"fillColors": "#2ECC71",
			"lineColor": "#2ECC71",
			"color": "#ffffff",
			"labelText": "[[percents]]%",
			"title": "Passed %",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "passed_percent",
			"newStack": true,
			"showBalloon": false
            },
		{
			"id": "g2",
			"fillColors": "#E74C3C",
			"lineColor": "#E74C3C",
			"color": "#ffffff",
			"labelText": "[[percents]]%",
			"title": "Failed %",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "failed_percent",
			"newStack": false,
			"showBalloon": false
            },
		{
			"id": "g3",
			"fillColors": "#3498DB",
			"lineColor": "#3498DB",
			"color": "#ffffff",
			"labelText": "[[percents]]%",
			"title": "Other %",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "other_percent",
			"newStack": false,
			"showBalloon": false
            },
		{
			"fillColors": "#239c56",
			"lineColor": "#239c56",
			"color": "#ffffff",
			"title": "Passed",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Passed",
			"newStack": true,
			"balloonText": ballonText,
			"showBalloon": true
            },
		{
			"fillColors": "#ac3326",
			"lineColor": "#ac3326",
			"color": "#ffffff",
			"title": "Failed",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Failed",
			"newStack": false,
			"balloonText": ballonText,
			"showBalloon": true
            },
		{
			"fillColors": "#2473a6",
			"lineColor": "#2473a6",
			"color": "#ffffff",
			"title": "Other",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Other",
			"newStack": false,
			"balloonText": ballonText,
			"showBalloon": true
            }
    ];



	var graphs = [];
	var graph = {};
	var i;

	for (i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["id"] = graphsData[i]["id"];
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["showBalloon"] = graphsData[i]["showBalloon"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}

	var config = {
		"type": "serial",
		"theme": "black",
		"categoryField": "studio",
		"marginRight": 40,
		"marginLeft": 10,
		"mouseWheelZoomEnabled": true,
		"mouseWheelScrollEnabled": true,
		"legend": {
			"enabled": true,
			"useGraphSettings": true
		},
		"dataProvider": chartData,
		"valueAxes": [{
				"id": "v1",
				"axisColor": "#88D498",
				"axisThickness": 2,
				"axisAlpha": 1,
				"gridAlpha": 0,
				"title": "Percent",
				"stackType": "100%",
				"position": "left",
				"autoGridCount": false
        },
			{
				"id": "v2",
				"axisColor": "#6ca979",
				"axisThickness": 2,
				"axisAlpha": 1,
				"gridAlpha": 0,
				"stackType": "regular",
				"position": "right",
				"title": "Test Cases",
				"autoGridCount": false
        }],
		"graphs": graphs,
		"titles": [
			{
				"id": chart_id,
				"size": 15,
				"text": chartTitle
            }
        ],
		"chartScrollbar": {
			"oppositeAxis": true,
			"scrollbarHeight": 20,
			"backgroundAlpha": 0,
			"selectedBackgroundAlpha": 0.1,
			"selectedBackgroundColor": "#888888",
			"graphFillAlpha": 0,
			"graphLineAlpha": 0.5,
			"selectedGraphFillAlpha": 0,
			"selectedGraphLineAlpha": 1,
			"autoGridCount": true,
			"color": "#AAAAAA"
		},
		"chartCursor": {
			"pan": false,
			"fullWidth": true,
			"valueLineBalloonEnabled": false,
			"cursorAlpha": .1,
			"zoomable": true
		},

		"export": {
			"enabled": true
		},
		"responsive": {
			"enabled": true,
			"rules": [
				{
					"maxWidth": 1000,
					"overrides": {
						"legend": {
							"enabled": false
						}
					}
                },
				{
					"maxWidth": 500,
					"overrides": {
						"valueAxes": {
							"inside": true
						}
					}
                }
            ]
		}
	};


	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});


	return config;
}




function adjustTimelineBallonText(graphDataItem, graph) {

	var data = BVATestCase_processedData["timelineChart"];
	var date = graphDataItem.dataContext.dateStr;
	var section = (graph.legendTextReal).replace(" %", "");

	var results = _.groupBy(data[date], function (testCase) {
		return (testCase.Result)
	});

	var phaseData = [];
	if (section !== "Other")
		phaseData = results[section];
	else {
		for (var type in results) {
			if (type !== "Passed" && type !== "Failed")
				phaseData = phaseData.concat(results[type]);
		}
	}

	var text = "";
	if (phaseData !== undefined && phaseData !== null) {
		if (section === "Other")
			text = "<b style='font-size:14px;'>" + section + "</b><br>" + date + ": <b style='font-size:13px;'>" + phaseData.length + "</b>";
		else
			text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + date + ": <b style='font-size:13px;'>" + phaseData.length + "</b>";

		var suites = {};
		for (var i = 0; i < phaseData.length; i++) {
			//text += "<br>";
			//text += phaseData[i].SuiteName + ": <b style='font-size:13px;'>" + phaseData[i].StudioName + "</b>";
			if (suites[phaseData[i].SuiteName] !== undefined) {
				suites[phaseData[i].SuiteName] += 1;
			} else
				suites[phaseData[i].SuiteName] = 1;
		}

		for (var type in suites) {
			text += "<br>";
			text += type + ": <b style='font-size:13px;'>" + suites[type] + "</b>";
		}
	} else {
		if (section === "Other") {
			text = "<b style='font-size:14px;'>" + section + "</b><br>" + date + ": <b style='font-size:13px;'>" + 0 + "</b>";
		} else
			text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + date + ": <b style='font-size:13px;'>" + 0 + "</b>";
	}

	return text;
}

function zoomTimeLineChart() {
	var date = new Date();
	date.setDate(date.getDate() + 1);

	var prevDate = new Date();
	prevDate.setMonth(prevDate.getMonth() - 1);

	var firstDate = new Date(bvaSC_data.data[0].StartTime);
	firstDate.setDate(firstDate.getDate() - 1);
	if (firstDate > prevDate)
		prevDate = firstDate;

	BVATestCase_Charts["timelineChart"].zoomToDates(prevDate, date);
}



function processBVATestCaseTimelineData(chart_id, data) {
	var tempChartData = [];

	var grouped = _.groupBy(data, function (testCase) {
		return (testCase.EndTime).substring(0, 10);
	});

	for (var date in grouped) {
		var dateVar = new Date(date);
		var today = new Date();
		today.setDate(today.getDate() + 1);

		if (dateVar <= today) {
			var newdate = new Date(date.substring(5, 7) + "/" + date.substring(8, 10) + "/" + date.substring(0, 4));
			var results = _.groupBy(grouped[date], function (testCase) {
			    return (testCase.Result);
			});
			var passed = 0,
				failed = 0,
				other = 0;

			if (results.Passed !== undefined)
				passed = results.Passed.length;
			if (results.Failed !== undefined)
				failed = results.Failed.length;
			for (var result in results) {
				if (result !== "Passed" && result !== "Failed")
					other += results[result].length;
			}

			var passedPercent = ((passed / (passed + failed + other)) * 100).toFixed(2);
			var failedPercent = ((failed / (passed + failed + other)) * 100).toFixed(2);
			var otherPercent = ((other / (passed + failed + other)) * 100).toFixed(2);

			tempChartData.push({
				date: newdate,
				dateStr: date,
				passed: passed,
				failed: failed,
				other: other,
				passed_percent: passedPercent,
				failed_percent: failedPercent,
				other_percent: otherPercent
			});
		}
	}

	BVATestCase_processedData[chart_id] = grouped;
	return tempChartData;
}


function getBVATestCaseTimelineConfig(chart_id, chartTitle, theme, data) {
	var chartData = processBVATestCaseTimelineData(chart_id, data);

	var graphsData = [
		{
		    "id": "g1",
		    "fillColors": "#2ECC71",
		    "lineColor": "#2ECC71",
		    "color": "#ffffff",
		    "title": "Passed %",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "passed_percent",
		    "newStack": true,
		    "showBalloon": false
		},
        {
            "id": "g2",
            "fillColors": "#E74C3C",
            "lineColor": "#E74C3C",
            "color": "#ffffff",
            "title": "Failed %",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "failed_percent",
            "newStack": false,
            "showBalloon": false
        },
        {
            "id": "g3",
            "fillColors": "#3498DB",
            "lineColor": "#3498DB",
            "color": "#ffffff",
            "title": "Other %",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "other_percent",
            "newStack": false,
            "showBalloon": false
        },
        {
            "id": "g4",
            "fillColors": "#239c56",
            "lineColor": "#239c56",
            "color": "#ffffff",
            "title": "Passed",
            //"labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "passed",
            "newStack": true,
            "showBalloon": true
        },
        {
            "id": "g5",
            "fillColors": "#ac3326",
            "lineColor": "#ac3326",
            "color": "#ffffff",
            "title": "Failed",
            //"labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "failed",
            "newStack": false,
            "showBalloon": true
        },
        {
            "id": "g6",
            "fillColors": "#2473a6",
            "lineColor": "#2473a6",
            "color": "#ffffff",
            "title": "Other",
            //"labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "other",
            "newStack": false,
            "showBalloon": true
        }];

    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["lineColor"] = graphsData[i]["lineColor"];
        graph["color"] = graphsData[i]["color"];
        graph["title"] = graphsData[i]["title"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["showBalloon"]= graphsData[i]["showBalloon"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);
    }

    var chartConfig = {
        "type": "serial",
        "theme": "black",
        "marginRight": 0,
        "marginLeft": 0,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisColor": "#88D498",
            "axisThickness": 2,
            "axisAlpha": 1,
            "gridAlpha": 0,
            "title": "Percent",
            "stackType": "100%",
            "position": "left",
            "autoGridCount": false
        },
			{
				"id": "v2",
				"axisColor": "#6ca979",
				"axisThickness": 2,
				"axisAlpha": 1,
				"gridAlpha": 0,
				"stackType": "regular",
				"position": "right",
				"title": "Test Cases",
				"autoGridCount": false
		}],
        "graphs": graphs,
        "chartScrollbar": {
            "oppositeAxis": true,
            "scrollbarHeight": 20,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": false,
            "fullWidth": true,
            "valueLineBalloonEnabled": false,
            "cursorAlpha": .1,
            "zoomable": true
        },
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
		"categoryField": "date",
		"categoryAxis": {
			"parseDates": true,
			"dashLength": 1,
			"minorGridEnabled": true
		},
		"legend": {
		    "enabled": true,
			"useGraphSettings": true
		},
		"export": {
			"enabled": true
		},
		"responsive": {
		    "enabled": true,
		    "rules": [
			    {
			        "maxWidth": 1000,
			        "overrides": {
			            "legend": {
			                "enabled": false
			            }
			        }
			    },
                {
                    "maxWidth": 500,
                    "overrides": {
                        "valueAxes": {
                            "inside": true
                        }
                    }
                }
		    ]
		},
		"dataProvider": chartData
	};



	chartConfig.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	chartConfig.export["backgroundColor"] = "#222222";
	chartConfig.export.drawing.enabled = true;
	chartConfig.export.drawing.color = "#FFFFFF";

	chartConfig.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});



	return chartConfig;
}




function BVAaddTestCaseSCDiv(prefix, div_id) {

	var parent_id = "#metric_" + prefix;

	var $div = $("<div/>", {
			id: div_id
		})
		.css("background-color", bgColor)
		.css("z-index", 90) // more then L2/L3 but less then loading div
		.css("width", "100%")
		.css("color", "#FFFFFF")
		.css("height", "100%")
		.css("top", "0")
		.css("left", "0");
	$(parent_id).prepend($div);

	$("#helpicon").unbind('click');
	$("#helpicon").click(function () {
		var URL = "HelpPage/helpPage.html#bvaTestCase";
		window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
	});

	return $div;
}