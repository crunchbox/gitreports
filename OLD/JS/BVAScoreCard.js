var bvaBuild = "";
var parent_prefix = "";
var bvaSC_jsonData = {};
var BVASC_dataTables = {};
var BVASC_processedData = {};
var BVASC_chartConfigDict = {};


var bvaSC_data = {
    gi_name: "", 
    ai_name: "",
    suite_count: "",
    suite_sudio: ""    
}





function loadBVAScoreCardData(prefix, build, parenturl) {
    bvaBuild = build;
    parent_prefix = prefix;

    if (parent_prefix === "kitPage") {
        parenturl = "/";
        $("#gobacklink_a").attr("href", parenturl);
    }
    if (parenturl == undefined)
        parenturl = "#/metric/studio/all";

    $("#gobacklink_a").attr("title", parenturl);

    get_bvaScoreCardData(); // get Data
}


function removeBVAScoreCard() {
    $.when($("#bva_scorecard").remove()).then(setTimeout(resetCurrentHash, 100));
    if (parent_prefix.endsWith("_L2") || parent_prefix.endsWith("_L3")) {
        $("#" + parent_prefix + "_chartdiv").show();
        var tempStr = parent_prefix;
        tempStr = parent_prefix.substr(0, parent_prefix.length - 3);
        $("#" + tempStr + "_dataTable_group").show();
        $("#" + tempStr + "_dataTable_link").show();
        $("#link_" + tempStr + "_4").remove();
        level--;
        $("#link_" + tempStr + "_" + level).addClass('active');
    } else displayTopLevelMetric(parent_prefix);
}


function get_bvaScoreCardData() {
    if (bvaSC_jsonData.hasOwnProperty(bvaBuild) == false) {
        var client = new $.RestClient('/rest/');
        client.add("bvaTestCasesByBuildFile");
        var restRequest = client.bvaTestCasesByBuildFile.create({},
        {
            buildFile: bvaBuild
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_BVAScoreCardData_callBack);

    } else get_BVAScoreCardData_callBack(bvaSC_jsonData[bvaBuild]);
}


function get_BVAScoreCardData_callBack(r) {
    if (bvaSC_jsonData.hasOwnProperty(bvaBuild) == false) {
        bvaSC_jsonData[bvaBuild] = r;
    }
    var currentData = r["BVATestCases_" + bvaBuild];

    BVASC_dataTables["buildData"] = currentData;

    configureBVASPCharts();

    return currentData;
}


function configureBVASPCharts() {
    var buildData = BVASC_dataTables["buildData"];
    if (buildData.length<0) {
        alert("No BVA Build Found!");
        removeBVAScoreCard();
        return;
    }

    removeLoadingDiv();

    if (buildData[0].hasOwnProperty("GIBuildFile")) {
        bvaSC_data.gi_name = (buildData[0])["GIBuildFile"];
    }
    if (buildData[0].hasOwnProperty("AIBuildFile")) {
        bvaSC_data.ai_name = (buildData[0])["AIBuildFile"];
    }
    if (buildData[0].hasOwnProperty("Studio")) {
        bvaSC_data.suite_sudio = (buildData[0])["Studio"];
    }
   
    bvaSC_data.suite_count = buildData.length;
    bvaSC_data.data = buildData; 

    $("#bvaData").ready(function () {
        if ($(window).width() < 750) {
            $(".tableLabel").css('text-align', 'left');
        } else
            $(".tableLabel").css('text-align', 'right');

        $("#gi_td").text(bvaSC_data.gi_name);
        $("#ai_td").text(bvaSC_data.ai_name);
        $("#scount_td").text(bvaSC_data.suite_count);
        $("#studio_td").text(bvaSC_data.suite_sudio);
    });


    var config = getBVASuiteTimelineConfig("suiteTimeline", "Suite Timeline", "black", bvaSC_data.data);

    BVASC_chartConfigDict["suiteTimelineChart"] = config;

    var chart = AmCharts.makeChart("suiteTimeline_chartdiv", config, 100);
    chart.handleResize();


    config = getBVARuntimeConfig("runtimePie", "Suite Runtimes", "black", bvaSC_data.data);
    BVASC_chartConfigDict["runtimePieChart"] = config;

    chart = AmCharts.makeChart("runtimePie_chartdiv", config, 100);

    removeLoadingDiv();
}

function processBVARuntimeData(data) {
    var tempChart = [];
    for (var i = 0; i < data.length; i++) {
        var row = {};
        row["suite"] = data[i].BuildFile;
        row["time"] = data[i].SuiteRunTime;
        tempChart.push(row);
    }

    return tempChart; 
}


function getBVARuntimeConfig(chart_id, chartTitle, theme, data) {
    var chartData = processBVARuntimeData(data);

    var chartConfig = {
        "type": "pie",
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "theme": theme,
        "pullOutOnlyOne": true,
        "dataProvider": chartData,
        "valueField": "time",
        "titleField": "suite",
        "hideLabelsPercent": 5,
        "outlineThickness": 0.5,
        "balloon": {
            "fixedPosition": true
        },
        "export": {
            "enabled": true
        }
    };

    return chartConfig;
}



function convertDate(dateItem) {
    var hours = dateItem.getHours(), mins = dateItem.getMinutes(), sec = dateItem.getSeconds();
    var period = "AM";

    if (hours > 12) {
        hours = hours - 12;
        period = "PM";
    }

    if (hours === 12)
        period = "PM";

    if (hours === 0)
        hours = 12;


    if (mins < 10)
        mins = "0" + mins;
    if (sec < 10)
        sec = "0" + sec;

    var dateStr = hours + ":" + mins + ":" + sec + " " + period;

    return dateStr; 
}


function processBVASuiteTimelineData(chart_id,data) {
    var tempChartData = [];

    var grouped = _.groupBy(data, function (suite) { return (suite.SuiteStartTime).substring(0, 10); });
    /*
    for (var i = 0; i < data.length; i++) {
        var newdate = new Date(data[i].SuiteStartTime);

        var passed = 0, failed = 0, other=0;
        if (data[i].PassCount !== undefined)
            passed = data[i].PassCount;
        if (data[i].FailedCount !== undefined)
            failed = data[i].FailedCount;
        if (data[i].OtherCount !== undefined)
            other = data[i].OtherCount;

        var passedPercent = ((passed / (passed + failed + other)) * 100).toFixed(2);
        var failedPercent = ((failed / (passed + failed + other)) * 100).toFixed(2);
        var otherPercent = ((other / (passed + failed + other)) * 100).toFixed(2);

        tempChartData.push({
            date: newdate,
            dateStr: data[i].BuildFile+"\n"+(newdate.toString()).substring(0, 15) + " " + convertDate(newdate),
            passed: passed,
            failed: failed,
            other:other, 
            passed_percent: passedPercent,
            failed_percent: failedPercent,
            other_percent:otherPercent
        });
    }
    */

    for (var date in grouped) {
        var item= grouped[date];
        var newdate = new Date(date.substring(5, 7) + "/" + date.substring(8, 10)+"/"+date.substring(0,4));

        var passed = 0, failed = 0, other = 0;
        for (var i = 0; i < item.length; i++) {
            if (item[i].PassCount !== undefined)
                passed += item[i].PassCount;
            if (item[i].FailedCount !== undefined)
                failed += item[i].FailedCount;
            if (item[i].OtherCount !== undefined)
                other += item[i].OtherCount;
        }
        
        var passedPercent = ((passed / (passed + failed + other)) * 100).toFixed(2);
        var failedPercent = ((failed / (passed + failed + other)) * 100).toFixed(2);
        var otherPercent = ((other / (passed + failed + other)) * 100).toFixed(2);

        tempChartData.push({
            date: newdate,
            dateStr: (newdate.toString()).substring(0, 15),
            passed: passed,
            failed: failed,
            other: other,
            passed_percent: passedPercent,
            failed_percent: failedPercent,
            other_percent: otherPercent
        });
    }

    BVASC_processedData[chart_id] = grouped;
    return tempChartData;
}


function getBVASuiteTimelineConfig(chart_id, chartTitle, theme, data) {
    var chartData = processBVASuiteTimelineData(chart_id,data);
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";

    var graphsData = [
    {
        "id": "g1",
        "fillColors": "#2ECC71",
        "lineColor": "#2ECC71",
        "color": "#ffffff",
        "title": "Passed %",
        "labelText": "[[percents]]%",
        "columnWidth": 1,
        "valueAxis": "v1",
        "valueField": "passed_percent",
        "newStack": true,
        "balloonText": ballonText,
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "fontSize": 10,
        "showAllValueLabels": true
    }, {
        "id": "g2",
        "fillColors": "#E74C3C",
        "lineColor": "#E74C3C",
        "color": "#ffffff",
        "title": "Failed %",
        "labelText": "[[percents]]%",
        "columnWidth": 1,
        "valueAxis": "v1",
        "valueField": "failed_percent",
        "newStack": false,
        "balloonText": ballonText,
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "fontSize": 10,
        "showAllValueLabels": true
    }, {
        "id": "g3",
        "fillColors": "#3498DB",
        "lineColor": "#3498DB",
        "color": "#ffffff",
        "title": "Other %",
        "labelText": "[[percents]]%",
        "columnWidth": 1,
        "valueAxis": "v1",
        "valueField": "other_percent",
        "newStack": false,
        "balloonText": ballonText,
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "fontSize": 10,
        "showAllValueLabels": true
    },
    {
        "fillColors": "#27AE60",
        "lineColor": "#27AE60",
        "color": "#ffffff",
        "title": "Passed",
        "labelText": "[[value]]",
        "columnWidth": 0.85,
        "valueAxis": "v2",
        "valueField": "passed",
        "newStack": true,
        "balloonText": ballonText,
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "fontSize": 10,
        "showAllValueLabels": true
    },
    {
        "fillColors": "#C0392B",
        "lineColor": "#C0392B",
        "color": "#ffffff",
        "title": "Failed",
        "labelText": "[[value]]",
        "columnWidth": 0.85,
        "valueAxis": "v2",
        "valueField": "failed",
        "newStack": false,
        "balloonText": ballonText,
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "fontSize": 10,
        "showAllValueLabels": true
    },
    {
        "fillColors": "#2980B9",
        "lineColor": "#2980B9",
        "color": "#ffffff",
        "title": "Other",
        "labelText": "[[value]]",
        "columnWidth": 0.85,
        "valueAxis": "v2",
        "valueField": "other",
        "newStack": false,
        "balloonText": ballonText,
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "fontSize": 10,
        "showAllValueLabels": true
    }
    ];
    for (var i = 0; i < graphsData.length; i++) {

        // do now show values if they are less than 4
        graphsData[i]["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }
    var chartConfig = AmCharts.makeChart("chartdiv", {
        "type": "serial",
        "theme": "black",
        "marginRight": 40,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled": true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisColor": "#88D498",
            "axisThickness": 2,
            "axisAlpha": 1,
            "title": "Percent",
            "stackType": "100%",
            "position": "left",
            "autoGridCount": false,
            "gridAlpha": 0
        },
		{
		    "id": "v2",
		    "axisColor": "#6ca979",
		    "axisThickness": 2,
		    "axisAlpha": 1,
		    "gridAlpha": 0,
		    "stackType": "regular",
		    "position": "right",
		    "title": "Test Cases",
		    "autoGridCount": false
		}],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": graphsData,
        "chartScrollbar": {
            "oppositeAxis": false,
            "offset": 30,
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 1,
            "cursorColor": "#258cbb",
            "limitToGraph": "g1",
            "valueLineAlpha": 0.2,
            "valueZoomable": true
        },
        "valueScrollbar": {
            "oppositeAxis": false,
            "offset": 50,
            "scrollbarHeight": 10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        },
        "dataProvider": chartData
    });



    chartConfig.graphs = graphsData;


    return chartConfig; 
}


function BVAaddScoreCardDiv(prefix, div_id) {

    var parent_id = "#metric_" + prefix;

    var $div = $("<div/>", {
        id: div_id
    })
		.css("background-color", bgColor)
		.css("z-index", 90) // more then L2/L3 but less then loading div
		.css("width", "100%")
		.css("color", "#FFFFFF")
		.css("height", "100%")
		.css("top", "0")
		.css("left", "0");
    $(parent_id).prepend($div);

    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#bvaScorecard";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    }
    );

    return $div;
}



