var aCloudToolName = "ScheduleBase Metric Dashboard";
var aCloudVersion = "4.6";
var analytics;

// set to false to enable live data
var runOffline = false;

var chartDict = {};
var chartConfigDict = {};
var chartDataDict = {};
var chartJSONTableDict = {};
var chartTypeDict = {};
var analyticsDict = {};
var dataTableIsVisible = {};
var dataTableDataDict = {};
var dataTableOptionsDict = {};
var dataTableHTMLTableDict = {};
var themeLogoDict = {};
var defectsFoundStudioMonth = {};
var restJsonDict = {};
var loadedMetrics = {};
var quarterList = [];
var chartTabs;
var chart;
var chartData;
var chartGraphs;
var level = 1;
var bgColor = "#222222";
var thisMetricName = ""; 

var currentTheme = "black";
var currentMonth = "all";
var currentStudio = "";
var initState = true;
var currentPrefix; // the inital tab open
var initMetric = "kitsReleased"; // initial metric to show on page load
var hasRouting = false;

var hasInitSubCat = false; // if there is a sub cat
var hasInitSubCatMonth = false; // if there is a sub cat, does it have a specific month
var initMonth = "all";
var initSubCategory = ""; // inital level 2 metric to show on page load
var chartResizerDict = {};
var showingTotal = false;
var allGraphsVisible = true;
var extraGraphData = {};
var username_and_domain = "";
var selectedStudio = "All";
var metricCacheBackupCallBack = {};
var showDarkTheme = false;
var chartColors = [];
var scrollbarColor = "#6F7D8A";
var scrollbarLineColor = "#ffffff";


if (!String.prototype.includes) {
    String.prototype.includes = function (search, start) {
        'use strict';
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If IsCallable(predicate) is false, throw a TypeError exception.
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }

            // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
            var thisArg = arguments[1];

            // 5. Let k be 0.
            var k = 0;

            // 6. Repeat, while k < len
            while (k < len) {
                // a. Let Pk be ! ToString(k).
                // b. Let kValue be ? Get(O, Pk).
                // c. Let testResult be ToBoolean(? Call(predicate, T, � kValue, k, O �)).
                // d. If testResult is true, return kValue.
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                // e. Increase k by 1.
                k++;
            }

            // 7. Return undefined.
            return undefined;
        }
    });
}

function styleSwitchToggled() {
    showDarkTheme = !showDarkTheme; 
    updateThePageStyle(); 
}

function updateThePageStyle() {

    if (!showDarkTheme) {
        $('#topbar_logo').attr('src', 'images/logo-igt-white-text.png');

        $('link[href="CSS/dark.css"]').attr('disabled', 'disabled');
        $('link[href="CSS/light.css"]').removeAttr('disabled');

        $('link[href="CSS/dashboardDark.css"]').attr('disabled', 'disabled');
        $('link[href="CSS/dashboardLight.css"]').removeAttr('disabled');

        $('link[href="CSS/layoutDark.css"]').attr('disabled', 'disabled');
        $('link[href="CSS/layoutLight.css"]').removeAttr('disabled');

        $('link[href="DataTable/darkDataTableCustom.css"]').attr('disabled', 'disabled');
        $('link[href="DataTable/lightDataTableCustom.css"]').removeAttr('disabled');
        currentTheme = "light";
        dashboardTheme = "light";
        dashboardMainColor = "#5b5b5b";
        chartColors = ["#de4c4f", "#0C51A1", "#eea638", "#1B9DDB", "#86a965", "#8aabb0", "#d8854f", 
                  "#9d9888", "#A682FF", "#724887", "#7256bc", // end defaults 
                  "#FF0000", "#008000", "#DC493A", "#F6828C", "#3BB273", "#EF8354", "#0000FF", "#CD853F",
                  "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#755C1B", "#F7A278", "#D2B48C",
			      "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#5FB49C", "#00BFFF", "#7768AE",
			      "#8FBC8B", "#00FF7F", "#80475E", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
			      "#7F0799", "#66CDAA"];
        bgColor = "#ffffff";
        scrollbarColor = "#e8e8e8";
        scrollbarLineColor = "#333333";
    } else {
        $('#topbar_logo').attr('src', 'images/logo-igt-blue-text.png');

        $('link[href="CSS/light.css"]').attr('disabled', 'disabled');
        $('link[href="CSS/dark.css"]').removeAttr('disabled');

        $('link[href="CSS/dashboardLight.css"]').attr('disabled', 'disabled');
        $('link[href="CSS/dashboardDark.css"]').removeAttr('disabled');

        $('link[href="CSS/layoutLight.css"]').attr('disabled', 'disabled');
        $('link[href="CSS/layoutDark.css"]').removeAttr('disabled');

        $('link[href="DataTable/lightDataTableCustom.css"]').attr('disabled', 'disabled');
        $('link[href="DataTable/darkDataTableCustom.css"]').removeAttr('disabled');

        currentTheme = "black";
        dashboardTheme = "black";
        dashboardMainColor = "#DDDDDD";
        chartColors = [
            "#de4c4f", "#eea638", "#69c8ff", "#a7a737", "#86a965", "#8aabb0", "#d8854f", "#cfd27e",
            "#9d9888", "#A682FF", "#724887", "#7256bc", // end defaults 
            "#FF0000", "#008000", "#FFC0CB", "#FFA500", "#FFF0F5", "#FFFF00", "#0000FF", "#CD853F",
            "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#FFFFE0", "#ADD8E6", "#D2B48C",
            "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#EEE8AA", "#00BFFF", "#FFDEAD",
            "#8FBC8B", "#00FF7F", "#FFF0F5", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
            "#40E0D0", "#66CDAA"
        ];
        bgColor = "#222222";
        scrollbarColor = "#6F7D8A";
        scrollbarLineColor = "#ffffff";
    }

    updateDashboardConfig();

    localStorage.setItem("dashboardTheme", showDarkTheme);
}

// on Page Load
$(document).ready(function () {

    if (localStorage.getItem("dashboardTheme") !== null) {
        if (localStorage.getItem("dashboardTheme") === "false") {
            showDarkTheme = false;            
        }
        else {
            showDarkTheme = true;            
        }
    }

    updateThePageStyle(); 

    //jira Submit Bug button
    jQuery.ajax({
        url: "http://cspjira:8080/jira/s/en_CAk79gcp-418945332/849/76/1.2.9/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?collectorId=38c0175c",
        type: "get",
        cache: true,
        dataType: "script"
    });

    get_StudioList();

    /* NEW METRIC STEP: Add prefix here*/
    // analytics names for prefixes
    analyticsDict["kitsReleased"] = "Kits Released Out Of PA";
    analyticsDict["bugFixOther"] = "Bug Fix / Other";
    analyticsDict["newThemeOther"] = "New Theme / Other";
    analyticsDict["defectsForKitsReleased"] = "Defects For Kits Released";
    analyticsDict["edas"] = "EDAs by Kit Relase Date";
    analyticsDict["edasEntered"] = "EDAs by Date Entered";
    analyticsDict["newThemeFirstPass"] = "First Pass New Theme";
    analyticsDict["NewThemeAvgTestTime"] = "New Theme Test Time";
    analyticsDict["NewThemeAvgTTGameType"] = "New Theme Test Time Type";
    analyticsDict["defectsPhaseFound"] = "Defects Closed by Phase Found";
    analyticsDict["avgDefects"] = "Average Defects for New Themes";
    analyticsDict["defectsClosedbyTesterType"] = "Defects Closed Tester Type";
    analyticsDict["bvaBuildsRun"] = "BVA Runs";
    analyticsDict["bvaSuitesRun"] = "BVA Suites";
    analyticsDict["BVATestCasesPassFail"] = "BVA Test Cases";
    analyticsDict["UTPTestCasesPassFail"] = "UTP Test Cases";
    analyticsDict["GameTracker"] = "Game Tracker";
    analyticsDict["mathThemesReleased"] = "Math Themes Released";
    analyticsDict["AllSitesFirstPass"] = "First Pass New Theme - All Sites";
    analyticsDict["QuartersPhaseFound"] = "Two Year Defects Closed";
    analyticsDict["QuartersFirstPass"] = "Two Year First Pass";
    analyticsDict["AllSitesNewOther"] = "SharePoint: New Theme / Other";
    analyticsDict["kitreleaselist"] = "Kit Release Grid";
    analyticsDict["kittestlist"] = "Kit Test Grid";
    analyticsDict["KitCountForTools"] = "Kit Count for Tools";
    analyticsDict["EQEScoreByPushDate"] = "EQE Score by Push Date";
    analyticsDict["cn_in"] = "CN/IN by Kit Release Date";
    analyticsDict["cn_inEntered"] = "CN/IN by Date Entered";
    analyticsDict["currentMathStates"] = "Current Math States";
    analyticsDict["mathDefectsFound"] = "Math Defects Found";
    analyticsDict["mathEdas"] = "Math EDAs";
    analyticsDict["edaNewOnly"] = "EDA/CN/IN for New Themes Only";
    analyticsDict["ContainmentRate"] = "Defect Containment Rate";

    /* NEW METRIC STEP: Add prefix here*/
    // prefix and JSON table names
    chartJSONTableDict["kitsReleased"] = "PAReleases_Month";
    chartJSONTableDict["bugFixOther"] = "PAReleases_Month";
    chartJSONTableDict["newThemeOther"] = "PAReleases_Month";
    chartJSONTableDict["defectsForKitsReleased"] = "DefectsClosedPerKitWBS_Month";
    chartJSONTableDict["edas"] = "EDACountPerKit_Month";
    chartJSONTableDict["edasEntered"] = "EDAEntered_Month";
    chartJSONTableDict["newThemeFirstPass"] = "PAReleases_Month";
    chartJSONTableDict["NewThemeAvgTestTime"] = "NewThemeTestTime_Month";
    chartJSONTableDict["NewThemeAvgTTGameType"] = "NewThemeTestTime_Month";
    chartJSONTableDict["defectsPhaseFound"] = "DefectClosedNewThemes_Month";
    chartJSONTableDict["avgDefects"] = "AvgDefectsPerStudioPerMonth";
    chartJSONTableDict["defectsClosedbyTesterType"] = "DefectsClosedTesterType_Month";
    chartJSONTableDict["bvaBuildsRun"] = "bvaTotal_Month";
    chartJSONTableDict["bvaSuitesRun"] = "bvaTotal_Month";
    chartJSONTableDict["BVATestCasesPassFail"] = "bvaTotal_Month";
    chartJSONTableDict["UTPTestCasesPassFail"] = "UTPCases_Month";
    chartJSONTableDict["GameTracker"] = "gameTracker_Month";
    chartJSONTableDict["mathThemesReleased"] = "MathThemeseReleasedMetric_Month";
    chartJSONTableDict["AllSitesFirstPass"] = "PAReleases_Month";
    chartJSONTableDict["QuartersPhaseFound"] = "DefectsClosed_Quarter";
    chartJSONTableDict["QuartersFirstPass"] = "FirstPass_Quarter";
    chartJSONTableDict["AllSitesNewOther"] = "newThemeOtherSharePnt_Month";
    chartJSONTableDict["kitreleaselist"] = "KitReleasedAll";
    chartJSONTableDict["kittestlist"] = "KitTestAll";
    chartJSONTableDict["KitCountForTools"] = "KitCountForTools_Month";
    chartJSONTableDict["EQEScoreByPushDate"] = "EQEScoreByPushDate";
    chartJSONTableDict["cn_in"] = "cn_in_Month";
    chartJSONTableDict["cn_inEntered"] = "cn_inEntered_Month";
    chartJSONTableDict["currentMathStates"] = "CurrentMathStates";
    chartJSONTableDict["mathDefectsFound"] = "MathDefectsFound_Month";
    chartJSONTableDict["mathEdas"] = "MathEdas_Month";
    chartJSONTableDict["edaNewOnly"] = "EdaNewOnly_Month";
    /*
	chartJSONTableDict["AllSitesFirstPass"]= "PAReleases_Month";
	chartJSONTableDict["QuartersPhaseFound"]= "DefectsClosed_Quarter";
	chartJSONTableDict["QuartersFirstPass"] = "FirstPass_Quarter";
	chartJSONTableDict["AllSitesNewOther"] = "newThemeOtherSharePnt_Month";
	chartJSONTableDict["kitreleaselist"] = "KitReleasedAll";
	chartJSONTableDict["kittestlist"] = "KitTestAll";*/

    // prefix and Chart Types
    /* NEW METRIC STEP: Add prefix here*/
    chartTypeDict["kitsReleased"] = "kitdefects";
    chartTypeDict["bugFixOther"] = "kitdefects";
    chartTypeDict["newThemeOther"] = "kitdefects";
    chartTypeDict["defectsForKitsReleased"] = "defects";
    chartTypeDict["edas"] = "edas";
    chartTypeDict["edasEntered"] = "edas";
    chartTypeDict["edaNewOnly"] = "edaNewOnly";
    chartTypeDict["newThemeFirstPass"] = "kitfp";
    chartTypeDict["NewThemeAvgTestTime"] = "testTime";
    chartTypeDict["NewThemeAvgTTGameType"] = "testTime";
    chartTypeDict["defectsPhaseFound"] = "defectsClosedNT";
    chartTypeDict["avgDefects"] = "avgDefects";
    chartTypeDict["defectsClosedbyTesterType"] = "defectsClosedTT";
    chartTypeDict["ContainmentRate"] = "dce";
    chartTypeDict["bvaBuildsRun"] = "bva";
    chartTypeDict["bvaSuitesRun"] = "bva";
    chartTypeDict["BVATestCasesPassFail"] = "bva";
    chartTypeDict["UTPTestCasesPassFail"] = "utp";
    chartTypeDict["AllSitesFirstPass"] = "kitfp_sp";
    chartTypeDict["QuartersPhaseFound"] = "defectsClosedQ";
    chartTypeDict["QuartersFirstPass"] = "kitfpQ";
    chartTypeDict["AllSitesNewOther"] = "kitdefects_sp";
    chartTypeDict["kitreleaselist"] = "kitreleaselist";
    chartTypeDict["kittestlist"] = "kittestlist";
    chartTypeDict["KitCountForTools"] = "kitCountTools";
    chartTypeDict["EQEScoreByPushDate"] = "EQEScore";
    chartTypeDict["cn_in"] = "cn_in";
    chartTypeDict["cn_inEntered"] = "cn_inEntered";
    chartTypeDict["mathThemesReleased"] = "mathThemesReleased";
    chartTypeDict["currentMathStates"] = "currentMathStates";
    chartTypeDict["mathDefectsFound"] = "mathDefectsFound";
    chartTypeDict["mathEdas"] = "mathEdas";

    crossroads.addRoute('kit/{kitNumber}', function (kitNumber) {
        JumpToKit(kitNumber);
    });

    crossroads.addRoute('bvaBuild/{bvaBuild}', function (bvaBuild) {
        JumpToBVABuild(bvaBuild);
    });

    crossroads.addRoute('testCase/{testCase}', function (testCase) {
        JumpToBVATestCase(testCase);
    });

    crossroads.addRoute('metric/{metrictype}/{month}/:subcat:', function (metrictype, month, subcat) {
        hasRouting = true;
        initMonth = month;
        initSubCategory = subcat;
        if (subcat)
            hasInitSubCat = true;
        initMetric = metrictype;
        loadMetricView(initMetric);
    });

    crossroads.addRoute('studio/{studioName}', function (studioName) {
        hasRouting = true;
        selectedStudio = studioName.replaceAll("_", " ");

    });

    function parseHash(newHash, oldHash) {
        crossroads.parse(newHash);
    }

    hasher.initialized.add(parseHash); //parse initial hash
    hasher.changed.add(parseHash); //parse hash changes
    hasher.init(); //start listening for history change

    $('#kitLookUp_input').watermark('Kit Number');

    $("#kitLookUp_input").keyup(function (event) {
        if (event.keyCode == 13) {
            var targetKit = $(this).val();

            if (targetKit.search("BVA_") !== -1)
                JumpToBVATestCase(targetKit.substring(4, targetKit.length));
            else
                JumpToKit(targetKit);
        }
    });

    if (runOffline == undefined || runOffline == false) {
        // only store session analytics if running live
        StartSession();
    }

    //show_loadingDiv("Starting Up...");
    /*
	if (!hasRouting)
	    get_MetricsData(chartTypeDict[initMetric], initMetric); // get first tab
    */

    $('.slick-slider').on('click', '.slick-slide', function (e) {
        var blah = e;
    });

    // Start Metrics New Home Dashboard 
    $('#dashboardContainer').load("dashboard.html");
    // Call dashboard.js function to start dashboard function
    loadDashboard(selectedStudio);
    // End Metrics New Home Dashboard

    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#kitsReleased";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });


    var today = new Date();
    var later = new Date("6/11/2017");

    if (window.location.host.indexOf(":81") !== -1 && today<=later) {
        $("#wrapper").addClass('hide');
    } else {
        $("#siteUnavailable").css('width', '0');
        $("#siteUnavailable").css('height', '0');
        $("#siteUnavailable").addClass('hide');
    }
});

$(window).resize(function () {
    var enable = true;
    if ($('#navsidebar').css("display") == "none")
        enable = false; // if sidebar is hidden then show it

    if (enable) {
        if ($(this).width() < 1000) {
            $('.navbar-collapse').collapse('hide');
            $('#page-wrapper').css("margin-left", "0");
            $('#page-wrapper').css("width", "100%");
        } else {
            $('.navbar-collapse').collapse('show');
            $('#page-wrapper').css("margin-left", "225px");
            $('#page-wrapper').css("width", "88%");
        }
    } else {

        $('#page-wrapper').css("margin-left", "0");
        $('#page-wrapper').css("width", "100%");

    }

});

function JumpToKit(targetKit) {
    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#kitPage";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });
    initState = false;
    level = 4;

    if ($("#dashboardContainer").is(":visible"))
        currentPrefix = "kitPage";

    showAndLoadScoreCard(currentPrefix, targetKit, -1);
}

function JumpToBVABuild(bvaBuild) {
    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#bvaScorecard";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });
    initState = false;
    level = 4;

    if ($("#dashboardContainer").is(":visible"))
        currentPrefix = "kitPage";
    showBVAScoreCard(currentPrefix, bvaBuild, -1);
}

function JumpToBVATestCase(testCase) {
    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#bvaTestCase";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });
    initState = false;
    level = 4;

    if ($("#dashboardContainer").is(":visible"))
        currentPrefix = "kitPage";
    showBVATestCaseSC(currentPrefix, testCase, -1);
}

function StartSession() {
    GetUserIP();
}

function updateWebToolData(username, toolname, packageData) {
    var client = new $.RestClient('/rest/');
    client.add('webToolData');
    var restRequest = client.webToolData.update(packagedData, {
        user: username,
        tool: toolname
    });
}

/*
function getWebToolData(username, toolname, requestData, callbackFunction) {
	var client = new $.RestClient('/rest/');
	client.add("webToolData");
	var restRequest = client.webToolData.create(requestData, {
		user: username,
		tool: toolname
	}); // post request
	restRequest.done(callbackFunction);
}*/

function getWebToolData(username, toolname, requestData, callbackFunction) {
    var client = new $.RestClient('/rest/',
    {
        stringifyData: true
    });
    client.add("webToolData");
    var restRequest = client.webToolData.create(requestData, {
        user: username,
        tool: toolname
    }); // post request
    restRequest.done(callbackFunction);
}

function GetUserIP() {
    var client = new $.RestClient('/rest/');
    client.add("currentUser");
    var restRequest = client.currentUser.create(); // post request
    // POST /rest/currentUser/
    restRequest.done(get_ClientIP_callBack);
}

function get_ClientIP_callBack(r) {
    var user = r["user"];
    username_and_domain = user;
    var username = user.split('\\')[1];
    var clientIP = r["ip"];
    var fullname = r["fullname"];

    $('#username_display_text').html(fullname);

    GetWebTools(username);

    // only store session analytics if running live
    if (user != undefined && analytics == undefined) {
        if (window.location.port == 81 || window.location.hostname == "localhost")
            aCA.debug = true;
        analytics = new aCA.AnalyticsSession(aCloudToolName, aCloudVersion, aCA.developmentTeamType.PA, user);
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'User IP', clientIP);
        if (currentPrefix)
            analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', currentPrefix);
        analytics.timeoutEnabled = true;
    }
    resetCurrentHash();
}

function toggleFullScreen(enable) {
    if (enable == undefined) {
        if ($('#navsidebar').css("display") == "none")
            enable = false; // if sidebar is hidden then show it
        else enable = true;
    }

    if (enable) {
        $('#navsidebar').css("display", "none");
        $('#page-wrapper').css("margin-left", "0");
        $('#page-wrapper').css("width", "100%");
        $('#fullscreenLink').html('Show Menu');
    } else {
        $('#navsidebar').removeAttr("style");

        $('#page-wrapper').css("margin-left", "225px");
        $('#page-wrapper').css("width", "88%");
        $('#fullscreenLink').html('Full Screen');
    }
}

function get_ThemeLogo(kit, callbackFunction) {
    if (themeLogoDict.hasOwnProperty(kit)) {
        callbackFunction(themeLogoDict[kit]);
    } else if (kit) {
        var client = new $.RestClient('/rest/');
        client.add("getThemeLogo");
        var restRequest;

        restRequest = client.getThemeLogo.create({}, {
            kit: kit
        }); // post request
        restRequest.done(callbackFunction);
    }
}

function get_defectsFoundPerStudioMonth(studio, month, callbackFunction) {
    if (defectsFoundStudioMonth.hasOwnProperty(studio + month)) {
        callbackFunction(defectsFoundStudioMonth[studio + month]);
    } else if (studio + month) {
        var client = new $.RestClient('/rest/');
        client.add("defectsForMonthAndStudio");
        var restRequest;

        restRequest = client.defectsForMonthAndStudio.create({}, {
            studio: studio.replaceAll(" ", "_"),
            month: month
        }); // post request
        restRequest.done(callbackFunction);
    }

}

function get_bvaTestCasesByBuildFile(buildFile, callbackFunction) {
    if (bvaTestCasesbuildFile.hasOwnProperty(buildFile)) {
        callbackFunction(bvaTestCasesbuildFile[buildFile]);
    } else if (buildFile) {
        var client = new $.RestClient('/rest/');
        client.add("bvaTestCasesByBuildFile");
        var restRequest;

        restRequest = client.bvaTestCasesByBuildFile.create({}, {
            buildFile: buildFile
        }); // post request
        restRequest.done(callbackFunction);
    }

}

function get_bvaTestCasesByCaseName(buildFile, callbackFunction) {
    if (bvaTestCasesByCaseName.hasOwnProperty(buildFile)) {
        callbackFunction(bvaTestCasesByCaseName[buildFile]);
    } else if (buildFile) {
        var client = new $.RestClient('/rest/');
        client.add("bvaTestCasesByCaseName");
        var restRequest;

        restRequest = client.bvaTestCasesByCaseName.create({}, {
            caseName: testCaseName
        }); // post request
        restRequest.done(callbackFunction);
    }

}

function get_bvaTestCaseData(chart_type, metric_prefix, studio, month) {
    if (loadedMetrics[chart_type]) {
        get_MetricsData_callBack(restJsonDict);
    } else {
        show_loadingDiv("Loading Data...");

        var client = new $.RestClient('/rest/');
        client.add("bvaTestCaseData");
        var restRequest;
        if (studio && month) {
            restRequest = client.bvaTestCaseData.create({}, {
                chart_type: chart_type,
                metric_prefix: metric_prefix,
                studio: studio.replaceAll(" ", "_"),
                month: month
            }); // post request
        } else restRequest = client.bvaTestCaseData.create(); // post request

        // POST /rest/kitPAReleased/        
        restRequest.done(get_bvaTestCaseData_callBack);
    }
}

function get_bvaTestCaseData_callBack(r) {
    if (r != null && r != undefined) {
        var chartType = r["chartType"];
        var prefix = r["metricPrefix"];
        if (loadedMetrics[chartType] != true) {
            $.extend(restJsonDict, r); // append the data from Rest Service
            loadedMetrics[chartType] = true;
        }
        chartTypeDict[prefix] = chartType;

        if (initState) {
            initState = false;
        }

        if (chartType.indexOf("UTP") !== -1)
            UTPRunsPerTestCase(prefix, restJsonDict, chartType, r["month"], r["studio"]);
        else
            BVARunsPerTestCase(prefix, restJsonDict, chartType, r["month"], r["studio"]);
    }
    resetCurrentHash();
}
/*
function get_CN_IN_KitList(studio, yearMonth, paCount) {
    if (loadedMetrics["cn_in_L3" + studio + yearMonth + paCount]) {
        get_MetricsData_callBack(restJsonDict);
    } else {

        var client = new $.RestClient('/rest/');
        client.add("get_cn_inKitList");
        var restRequest;
        if (studio && yearMonth && paCount) {
            restRequest = client.get_cn_inKitList.create({}, {
                studio: studio,
                yearMonth: yearMonth,
                paCount: paCount
            }); // post request
        } else restRequest = client.get_cn_inKitList.create(); // post request

        // POST /rest/kitPAReleased/        
        restRequest.done(get_CN_IN_KitList_callBack);

    }
}

function get_CN_IN_KitList_callBack(r, studio, yearMonth, paCount) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("cn_in_L3" + r["studio"] + r["yearmonth"] + r["pacount"]) == false) {
            restJsonDict["cn_in_L3" + r["studio"] + r["yearmonth"] + r["pacount"]] = r;
        }

        loadedMetrics["cn_in_L3" + r["studio"] + r["yearmonth"] + r["pacount"]] = true;
        CN_INForKits(currentPrefix, restJsonDict, r["studio"], r["yearmonth"], r["pacount"]);

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}
*/
function get_KitListForTool(chart_type, tool, monthInput, startDate, endDate, monthname) {
    if (loadedMetrics["KitCountForTools_L3" + tool + monthname]) {
        get_MetricsData_callBack(restJsonDict);
    } else {
        $.ajax({
            url: '/rest/kitsPerToolAndDate/',
            dataType: 'json',
            data: "toolname=" + tool + "&startDate=" + startDate + "&endDate=" + endDate,
            success: function (result) {
                get_KitListForTool_callBack(result["KitsPerTool"], tool, monthname);
            },
            error: function (request, textStatus, errorThrown) {
                // alert("get_KitListForTool: "+textStatus);
            }
        });

    }
}

function get_KitListForTool_callBack(r, tool, monthname) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("KitCountForTools_L3" + tool + monthname) == false) {
            restJsonDict["KitCountForTools_L3" + tool + monthname] = r;
        }

        loadedMetrics["KitCountForTools_L3" + tool + monthname] = true;
        KitListForTools(currentPrefix, restJsonDict, monthname, tool);

    } else {
        alert("No Data From Server");
    }
}
/*
function get_edasDataL3(metric, studio, yearMonth) {
    if (loadedMetrics["EdasEnteredData_L3" +metric+ studio + yearMonth]) {
        get_edasDataL3_callBack(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("EdaMetricForMonthAndStudio");
        var restRequest;
        if (studio && yearMonth) {
            restRequest = client.EdaMetricForMonthAndStudio.create({}, {
                metric: metric, 
                studio: studio.replaceAll(" ", "_"),
                yearMonth: yearMonth
            }); // post request
        } else restRequest = client.EdaMetricForMonthAndStudio.create(); // post request



        // POST /rest/kitPAReleased/  
        restRequest.done(function(r) {
            get_edasDataL3_callBack(r, metric, studio, yearMonth); 
        });
    }
}

function get_edasDataL3_callBack(r, metric, studio, yearMonth) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty(metric+"_L3" + r["studio"] + r["yearmonth"]) == false) {
            restJsonDict[metric+"_L3" + r["studio"] + r["yearmonth"]] = r;
        }

        loadedMetrics[metric + "_L3" + r["studio"] + r["yearmonth"]] = true;

        if(metric==="edasEntered")
            GetEdasEnteredDataL3(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);
        else if(metric ==="cn_in")
            CN_INForKits(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);
        else if (metric === "cn_inEntered")
            CN_INEnteredForKits(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);
        else if (metric === "edas")
            edaCountL3(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);

    } else {
        alert("No Data From Server");
    }
}*/

function get_mathThemesL3(metric, studio, month) {
    if (loadedMetrics[metric + studio + month]) {
        get_mathThemesL3_callBack(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("mathThemesReleasedL3");
        var restRequest;
        if (studio && month) {
            restRequest = client.mathThemesReleasedL3.create({}, {
                studio: studio,
                month: month
            }); // post request
        } else restRequest = client.mathThemesReleasedL3.create(); // post request



        // POST /rest/kitPAReleased/  
        restRequest.done(function (r) {
            get_mathThemesL3_callBack(r, metric, studio, month);
        });
    }
}

function get_mathThemesL3_callBack(r, metric, studio, month) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty(metric + r["studio"] + r["month"]) == false) {
            restJsonDict[metric + r["studio"] + r["month"]] = r;
        }

        loadedMetrics[metric + r["studio"] + r["month"]] = true;

        if (metric === "mathThemesReleased")
            MathThemesReleasedDataL3(metric, restJsonDict, r["month"], r["studio"]);
        else if (metric == "mathDefectsFound")
            MathDefectsFoundDataL3(metric, restJsonDict, r["month"], r["studio"]);
    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}


function get_DefectsKitsReleasedL3(metric, studio, monthname) {
    if (loadedMetrics["DefectsKitsReleasedL3"]) {
        get_DefectsKitsReleasedL3_callBack(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("kitPAReleased");
        var restRequest = client.kitPAReleased.create({}, {
            metric: metric,
            chart: "defects"
        }); // post request

        // POST /rest/kitPAReleased/  
        restRequest.done(function (r) {
            get_DefectsKitsReleasedL3_callBack(r, metric, studio, monthname);
        });
    }
}

function get_DefectsKitsReleasedL3_callBack(r, metric, studio, monthname) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("DefectsKitsReleasedL3") == false) {
            restJsonDict["DefectsKitsReleasedL3"] = r;
        }

        loadedMetrics["DefectsKitsReleasedL3"] = true;

        if (metric === "avgDefects")
            avgDefectsDataL3(currentPrefix, restJsonDict, studio, monthname);

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}



function get_edasDataL3(metric, studio, yearMonth) {
    if (loadedMetrics["EdasEnteredData_L3" +metric+ studio + yearMonth]) {
        get_edasDataL3_callBack(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("EdaMetricForMonthAndStudio");
        var restRequest;
        if (studio && yearMonth) {
            restRequest = client.EdaMetricForMonthAndStudio.create({}, {
                metric: metric, 
                studio: studio,
                yearMonth: yearMonth
            }); // post request
        } else restRequest = client.EdaMetricForMonthAndStudio.create(); // post request



        // POST /rest/kitPAReleased/  
        restRequest.done(function(r) {
            get_edasDataL3_callBack(r, metric, studio, yearMonth); 
        });
    }
}

function get_edasDataL3_callBack(r, metric, studio, yearMonth) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty(metric+"_L3" + r["studio"] + r["yearmonth"]) == false) {
            restJsonDict[metric+"_L3" + r["studio"] + r["yearmonth"]] = r;
        }

        loadedMetrics[metric + "_L3" + r["studio"] + r["yearmonth"]] = true;

        if(metric==="edasEntered")
            GetEdasEnteredDataL3(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);
        else if(metric ==="cn_in")
            CN_INForKits(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);
        else if (metric === "cn_inEntered")
            CN_INEnteredForKits(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);
        else if (metric === "edas")
            edaCountL3(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_edasNewOnlyDataL3(metric, studio, yearMonth) {
    if (loadedMetrics["edaNewOnly" + studio + yearMonth]) {
        get_edasNewOnlyDataL3_callBack(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("EdaMetricForMonthAndStudio");
        var restRequest;
        if (studio && yearMonth) {
            restRequest = client.EdaMetricForMonthAndStudio.create({}, {
                metric: metric,
                studio: studio,
                yearMonth: yearMonth
            }); // post request
        } else restRequest = client.EdaMetricForMonthAndStudio.create(); // post request

        // POST /rest/kitPAReleased/  
        restRequest.done(function (r) {
            get_edasNewOnlyDataL3_callBack(r, metric, studio, yearMonth);
        });
    }
}

function get_edasNewOnlyDataL3_callBack(r, metric, studio, yearMonth) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty(metric + r["studio"] + r["yearmonth"]) == false) {
            restJsonDict[metric + r["studio"] + r["yearmonth"]] = r;
        }

        loadedMetrics[metric + r["studio"] + r["yearmonth"]] = true;

        edaNewOnlyL3(metric, restJsonDict, r["yearmonth"], r["studio"]);

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_dceDataL3(metric, studio, yearMonth) {
    if (loadedMetrics["ContainmentRate_L3" + studio.replaceAll(" ", "_") + yearMonth]) {
        get_dceDataL3_callback(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("DCRMetricForMonthAndStudio");
        var restRequest;
        if (studio && yearMonth) {
            restRequest = client.DCRMetricForMonthAndStudio.create({}, {
                Studio: studio.replaceAll(" ", "_"),
                yearMonth: yearMonth
            }); // post request
        } else restRequest = client.DCRMetricForMonthAndStudio.create(); // post request



        // POST /rest/kitPAReleased/  
        restRequest.done(function(r) {
            get_dceDataL3_callback(r, metric, studio, yearMonth);
        });
    }
}

function get_dceDataL3_callback(r, metric, studio, yearMonth) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty(metric + "_L3" + r["studio"].replaceAll(" ", "_") + r["yearmonth"]) == false) {
            restJsonDict[metric + "_L3" + r["studio"].replaceAll(" ", "_") + r["yearmonth"]] = r;
        }
        loadedMetrics[metric + "_L3" + r["studio"].replaceAll(" ", "_") + r["yearmonth"]] = true;
        getDCEL3Data(metric, restJsonDict, r["yearmonth"], r["studio"]);
    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_edasEnteredL3(studio, yearMonth) {
    if (loadedMetrics["EdasEnteredData_L3" + studio + yearMonth]) {
        get_edasEnteredL3_callBack(restJsonDict);
    } else {
        var client = new $.RestClient('/rest/');
        client.add("EDAEnteredForMonth");
        var restRequest;
        if (studio && yearMonth) {
            restRequest = client.EDAEnteredForMonth.create({}, {
                studio: studio.replaceAll(" ", "_"),
                yearMonth: yearMonth
            }); // post request
        } else restRequest = client.EDAEnteredForMonth.create(); // post request

        // POST /rest/kitPAReleased/        
        restRequest.done(get_edasEnteredL3_callBack);
    }
}

function get_edasEnteredL3_callBack(r, studio, yearMonth) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("EdasEnteredData_L3" + r["studio"] + r["yearmonth"]) == false) {
            restJsonDict["EdasEnteredData_L3" + r["studio"] + r["yearmonth"]] = r;
        }

        loadedMetrics["EdasEnteredData_L3" + r["studio"] + r["yearmonth"]] = true;
        GetEdasEnteredDataL3(currentPrefix, restJsonDict, r["yearmonth"], r["studio"]);

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_KitCountForToolsList(chart_type) {
    if (loadedMetrics[chart_type]) {
        get_MetricsData_callBack(restJsonDict);
    } else {

        var date = new Date();
        var month = (date.getMonth() + 1);
        if (month < 10)
            month = "0" + month;

        var day = date.getDate();
        if (day < 10)
            day = "0" + day;

        var startDate = (date.getFullYear() - 1) + month + day;
        var endDate = (date.getFullYear()) + month + day;

        var rest_params = { startDate: startDate, endDate: endDate };
        metricCacheBackupCallBack["kitCountForAllTools"] = get_KitCountForToolsList_callBack;
        var cache_filename = compose_cache_filename("kitCountForAllTools", rest_params);
        get_CachedData(cache_filename, get_KitCountForToolsList_callBack, cacheData_errorHandler);
    }
}

function get_KitCountForToolsList_RestRequest(startDate, endDate) {

    $.ajax({
        url: '/rest/kitCountForAllTools/',
        dataType: 'json',
        data: "startDate=" + startDate + "&endDate=" + endDate,
        success: get_KitCountForToolsList_callBack,
        error: function (request, textStatus, errorThrown) {
            console.log("get_KitCountForToolsList: " + textStatus);
        }
    });
}

function get_KitCountForToolsList_callBack(r) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("KitCountAllTools") == false) {
            restJsonDict["KitCountAllTools"] = r["KitCountAllTools"];
        }

        loadedMetrics["KitCountForTools"] = true;
        get_KitCountForToolsMetrics();

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

window.onbeforeunload = function () {
    if (analytics)
        analytics.end(); // end session
};

function loadMetricView(prefix, target_level) {

    if (target_level == undefined) {
        revertToTopLevel(currentPrefix);
        currentPrefix = prefix;
        show_loadingDiv("Loading Data...");

        var $activediv = $("div.container-fluid > #page-wrapper > div.active");
        $activediv.removeClass('active');
        $activediv.addClass('hide');

        $activediv = $("#dashboardContainer");
        $activediv.removeClass('active');
        $activediv.addClass('hide');
        //toggleFullScreen();

        var $nextdiv = $("#metric_" + prefix);
        $nextdiv.removeClass('hide');
        $nextdiv.addClass('active');

        removeAllActiveClass();
        $.fn.dataTable.ext.search = []; // clear any global data table filters

        $('#link_' + prefix).addClass('active');

        displayTopLevelMetric(prefix);


    } else {
        GoUpLevel(prefix, target_level);
    }

    allGraphsVisible = true;
    $("#" + prefix + "_chart_link a").html("Hide All Bars");
}

function removeAllActiveClass() {
    $('#navsidebar li.active').removeClass('active'); // remove all active nav hightlights

    // removes lingering focus
    $('#navsidebar li').blur();
    $('#navsidebar li > a').blur();
}

/* NEW METRIC SPOT: 
* This function is where you add your prefix so that click events 
* on the dashboard card get routed to the metric */
function displayTopLevelMetric(prefix) {
    switch (prefix) {
        case "kitsReleased":
            revertToTopLevel(prefix);
            get_StudioGameMetrics();
            break;
        case "newThemeOther":
            revertToTopLevel(prefix);
            get_NewThemeOtherGameMetrics();
            break;
        case "bugFixOther":
            revertToTopLevel(prefix);
            get_BugFixOtherGameMetrics();
            break;
        case "defectsForKitsReleased":
            revertToTopLevel(prefix);
            get_DefectsForKitsReleasedMetrics();
            break;
        case "edas":
            revertToTopLevel(prefix);
            get_EDAForKitsReleasedMetrics();
            break;
        case "edasEntered":
            revertToTopLevel(prefix);
            get_EDAEnteredMetrics();
            break;
        case "newThemeFirstPass":
            revertToTopLevel(prefix);
            get_NewThemeFirstPassMetrics();
            break;
        case "NewThemeAvgTestTime":
            revertToTopLevel(prefix);
            get_NewThemeTestTimeMetrics();
            break;
        case "NewThemeAvgTTGameType":
            revertToTopLevel(prefix);
            get_NewThemeTestTimeMetricsType();
            break;
        case "defectsPhaseFound":
            revertToTopLevel(prefix);
            get_DefectsFoundNewThemesMetrics();
            break;
        case "avgDefects":
            revertToTopLevel(prefix);
            get_avgDefectsNewThemeMetric();
            break;
        case "defectsClosedbyTesterType":
            revertToTopLevel(prefix);
            get_DefectsFoundTesterTypesMetrics();
            break;
        case "ContainmentRate":
            revertToTopLevel(prefix);
            get_DceMetrics();
            break;
        case "bvaBuildsRun":
            revertToTopLevel(prefix);
            get_BVABuildsRunMetrics();
            break;
        case "bvaSuitesRun":
            revertToTopLevel(prefix);
            get_BVASuitesRunMetrics();
            break;
        case "BVATestCasesPassFail":
            revertToTopLevel(prefix);
            get_BVATestCasesMetrics();
            break;
        case "UTPTestCasesPassFail":
            revertToTopLevel(prefix);
            get_UTPTestCasesMetrics();
            break;
        case "GameTracker":
            revertToTopLevel(prefix);
            get_GameTrackerMetrics();
            break;
        case "AllSitesFirstPass":
            revertToTopLevel(prefix);
            get_NewThemeFirstPassMetrics_SP();
            break;
        case "QuartersPhaseFound":
            revertToTopLevel(prefix);
            get_TwoYearDefectsFoundMetrics();
            break;
        case "QuartersFirstPass":
            revertToTopLevel(prefix);
            get_TwoYearFirstPassMetrics();
            break;
        case "AllSitesNewOther":
            revertToTopLevel(prefix);
            get_NewThemeOtherSharePointMetrics();
            break;
        case "kitreleaselist":
            revertToTopLevel(prefix);
            get_KitReleaseGrid();
            break;
        case "kittestlist":
            revertToTopLevel(prefix);
            get_KitTestGrid();
            break;
        case "KitCountForTools":
            revertToTopLevel(prefix);
            get_KitCountForToolsMetrics();
            break;
        case "EQEScoreByPushDate":
            revertToTopLevel(prefix);
            get_EQEScoreByPushDateMetrics();
            break;
        case "cn_in":
            revertToTopLevel(prefix);
            get_cn_inMetrics();
            break;
        case "cn_inEntered":
            revertToTopLevel(prefix);
            get_cn_inEnteredMetrics();
            break;
        case "edaNewOnly":
            revertToTopLevel(prefix);
            get_edaNewOnlyMetrics();
            break;
        case "mathThemesReleased":
            revertToTopLevel(prefix);
            get_mathThemesReleasedMetric();
            break;
        case "currentMathStates":
            revertToTopLevel(prefix);
            get_MathStatesGrid();
            break;
        case "mathDefectsFound":
            revertToTopLevel(prefix);
            get_mathDefectsFoundMetric();
            break;
        case "mathEdas":
            revertToTopLevel(prefix);
            get_mathEdasMetric();
            break;
    }
}

function get_StudioList(callbackFunc) {

    if (restJsonDict.hasOwnProperty("StudioList"))
        get_StudioList_callBack(restJsonDict["StudioList"]);
    else {
        var client = new $.RestClient('/rest/');
        client.add("StudioAliases");
        var restRequest = client.StudioAliases.create(); // post request
        // POST /rest/StudioAliases/

        if (callbackFunc === null || callbackFunc === undefined)
            restRequest.done(get_StudioList_callBack);
        else
            restRequest.done(callbackFunc);
    }
}

function get_UTPList(callbackFunc) {

    if (restJsonDict.hasOwnProperty("UTPTestCasesListRaw") &&
        restJsonDict.hasOwnProperty("UTPTestCasesList") &&
        callbackFunc === undefined)
    {
        get_UTPList_callBack(restJsonDict);
    }
    else if (restJsonDict.hasOwnProperty("UTPTestCasesListRaw") &&
        restJsonDict.hasOwnProperty("UTPTestCasesList") &&
        restJsonDict.hasOwnProperty("UTPCases_Month0"))
    {
        callbackFunc(restJsonDict);
    }
    else {
        var rest_params = {};
        if (callbackFunc == undefined)
            callbackFunc = get_UTPList_callBack;
        metricCacheBackupCallBack["UTPGetTestCases"] = callbackFunc;
        var cache_filename = compose_cache_filename("UTPGetTestCases", rest_params);
        get_CachedData(cache_filename, callbackFunc, cacheData_errorHandler);
    }
}

function get_UTPList_RestRequest(callbackFunc) {
    /*
    var client = new $.RestClient('/rest/');
    client.add("UTPGetTestCases");
    var restRequest = client.UTPGetTestCases.read();
    restRequest.done(get_UTPList_callBack);*/
    //$.getJSON('/rest/UTPGetTestCases/', function (data) { get_UTPList_callBack(data) });
    $.ajax({
        url: '/rest/UTPGetTestCases/',
        dataType: 'json',
        success: function (result) {
            if (callbackFunc === undefined)
                get_UTPList_callBack(result);
            else {
                callbackFunc(result);
            }
        },
        error: function (request, textStatus, errorThrown) {
            //console.log("get_UTPList:"+ textStatus);
        }
    });
}

function get_GameTrackerList(callbackFunc) {
    //check callback
    if (callbackFunc === undefined)
        callbackFunc = get_GameTrackerList_callBack; 

    if (restJsonDict.hasOwnProperty("GameTrackerList") && callbackFunc === undefined) {
        get_GameTrackerList_callBack(restJsonDict["GameTrackerList"]);
    }
    else {
        var rest_params = {};
        metricCacheBackupCallBack["GameTrackerUTPInfo"] = callbackFunc;
        var cache_filename = compose_cache_filename("GameTrackerUTPInfo", rest_params);
        get_CachedData(cache_filename, callbackFunc, cacheData_errorHandler);
    }
}

function get_GameTrackerList_RestRequest(callbackFunc) {
    $.ajax({
        url: '/rest/GameTrackerUTPInfo/',
        dataType: 'json',
        success: function (result) {
            if (callbackFunc === undefined)
                get_GameTrackerList_callBack(result);
            else {
                callbackFunc(result);
            }
        },
        error: function (request, textStatus, errorThrown) {
            //alert("get_GameTrackerList: "+textStatus);
        }
    });
}

function GetUrlForCurrentChart() {
    var url = hasher.getBaseURL() + "#/";
    url = url + GetURLFragmentForCurrentChart();
    window.prompt("URL to This Chart", url);
}

function GetURLFragmentForCurrentChart() {
    var url = "";
    if (document.getElementById("scoreCard") !== null)
        url = url + "kit/" + kitNumber;
    else if (document.getElementById("bva_scorecard") !== null && bvaBuild !== "")
        url = url + "bvaBuild/" + bvaBuild;
    else if (document.getElementById("bva_testCaseSC") !== null && testCaseName !== "")
        url = url + "testCase/" + testCaseName;
    else if ($("#dashboardContainer").is(":visible")) {
        url = "studio/" + selectedStudio.replaceAll(" ", "_");
    } else {
        url = url + "metric/";
        if (level == 1) {
            if (currentMonth != "") {
                url = url + currentPrefix + "/" + currentMonth;
            } else url = url + currentPrefix + "/all";
        } else {
            var prefix = currentPrefix;
            var month = currentMonth;
            var studio = currentStudio.replaceAll(" ", "_");
            if (currentPrefix.endsWith("_L2") || currentPrefix.endsWith("_L3"))
                prefix = prefix.substr(0, prefix.length - 3); // get base prefix if needed
            url = url + prefix + "/";
            if (level == 2) {

                var tempArray = studio.split('/');
                if (tempArray != null && tempArray.length == 2) {
                    studio = tempArray[0];
                }
                url = url + "all/" + studio;

            } else if (level == 3) {
                studio = studio.replaceAll("/", ":");
                url = url + month + "/" + studio;
            }
        }
    }
    return url;
}

function setHashSilently(hash) {
    if (hasher.getHash() != hash && hash != "") {
        hasher.changed.active = false; //disable changed signal    
        hasher.setHash(hash); //set hash without dispatching changed signal
        hasher.changed.active = true; //re-enable signal
    }
}

function resetCurrentHash() {
    setHashSilently(GetURLFragmentForCurrentChart());
}

// function that makes the crossroads url jump work
function whatIsInitSubCat(prefix) {
    var subcat_type = "";
    // if month and verify month
    if (initMonth != "all" && !hasInitSubCat && isMonthString(initMonth)) {
        subcat_type = "month";
    } else if (hasInitSubCat && (initMonth == "all" || isMonthString(initMonth))) {
        var studio = "";
        var cat = "";
        var tempArray;
        initSubCategory = initSubCategory.replaceAll("_", " ");

        // get stack choice if needed
        if (prefix == "kitsReleased" || prefix == "defectsForKitsReleased" || prefix == "newThemeFirstPass" || prefix == "AllSitesFirstPass" || prefix == "NewThemeAvgTestTime" || prefix == "NewThemeAvgTTGameType" ||
			prefix == "defectsPhaseFound" || prefix == "avgDefects" || prefix == "bvaBuildsRun" || prefix == "bvaSuitesRun" || prefix == "BVATestCasesPassFail" || prefix == "UTPTestCasesPassFail" || prefix == "GameTracker" || prefix == "ContainmentRate" || prefix == "kitreleaselist" || prefix == "kittestlist" || prefix == "KitCountForTools" || prefix == "cn_in" || prefix == "cn_inEntered" || prefix == "mathThemesReleased" || prefix == "currentMathStates" || prefix == "mathDefectsFound") {
            studio = initSubCategory;
        } else if (prefix == "bugFixOther" || prefix == "newThemeOther" || prefix == "defectsClosedbyTesterType" || prefix == "AllSitesNewOther" || prefix == "edas" || prefix == "edasEntered") {
            if (initMonth != "all") {
                tempArray = initSubCategory.split(':');
                if (tempArray != null && tempArray.length == 2) {
                    studio = tempArray[0];
                    cat = tempArray[1];
                } else {
                    studio = initSubCategory;
                    cat = "Other"; // default to Other

                    if (prefix == "defectsClosedbyTesterType")
                        cat = "Game Tester";
                }
                initSubCategory = studio + '/' + cat; // rewrite for use in drilldown functions
            } else studio = initSubCategory; // just L2 not L3 so no stack choice needed
        }

        // verify studio is correct
        if (studio != "") {
            for (var contact in studio_alias) {
                if (studio_alias.hasOwnProperty(contact)) {
                    if (studio_alias[contact] == studio || studio == "Total") {
                        if (cat != "") {
                            if (prefix == "bugFixOther" && (cat == "Bug Fix" || cat == "Other")) {
                                subcat_type = "studio";
                            } else if (prefix == "newThemeOther" && (cat == "New Theme" || cat == "Other")) {
                                subcat_type = "studio";
                            } else if (prefix == "AllSitesNewOther" && (cat == "New Theme" || cat == "Other")) {
                                subcat_type = "studio";
                            } else if (prefix == "defectsClosedbyTesterType" && (cat == "SDET" || cat == "Game Tester")) {
                                subcat_type = "studio";
                            }
                            else if (prefix == "edas" && (cat == "PA" || cat == "Other")) {
                                subcat_type = "studio";
                            }
                        } else if (initMonth == "all" && (prefix == "bugFixOther" || prefix == "newThemeOther" || prefix == "AllSitesNewOther" || prefix == "defectsClosedbyTesterType" || prefix == "edas"))
                            subcat_type = "studio";
                        else if (prefix == "studio" || prefix == "defectsForKitsReleased" || prefix == "newThemeFirstPass" || prefix == "AllSitesFirstPass" || prefix == "NewThemeAvgTestTime" || prefix == "NewThemeAvgTTGameType" || prefix == "defectsPhaseFound" || prefix == "avgDefects" || prefix == "bvaBuildsRun" || prefix == "bvaSuitesRun" || prefix == "BVATestCasesPassFail" || prefix == "UTPTestCasesPassFail" || prefix == "GameTracker" || prefix == "ContainmentRate" || prefix == "KitCountForTools" || prefix == "cn_in" || prefix == "cn_inEntered" || prefix == "mathThemeseReleased") {
                            subcat_type = "studio";
                        } else subcat_type = "studio";
                        break;
                    }
                }
            }
        }

        if (studio != "" && initMonth != "all")
            hasInitSubCatMonth = true; // L3 drill down needed
    }
    return subcat_type;
}

var studio_alias = {};
var utp_list;

function get_DashboardStudioList_callBack(r) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("StudioList") == false) {
            //current_XML_Data = $.parseXML(r);
            restJsonDict["StudioList"] = r;
        } //else current_XML_Data = r;        
        var studio_alias_list = r["StudioList"];
        studio_alias = GetStudioAliasDictionaryFromJSON(studio_alias_list);
        DashboardUpdateStudioList(studio_alias);
    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_StudioList_callBack(r) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("StudioList") == false) {
            //current_XML_Data = $.parseXML(r);
            restJsonDict["StudioList"] = r;
        } //else current_XML_Data = r;        
        var studio_alias_list = r["StudioList"];
        studio_alias = GetStudioAliasDictionaryFromJSON(studio_alias_list);
    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_UTPList_callBack(r) {
    if (r != null && r != undefined) {
        var restObj = {};
        if (r["UTP"])
            restObj = r["UTP"];
        else restObj = r["UTPTestCasesListRaw"];

        if (restJsonDict.hasOwnProperty("UTPTestCasesListRaw") == false) {
            restJsonDict["UTPTestCasesListRaw"] = restObj;
        }
        var uniqueCasesNames = [];
        for (var i = 0; i < restObj.length; i++) {
            uniqueCasesNames.push(restObj[i].Name);
        }
        restJsonDict["UTPTestCasesList"] = uniqueCasesNames;
        utp_list = uniqueCasesNames;

        if (currentPrefix == "UTPTestCasesPassFail")
            get_MetricsData("utp", "UTPTestCasesPassFail");

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_GameTrackerList_callBack(r) {
    if (r != null && r !== undefined) {
        if (restJsonDict.hasOwnProperty("GameTrackerList") == false) {
            restJsonDict["GameTrackerList"] = r;
        }

        //group data by date
        //var data = {};
        for (var i = 0; i < r.length; i++) {
            var latestBuild = r[i].Builds[r[i].Builds.length - 1]; //r[i].Builds[0];
            var date = new Date(latestBuild.Locations[0].Created);
            var month = date.getMonth();
            var year = date.getFullYear();
            var today = new Date();

            if (today.getFullYear() === year) {
                if (month === 11)
                    month = 12;
            }

            if (restJsonDict["gameTracker_Month" + month] === undefined) {
                restJsonDict["gameTracker_Month" + month] = [];
            }

            restJsonDict["gameTracker_Month" + month].push(r[i]);
        }

        //jsonTable["GameTrackerList"] = data;
        loadedMetrics["GameTracker"] = true;
        get_GameTrackerMetrics();

    } else {
        alert("No Data From Server");
    }
    resetCurrentHash();
}

function get_CachedData(filename, callback_function, no_cache_callback) {
    var client = new $.RestClient('/rest/');
    client.add("GetCachedData", { ajax: { error: no_cache_callback } });
    var restRequest;
    restRequest = client.GetCachedData.create({}, {
        file: filename
    }); // post request

    restRequest.done(callback_function);
}

function compose_cache_filename(restcommand, params) {
    var filename = restcommand;
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            filename += "&" + key + "-" + params[key];
        }
    }
    filename += ".json";
    return encodeURI(filename);
}

function cacheData_errorHandler(xhr, ajaxOptions, thrownError) {
    if (xhr == null || xhr.responseJSON == null || xhr.responseJSON.ErrorDetails == null)
        console.log("Unknown Rest Error: " + thrownError);
    else {
        var filename = xhr.responseJSON.ErrorDetails;
        var sections = filename.replace('.json', '').split("&")
        var restcommand = sections[0];
        var parts = [];
        var params = {};
        for (var i = 1; i < sections.length; i++) {
            parts = sections[i].split("-");
            params[parts[0]] = parts[1];
        }


        switch (restcommand) {
            case "kitPAReleased":
                get_MetricsDataRestRequest(params["chart"], params["metric"], params["quarters"]);
                break;
            case "dashboardOverviewData":
                get_DashboardOverviewData_RestRequest(params["metric"], params["startDate"],
                                                        params["endDate"], params["yearStart"],
                                                        params["studio"], params["paGroup"],
                                                        dashboardCacheBackupCallBack[[params["metric"], params["startDate"]]]);
                break;
            case "GameTrackerUTPInfo":
                get_GameTrackerList_RestRequest(metricCacheBackupCallBack["GameTrackerUTPInfo"]);
                break;
            case "UTPGetTestCases":
                get_UTPList_RestRequest(metricCacheBackupCallBack["UTPGetTestCases"]);
                break;
            case "kitCountForAllTools":
                get_KitCountForToolsList_RestRequest(params["startDate"], params["endDate"]);
                break;
            default: console.log(xhr.responseJSON);
        }
    }
}

function get_MetricsData(chart_type, prefix, quarters) {
    if (loadedMetrics[chart_type]) {
        get_MetricsData_callBack(restJsonDict);
    } else {
        show_loadingDiv("Loading Data...");
        // DateTime.Now.AddDays(1).ToString("yyyyMMdd");
        if (quarters == null)
            quarters = false;
        var rest_params = { chart: chart_type, metric: prefix, date: getTomorrowsISODate(), quarters: quarters };

        var cache_filename = compose_cache_filename("kitPAReleased", rest_params);
        get_CachedData(cache_filename, get_MetricsData_callBack, cacheData_errorHandler);
    }
}

function get_MetricsDataRestRequest(chart_type, prefix, quarters) {
    if (quarters === "false") quarters = false;
    var client = {};
    client = new $.RestClient('/rest/');
    client.add("kitPAReleased");
    var restRequest;
    if (chart_type && prefix && !quarters) {
        restRequest = client.kitPAReleased.create({}, {
            chart: chart_type,
            metric: prefix
        }); // post request
    } else if (chart_type && prefix && quarters) {
        restRequest = client.kitPAReleased.create({}, {
            chart: chart_type,
            metric: prefix,
            quarters: quarters
        });
    } else restRequest = client.kitPAReleased.create(); // post request

    // POST /rest/kitPAReleased/        
    restRequest.done(get_MetricsData_callBack);
}


/* NEW METRIC STEP:
* This function is where you need to call your metric's getData function in the switch
*/
function get_MetricsData_callBack(r) {
    if (r != null && r != undefined) {
        var chartType = r["chartType"];
        var prefix = r["metricPrefix"];
        if (loadedMetrics[chartType] != true) {
            $.extend(restJsonDict, r); // append the data from Rest Service
            loadedMetrics[chartType] = true;
        }
        chartTypeDict[prefix] = chartType;

        if (initState) {
            initState = false;
        }

        switch (prefix) {
            case "kitsReleased":
                get_StudioGameMetrics(); // default
                break;
            case "bugFixOther":
                get_BugFixOtherGameMetrics();
                break;
            case "newThemeOther":
                get_NewThemeOtherGameMetrics();
                break;
            case "defectsForKitsReleased":
                get_DefectsForKitsReleasedMetrics();
                break;
            case "edas":
                get_EDAForKitsReleasedMetrics();
                break;
            case "edasEntered":
                get_EDAEnteredMetrics();
                break;
            case "newThemeFirstPass":
                get_NewThemeFirstPassMetrics();
                break;
            case "NewThemeAvgTestTime":
                get_NewThemeTestTimeMetrics();
                break;
            case "NewThemeAvgTTGameType":
                get_NewThemeTestTimeMetricsType();
                break;
            case "defectsPhaseFound":
                get_DefectsFoundNewThemesMetrics();
                break;
            case "avgDefects":
                get_avgDefectsNewThemeMetric();
                break;
            case "defectsClosedbyTesterType":
                get_DefectsFoundTesterTypesMetrics();
                break;
            case "ContainmentRate":
                get_DceMetrics();
                break;
            case "bvaBuildsRun":
                get_BVABuildsRunMetrics();
                break;
            case "bvaSuitesRun":
                get_BVASuitesRunMetrics();
                break;
            case "BVATestCasesPassFail":
                get_BVATestCasesMetrics();
                break;
            case "UTPTestCasesPassFail":
                get_UTPTestCasesMetrics();
                break;
            case "GameTracker":
                get_GameTrackerMetrics();
                break;
            case "AllSitesFirstPass":
                get_NewThemeFirstPassMetrics_SP();
                break;
            case "QuartersPhaseFound":
                get_TwoYearDefectsFoundMetrics();
                break;
            case "QuartersFirstPass":
                get_TwoYearFirstPassMetrics();
                break;
            case "AllSitesNewOther":
                get_NewThemeOtherSharePointMetrics();
                break;
            case "kitreleaselist":
                get_KitReleaseGrid();
                break;
            case "kittestlist":
                get_KitTestGrid();
                break;
            case "KitCountForTools":
                get_KitCountForToolsMetrics();
                break;
            case "EQEScoreByPushDate":
                get_EQEScoreByPushDateMetrics();
                break;
            case "cn_in":
                get_cn_inMetrics();
                break;
            case "cn_inEntered":
                get_cn_inEnteredMetrics();
                break;
            case "mathThemesReleased":
                get_mathThemesReleasedMetric();
                break;
            case "currentMathStates":
                get_MathStatesGrid();
                break;
            case "mathDefectsFound":
                get_mathDefectsFoundMetric();
                break;
            case "mathEdas":
                get_mathEdasMetric();
                break;
            case "edaNewOnly":
                get_edaNewOnlyMetrics();
                break;
        }
    }
    resetCurrentHash();
}

function clearChart(prefix) {
    if (chartDict.hasOwnProperty(prefix)) {
        chart = chartDict[prefix];
        chart.clear();
    }
}

function chartInit(event) {

    var chart = event.chart;

    // Get the rolling numbers so they can be activated from the legend
    // these metrics use the default rolling total method
    if ((currentPrefix == "kitsReleased_L2" || currentPrefix == "newThemeOther_L2" || currentPrefix == "defectsForKitsReleased_L2"  || currentPrefix == "edas_L2" || currentPrefix == "edasEntered_L2" || currentPrefix == "AllSitesNewOther_L2" || currentPrefix == "cn_in_L2" || currentPrefix == "cn_inEntered_L2" || currentPrefix == "mathThemesReleased_L2" )
        && !chart.dataProvider[0].hasOwnProperty("rollingTotal")) {
        getRollingTotal();
        // these metrics use custom rolling figures
    } else if ((currentPrefix == "ContainmentRate_L2") && !chart.dataProvider[0].hasOwnProperty("rollingTotal")) {
        getRollingAvg_dce();
    } else if (currentPrefix == "UTPTestCasesPassFail_L2" && !chart.dataProvider[0].hasOwnProperty("rollingTotal")) {
        UTPTestCasesL2ShowRollingAverages();
    } else if (currentPrefix == "GameTracker_L2" && !chart.dataProvider[0].hasOwnProperty("rollingPassedPercent")) {
        GameTrackerL2ShowRollingAverages();
    } else if (currentPrefix == "BVATestCasesPassFail_L2" && !chart.dataProvider[0].hasOwnProperty("rollingPassedPercent")) {
        BVATestCasesL2ShowRollingAverages();
    } else if (currentPrefix == "QuartersPhaseFound_L2" && !chart.dataProvider[0].hasOwnProperty("rollingPrePAPercent")) {
        DefectFound2YShowRollingPercents();
    } else if (currentPrefix == "EQEScoreByPushDate_L2" && !chart.dataProvider[0].hasOwnProperty("rollingAvg")) {
        getRollingAverageTestTimeEQEL2();
    }

    // hide all line graphs
    for (i = 0; i < chart.graphs.length; i++) {
        if (chart.graphs[i].type == "line" && !chart.graphs[i].hidden)
            event.chart.hideGraph(chart.graphs[i]);
    }

    // add legend listener
    chart.legend.addListener("showItem", showLegendItem);

    var prefix = event.chart.titles[0].id;

    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var max_chart_height = h - 98;

    $('#' + prefix + "_chartdiv").height(max_chart_height);

    var totalLineGraph = chart.getGraphById("totalline");
    if (totalLineGraph != null)
        chart.hideGraph(totalLineGraph); // hide total line by default
}

function BVAchartInit(event) {
    chart = event.chart;
    var prefix = event.chart.titles[0].id;

    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var max_chart_height = h - 98;

    $('#' + prefix + "_chartdiv").height(max_chart_height);

    var totalLineGraph = chart.getGraphById("totalline");
    if (totalLineGraph != null)
        chart.hideGraph(totalLineGraph); // hide total line by default

}

function UTPchartInit(event) {
    chart = event.chart;
    var prefix = event.chart.titles[0].id;

    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var max_chart_height = h - 98;

    $('#' + prefix + "_chartdiv").height(max_chart_height);

    var totalLineGraph = chart.getGraphById("totalline");
    if (totalLineGraph != null)
        chart.hideGraph(totalLineGraph); // hide total line by default

}

function forceChartFillParent(prefix) {
    // this is a hack to force the chart to fill the screen
    var chart_div_id = "#" + prefix + "_chartdiv";
    $(chart_div_id).ready(function () {
        if (chartResizerDict[prefix]) {
            $(chart_div_id).height($(chart_div_id).parent().height() - 38);
            chartDict[prefix].invalidateSize();
            chartDict[prefix].validateNow();

            chartResizerDict[prefix] = false;
        }
    });
}

function addListeners(event) {
    chart = event.chart;

    $('a[href="http://www.amcharts.com/javascript-charts/"]').remove(); // remove watermark

    var prefix = event.chart.titles[0].id;

    chart.addListener("clickGraphItem", handleItemClick);

    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", handleCategoryClick);

    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var max_chart_window_height = h - (60 + 38);

    // fixed in chartInit now
    //forceChartFillParent(prefix);
    //if ($('#page-wrapper').height() > max_chart_window_height)
    //	forceChartFillParent(prefix);

    removeLoadingDiv();

    if (hasInitSubCat) {
        var subcat_type = whatIsInitSubCat(prefix); // validates initSubCat and initStackCat and gives type
        hasInitSubCat = false;

        var $subnav;
        if (subcat_type == "month") {
            removeAllActiveClass();

            $subnav = $("<li id='link_" + prefix + "_Cat' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + initSubCategory + "' onclick='loadMetricView(\"" + prefix + "\", 1.5)>" + initSubCategory + "</a></li>");
            $("#link_" + prefix).after($subnav);

            if (prefix == "ContainmentRate")
                DrillDownToMonthDce(prefix, initSubCategory);
            else DrillDownToMonth(prefix, initSubCategory);
        } else if (subcat_type == "studio") {
            removeAllActiveClass();

            $subnav = $("<li id='link_" + prefix + "_2' class='subnav active'><a href='#' title='#/metric/" + prefix + "/all/" + initSubCategory + "' onclick='loadMetricView(\"" + prefix + "\", 2)'>" + initSubCategory + "</a></li>");
            $("#link_" + prefix).after($subnav);

            if (prefix == "newThemeFirstPass")
                FirstPass_DrillDownToLevel2(prefix, initSubCategory);
            else if (prefix == "AllSitesFirstPass")
                FirstPass_DrillDownToLevel2_SP(prefix, initSubCategory);
            else if (prefix == "defectsPhaseFound")
                DefectsFoundNT_DrillDownToLevel2(prefix, initSubCategory);
            else if (prefix == "defectsClosedbyTesterType")
                DefectsFoundNT_DrillDownToLevel2(prefix, initSubCategory);
            else if (prefix == "ContainmentRate")
                DrillDownToStudioDce(prefix, initSubCategory);
            else DrillDownToStudio(prefix, initSubCategory);
            $("#helpicon").unbind('click');
            $("#helpicon").click(function () {
                var URL = "HelpPage/helpPage.html#" + prefix + "_L2";
                window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
            });
        }
        $("#helpicon").unbind('click');
        $("#helpicon").click(function () {
            var URL = "HelpPage/helpPage.html#" + prefix + "_L2";
            window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
        });
    } else resetCurrentHash();
}

function handleCategoryClick(event) {
    show_loadingDiv("Loading Data...");
    var prefix = event.chart.titles[0].id;

    removeAllActiveClass();
    $subnav = $("<li id='link_" + prefix + "_Cat' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + event.value + "' onclick='loadMetricView(\"" + prefix + "\", 1.5)>" + event.value + "</a></li>");
    if (document.getElementById("link_" + prefix + "_Cat") == null) {
        removeAllActiveClass();

        $("#link_" + prefix).after($subnav);
    }


    if (currentPrefix == "ContainmentRate")
        DrillDownToMonthDce(event.chart.titles[0].id, event.value);
    else
        DrillDownToMonth(event.chart.titles[0].id, event.value);
}

function toggleTotalLine(chart, show) {
    for (var i = 0; i < chart.graphs.length; i++) {
        if ("totalline" == chart.graphs[i].id) {
            if (show)
                chart.showGraph(chart.graphs[i]);
            else chart.hideGraph(chart.graphs[i]);
        }
    }
}

/* NEW METRIC STEP
* Add prefix here to allow for clicking on a month on the x-axis
*/
function DrillDownToMonth(prefix, monthname) {

    currentMonth = monthname;
    currentStudio = "";
    //clearTable(prefix + "_dataTable");
    showHideDataTable(prefix + "_dataTable", "100%", false);
    var table;
    if (prefix == "QuartersPhaseFound" || prefix == "QuartersFirstPass")
        table = GetDataForQuarter(restJsonDict, chartJSONTableDict[prefix], monthname);
    else
        table = GetDataForMonth(restJsonDict, chartJSONTableDict[prefix], monthname);

    chart = chartDict[prefix];

    var row = {};
    if (prefix == "QuartersPhaseFound" || prefix == "QuartersFirstPass") {
        row["quarter"] = monthname;
    } else {
        row["month"] = monthname;
    }
    row["Total"] = table.length;

    currentPrefix = prefix;

    if (prefix == "newThemeOther")
        row = parseByNewThemeOtherToChartRow(row, table, studio_alias);
    if (prefix == "AllSitesNewOther")
        row = parseByNewThemeOtherSharePntToChartRow(row, table, studio_alias);
    else if (prefix == "bugFixOther")
        row = parseByBugFixOtherToChartRow(row, table, studio_alias);
    else if (prefix == "kitsReleased")
        row = parseByStudioToChartRow(row, table, studio_alias);
    else if (prefix == "defectsForKitsReleased")
        row = parseDefectsReleasedToChartRow(row, table, studio_alias);
    else if (prefix == "edas" || prefix == "edasEntered" || prefix == "edaNewOnly")
        row = parseEDAsToChartRow(row, table, studio_alias);
    else if (prefix == "newThemeFirstPass")
        row = parseNewThemeToChartRow(row, table, studio_alias);
    else if (prefix == "AllSitesFirstPass")
        row = parseNewThemeToChartRow_SP(row, table, studio_alias);
    else if (prefix == "NewThemeAvgTestTime")
        row = parseTestTimeToMonth(row, table, studio_alias, prefix);
    else if (prefix == "NewThemeAvgTTGameType")
        row = parseTestTimeToMonth(row, table, studio_alias, prefix);
    else if (prefix == "defectsPhaseFound")
        row = parseDefectsFoundNTToChartRow(row, table, studio_alias);
    else if (prefix == "avgDefects")
        row = parseAvgDefectsNewThemesL1(row, table, studio_alias);
    else if (prefix == "defectsClosedbyTesterType")
        row = parseDefectsFoundTesterToChartRow(row, table, studio_alias);
    else if (prefix == "bvaBuildsRun")
        row = parseBVABuildsRun(row, table, studio_alias);
    else if (prefix == "bvaSuitesRun")
        row = parseBVASuitesRun(row, table, studio_alias);
    else if (prefix == "BVATestCasesPassFail")
        row = parseBVATestCases(row, table, studio_alias);
    else if (prefix == "UTPTestCasesPassFail")
        row = parseUTPTestCases(row, table, studio_alias);
    else if (prefix == "GameTracker")
        row = parseGameTrackerData(row, table, studio_alias);
    else if (prefix == "QuartersPhaseFound")
        row = parseByTwoYearDefectsFoundToChartRow(row, table, studio_alias);
    else if (prefix == "QuartersFirstPass")
        row = parseByTwoYearFirstPassToChartRow(row, table, studio_alias);
    else if (prefix == "KitCountForTools")
        row = parseByKitCountForTools(row, table, studio_alias);
    else if (prefix == "cn_in")
        row = parseCN_INdata(row, table, studio_alias);
    else if (prefix == "cn_inEntered")
        row = parseCN_INEnteredData(row, table, studio_alias);
    else if (prefix == "mathThemesReleased")
        row = parseMathThemesReleased(row, table, studio_alias);
    else if (prefix == "mathDefectsFound")
        row = parseMathDefectsFound(row, table, studio_alias);
    else if (prefix == "mathEdas")
        row = parseMathEdas(row, table, studio_alias);

    var data = [];
    data.push(row);
    chart.dataProvider = data;
    toggleTotalLine(chart, false);

    var chart_title = chart.titles[0].text;

    // let's add a label to go back to yearly data
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:resetChart("' + prefix + '","' + chart_title + '");');

    chart.titles[0].text = chart_title + " for " + monthname;

    //chart.startDuration = 1; // enable animation for the drill down

    chart.validateData();
    chart.animateAgain();

    resetCurrentHash();

    dataTableDataDict[prefix + "_dataTable"] = data;
    removeLoadingDiv();
}

/* NEW METRIC STEP
* Need to add prefix here to Drill down to L2
*/
function DrillDownToStudio(prefix, itemSelection) {
    currentMonth = "all";
    currentStudio = itemSelection;

    // turn off show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");

    showHideDataTable(prefix, "100%", false);

    if (prefix == "QuartersFirstPass")
        var data = parseMetricDataByQuarter(restJsonDict, chartJSONTableDict[prefix], studio_alias, prefix, itemSelection);
    else if (prefix == "avgDefects") {
        var data = parseAvgDefectsNewThemesL2(restJsonDict, chartJSONTableDict[prefix], studio_alias, prefix, itemSelection);
    }
    else
        var data = parseMetricData(restJsonDict, chartJSONTableDict[prefix], studio_alias, prefix, itemSelection);

    var graphs;
    var stacks;
    var studioName;
    var useStackChart = false;

    if (prefix == "kitsReleased" || prefix == "defectsForKitsReleased"  || prefix == "NewThemeAvgTestTime" ||
		prefix == "NewThemeAvgTTGameType" || prefix == "bvaBuildsRun" || prefix == "bvaSuitesRun" ||
        prefix == "mathThemesReleased" || prefix == "mathDefectsFound" || prefix == "mathEdas") {
        graphs = GetGraphs(data, "month");

        //add rolling total graph
        if (prefix === "NewThemeAvgTestTime" || prefix === "NewThemeAvgTTGameType")
            graphs.push(GetRollingGraph("rollingAvg", "Rolling Average", "#33FFBD"));
        else {
            graphs.push(GetRollingGraph("rollingTotal", "Rolling Total", "#33FFBD"));
        }
        studioName = itemSelection;

    }
    else if (prefix == "avgDefects") {
        graphs = avgDefectsGetGraphsL2(data, "month");
        studioName = itemSelection;
    }
    else {
        if (prefix == "newThemeOther") {
            graphs = GetStackedGraphs(data, "month", "Other", "New Theme");
            stacks = ["Other", "New Theme"];
            graphs = GetStackedRollingTotal(graphs, data, "month", stacks);
        } else if (prefix == "AllSitesNewOther") {
            graphs = GetStackedGraphs(data, "month", "Other", "New Theme");
            stacks = ["Other", "New Theme"];
            graphs = GetStackedRollingTotal(graphs, data, "month", stacks);
        } else if (prefix == "bugFixOther") {
            graphs = GetStackedGraphs(data, "month", "Other", "Bug Fix");
            stacks = ["Other", "Bug Fix"];
            graphs = GetStackedRollingTotal(graphs, data, "month", stacks);
        } else if (prefix == "defectsClosedbyTesterType") {
            graphs = GetStackedGraphs(data, "month", "Game Tester", "SDET");
            stacks = ["Game Tester", "SDET"];
            graphs = GetStackedRollingTotal(graphs, data, "month", stacks);
        } else if (prefix == "QuartersFirstPass") {
            graphs = GetGraphs(data, "quarter");
            graphs.push(GetRollingGraph("rollingTotal", "Rolling Total", "#33FFBD"));
        } else if (prefix == "edas") {
            graphs = GetStackedGraphs(data, "month", "PA", "Other");
            for (i = 0; i < graphs.length; i++) {
                graphs[i].balloonFunction = function (item, graph) {
                    var monthName = item.category;
                    var studio = graph.valueField;
                    var kitsReleasedValue = getKitsForBaloon(monthName, studio);
                    return graph.title + " of " + item.category + ": " + item.values.value + "</br> Kits Released: " + kitsReleasedValue;
                }
            }
            stacks = ["PA", "Other"];
            graphs = GetStackedRollingTotal(graphs, data, "month", stacks);
        } else if (prefix == "edasEntered" || "edaNewOnly") {
            graphs = GetStackedGraphs(data, "month", "PA", "Other");
            stacks = ["PA", "Other"];
            graphs = GetStackedRollingTotal(graphs, data, "month", stacks);
        }

        studioName = itemSelection.split('/')[0];
        useStackChart = true;
    }

    var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title
    var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
    var chartTitle_firstpart = chartTitle.split('-')[0];

    if (prefix == "QuartersFirstPass") {
        var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + "-" + studioName, yAxisTitle, "quarter", currentTheme, useStackChart);
    } else {
        var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + "- " + studioName, yAxisTitle, "month", currentTheme, useStackChart);
    }

    // add legend item click listener

    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = graphs;
    levelTwoConfig.valueAxes = [
		{
		    "id": "v1",
		    "title": yAxisTitle,
		    "stackType": "regular"
		},
		{
		    "id": "v2",
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false,
		    "position": "right",
		    "autoGridCount": false,
		    "synchronizeWith": "v1",
		    "synchronizationMultiplier": 1
		}];

    $("#" + prefix + "_chartdiv").hide();

    var L2_id = prefix + "_L2_chartdiv";

    currentPrefix = prefix + "_L2";

    addLevelChartDiv(prefix, L2_id);

    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;

    // let's add a label to go back to yearly data
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addListener("clickGraphItem", L2_handleItemClick);

    chart.addListener("init", chartInit);
    chart.addListener("rendered", L2_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    dataTableDataDict[prefix + "_dataTable"] = data; // store data in datatable lookup

    removeLoadingDiv();
}

function showLegendItem(event) {
    var shownGraphs = [];
    var chart = event.chart;

    for (i = 0; i < chart.graphs.length; i++) {
        if (chart.graphs[i]["hidden"] == false)
            shownGraphs.push(chart.graphs[i].title);
    }

    if (event.dataItem.type == "line") {
        if (currentPrefix == "ContainmentRate_L2") {
            getRollingAvg_dce();
        } else if (currentPrefix == "defectsPhaseFound_L2") {
            DefectFoundNTShowRollingPercents();
        } else if (currentPrefix == "UTPTestCasesPassFail_L2") {
            UTPTestCasesL2ShowRollingAverages();
        } else if (currentPrefix == "BVATestCasesPassFail_L2") {
            BVATestCasesL2ShowRollingAverages();
        } else if (currentPrefix == "GameTracker_L2") {
            GameTrackerL2ShowRollingAverages();
        } else if (currentPrefix == "QuartersPhaseFound_L2") {
            DefectFound2YShowRollingPercents();
        } else if (currentPrefix == "EQEScoreByPushDate_L2") {
            getRollingAverageTestTimeEQEL2();
        } else if (currentPrefix == "kitsReleased") {
            // do nothing
        } else {
            getRollingTotal();
        }
    }
    // hide all line graphs rolling total might have turned on
    for (i = 0; i < chart.graphs.length; i++) {
        if ((chart.graphs[i].type == "line") && (shownGraphs.indexOf(chart.graphs[i].title) == -1) && (chart.graphs[i].title != event.dataItem.title))
            event.chart.hideGraph(chart.graphs[i]);
    }

    chart.invalidateSize();
    chart.validateNow();
}

/* NEW METRIC STEP
* Need to add prefix here to avoid rolling total alert
*/
function getRollingTotal() {

    var stacks;

    if (currentPrefix == "kitsReleased_L2" || currentPrefix == "defectsForKitsReleased_L2" || currentPrefix == "avgDefects_L2" || currentPrefix == "ContainmentRate" ||
        currentPrefix == "NewThemeAvgTestTime_L2" || currentPrefix == "NewThemeAvgTTGameType_L2" || currentPrefix == "bvaBuildsRun_L2" ||
        currentPrefix == "bvaSuitesRun_L2" || currentPrefix == "mathThemesReleased_L2" || currentPrefix == "mathDefectsFound_L2" || 
        currentPrefix == "mathEdas_L2") {
        genericSingleRollingTotal();
    } else {
        if (currentPrefix == "newThemeOther_L2") {
            stack = ["Other", "New Theme"];
            genericStackedRollingTotal(stack);
        } else if (currentPrefix == "AllSitesNewOther_L2") {
            stack = ["Other", "New Theme"];
            genericStackedRollingTotal(stack);
        } else if (currentPrefix == "edas_L2" || currentPrefix == "edaNewOnly_L2") {
            stack = ["PA", "Other"];
            genericStackedRollingTotal(stack);
        } else if (currentPrefix == "edasEntered_L2") {
            stack = ["PA", "Other"];
            genericStackedRollingTotal(stack);
        } else if (currentPrefix == "bugFixOther_L2") {
            stack = ["Other", "Bug Fix"];
            genericStackedRollingTotal(stack);
        } else if (currentPrefix == "cn_in_L2" || currentPrefix == "cn_inEntered_L2") {
            stack = ["PA", "Other"];
            genericStackedRollingTotal(stack);
        } else if (currentPrefix == "newThemeOther" || currentPrefix == "newThemeFirstPass" || currentPrefix == "QuartersFirstPass"
            || currentPrefix == "defectsForKitsReleased" || currentPrefix == "edas" || currentPrefix == "edaNewOnly"
            || currentPrefix == "edasEntered" || currentPrefix == "cn_inEntered" || currentPrefix == "cn_in" || currentPrefix == "QuartersPhaseFound"
            || currentPrefix == "mathThemesReleased" || currentPrefix == "avgDefects" || currentPrefix == "mathEdas" || currentPrefix == "mathDefectsFound") {
            // do nothing
        } else {
            alert("getRollingTotal(): prefix option missing!");
        }

    }
}

function genericSingleRollingTotal() {
    var chart = chartDict[currentPrefix];
    var i;
    //clear Graphs
    if (currentPrefix == "QuartersPhaseFound_L2" || currentPrefix == "QuartersFirstPass_L2") {
        for (i = 0; i < 8; i++) {
            if ((chart.dataProvider[i])["rollingTotal"] !== undefined) {
                delete (chart.dataProvider[i])["rollingTotal"];
            }
        }
    } else {
        for (i = 0; i < 13; i++) {
            if ((chart.dataProvider[i])["rollingTotal"] !== undefined) {
                delete (chart.dataProvider[i])["rollingTotal"];
            }
        }
    }

    //update graphs with new %s
    var start = chart.start;
    var end = chart.end;

    var tot = 0;
    var graphName = (chart.legend.legendData[0]).title;
    /*
	for (graphName in (chart.dataProvider[0])) {
	    if(graphName!=="month")
	        break;
	}
	*/

    //iterate through the month
    for (i = start; i <= end; i++) {

        if ((chart.dataProvider[i])[graphName] !== undefined)
            tot += (chart.dataProvider[i])[graphName];

        (chart.dataProvider[i])["rollingTotal"] = tot;
    }

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingTotal"));

    chart.invalidateSize();
    chart.validateNow();
}

function genericStackedRollingTotal(stack) {
    var chart = chartDict[currentPrefix];
    var numStacks = stack.length;
    var init = 0;
    var i;
    var j;


    //clear Graphs
    for (i = 0; i < 13; i++) {
        if ((chart.dataProvider[i])["rollingTotal"] !== undefined) {
            delete (chart.dataProvider[i])["rollingTotal"];
        }

        for (j = 0; j < numStacks; j++) {
            if ((chart.dataProvider[i])["rollingTotal" + j] !== undefined) {
                delete (chart.dataProvider[i])["rollingTotal" + j];
            }
        }
    }

    //update graphs with new %s
    var start = chart.start;
    var end = chart.end;
    var generalTot = 0;
    var graphNames = [];
    var indivTot = [];

    for (i = 0; i < numStacks; i++) {
        graphNames.push((chart.legend.legendData[i]).title);
        indivTot.push(0);
    }


    //iterate through the month
    for (i = start; i <= end; i++) {
        j = 0;

        for (; j < numStacks; j++) {
            if ((chart.legend.legendData[j]).columnsArray.length > 0) {
                if ((chart.dataProvider[i])[graphNames[j]] !== undefined)
                    generalTot += (chart.dataProvider[i])[graphNames[j]];
                else
                    generalTot += 0;
            }


            if ((chart.dataProvider[i])[graphNames[j]] !== undefined)
                indivTot[j] += (chart.dataProvider[i])[graphNames[j]];

        }


        (chart.dataProvider[i])["rollingTotal"] = generalTot;

        for (j = 0; j < numStacks; j++) {
            (chart.dataProvider[i])["rollingTotal" + j] = indivTot[j];
        }
    }

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingTotal"));

    for (j = 0; j < numStacks; j++) {
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingTotal" + j));
    }

    chart.invalidateSize();
    chart.validateNow();

}

function getRollingAverageTestTime() {

    var chart = chartDict[currentPrefix];
    var i, j;
    //clear Graphs
    for (i = 0; i < 13; i++) {
        if ((chart.dataProvider[i])["rollingAvg"] !== undefined) {
            delete (chart.dataProvider[i])["rollingAvg"];
        }
    }

    //get the visible graphs
    var visibleGraph = [];
    for (j = 0; j < chart.graphs.length; j++) {
        if ((chart.graphs[j]).columnsArray.length > 0 && (chart.graphs[j]).title !== "Total") {
            visibleGraph.push((chart.graphs[j]).title);
        }

    }


    var start = chart.start;
    var end = chart.end;
    var data = extraGraphData[currentPrefix];

    var totalTime = 0;
    var totalKits = 0;
    //iterate through visibal months
    for (i = start; i <= end; i++) {

        var month = (chart.dataProvider[i]).month;
        //for all the visible graphs, update the data point
        for (j = 0; j < visibleGraph.length; j++) {
            var studio = visibleGraph[j];

            if ((data[month]).hasOwnProperty(studio)) {
                totalKits += ((data[month])[studio])[0];
                totalTime += ((data[month])[studio])[1];
            }
        }

        if (totalKits > 0)
            (chart.dataProvider[i])["rollingAvg"] = Math.round((totalTime / totalKits) * 100) / 100;
        else 
			(chart.dataProvider[i])["rollingAvg"] = 0;
    }

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAvg"));

    chart.invalidateSize();
    chart.validateNow();

}

function getRollingAverageTestTimeL2(studio) {
    var chart = chartDict[currentPrefix];
    var i;
    //clear Graphs
    for (i = 0; i < 13; i++) {
        if ((chart.dataProvider[i])["rollingAvg"] !== undefined) {
            delete (chart.dataProvider[i])["rollingAvg"];
        }
    }


    var start = chart.start;
    var end = chart.end;
    var prefix = currentPrefix.replace("_L2", "");
    var data = extraGraphData[prefix];

    var totalTime = 0;
    var totalKits = 0;
    for (i = start; i <= end; i++) {

        var month = (chart.dataProvider[i]).month;

        if (((data[month])[studio]) !== undefined) {
            totalKits += ((data[month])[studio])[0];
            totalTime += ((data[month])[studio])[1];
            (chart.dataProvider[i])["rollingAvg"] = Math.round((totalTime / totalKits) * 100) / 100;
        }

        if (totalKits > 0)
            (chart.dataProvider[i])["rollingAvg"] = Math.round((totalTime / totalKits) * 100) / 100;
        else {
            (chart.dataProvider[i])["rollingAvg"] = 0;
        }
    }

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAvg"));

    chart.invalidateSize();
    chart.validateNow();
}

function calculateRollingPercentagesDefectsFound(chart, start, end) {

    var tot_pre = 0,
		tot_post = 0,
		tot_pa = 0,
		tot_early = 0;
    var tot = 0;

    // clear previous entries
    for (var i = 0; i < end; i++) {
        delete (chart.dataProvider[i])["rollingPrePAPercent"];
        delete (chart.dataProvider[i])["rollingPostPAPercent"];
        delete (chart.dataProvider[i])["rollingPaPercent"];
        delete (chart.dataProvider[i])["rollingEarlyTestingPercent"];
    }

    //iterate through the month
    for (var i = start; i <= chart.dataProvider.length; i++) {

        if ((chart.legend.legendData[0]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).prePA;

        //check if rejected or "rejected %" is active
        if ((chart.legend.legendData[1]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).postPA;

        //check if pending or "pending %" is active
        if ((chart.legend.legendData[2]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).pa;

        //check if rejected or "na %" is active
        if ((chart.legend.legendData[3]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).earlyTesting;

        tot_pre += (chart.dataProvider[i]).prePA;
        tot_post += (chart.dataProvider[i]).postPA;
        tot_pa += (chart.dataProvider[i]).pa;
        tot_early += (chart.dataProvider[i]).earlyTesting;


        (chart.dataProvider[i])["rollingPrePAPercent"] = (tot_pre / tot * 100).toFixed(2);
        (chart.dataProvider[i])["rollingPostPAPercent"] = (tot_post / tot * 100).toFixed(2);
        (chart.dataProvider[i])["rollingPaPercent"] = (tot_pa / tot * 100).toFixed(2);
        (chart.dataProvider[i])["rollingEarlyTestingPercent"] = (tot_early / tot * 100).toFixed(2);
    }
}

function ShowTrendLine(quarters, graphId, type) {
    // used for first pass charts
    var chart = chartDict[currentPrefix];
    var start = chart.start;
    var end = chart.end;

    calculateRollingPercentages(chart, start, end);

    if (type == "showItem")
        chart.showGraph(chart.getGraphById(graphId));
    else if (type == "hideItem")
        chart.hideGraph(chart.getGraphById(graphId));

    chart.invalidateSize();
    chart.validateNow();
}

function ShowRollingTotal(quarters) {
    /*if (currentPrefix == "newThemeFirstPass_L2")
	    var chart = chartDict["newThemeFirstPass_L2"];
	else if (currentPrefix == "QuartersFirstPass_L2")
	    var chart = chartDict["QuartersFirstPass_L2"]
	else if (currentPrefix == "AllSitesFirstPass_L2")
		var chart = chartDict["QuartersFirstPass_L2"];
	*/
    var chart = chartDict[currentPrefix];
    var limit;
    //clear Graphs
    if (quarters)
        limit = 8;
    else
        limit = 13;

    for (var i = 0; i < limit; i++) {
        if ((chart.dataProvider[i])["rollingAppPercent"] !== undefined) {
            delete (chart.dataProvider[i])["rollingAppPercent"];
            delete (chart.dataProvider[i])["rollingRejPercent"];
            delete (chart.dataProvider[i])["rollingPenPercent"];
            delete (chart.dataProvider[i])["rollingNAPercent"];
        }
    }

    //update graphs with new %s
    var start = chart.start;
    var end = chart.end;
    calculateRollingPercentages(chart, start, end);

    //show graphs
    if ((chart.legend.legendData[0]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAppPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingAppPercent"));

    //Turn off rejected if  "rejected %" are off
    if ((chart.legend.legendData[1]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingRejPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingRejPercent"));

    //Turn off pending if "pending %" are off
    if ((chart.legend.legendData[2]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPenPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPenPercent"));

    if ((chart.legend.legendData[3]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingNAPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingNAPercent"));


    //Turn off rejected if  "rejected %" are off
    if ((chart.legend.legendData[1]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingRejPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingRejPercent"));

    //Turn off pending if "pending %" are off
    if ((chart.legend.legendData[2]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPenPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPenPercent"));

    if ((chart.legend.legendData[3]).columnsArray.length > 0)
        chartDict[currentPrefix].showGraph(chart.getGraphById("rollingNAPercent"));
    else
        chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingNAPercent"));


    chart.invalidateSize();
    chart.validateNow();
}

function calculateRollingPercentages(chart, start, end) {

    var tot_app = 0,
		tot_rej = 0,
		tot_pen = 0,
		tot_na = 0;
    var tot = 0;

    // clear previous entries
    for (var i = 0; i < chart.dataProvider.length; i++) {
        delete (chart.dataProvider[i])["rollingAppPercent"];
        delete (chart.dataProvider[i])["rollingRejPercent"];
        delete (chart.dataProvider[i])["rollingPenPercent"];
        delete (chart.dataProvider[i])["rollingNAPercent"];
    }

    //iterate through the month
    for (var i = start; i <= end; i++) {

        if ((chart.legend.legendData[0]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).approved;

        //check if rejected or "rejected %" is active
        if ((chart.legend.legendData[1]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).rejected;

        //check if pending or "pending %" is active
        if ((chart.legend.legendData[2]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).pending;

        //check if rejected or "na %" is active
        if ((chart.legend.legendData[3]).columnsArray.length > 0)
            tot += (chart.dataProvider[i]).na;

        tot_app += (chart.dataProvider[i]).approved;
        tot_rej += (chart.dataProvider[i]).rejected;
        tot_pen += (chart.dataProvider[i]).pending;
        tot_na += (chart.dataProvider[i]).na;


        (chart.dataProvider[i])["rollingAppPercent"] = (tot_app / tot * 100).toFixed(2);
        (chart.dataProvider[i])["rollingRejPercent"] = (tot_rej / tot * 100).toFixed(2);
        (chart.dataProvider[i])["rollingPenPercent"] = (tot_pen / tot * 100).toFixed(2);
        (chart.dataProvider[i])["rollingNAPercent"] = (tot_na / tot * 100).toFixed(2);
    }

}

//used by first_pass 
function adjustL2Text(graphDataItem, graph) {

    //var chart = chartDict["newThemeFirstPass_L2"];

    var chart = chartDict["newThemeFirstPass_L2"];
    var data = chartDataDict["newThemeFirstPass_L2"];

    var total = graphDataItem.dataContext.approved + graphDataItem.dataContext.rejected + graphDataItem.dataContext.pending;

    var oldPercentage = graphDataItem.values.value;
    var newTot = 0;

    //add approved value if it's visible
    if ((chart.legend.legendData[0]).columnsArray.length > 0)
        newTot = newTot + graphDataItem.dataContext.approved;


    //add rejected value if it's visible
    if ((chart.legend.legendData[1]).columnsArray.length > 0)
        newTot = newTot + graphDataItem.dataContext.rejected;

    //add pending value if it's visible
    if ((chart.legend.legendData[2]).columnsArray.length > 0)
        newTot = newTot + graphDataItem.dataContext.pending;

    var originalValue = (oldPercentage / 100) * total;
    var newPercentage = Math.round(originalValue) / newTot;

    graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;

    return "<b>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + Math.round(graphDataItem.values.percents * 100) / 100 + "%</b>";
}

//used by first_pass
function adjustL2LabelText(graphDataItem, graph) {
    graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;
    if (graphDataItem.values.value < 1)
        return "";
    else
        return Math.round((graphDataItem.values.percents * 100) / 100) + "%";
}

function L2_handleRendered(event) {

    if (hasInitSubCatMonth) // if need to go to level 3 
    {
        var prefix = event.chart.titles[0].id;

        removeAllActiveClass();
        //$('#navsidebar li.active').removeClass('active');
        var $subnav = $("<li id='link_" + prefix + "_3' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + initMonth + "/" + initSubCategory + "' onclick='loadMetricView(\"" + prefix + "\", 3)'>" + initMonth + "</a></li>");
        $("#link_" + prefix + "_2").after($subnav);

        hasInitSubCatMonth = false;
        if (prefix == "newThemeFirstPass") {
            FirstPass_DrillDownToLevel3(prefix, initMonth, initSubCategory, 'Approved');
        } else if (prefix == "AllSitesFirstPass") {
            FirstPass_DrillDownToLevel3_SP(prefix, initMonth, initSubCategory, 'Approved');
        } else if (prefix == "defectsPhaseFound") {
            DefectsFoundNT_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        } else if (prefix == "defectsClosedbyTesterType") {
            DefectsFoundTT_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        } else if (prefix == "edasEntered") {
            edasEntered_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        } else if (prefix == "ContainmentRate") {
            DrillDownToLevel3_dce(prefix, initMonth, initSubCategory, "DCE");
        } else if (prefix == "GameTracker") {
            GameTracker_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        }
        else if (prefix == "KitCountForTools")
            KitCountForTools_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        else if (prefix == "mathThemesReleased") {
            MathThemesReleased_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        }
        else if (prefix == "avgDefects") {
            avgDefects_DrillDownToLevel3(prefix, initMonth, initSubCategory, '');
        }
        else DrillDownToLevel3(prefix, initMonth, initSubCategory);

        $("#helpicon").unbind('click');
        $("#helpicon").click(function () {
            var URL = "HelpPage/helpPage.html#" + prefix + "_L3";
            window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
        });

    } else resetCurrentHash();

    removeLoadingDiv();
}

function handleItemClick(event) {
    show_loadingDiv("Loading Data...");
    var prefix = event.chart.titles[0].id;
    var $subnav;

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', analyticsDict[prefix] + " L2");

    if (document.getElementById("link_" + prefix + "_Cat") != null) {
        $("#link_" + prefix + "_Cat").remove();
    }

    if (event.graph.id == "totalline") // line item click
    {

        $subnav = $("<li id='link_" + prefix + "_Cat' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + event.item.category + "' onclick='loadMetricView(\"" + prefix + "\", 1.5)'>" + event.item.category + "</a></li>");
        if (document.getElementById("link_" + prefix + "_Cat") == null) {
            removeAllActiveClass();
            //$('#navsidebar li.active').removeClass('active');
            $("#link_" + prefix).after($subnav);
        }
        DrillDownToMonth(prefix, event.item.category);

    } else {
        var subcategory = event.graph.valueField;

        if (prefix == "KitCountForTools")
            subcategory = event.item.dataContext.tool;

        $subnav = $("<li id='link_" + prefix + "_2' class='subnav active'><a href='#' title='#/metric/" + prefix + "/all/" + subcategory + "' onclick='loadMetricView(\"" + prefix + "\", 2)'>" + subcategory + "</a></li>");

        if (document.getElementById("link_" + prefix + "_2") == null) {
            removeAllActiveClass();
            //$('#navsidebar li.active').removeClass('active');
            if (document.getElementById("link_" + prefix + "_Cat") == null)
                $("#link_" + prefix).after($subnav);
            else $("#link_" + prefix + "_Cat").after($subnav); // if going from Category view to second drill down
        }

        if (prefix == "newThemeFirstPass")
            FirstPass_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "AllSitesFirstPass")
            FirstPass_DrillDownToLevel2_SP(prefix, subcategory);
        else if (prefix == "defectsPhaseFound")
            DefectsFoundNT_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "QuartersPhaseFound")
            TwoYearDefectsFound_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "defectsClosedbyTesterType")
            DefectsFoundTT_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "ContainmentRate")
            DrillDownToStudioDce(prefix, subcategory);
        else if (prefix == "BVATestCasesPassFail")
            BVATestCases_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "UTPTestCasesPassFail")
            UTPTestCases_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "GameTracker")
            GameTracker_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "QuartersFirstPass")
            TwoYearFirstPassDrillDownToLevel2(prefix, subcategory);
        else if (prefix == "KitCountForTools")
            KitCountForTools_DrillDownToLevel2(prefix, event.item.dataContext.tool);
        else if (prefix == "EQEScoreByPushDate")
            EQEScoreByPushDate_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "cn_in")
            CN_IN_DrillDownToLevel2(prefix, subcategory);
        else if (prefix == "cn_inEntered")
            CN_INEntered_DrillDownToLevel2(prefix, subcategory);

        else DrillDownToStudio(prefix, subcategory);

        $("#helpicon").unbind('click');
        $("#helpicon").click(function () {
            var URL = "HelpPage/helpPage.html#" + prefix + "_L2";
            window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
        });
    }

}

var themeLookUp;

/* NEW METRIC STEP
* Need to add prefix here to drill down to level 3
*/
function L2_handleItemClick(event) {
    show_loadingDiv("Loading Data...");
    var prefix = event.chart.titles[0].id;
    var month = event.item.category;
    var subcategory = event.graph.valueField;

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', analyticsDict[prefix] + " L3");

    var $subnav = $("<li id='link_" + prefix + "_3' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + month + "/" + subcategory + "' onclick='loadMetricView(\"" + prefix + "\", 3)'>" + month + "</a></li>");

    if (prefix == "newThemeFirstPass")
        FirstPass_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "AllSitesFirstPass")
        FirstPass_DrillDownToLevel3_SP(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "QuartersFirstPass")
        TwoYearFirstPassDrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "defectsPhaseFound")
        DefectsFoundNT_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "QuartersPhaseFound")
        TwoYearDefectsFoundDrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "edas") 
        edaCountDrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "edasEntered") 
        edasEntered_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "edaNewOnly")
        edaNewOnlyDrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "defectsClosedbyTesterType")
        DefectsFoundTT_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "ContainmentRate")
        DrillDownToLevel3_dce(prefix, month, subcategory, event.graph.title);
    else if (prefix == "bvaBuildsRun")
        BVABuildsRun_DrillDownToLevel3(prefix, month, subcategory);
    else if (prefix == "bvaSuitesRun")
        BVASuitesRun_DrillDownToLevel3(prefix, month, subcategory);
    else if (prefix == "BVATestCasesPassFail")
        BVATestCases_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "UTPTestCasesPassFail")
        UTPTestCases_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "cn_in") 
        CN_IN_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "cn_inEntered")
        CN_IN_EnteredDrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "GameTracker")
        GameTracker_DrillDownToLevel3(prefix, month, event.item.dataContext.studio, subcategory);
    else if (prefix == "KitCountForTools")
        KitCountForTools_DrillDownToLevel3(prefix, month, event.item.dataContext.tool, subcategory);
    else if (prefix == "EQEScoreByPushDate")
        EQEScoreByPushDate_DrillDownToLevel3(prefix, currentStudio);
    else if (prefix == "mathThemesReleased")
        MathThemesReleased_DrillDownToLevel3(prefix, month, event.graph.title, subcategory);
    else if (prefix == "mathDefectsFound")
        MathDefectsFound_DrillDownToLevel3(prefix, month, event.graph.title, subcategory);
    else if (prefix == "mathEdas")
        MathEdas_DrillDownToLevel3(prefix, month, event.graph.title, subcategory);
    else if (prefix == "avgDefects")
        avgDefects_DrillDownToLevel3(prefix, month, event.graph.title, subcategory);
    else DrillDownToLevel3(prefix, month, subcategory);

    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#" + prefix + "_L3";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });

    if (document.getElementById("link_" + prefix + "_3") == null) {
        removeAllActiveClass();
        $("#link_" + prefix + "_2").after($subnav);
    }
}

function DrillDownToLevel3(prefix, month, subcategory) {

    currentMonth = month;
    currentStudio = subcategory;

    var returnData;
    if (prefix.startsWith("eda"))
        returnData = GetDefectCountPerTheme(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
    else if (prefix.startsWith("NewThemeAvgTTGameType") || prefix.startsWith("NewThemeAvgTestTime"))
        returnData = GetTestTimeCountPerTheme(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
    else
        returnData = GetDefectCountPerTheme(prefix, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], month, subcategory, studio_alias);

    var data = returnData.chart;
    themeLookUp = returnData.themes;

    $("#" + prefix + "_L2_chartdiv").hide();

    var L3_id = prefix + "_L3_chartdiv";

    currentPrefix = prefix + "_L3";

    addLevelChartDiv(prefix, L3_id);

    //var levelThreeConfig = generateConfig(prefix, "Released Themes - " + subcategory + " for " + month, "Defects", "theme", currentTheme, false);

    var levelThreeConfig;

    if (prefix.startsWith("eda"))
        levelThreeConfig = generateConfig(prefix, "Released Themes - " + subcategory + " for " + month, "EDA Count", "theme", currentTheme, false);
    else if (prefix.startsWith("NewThemeAvgTestTime") || prefix.startsWith("NewThemeAvgTTGameType"))
        levelThreeConfig = generateConfig(prefix, "Released Themes - " + subcategory + " for " + month, "Work Days", "theme", currentTheme, false);
    else
        levelThreeConfig = generateConfig(prefix, "Released Themes - " + subcategory + " for " + month, "Defects", "theme", currentTheme, false);
    /*
	for (var i = 0; i < data.length; i++) {
	    for (var item in data[i]) {
            if (item === "fullTheme" || item === "theme") {
                (data[i])[item] = (data[i])[item].replace(/'/g, " ");
                (data[i])[item] = (data[i])[item].replace(/\//g, " ");
                (data[i])[item] = (data[i])[item].replace(/"/g, " ");
                (data[i])[item] = (data[i])[item].replace(/\$/g, " ");
                (data[i])[item] = (data[i])[item].replace(/%/g, " ");
                (data[i])[item] = (data[i])[item].replace(/_/g, " ");
                (data[i])[item] = (data[i])[item].replace(/\(/g, " ");
                (data[i])[item] = (data[i])[item].replace(/\)/g, " ");
            }
        }
    }*/

    levelThreeConfig.dataProvider = data;


    levelThreeConfig.legend.enabled = false;
    levelThreeConfig.graphs = [{
        balloonFunction: function (graphDataItem, graph) {
            var fulltheme = graphDataItem.dataContext.fullTheme;
            var value = graphDataItem.dataContext.value;
            return fulltheme + ": " + value;
        },
        fillAlphas: 1,
        id: "AmGraph_1",
        type: "column",
        valueField: "value"
    }];
    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        categoryBalloonFunction: function (category) {
            return themeLookUp[category];
        },
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };
    levelThreeConfig.categoryAxis.autoWrap = true;

    delete levelThreeConfig.chartScrollbar.graph;
    delete levelThreeConfig.chartScrollbar.graphType;
    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    var labelText = "Sort By Defects";

    if (prefix.startsWith("NewThemeAvgTestTime") || prefix.startsWith("NewThemeAvgTTGameType"))
        labelText = "Sort By Work Days";

    chart.addLabel(
		'!180', '20',
		labelText,
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortDefectsThemeL3ByDefectCount("' + currentPrefix + '", true);');
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addListener("init", chartInit);
    chart.addListener("clickGraphItem", L3_handleItemClick);
    chart.addListener("rendered", L3_rendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    //var dataTable = GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], month, subcategory, studio_alias);

    var dataTable;
    if (prefix.startsWith("eda"))
        dataTable = GetDataForEntry(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
    else if (prefix.startsWith("NewThemeAvg")) {
        dataTable = data;
    } else
        dataTable = GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], month, subcategory, studio_alias);


    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    //clearTable(prefix + "_dataTable");
    //drawTable(dataTable, prefix + "_dataTable");
}

function SortDefectsThemeL3ByDefectCount(prefix, sortByDefects) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix];
    var labelIndex;

    var switchText = "Sort By Defects";

    if (prefix.startsWith("NewThemeAvgTestTime"))
        switchText = "Sort By Work Days";

    if (sortByDefects) {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.value > b.value) return -1;
            if (a.value < b.value) return 1;
            return 0;
        });

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
        chart.allLabels[labelIndex].text = "Sort By Theme Name";
        chart.allLabels[labelIndex].url = 'javascript:SortDefectsThemeL3ByDefectCount("' + prefix + '", false);';
    } else {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.theme < b.theme) return -1;
            if (a.theme > b.theme) return 1;
            return 0;
        });
        labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort By Theme Name");
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortDefectsThemeL3ByDefectCount("' + prefix + '", true);';
    }
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}

function L3_rendered(event) {
    removeLoadingDiv();

    chart = event.chart;
    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", L3_handleCategoryClick);
    resetCurrentHash();

    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo); // zoom into the last 5
    chart.invalidateSize();
}

function BVA_L3_rendered(event) {
    removeLoadingDiv();

    chart = event.chart;
    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", BVA_L3_handleCategoryClick);
    resetCurrentHash();

    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo); // zoom into the last 5
    chart.invalidateSize();
}

function L3_handleCategoryClick(event) {
    show_loadingDiv("Loading Data...");
    var prefix = event.chart.titles[0].id;

    var dataItem = event.serialDataItem.dataContext;
    //var targetTheme = dataItem.theme;
    var targetKit = dataItem.kit;
    var month = dataItem.month;
    //var defectCount = dataItem.value;

    currentPrefix = prefix + "_L3";

    showAndLoadScoreCard(currentPrefix, targetKit, month);
}

function BVATestCase_L3_handleCategoryClick(event) {
    show_loadingDiv("Loading Data...");
    var prefix = event.chart.titles[0].id;
    var dataItem = event.serialDataItem.dataContext;
    var caseName = dataItem.CaseName;
    var month = dataItem.month;

    if (!prefix.endsWith("_L3"))
        currentPrefix = prefix + "_L3";

    showBVATestCaseSC(currentPrefix, caseName, month);
}

function UTPTestCase_L3_handleCategoryClick(event) {
    show_loadingDiv("Loading Data...");
    var prefix = event.chart.titles[0].id;
    var dataItem = event.serialDataItem.dataContext;
    var caseName = dataItem.CaseName;
    var month = dataItem.month;

    if (!prefix.endsWith("_L3"))
        currentPrefix = prefix + "_L3";

    showBVATestCaseSC(currentPrefix, caseName, month);
}

function BVA_L3_handleCategoryClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;
    var dataItem = event.serialDataItem.dataContext;
    var buildFile = dataItem.buildFile;
    var month = dataItem.month;

    currentPrefix = prefix + "_L3";

    showBVAScoreCard(currentPrefix, buildFile, month);
}

function L3_handleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;

    var dataItem = event.item.dataContext;
    //var targetTheme = dataItem.theme;
    var targetKit = dataItem.kit;
    if (!targetKit) targetKit = dataItem.Kit;
    var month = dataItem.month;
    //var defectCount = dataItem.value;

    currentPrefix = prefix + "_L3";
    level = 4;
    showAndLoadScoreCard(currentPrefix, targetKit, month);
}

function BVATestCase_L3_handleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;
    var dataItem = event.item.dataContext;
    var caseName = dataItem.CaseName;
    var month = dataItem.month;

    if (!prefix.endsWith("_L3"))
        currentPrefix = prefix + "_L3";
    level = 4;
    //get_bvaTestCasesByBuildFile(buildFile, DefectsFoundCallbackFunc);
    showBVATestCaseSC(currentPrefix, caseName, month);
}

function KitCountForTools_L3_handleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;
    var dataItem = event.item.dataContext;
    var kit = dataItem.KitNumber;
    var month = dataItem.month;

    if (!prefix.endsWith("_L3"))
        currentPrefix = prefix + "_L3";
    level = 4;
    showAndLoadScoreCard(currentPrefix, kit, month);
}

function BVA_L3_handleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;
    var dataItem = event.item.dataContext;
    var buildFile = dataItem.buildFile;
    var month = dataItem.month;

    currentPrefix = prefix + "_L3";
    level = 4;
    //get_bvaTestCasesByBuildFile(buildFile, DefectsFoundCallbackFunc);
    showBVAScoreCard(currentPrefix, buildFile, month);
}

function UTPTestCase_L3_handleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;
    var dataItem = event.item.dataContext;
    var caseName = dataItem.CaseName;
    var month = dataItem.month;

    if (!prefix.endsWith("_L3"))
        currentPrefix = prefix + "_L3";
    level = 4;
    //get_bvaTestCasesByBuildFile(buildFile, DefectsFoundCallbackFunc);
    showBVATestCaseSC(currentPrefix, caseName, month);
}

function GameTracker_L3_handleItemClick(event) {
    /*show_loadingDiv("Loading Data...");

	var prefix = event.chart.titles[0].id;
	var dataItem = event.item.dataContext;
	var caseName = dataItem.CaseName;
	var month = dataItem.month;

	if (!prefix.endsWith("_L3"))
	    currentPrefix = prefix + "_L3";
	level = 4;
	//get_bvaTestCasesByBuildFile(buildFile, DefectsFoundCallbackFunc);
	showBVATestCaseSC(currentPrefix, caseName, month);
	*/
    var game = event.item.dataContext.game;
    game = game.replace("GI", "GAME");
    var url = "http://acloud.insideigt.com/GameTrack/client/#/contents/none/" + game;
    window.open(url, "_blank");
}

function UTP_L3_handleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;
    var dataItem = event.item.dataContext;
    var buildFile = dataItem.buildFile;
    var month = dataItem.month;

    currentPrefix = prefix + "_L3";
    level = 4;
    //get_bvaTestCasesByBuildFile(buildFile, DefectsFoundCallbackFunc);
    showBVAScoreCard(currentPrefix, buildFile, month);
}

function revertToTopLevel(prefix) {
    if (level > 1) {
        if (prefix.endsWith("_L2") || prefix.endsWith("_L3"))
            prefix = prefix.substr(0, prefix.length - 3); // get base prefix if needed

        removeScoreCardDiv();
        removeBVAScoreCardDiv();
        removeBVATestCaseSCDiv();

        while (level > 1) {
            var L_id = prefix + "_L" + level + "_chartdiv";
            $("#" + L_id).remove(); // remove L# div     
            level--;
        }
    }
    currentPrefix = prefix;
    currentMonth = "all";
    currentStudio = "";

    $('#navsidebar li.subnav').remove(); // remove all subnavs	

    resetCurrentHash();
    $("#" + prefix + "_chartdiv").show();
    $('#' + prefix + "_chart_link").show();
    if ($("#link_" + prefix).hasClass("active") == false)
        $("#link_" + prefix).addClass("active");
    showHideDataTable(prefix + "_dataTable", "100%", false);
    $("#" + prefix + "_dataTable_group").show();
    $("#" + prefix + "_dataTable_link").show();
    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#" + currentPrefix;
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });
    showTableChartsHeaders(parent_prefix);
}

function removeBVATestCaseSCDiv() {
    if (document.getElementById("bva_testCaseSC") !== null) {
        $("#bva_testCaseSC").remove();
    }
}

function removeBVAScoreCardDiv() {
    if (document.getElementById("bva_scorecard") !== null) {
        $("#bva_scorecard").remove();
    }
}

function removeScoreCardDiv() {
    if (document.getElementById("scoreCard") !== null) {
        $("#scoreCard").remove();
    }
}

function showAndLoadScoreCard(prefix, targetKit, month) {

    if (targetKit) {
        removeScoreCardDiv();

        var tempStr = prefix;
        if (prefix.endsWith("_L2") || prefix.endsWith("_L3"))
            tempStr = prefix.substr(0, prefix.length - 3);

        var $subnav = $("<li id='link_" +
            tempStr +
            "_4' class='subnav active'><a href='#' title='#/kit/" +
            targetKit +
            "' onclick='onclick='loadMetricView(\"" +
            prefix +
            "\", 4)'>" +
            targetKit +
            "</a></li>");

        if (document.getElementById("link_" + tempStr + "_4") == null) {
            removeAllActiveClass();

            if (level > 1)
                $("#link_" + tempStr + "_3").after($subnav);
            else $("#link_" + tempStr).after($subnav);
        }

        level = 4;

        //prefix + "_L3_chartdiv"

        if ($("#dashboardContainer").is(":visible")) {
            $("#metric_kitPage").removeClass('hide');
            $("#metric_kitPage").addClass('active');

            var $activediv = $("#dashboardContainer");
            $activediv.removeClass('active');
            $activediv.addClass('hide');
        } else {
            /*
            $("#metric_kittestlist_header").hide();
            $("#kittestlist_MonthDropDown").hide();
            $("#kittestlist_StudioDropDown").hide();

            $("#metric_currentMathStates_header").hide();
            $("#currentMathStates_MonthDropDown").hide();
            $("#currentMathStates_StudioDropDown").hide();

            $("#metric_kitreleaselist_header").hide();
            $("#kitreleaselist_MonthDropDown").hide();
            $("#kitreleaselist_StudioDropDown").hide();
            */
            hideTableChartsHeaders("kittestlist");
            hideTableChartsHeaders("currentMathStates");
            hideTableChartsHeaders("kitreleaselist");
            $("#" + prefix + "_chartdiv").hide();
            $("#" + tempStr + "_dataTable_link").hide();
            $("#" + tempStr + "_dataTable_group").hide();
            $('#' + prefix + "_chart_link").hide();
        }


        //show_loadingDiv("Loading Kit Details...", "#page-wrapper", true, "100%", 0, 0);
        var $div = addScoreCardDiv(tempStr, "scoreCard");
        $div.load("scorecard.html #scorecard",
            function() {
                loadScoreCardData(prefix, targetKit, "#/" + hasher.getHash());
                resetCurrentHash();
            });
    } else {
        alert("There is no matrix kit data for this game.")
    }
}

function hideTableChartsHeaders(prefix) {
    $("#metric_"+prefix+"_header").hide();
    $("#"+prefix+"_MonthDropDown").hide();
    $("#" + prefix + "_StudioDropDown").hide();
}

function showTableChartsHeaders(prefix) {
    $("#metric_" + prefix + "_header").show();
    $("#" + prefix + "_MonthDropDown").show();
    $("#" + prefix + "_StudioDropDown").show();
}

function showBVAScoreCard(prefix, targetBuild, month) {
    removeBVAScoreCardDiv();
    var tempStr = prefix;
    if (prefix.endsWith("_L2") || prefix.endsWith("_L3"))
        tempStr = prefix.substr(0, prefix.length - 3);

    var $subnav = $("<li id='link_" + tempStr + "_4' class='subnav active'><a href='#' title='#/bvaBuild/" + targetBuild + "' onclick='onclick='loadMetricView(\"" + prefix + "\", 4)'>" + targetBuild + "</a></li>");

    if (document.getElementById("link_" + tempStr + "_4") == null) {
        removeAllActiveClass();

        if (level > 1)
            $("#link_" + tempStr + "_3").after($subnav);
        else $("#link_" + tempStr).after($subnav);
    }

    level = 4;

    if ($("#dashboardContainer").is(":visible")) {
        $("#metric_kitPage").removeClass('hide');
        $("#metric_kitPage").addClass('active');

        var $activediv = $("#dashboardContainer");
        $activediv.removeClass('active');
        $activediv.addClass('hide');
    } else {
        $("#" + prefix + "_chartdiv").hide();
        $("#" + tempStr + "_dataTable_link").hide();
        $("#" + tempStr + "_dataTable_group").hide();
        $('#' + prefix + "_chart_link").hide();
    }

    show_loadingDiv("Loading BVA Build Details...", "#page-wrapper", true, "100%", 0, 0);
    var $div = BVAaddScoreCardDiv(tempStr, "bva_scorecard");
    $div.load("bva_scorecard.html #bva_scorecard", function () {
        loadBVAScoreCardData(prefix, targetBuild, "#/" + hasher.getHash());
        resetCurrentHash();
    });
}

function showBVATestCaseSC(prefix, targetCase, month) {
    removeBVATestCaseSCDiv();
    var tempStr = prefix;
    if (prefix.endsWith("_L2") || prefix.endsWith("_L3"))
        tempStr = prefix.substr(0, prefix.length - 3);

    var $subnav = $("<li id='link_" + tempStr + "_4' class='subnav active'><a href='#' title='#/testCase/" + targetCase + "' onclick='onclick='loadMetricView(\"" + prefix + "\", 4)'>" + targetCase + "</a></li>");

    if (document.getElementById("link_" + tempStr + "_4") == null) {
        removeAllActiveClass();

        if (level > 1)
            $("#link_" + tempStr + "_3").after($subnav);
        else $("#link_" + tempStr).after($subnav);
    }

    level = 4;

    //prefix + "_L3_chartdiv"
    if ($("#dashboardContainer").is(":visible")) {
        $("#metric_kitPage").removeClass('hide');
        $("#metric_kitPage").addClass('active');

        var $activediv = $("#dashboardContainer");
        $activediv.removeClass('active');
        $activediv.addClass('hide');
    } else {
        $("#" + prefix + "_chartdiv").hide();
        $("#" + tempStr + "_dataTable_link").hide();
        $("#" + tempStr + "_dataTable_group").hide();
        $('#' + prefix + "_chart_link").hide();
    }

    show_loadingDiv("Loading BVA Test Case Details...", "#page-wrapper", true, "100%", 0, 0);
    var $div = BVAaddTestCaseSCDiv(tempStr, "bva_testCaseSC");
    $div.load("bva_testCaseSC.html #bva_testCaseSC", function () {
        loadBVATestCaseSCData(prefix, targetCase, "#/" + hasher.getHash());
        resetCurrentHash();
    });
}

function addLevelChartDiv(prefix, div_id) {
    var $div = $("<div/>", {
        id: div_id
    })
		.css("background-color", bgColor)
		.css("z-index", 50) // more then chartdiv but less then loading div
		.css("width", "100%")
		.css("height", $("#" + prefix + "_chartdiv").css("height"))
		.css("top", $("#" + prefix + "_chartdiv").offset().top)
		.css("left", $("#" + prefix + "_chartdiv").offset().left);
    $("#metric_" + prefix).prepend($div);
}

function addScoreCardDiv(prefix, div_id) {

    var parent_id = "#metric_" + prefix;

    var $div = $("<div/>", {
        id: div_id
    })
		.css("background-color", bgColor)
		.css("z-index", 90) // more then L2/L3 but less then loading div
		.css("width", "100%")
		.css("color", "#FFFFFF")
		.css("height", "100%")
		.css("top", "0")
		.css("left", "0");
    $(parent_id).prepend($div);

    $("#helpicon").unbind('click');
    $("#helpicon").click(function () {
        var URL = "HelpPage/helpPage.html#kitPage";
        window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
    });
    return $div;
}

function resetChart(prefix, title) {
    chart = chartDict[prefix];
    chartData = chartDataDict[prefix];
    currentMonth = "all";
    currentStudio = "";
    if (document.getElementById("link_" + prefix + "_Cat") != null) {
        $("#link_" + prefix + "_Cat").remove();
    }
    resetCurrentHash();

    if (chart && chartData) {
        showHideDataTable(prefix + "_dataTable", "100%", false);
        chart.dataProvider = chartData;
        //toggleTotalLine(chart, true);

        // remove the "Go back" label
        chart.allLabels = [];
        chart.startDuration = 0;
        chart.titles[0].text = title;

        chart.invalidateSize();
        chart.validateData();
        chart.animateAgain();
        dataTableDataDict[prefix + "_dataTable"] = chartData;
        //drawTable(chartData, prefix + "_dataTable");
    }
}

function GoUpLevel(prefix, target_level) {
    var L_id;
    removeAllActiveClass();

    if (level == undefined || level < 1)
        level = 1;

    if (target_level == undefined)
        target_level = level - 1;

    while (target_level < level) {
        if (level == 4) {
            removeScoreCardDiv();
            $('#link_' + prefix + "_4").remove();
            if (target_level == 3) {
                var tempStr = prefix;
                if (prefix.endsWith("_L3"))
                    tempStr = prefix.substr(0, prefix.length - 3);
                $("#" + tempStr + "_L" + target_level + "_chartdiv").show();
                $("#" + tempStr + "_L" + target_level + "_dataTable_group").show();
                $("#" + tempStr + "_L" + target_level + "_dataTable_link").show();
                $('#link_' + prefix + "_3").addClass('active');


            }

            showTableChartsHeaders("kittestlist");
            showTableChartsHeaders("currentMathStates");
            showTableChartsHeaders("kitreleaselist");
        }
        if (level == 3) {
            L_id = prefix + "_L" + level + "_chartdiv";
            $("#" + L_id).remove(); // remove L# div  
            $('#link_' + prefix + "_3").remove();
            if (target_level == 2) {
                currentPrefix = prefix + "_L2";
                currentMonth = "all";
                resetCurrentHash();
                chart = chartDict[currentPrefix];
                chartData = chartDataDict[currentPrefix];


                $("#" + prefix + "_L2_chartdiv").show();
                $('#link_' + prefix + "_2").addClass('active');
                chart.invalidateSize();
                /*chart.validateData();
				chart.animateAgain();*/

                dataTableDataDict[prefix + "_dataTable"] = chartData;
                showHideDataTable(prefix + "_dataTable", "100%", false);

                // if theme is first pass, show prefix_chart_link button 
                if (prefix == "newThemeFirstPass")
                    $("#" + prefix + "_chart_link").show();
                else if (prefix == "AllSitesFirstPass")
                    $("#" + prefix + "_chart_link").show();
            }


        }
        if (level == 2) {
            L_id = prefix + "_L" + level + "_chartdiv";
            $("#" + L_id).remove(); // remove L# div  
            $('#link_' + prefix + "_2").remove();
            if (target_level == 1) {
                currentPrefix = prefix;
                currentMonth = "all";
                currentStudio = "";
                resetCurrentHash();
                chart = chartDict[prefix];
                chartData = chartDataDict[prefix];

                dataTableDataDict[prefix + "_dataTable"] = chartData;
                showHideDataTable(prefix + "_dataTable", "100%", false);

                $("#" + prefix + "_chart_link").show();
                $("#" + prefix + "_chartdiv").show();
                $('#link_' + prefix).addClass('active');
                chart.invalidateSize();
                /*chart.validateData();
				chart.animateAgain();*/
            } else if (target_level == 1.5) {
                currentPrefix = prefix;
                currentStudio = "";
                resetCurrentHash();
                chart = chartDict[prefix];
                chartData = chartDataDict[prefix];

                dataTableDataDict[prefix + "_dataTable"] = chartData;
                showHideDataTable(prefix + "_dataTable", "100%", false);

                $("#" + prefix + "_chart_link").show();
                $("#" + prefix + "_chartdiv").show();
                $('#link_' + prefix + "_Cat").addClass('active');
                chart.invalidateSize();
                /*chart.validateData();
				chart.animateAgain();*/
            }
        }
        level--;
    }
    $("#helpicon").unbind('click');
    if (target_level == 1) {
        if (document.getElementById("link_" + prefix + "_Cat") != null) {
            $("#link_" + prefix + "_Cat").remove();
        }

        $("#helpicon").click(function () {
            var URL = "HelpPage/helpPage.html#" + prefix;
            window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
        });
    } else {
        $("#helpicon").click(function () {
            var URL = "HelpPage/helpPage.html#" + prefix + "_L" + target_level;
            window.open(URL, "_blank", "resizable=0,height=600,width=500,scrollbars=1");
        });
    }

}

function getActivePrefix() {
    var activeTab = $('#tabs .ui-tabs-panel:not(.ui-tabs-hide)').prop("id");

    return activeTab.split("_")[1];
}

function showHideDataTable(dataTableID, width, show) {

    var data = dataTableDataDict[dataTableID];
    

    if (data != null) {
        if (data[0]) data = data[0];
        if (data.hasOwnProperty("month") && isNaN(data["month"])) {

        if (dataTableOptionsDict[dataTableID] == undefined) {
            dataTableOptionsDict[dataTableID] = {};
        }
        dataTableOptionsDict[dataTableID]["columnDefs"] = [
            {
                "targets": 0,
                "data": "month",
                "render": function (dataItem, type) {
                    if (level != 3 && currentPrefix != "kitreleaselist" && currentPrefix != "kittestlist" && currentPrefix != "currentMathStates") {
                        // TODO: make sure this is right on all levels
                        // date stuff
                        var currentYear = new Date().getFullYear(); // current Year
                        var lastYear = currentYear - 1; // last year
                        var year = lastYear; // set year to last year
                        var months = [
                            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November",
                            "December"
                        ];
                        var currentMonth = months[new Date().getMonth()]; // current month name
                        var month;
                        var monthName = dataItem; // store monthName of dataItem

                        if (months.indexOf(dataItem) < months.indexOf(currentMonth)) {
                            year = currentYear; // set to current year if after pivot point
                        }
                        if (dataItem == "Current Month") {
                            month = currentMonth + 1;
                            if (month < 10)
                                month = "0" + month;
                        } else {
                            month = months.indexOf(dataItem) + 1;
                            if (month < 10)
                                month = "0" + month;
                        }

                        return year + "" + month + " - " + monthName;

                    } else {
                        return dataItem;
                    }
                }
            }
        ];
        }
    }


    if (show != undefined) {
        dataTableIsVisible[dataTableID] = !show; // set visible to opposite of show so the below code toggles it
    }

    if (dataTableIsVisible.hasOwnProperty(dataTableID) && dataTableIsVisible[dataTableID]) {
        clearTable(dataTableID);
        dataTableIsVisible[dataTableID] = false;
        $("#" + dataTableID + "_link a").html("Show Data Table");
    } else if (dataTableDataDict[dataTableID] != undefined) {
        drawTable(dataTableDataDict[dataTableID], dataTableID, width, dataTableOptionsDict[dataTableID]);
        dataTableIsVisible[dataTableID] = true;
        $("#" + dataTableID + "_link a").html("Hide Data Table");
    }
}

function showHideAllGraphs(key) {

    var chart = "";
    var i;

    // if key is first pass and level is 2, you want the chart to by level 2 first pass chart    
    if (key == "newThemeFirstPass" && level == 2)
        chart = chartDict[key + "_L2"];
    else if (key == "AllSitesFirstPass" && level == 2)
        chart = chartDict[key + "_L2"];
    else if (key == "defectsPhaseFound" && level == 2)
        chart = chartDict[key + "_L2"];
    else
        chart = chartDict[key];

    if (allGraphsVisible) {
        for (i = 0; i < chart.graphs.length; i++) {
            chart.hideGraph(chart.graphs[i]);
        }
        toggleTotalLine(chart, false);

        $("#" + key + "_chart_link a").html("Show All Bars");

        allGraphsVisible = false;

    } else {
        for (i = 0; i < chart.graphs.length; i++) {
            chart.showGraph(chart.graphs[i]);
        }
        toggleTotalLine(chart, true);

        $("#" + key + "_chart_link a").html("Hide All Bars");

        allGraphsVisible = true;
    }

    return false;
}

var resizeTimeout;

window.addEventListener("resize", resizeHandler, false);

function resizeHandler() {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function () {
        if (level == 2 && currentPrefix == "newThemeFirstPass_L2") {
            chartDict["newThemeFirstPass_L2"].columnSpacing = setColumnSpacingFirstPassInt();
            chartDict["newThemeFirstPass_L2"].validateNow();
            chartDict["newThemeFirstPass_L2"].animateAgain();
            console.log("resized!");
        } else if (level == 2 && currentPrefix == "QuartersFirstPass_L2") {
            chartDict["QuartersFirstPass_L2"].columnSpacing = setColumnSpacingFirstPassInt();
            chartDict["QuartersFirstPass_L2"].validateNow();
            chartDict["QuartersFirstPass_L2"].animateAgain();
            console.log("resized!");
        }
    }, 500);
}

function loadTheMetric(e) {
    var $activediv = $("#dashboardContainer");
    $activediv.removeClass('active');
    $activediv.addClass('hide');

    loadMetricView(metricName);
}
