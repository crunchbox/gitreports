/*****************************************************************************\

 Javascript "Web Service Helper" library

 @version: 0.1 - 2016.05.06
 @author: Sean M McClain

\*****************************************************************************/

function DEPRECATED_GetViewTable(xml, categoryNodeName, replace_underscore) {
	var viewTable = [];
	var dateStr;
	var rowDict;

	$(xml.firstChild).find('DocumentElement').find(categoryNodeName).each(function () {
		rowDict = {};
		$(this).children().each(function () {
			var tagName = this.tagName;
			var val = $(this).text();
			if (tagName.toLowerCase().indexOf("date") != -1) {
				dateStr = val;
				if (dateStr.length == 8) {
					dateStr = dateStr.substr(0, 4) + "-" + dateStr.substr(4, 2) + "-" + dateStr.substr(6, 2);
				}
				if (replace_underscore) {
					tagName = tagName.replace("_", " ");
				}
				rowDict[tagName] = dateStr;
			} else {
				rowDict[tagName] = val;
			}
		});
		viewTable.push(rowDict);
	});

	return viewTable;
}

function generateConfig(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
	var config = {
		"type": "serial",
		"theme": theme,
		"categoryField": catField,
		"mouseWheelZoomEnabled": true,
		"mouseWheelScrollEnabled": true,
		"categoryAxis": {
			"gridPosition": "start",
			"labelOffset": -2
		},
		"valueAxes": [
			{
				"id": "v1",
				"title": yAxisTitle
            }
        ],
		"legend": {
			"enabled": true,
			"useGraphSettings": true,
			"valueFunction": function (item, value) {
			    if (value == "")
			        return 0;
			    else
			        return value;
			}
		},
		"titles": [
			{
				"id": chartId,
				"size": 15,
				"text": chartTitle
            }
        ],
		"chartCursor": {
			valueBalloonsEnabled: false,
			fullWidth: true,
			cursorAlpha: 0.1,
			zoomable: true,
			pan: false
		},
		"chartScrollbar": {
			"graph": "totalline",
			"graphType": "line",
			"color": scrollbarLineColor,
			"selectedBackgroundColor": scrollbarColor,
			"selectedBackgroundAlpha": "0.75",
			"selectedGraphLineColor": scrollbarLineColor,
			"autoGridCount": true
		},
		"export": {
			"enabled": true
		},
		"responsive": {
			"enabled": true,
			"rules": [
				{
					"maxWidth": 1000,
					"overrides": {
						"legend": {
							"enabled": false
						}
					}
                },
				{
					"maxWidth": 500,
					"overrides": {
						"valueAxes": {
							"inside": true
						}
					}
                }
            ]
		}
	};

	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});

	if (useStackCharts) {
		config.valueAxes[0].stackType = "regular";
	}

	return config;
}

function GetGraphs(data, categoryField) {
	var graphs = [];
	var ballonText = "[[title]] of [[category]]:[[value]]";
	var max_len_index = getLongestRowIndex(data);

	var colors = chartColors;
	/*
	var rowData = data[max_len_index]; // get row with the most cols    
	var groups = [];
    
	for (var heading in rowData) {
	    if (rowData.hasOwnProperty(heading)) {
	        if (heading != categoryField) {
	            groups.push(heading);
	        }
	    }
	}
	*/

	var groups = [];
	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		for (var heading in rowData) {
			if (rowData.hasOwnProperty(heading)) {
				if (heading != categoryField) {

					if (groups.indexOf(heading) == -1) {
						groups.push(heading);
					}
				}
			}
		}
	}


	var stacks = {};
	var cat;
	var totalline_col;

	for (var index in groups) {
		if (groups.hasOwnProperty(index)) {
			var column = {};
			var key = groups[index];
			if (key == "Total") {
				column["balloonText"] = ballonText;
				column["id"] = "totalline";
				column["title"] = "Total";
				column["bullet"] = "round";
				column["lineThickness"] = 3;
				column["bulletSize"] = 7;
				column["bulletBorderAlpha"] = 1;
				column["connect"] = true;
				column["useLineColorForBulletBorder"] = true;
				column["bulletBorderThickness"] = 3;
				column["fillAlphas"] = 0;
				column["lineAlpha"] = 1;
				column["valueField"] = key;
				graphs.push(column);

				column = {};
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["type"] = "column";
				column["valueField"] = key;
				graphs.push(column);
			} else {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["type"] = "column";
				column["valueField"] = key;

				graphs.push(column);
			}

		}
	}
	// set fill colors of bars/columns
	for (i = 0; i < graphs.length; i++) {
		graphs[i].fillColors = colors[i];
		graphs[i].lineColor = colors[i];
	}

	return graphs;
}

function GetRollingGraph(fieldID, fieldStrName, color) {
	var ballonText = "[[category]]:[[value]]";

	column = {};
	column["balloonText"] = ballonText;
	column["id"] = fieldID;
	column["title"] = fieldStrName;
	column["bullet"] = "square";
	column["lineThickness"] = 3;
	column["bulletSize"] = 7;
	column["bulletBorderAlpha"] = 1;
	column["connect"] = true;
	column["useLineColorForBulletBorder"] = true;
	column["bulletBorderThickness"] = 3;
	column["fillAlphas"] = 0;
	column["lineAlpha"] = 1;
	column["valueField"] = fieldID;
	column["lineColor"] = color;
	column["hidden"] = true;

	return column;
}

function GetGraphsEDA(data, categoryField) {

	var graphs = [];
	var ballonText = "[[title]] of [[category]]:[[value]]";

	var max_len_index = getLongestRowIndex(data);

	//var rowData = data[max_len_index]; // get row with the most cols    
	var groups = [];
	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		for (var heading in rowData) {
			if (rowData.hasOwnProperty(heading)) {
				if (heading != categoryField) {

					if (groups.indexOf(heading) == -1) {
						groups.push(heading);
					}
				}
			}
		}
	}

	var stacks = {};
	var cat;
	var totalline_col;

	for (var index in groups) {
		if (groups.hasOwnProperty(index)) {
			var column = {};

			var key = groups[index];
			if (key == "Total") {
				column["balloonText"] = ballonText;
				column["id"] = "totalline";
				column["title"] = "Total";
				column["bullet"] = "round";
				column["lineThickness"] = 3;
				column["bulletSize"] = 7;
				column["bulletBorderAlpha"] = 1;
				column["connect"] = true;
				column["useLineColorForBulletBorder"] = true;
				column["bulletBorderThickness"] = 3;
				column["fillAlphas"] = 0;
				column["lineAlpha"] = 1;
				column["valueField"] = key;
				graphs.push(column);
				column = {};
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["type"] = "column";
				column["valueField"] = key;
				graphs.push(column);
			} else {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["type"] = "column";
				column["valueField"] = key;

				graphs.push(column);
			}
		}
	}

	return graphs;
}

function GetStackedGraphs(data, categoryField, bottomField, topField) {
	var graphs = [];
	var ballonText = "[[title]] of [[category]]:[[value]]";

	var colors = chartColors;

	var max_len_index = getLongestRowIndex(data);

	/*
	var rowData = data[max_len_index]; // get row with the most cols
	var groups = [];
	for (var heading in rowData) {
	    if (rowData.hasOwnProperty(heading)) {
	        if (heading != categoryField)
	            groups.push(heading);
	    }
	}
	*/

	var groups = [];
	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		for (var heading in rowData) {
			if (rowData.hasOwnProperty(heading)) {
				if (heading != categoryField) {

					if (groups.indexOf(heading) == -1) {
						groups.push(heading);
					}
				}
			}
		}
	}

	var stacks = {};
	var cat;
	var totalline_col;
	var index;
	var column;
	var key;

	for (index in groups) {
		if (groups.hasOwnProperty(index)) {
			column = {};

			key = groups[index];
			if (key.indexOf(topField, 0) != -1) {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["newStack"] = false;
				column["type"] = "column";
				column["valueField"] = key;

				cat = key.split("/")[0];
				if (stacks.hasOwnProperty(cat) == false)
					stacks[cat] = {};
				stacks[cat][topField] = column;
			} else if (key.indexOf(bottomField, 0) != -1) {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["newStack"] = true;
				column["type"] = "column";
				column["valueField"] = key;

				cat = key.split("/")[0];
				if (stacks.hasOwnProperty(cat) == false)
					stacks[cat] = {};
				stacks[cat][bottomField] = column;
			}
		}
	}

	for (cat in stacks) {
		if (stacks.hasOwnProperty(cat)) {
			if (stacks[cat].hasOwnProperty(bottomField) == false && stacks[cat].hasOwnProperty(topField) == true) {
				stacks[cat][topField]["newStack"] = true;
			} else graphs.push(stacks[cat][bottomField]);

			if (stacks[cat].hasOwnProperty(topField))
				graphs.push(stacks[cat][topField]);
		}
	}

	// set fill colors of bars/columns
	for (i = 0; i < graphs.length; i++) {
		graphs[i].fillColors = colors[i];
		graphs[i].lineColor = colors[i];
	}

	for (index in groups) {
		if (groups.hasOwnProperty(index)) {
			column = {};

			key = groups[index];
			if (key == "Total") {
				column["balloonText"] = ballonText;
				column["id"] = "totalline";
				column["title"] = "Total";
				column["bullet"] = "round";
				column["lineThickness"] = 3;
				column["bulletSize"] = 7;
				column["bulletBorderAlpha"] = 1;
				column["connect"] = true;
				column["useLineColorForBulletBorder"] = true;
				column["bulletBorderThickness"] = 3;
				column["fillAlphas"] = 0;
				column["lineAlpha"] = 1;
				column["valueField"] = key;
				graphs.push(column);
			}
		}
	}

	return graphs;
}

function GetStackedRollingTotal(graphs, data, categoryField, stacks) {
	var ballonText = "[[title]] of [[category]]:[[value]]";

	column = {};
	column["balloonText"] = ballonText;
	column["id"] = "rollingTotal";
	column["title"] = "Rolling Total";
	column["bullet"] = "square";
	column["lineThickness"] = 3;
	column["bulletSize"] = 7;
	column["bulletBorderAlpha"] = 1;
	column["connect"] = true;
	column["useLineColorForBulletBorder"] = true;
	column["bulletBorderThickness"] = 3;
	column["fillAlphas"] = 0;
	column["lineAlpha"] = 1;
	column["valueField"] = "rollingTotal";
	column["hidden"] = true;

	graphs.push(column);

	for (var i = 0; i < stacks.length; i++) {
		var id = "rollingTotal" + i;
		var title = stacks[i] + " Rolling Total";

		column = {};
		column["balloonText"] = ballonText;
		column["id"] = id;
		column["title"] = title;
		column["bullet"] = "round";
		column["lineThickness"] = 3;
		column["bulletSize"] = 7;
		column["bulletBorderAlpha"] = 1;
		column["connect"] = true;
		column["useLineColorForBulletBorder"] = true;
		column["bulletBorderThickness"] = 3;
		column["fillAlphas"] = 0;
		column["lineAlpha"] = 1;
		column["valueAxis"] = "v2";
		column["valueField"] = id;
		column["hidden"] = true;

		graphs.push(column);
	}

	return graphs;
}

function GetLineGraphs(data, categoryField) {

	var graphs = [];
	var ballonText = "[[title]] of [[category]]:[[value]]";

	var groups = [];
	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		for (var heading in rowData) {
			if (rowData.hasOwnProperty(heading)) {
				if (heading != categoryField) {

					if (groups.indexOf(heading) == -1) {
						groups.push(heading);
					}
				}
			}
		}
	}

	var stacks = {};
	var cat;
	var totalline_col;

	for (var index in groups) {
		if (groups.hasOwnProperty(index)) {
			var column = {};

			var key = groups[index];
			if (key == "Total") {
				column["balloonText"] = ballonText;
				column["id"] = "totalline";
				column["title"] = "Total";
				column["lineThickness"] = 3;
				column["bulletSize"] = 7;
				column["bulletBorderAlpha"] = 1;
				column["connect"] = true;
				column["useLineColorForBulletBorder"] = true;
				column["bulletBorderThickness"] = 3;
				column["dashLengthField"] = key + "|dash";
				column["bulletField"] = key + "|bullet";
				column["fillAlphas"] = 0;
				column["lineAlpha"] = 1;
				column["valueField"] = key;
				column["hidden"] = true;
				graphs.push(column);
			} else {
				column["balloonText"] = ballonText;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["lineThickness"] = 3;
				column["bulletSize"] = 7;
				column["bulletBorderAlpha"] = 1;
				column["connect"] = true;
				column["useLineColorForBulletBorder"] = true;
				column["bulletBorderThickness"] = 3;
				column["dashLengthField"] = key + "|dash";
				column["bulletField"] = key + "|bullet";
				column["fillAlphas"] = 0;
				column["lineAlpha"] = 1;
				column["valueField"] = key;
				column["hidden"] = true;
				graphs.push(column);
			}
		}
	}

	return graphs;
}

function UpdateDashLinesAndBulletsWithGraphs(chartData, chartGraphs) {
	var rowData;
	var rowHasData = false;
	var valueField;
	for (var g in chartGraphs) {
		if (chartGraphs.hasOwnProperty(g)) {
			valueField = chartGraphs[g]["valueField"];
			for (var i = 0; i < chartData.length; i++) {
				rowData = chartData[i];
				rowHasData = false;
				for (var heading in rowData) {
					if (rowData.hasOwnProperty(heading)) {
						if (heading != "month" && heading.endsWith("|dash") == false) {
							if (valueField == heading) {
								rowHasData = true;
								break;
							}
						}
					}
				}
				if (!rowHasData) {
					chartData[i][valueField + "|bullet"] = "none";

					if (i > 0) {
						if (chartData[i - 1][valueField + "|bullet"] != null &&
							chartData[i - 1][valueField + "|bullet"] != "round") {
							chartData[i - 1][valueField + "|dash"] = 5;
						} else chartData[i - 1][valueField + "|dash"] = 5; // always dash into a none bullet point
					}
					if (i < chartData.length - 1) {
						if (chartData[i + 1][valueField + "|bullet"] != null &&
							chartData[i + 1][valueField + "|bullet"] != "round") {
							chartData[i][valueField + "|dash"] = 5;
						} else chartData[i][valueField + "|dash"] = 0;
					}

				} else {
					chartData[i][valueField + "|bullet"] = "round";

					if (i > 0) {
						if (chartData[i - 1][valueField + "|bullet"] == null ||
							chartData[i - 1][valueField + "|bullet"] == "round") {
							chartData[i - 1][valueField + "|dash"] = 0;
						} else chartData[i - 1][valueField + "|dash"] = 5;
					}
					if (i < chartData.length - 1) {
						if (chartData[i + 1][valueField + "|bullet"] == null ||
							chartData[i + 1][valueField + "|bullet"] == "round") {
							chartData[i][valueField + "|dash"] = 0;
						} else chartData[i][valueField + "|dash"] = 5;
					}
				}
			}
		}
	}
	return chartData;
}

function getLongestRowIndex(data) {
	var max_len = 0;

	var max_len_index = 0;

	for (var index = 0; index < data.length; index++) {
		if (Object.keys(data[index]).length > max_len) {
			max_len = Object.keys(data[index]).length;
			max_len_index = index;
		}
	}

	return max_len_index;
}

var rotateArray = function (a, inc) {
	var l, i, x;
	for (l = a.length, inc = (Math.abs(inc) >= l && (inc %= l), inc < 0 && (inc += l), inc), i, x; inc; inc = (Math.ceil(l / inc) - 1) * inc - l + (l = inc))
		for (i = l; i > inc; x = a[--i], a[i] = a[i - inc], a[i - inc] = x);
	return a;
};

function GetStudioAliasDictionary(xml, table_name) {
	var studio_alias_list = GetViewTable(xml, table_name, false);
	return GetStudioAliasDictionaryFromJSON(studio_alias_list);
}

function GetStudioAliasDictionaryFromJSON(studio_alias_list) {
	var studio_alias = {};
	for (var dataindex in studio_alias_list) {
		if (studio_alias_list.hasOwnProperty(dataindex)) {
			datarow = studio_alias_list[dataindex];
			studio_alias[datarow["studio"]] = datarow["alias"];
		}
	}

	
	return studio_alias;
}

function parseMetricDataByQuarter(jsonTable, table_name, studio_alias, chartSelection, sub_category) {
    var chartTable = [];
    var currentFiscal;
    var viewTable;
    var row;

    var quarter;
    var quarters = getQuarterList();

    var twoYearData = {};

    for(quarter = 0; quarter < 8; quarter++) {
        row = {};
        viewTable = jsonTable[table_name + quarter.toString()];

        if (quarter == 7)
            row["quarter"] = "Current Month";
        else
            row["quarter"] = quarters[quarter];

        if (sub_category == undefined || sub_category.startsWith("Total"))
            row["Total"] = viewTable.length;

        if (chartSelection == "QuartersPhaseFound")
            row = parseByTwoYearDefectsFoundToChartRow(row, viewTable, studio_alias, sub_category);
        else if (chartSelection == "QuartersFirstPass")
            row = parseByTwoYearFirstPassToChartRow(row, viewTable, studio_alias, sub_category);


        if (row["quarter"] == "Current Month")
            currentFiscal = row;
        else chartTable.push(row);
    }

    chartTable.push(currentFiscal);

    return chartTable;
}

/* 
* NEW METRIC STEP: Need to write own parse metric function or add prefix here
*/
function parseMetricData(jsonTable, table_name, studio_alias, chartSelection, sub_category) {
	var chartTable = [];
	var currentFiscal;
	var viewTable;
	var row;

	var month;
	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	var yearData = {};
	for (month = 0; month < 13; month++) {
		row = {};
		viewTable = jsonTable[table_name + month.toString()];

		if (month == 12)
			row["month"] = "Current Month";
		else
			row["month"] = months[month];


		if (sub_category == undefined || sub_category.startsWith("Total")) {
		    if (viewTable === undefined)
		        row["Total"] = 0;
            else 
		        row["Total"] = viewTable.length;
		}
			
        
		if (chartSelection == "newThemeOther")
		    row = parseByNewThemeOtherToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "AllSitesNewOther")
		    row = parseByNewThemeOtherSharePntToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "bugFixOther")
		    row = parseByBugFixOtherToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "kitsReleased")
		    row = parseByStudioToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "defectsForKitsReleased")
		    row = parseDefectsReleasedToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "edas" || chartSelection == "edasEntered" || chartSelection == "edaNewOnly")
		    row = parseEDAsToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "newThemeFirstPass")
		    row = parseNewThemeToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "AllSitesFirstPass")
		    row = parseNewThemeToChartRow_SP(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "NewThemeAvgTestTime") {
		    var data = {};
		    row = parseTestTimeToChartRow(row, viewTable, studio_alias, sub_category, data);
		    yearData[row["month"]] = data;
		} else if (chartSelection == "NewThemeAvgTTGameType") {
		    var data = {};
		    row = parseTestTimeTypeToChartRow(row, viewTable, studio_alias, sub_category, data);
		    yearData[row["month"]] = data;
		} else if (chartSelection == "defectsPhaseFound")
		    row = parseDefectsFoundNTToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "defectsClosedbyTesterType")
		    row = parseDefectsFoundTesterToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "bvaBuildsRun")
		    row = parseBVABuildsRun(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "bvaSuitesRun")
		    row = parseBVASuitesRun(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "BVATestCasesPassFail")
		    row = parseBVATestCases(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "UTPTestCasesPassFail")
		    row = parseUTPTestCases(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "GameTracker")
		    row = parseGameTrackerData(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "EQEScoreByPushDate")
		    row = parseEQEScoreToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "cn_in" || chartSelection == "cn_inEntered")
		    row = parseCN_INdata(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "ContainmentRate")
		    row = parseDceToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "mathThemesReleased")
		    row = parseMathThemesReleased(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "mathDefectsFound")
		    row = parseMathDefectsFound(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "mathEdas")
		    row = parseMathEdas(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "avgDefects")
		    row = parseAvgDefectsNewThemesL1(row, viewTable, studio_alias, sub_category);
		if (row["month"] == "Current Month")
			currentFiscal = row;
		else chartTable.push(row);
	}

	if (sub_category === undefined) {
	    if (chartSelection == "NewThemeAvgTestTime")
	        extraGraphData["NewThemeAvgTestTime"] = yearData;
	    else if (chartSelection == "NewThemeAvgTTGameType")
	        extraGraphData["NewThemeAvgTTGameType"] = yearData;
	}


	chartTable = chartTable.concat(chartTable.splice(0, new Date().getMonth()));
	chartTable.push(currentFiscal);

	return chartTable;
}

function getMonthFromString(mon) {
	return new Date(Date.parse(mon + " 1, 2012")).getMonth();
}

function ISODateParse(str) {
	var y = str.substr(0, 4),
		m = str.substr(4, 2) - 1,
		d = str.substr(6, 2);
	var D = new Date(y, m, d);
	return (D.getFullYear() == y && D.getMonth() == m && D.getDate() == d) ? D : 'invalid date';
}

function getStringFromMonth(mon) {
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    if (mon == 13)
        return "Current Month";
    else
        return monthNames[mon - 1];
}

function getSortedMonthList() {
	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	var sortedMonths = monthNames.concat(monthNames.splice(0, new Date().getMonth()));

	sortedMonths.push("Current Month");
	return sortedMonths;
}

function getStudioAliasList(studio_alias) {
	var alias_hash = {};
	var alias_list = [];
	var studio;
	var alias;
	for (studio in studio_alias) {
		if (studio_alias.hasOwnProperty(studio)) {
			alias = studio_alias[studio];
			if (alias_hash.hasOwnProperty(alias) == false) {
				alias_hash[alias] = studio;
				alias_list.push(alias);
			}
		}
	}
	return alias_list;
}

function getQuarterFromString(string) {
    return quarterList.indexOf(string);
}

function isMonthString(mon) {
	var timestamp = Date.parse(mon + " 1, 2012");
	if (isNaN(timestamp))
		return false;
	return true;
}

function GetDataForMonth(jsonTable, table_name, monthname) {
	var month;
	if (monthname == "Current Month")
		month = 12;
	else month = getMonthFromString(monthname);

	viewTable = jsonTable[table_name + month.toString()];

	return viewTable;
}

function GetDataForQuarter(jsonTable, table_name, monthname) {
    var quarter;
    if (monthname == "Current Month")
        quarter = 7;
    else month = getQuarterFromString(monthname);

        viewTable = jsonTable[table_name + month.toString()];
    return viewTable;
}

function GetDataForEntry_Quarters(chartSelection, jsonTable, table_name, quartername, sub_category, studio_alias) {
    var quarter;
    var tempArray;
    var studio;
    var cat;
    var dataStudio;

    var quarterStart = 0;
    var quarterEnd = 8;
    if (quartername != "all") {
        if (quartername == "Current Month")
            quarter = 12;
        else quarter = getQuarterFromString(quartername);
        quarterStart = quarter;
        quarterEnd = quarter;
    }
    var viewTable = [];

    for (quarter = quarterStart; quarter <= quarterEnd; quarter++) {
        var dataTable = jsonTable[table_name + quarter.toString()];

        if (chartSelection == "QuartersFirstPass") {
            studio = sub_category;
        } else {
            studio = sub_category;
        }

        for (var dataindex in dataTable) {
            if (dataTable.hasOwnProperty(dataindex)) {
                datarow = dataTable[dataindex];
                dataStudio = datarow["Studio"];
                if (studio_alias) {
                    if (studio_alias.hasOwnProperty(dataStudio))
                        dataStudio = studio_alias[dataStudio];
                }

                if (studio == "Total" || studio == dataStudio) {
                    if (chartSelection == "QuartersFirstPass") {
                        if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System')
                            viewTable.push(datarow);
                    } else if (chartSelection == "QuartersPhaseFound")
                        viewTable.push(datarow);
                }
            }
        }
    }
    return viewTable;
}

function GetDataForEntry(chartSelection, jsonTable, table_name, monthname, sub_category, studio_alias) {
	var month;
	var tempArray;
	var studio;
	var cat;
	var dataStudio;

	var monthStart = 0;
	var monthEnd = 12;
	if (monthname != "all") {
		if (monthname == "Current Month")
			month = 12;
		else month = getMonthFromString(monthname);
		monthStart = month;
		monthEnd = month;
	}
	var viewTable = [];

	for (month = monthStart; month <= monthEnd; month++) {

	    var dataTable = jsonTable[table_name + month.toString()];
	    if (chartSelection === "avgDefects") {
	        dataTable = (jsonTable["DefectsKitsReleasedL3"])[table_name + month.toString()];
	    } else {
	        dataTable = jsonTable[table_name + month.toString()];
	    }

		if (chartSelection == "bugFixOther" || chartSelection == "newThemeOther" || chartSelection == "edas" || chartSelection == "edasEntered" || chartSelection == "AllSitesNewOther" || chartSelection == "defectsClosedbyTesterType") {
			tempArray = sub_category.split('/');
			studio = tempArray[0];
			cat = tempArray[1];
		} else {
			studio = sub_category;
		}


		for (var dataindex in dataTable) {
			if (dataTable.hasOwnProperty(dataindex)) {
				datarow = dataTable[dataindex];
				dataStudio = datarow["Studio"];
				if (studio_alias) {
					if (studio_alias.hasOwnProperty(dataStudio))
						dataStudio = studio_alias[dataStudio];
				}

				if (studio == "Total" || studio == dataStudio) {
					if (chartSelection == "newThemeOther") {
						if (cat == "New Theme") {
							if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System')
								viewTable.push(datarow);
						} else {
							if (datarow["Category"] != 'New Game Theme' && datarow["Category"] != 'Localization' && datarow["Category"] != 'New System')
								viewTable.push(datarow);
						}
					} else if (chartSelection == "AllSitesNewOther") {
					    if (cat == "New Theme") {
					        if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System')
					            viewTable.push(datarow);
					    } else {
					        if (datarow["Category"] != 'New Game Theme' && datarow["Category"] != 'Localization' && datarow["Category"] != 'New System')
					            viewTable.push(datarow);
					    }
					} else if (chartSelection == "bugFixOther") {
						if (cat == "Bug Fix" && datarow["Category"] == "Bug Fix") {
							viewTable.push(datarow);
						} else if (cat == "Other" && datarow["Category"] != "Bug Fix") {
							viewTable.push(datarow);
						}
					}
					else if (chartSelection == "edas" || chartSelection == "edasEntered") {
					    if (cat == "PA" && datarow["Responsible_Group"] == "PA") {
					        viewTable.push(datarow);
					    } else if (cat == "Other" && datarow["Responsible_Group"] != "PA") {
					        viewTable.push(datarow);
					    }
					}
					else if (chartSelection == "defectsClosedbyTesterType") {
						if (cat == "SDET" && datarow["TesterType"] == "SDET") {
							viewTable.push(datarow);
						} else {
							if (dataStudio == studio || studio == "Total")
								viewTable.push(datarow);
						}
					} else if (chartSelection == "newThemeFirstPass") {
						if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System')
							viewTable.push(datarow);
					} else if (chartSelection == "defectsPhaseFound") {
						if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System')
							viewTable.push(datarow);
					} else {
						if (dataStudio == studio || studio == "Total")
							viewTable.push(datarow);
					}

				}
			}
		}
	}

	return viewTable;
}

function GetDefectCountPerTheme(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
	var month;
	var tempArray;
	var tempInt;
	var studio;
	var cat;
	var dataStudio;
	var dataTheme;
	var dataThemeKey;
	var dataKit;
	var defectrow;
	var datarow;
	var row;
	var isValidKit;
	var chartTable = [];
	var themeDict = {};
	var themeLookUp = {}; // for the cursor category balloon
	var tempTheme = "";
	var compareVal;

	if (chartSelection == "QuartersPhaseFound" || chartSelection == "QuartersFirstPass") {
	    if (monthname == "Current Month")
	        month = 7;
	    else month = getQuarterFromString(monthname);
	} else {
	    if (monthname == "Current Month")
	        month = 12;
	    else month = getMonthFromString(monthname);
	}

    var defectTable = [];
	if (chartSelection === "avgDefects") {
	    defectTable = (jsonTable["DefectsKitsReleasedL3"])[defect_table_name + month.toString()];
	} else {
	    defectTable = jsonTable[defect_table_name + month.toString()];
	}

	if (chartSelection == "bugFixOther" || chartSelection == "newThemeOther" || chartSelection == "AllSitesNewOther" || chartSelection == "defectsClosedbyTesterType" || chartSelection == "edas" || chartSelection == "edasEntered") {
		tempArray = sub_category.split('/');
		studio = tempArray[0];
		cat = tempArray[1];
	} else {
		studio = sub_category;
	}



	for (var dataindex in defectTable) {
		if (defectTable.hasOwnProperty(dataindex)) {
			datarow = defectTable[dataindex];
			dataStudio = datarow["Studio"];
			dataTheme = datarow["KitTheme"];
			dataKit = datarow["Kit"];
			dataThemeKey = datarow["SAP_Theme_ID"];

			if (chartSelection === "NewThemeAvgTTGameType")
				compareVal = datarow["Type"];
			else {
				compareVal = dataStudio;
			}

			if (studio_alias || chartSelection !== "NewThemeAvgTTGameType") {
				if (studio_alias.hasOwnProperty(compareVal))
					compareVal = studio_alias[compareVal];
			}

			isValidKit = false;
			if (studio == "Total" || studio == compareVal || studio.endsWith("_NCR")) {
				if (chartSelection == "newThemeOther") {
					if (studio == "Total" || compareVal == studio) {
						if (cat == "New Theme") {
							if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
								isValidKit = true;
							}
						} else {
							if (datarow["Category"] != 'New Game Theme' && datarow["Category"] != 'Localization' && datarow["Category"] != 'New System') {
								isValidKit = true;
							}
						}
					}
				} else if (chartSelection == "AllSitesNewOther") {
				    if (studio == "Total" || compareVal == studio) {
				        if (cat == "New Theme") {
				            if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
				                isValidKit = true;
				            }
				        } else {
				            if (datarow["Category"] != 'New Game Theme' && datarow["Category"] != 'Localization' && datarow["Category"] != 'New System') {
				                isValidKit = true;
				            }
				        }
				    }
				}  else if (chartSelection == "bugFixOther") {
					if (studio == "Total" || compareVal == studio) {
						if (cat == "Bug Fix" && datarow["Category"] == "Bug Fix") {
							isValidKit = true;
						} else if (cat == "Other" && datarow["Category"] != "Bug Fix") {
							isValidKit = true;
						}
					}
				} else if (chartSelection == "edas") {
				    if (studio == "Total" || compareVal == studio) {
				        if (cat == "PA" && datarow["Responsible_Group"] == "PA") {
				            isValidKit = true;
				        } else if (cat == "Other" && datarow["Responsible_Group"] != "PA") {
				            isValidKit = true;
				        }
				    }
				} else if (chartSelection == "edasEntered") {
				    if (studio == "Total" || compareVal == studio) {
				        if (cat == "PA" && datarow["Responsible_Group"] == "PA") {
				            isValidKit = true;
				        } else if (cat == "Other" && datarow["Responsible_Group"] != "PA") {
				            isValidKit = true;
				        }
				    }
				} else if (chartSelection == "ContainmentRate") {
					isValidKit = true;
				} else if (chartSelection == "defectsClosedbyTesterType") {
					if (studio == "Total" || compareVal == studio) {
						if (cat == "SDET" && datarow["TesterType"] == "SDET") {
							isValidKit = true;
						} else if (cat == "Game Tester" && datarow["TesterType"] != "SDET") {
							isValidKit = true;
						}
					}
				} else {
					if (studio == "Total" || compareVal == studio) {
						isValidKit = true;
					}
				}
			}
			if (isValidKit) {
				row = {};
				if (dataTheme.length >= 27) {
					tempTheme = dataTheme.substring(0, 26);
					if (themeDict.hasOwnProperty(tempTheme))
						themeDict[tempTheme] = themeDict[tempTheme] + 1;
					else themeDict[tempTheme] = 1;
					row["theme"] = tempTheme + "_" + themeDict[tempTheme]; // max out                     
				} else {
					row["theme"] = dataTheme;
				}
				themeLookUp[row["theme"]] = dataTheme; // for the cursor balloon
				row["fullTheme"] = dataTheme;

				if (chartSelection === "NewThemeAvgTestTime" || chartSelection === "NewThemeAvgTTGameType")
					row["value"] = parseFloat(datarow["Testing_Time"]);
				else
					row["value"] = parseInt(datarow["Count"]);

				row["kit"] = dataKit;
				row["month"] = month;
				row["themekey"] = dataThemeKey;
				chartTable.push(row);
			}
		}
	}

	chartTable.sort(function (a, b) {
		if (a.theme < b.theme) return -1;
		if (a.theme > b.theme) return 1;
		return 0;
	});

	return {
		chart: chartTable,
		themes: themeLookUp
	};
}

function getQuarter(integer) {
    if (integer >= 0 && integer < 3)
        return 1;
    else if (integer >= 3 && integer < 6)
        return 2;
    else if (integer >= 6 && integer < 9)
        return 3;
    else if (integer >= 9 && integer < 12)
        return 4;
}

/* Returns a list of past 7 quarters based on current year and quarter (date) */
function getQuarterList() {
    var now = new Date();
    var quarter = getQuarter(now.getMonth());
    var year = now.getFullYear();
    var quarters = [];
    var prev;

    for (var i = 0; i < 7; i++) {
        prev = getPreviousQuarter(quarter, year);
        quarter = prev.quarter;
        year = prev.year;
        quarters.unshift("Q" + quarter + " " + year);
    }
    quarterList = quarters;
    quarterList[quarterList.length] = "Current Month";
    return quarters;
}

function getQuarterListObject() {
    var quarterArray = getQuarterList();
    var quarterObject = [];


    for (i = 0; i < 8; i++) {
        var row = {};
        row.quarter = quarterArray[i];
        row.num = i;
        quarterObject.push(row);
    }

    return quarterObject;
}

function getPreviousQuarter(quarter, year) {
    var newQuarter;
    var newYear;

    switch (quarter) {
        case 1:
            newQuarter = 4;
            newYear = year - 1;
            break;
        case 2:
            newQuarter = 1;
            newYear = year;
            break;
        case 3:
            newQuarter = 2;
            newYear = year;
            break;
        case 4:
            newQuarter = 3;
            newYear = year;
    }
    return {
        "quarter": newQuarter,
        "year": newYear
    };
}
