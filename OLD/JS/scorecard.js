var SC_dataTables = {};
var SC_chartConfigDict = {};

var SC_chartDict = {};
var SC_jsonData = {};

var dataTableIsVisible = {};
var dataTableDataDict = {};
var dataTableOptionsDict = {};
var dataTableClassListDict = {};
var hideColumnsDict = {};
var chartTableSizes = [];

var parent_prefix = "";
var kitNumber = "";
var gameTheme = "";
var wbs = "";
var eqe = "";
var eqeDate = "";
var compName = "";
var fifoDate = "";
var outOfPaDate = "";
var kitState = "";
var setTableTo1x4 = true;
var chartRow = 4;
var chartCol = 1;
var divsDict = {};
var timeLineLegend = null;
var activeCharts = {};
var chartEmptyText = {};
var enlargedChart;
var timeLineChartLen = null;
var singleRowCharts = false;
var xmlHttp = new XMLHttpRequest();
var eftHours = "";

$(window).resize(function () {

	dealWithChartSizes();

	//dealWithLegends();

	for (var key in SC_chartDict) {
		if (SC_chartDict.hasOwnProperty(key)) {
			SC_chartDict[key].handleResize();
			SC_chartDict[key].validateNow();
		}
	}


	if ($(window).width() < 750) {
		$(".tableLabel").css('text-align', 'left');
	} else
		$(".tableLabel").css('text-align', 'right');

	if ($(window).width() < 750) {
		$("#SC_Theme_Logo").css('float', 'left');
		$("#SC_Theme_Logo").css('right', 'auto');
		$("#SC_Theme_Logo").css('left', 'auto');
	} else {

		$("#SC_Theme_Logo").css('float', 'right');
		$("#SC_Theme_Logo").css('left', 'auto');
		$("#SC_Theme_Logo").css('right', '0');
	}
});

$(window).load(function () {
	activeCharts["defectPhaseFound_chartdiv"] = 1;
	activeCharts["timeLine_chartdiv"] = 1;
	activeCharts["defectTrend_chartdiv"] = 1;
	activeCharts["slaVsActual_chartdiv"] = 1;
	activeCharts["timeSpentIn_chartdiv"] = 1;
	activeCharts["defectSummary_chartdiv"] = 0;
	activeCharts["defectSeverity_chartdiv"] = 0;
	enlargedChart = null;


	chartEmptyText["defectPhaseFound_chartdiv"] = "No closed defects for this kit";
	chartEmptyText["timeLine_chartdiv"] = "No data available";
	chartEmptyText["defectTrend_chartdiv"] = "No defects for this kit";
	chartEmptyText["slaVsActual_chartdiv"] = "No data available";
	chartEmptyText["timeSpentIn_chartdiv"] = "No data available";
	chartEmptyText["defectSummary_chartdiv"] = "No closed defects for this kit";
	chartEmptyText["defectSeverity_chartdiv"] = "No closed defects for this kit";

	dealWithChartSizes();

	if ($(window).width() < 750) {
	    $(".tableLabel").css('text-align', 'left');
	} else
	    $(".tableLabel").css('text-align', 'right');
});

function dealWithChartSizes() {
	var chart_name;
	if (enlargedChart == null) {
		if ($(window).width() < 900) {
			singleRowCharts = true;
			for (chart_name in activeCharts) {
			    if (chart_name !== "timeLine_chartdiv")
				    $("#" + chart_name).css('width', '100%');
			}

		} else {

			singleRowCharts = false;
			for (chart_name in activeCharts) {
			    if (chart_name !== "timeLine_chartdiv")
				    $("#" + chart_name).css('width', '50%');
			}

		}

		for (chart_name in activeCharts) {
		    if (chart_name !== "timeLine_chartdiv")
			    $("#" + chart_name).css('height', '400px');
		}
	} else {
		$("#" + enlargedChart).css('width', $("#tableWrapper").width());
		$("#" + enlargedChart).css('height', '400px');
	}
}

function getCoverage() {
    /*
	var totalCoverage = 1850;
	if (SC_chartDict != undefined && SC_chartDict.hasOwnProperty("timeline")) {

		if (timeLineChartLen !== null && timeLineChartLen !== undefined) {
			if (timeLineChartLen < 50)
				totalCoverage = 1500;
			else if (timeLineChartLen < 100)
				totalCoverage = 1600;

			else if (timeLineChartLen < 150)
				totalCoverage = 1700;

			else if (timeLineChartLen < 200)
				totalCoverage = 1800;
			else if (timeLineChartLen < 250)
				totalCoverage = 1850;
		}


		if (enlargedChart === "timeLine_chartdiv")
			totalCoverage = 1200;
	}
	return totalCoverage;*/
}

function dealWithLegends() {
    /*
	var totalCov = getCoverage();

	if ($(window).width() < totalCov) {
		if (SC_chartDict != undefined && SC_chartDict.hasOwnProperty("timeline")) {
			if (SC_chartDict["timeLine"].legend !== undefined) {
				timeLineLegend = SC_chartDict["timeLine"].legend;
			}
			SC_chartDict["timeLine"].removeLegend();
		}
	} else {
		if (SC_chartDict != undefined && SC_chartDict.hasOwnProperty("timeline")) {
			if (SC_chartDict["timeLine"].legend === undefined) {
				SC_chartDict["timeLine"].addLegend(timeLineLegend);

			}
		}
	}*/
}

function get_ScoreCardData() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false 
        || SC_jsonData[kitNumber].hasOwnProperty("BasicData") == false
        || SC_jsonData[kitNumber].hasOwnProperty("PROs") == false
        || SC_jsonData[kitNumber].hasOwnProperty("ThemeLogo") == false) 
    {
        var client = new $.RestClient('/rest/');
        client.add("scorecard");
        var restRequest = client.scorecard.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_callBack);
    } else get_ScoreCardData_callBack(SC_jsonData[kitNumber]);    
}

function get_ScoreCardData_callBack(r) {
    var SC_current_data;
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty("BasicData") == false
        || SC_jsonData[kitNumber].hasOwnProperty("PROs") == false
        || SC_jsonData[kitNumber].hasOwnProperty("ThemeLogo") == false) 
    {
        SC_jsonData[kitNumber]["BasicData"] = r["BasicData"];
        SC_jsonData[kitNumber]["PROs"] = r["PROS"];
        SC_jsonData[kitNumber]["ThemeLogo"] = r["ThemeLogo"];
    }   

    SC_current_data = r;

    SC_dataTables["BasicData"] = SC_current_data["BasicData"];
    SC_dataTables["PROs"] = SC_current_data["PROS"];
    SC_dataTables["ThemeLogo"] = SC_current_data["ThemeLogo"];

    loadEarlyTestingHours(kitNumber);

    configureSPCharts_BaseData();

    resetCurrentHash();

    return SC_current_data;
}

function get_ScoreCardData_Defects() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false 
        || SC_jsonData[kitNumber].hasOwnProperty("DefectsClosed") == false 
        || SC_jsonData[kitNumber].hasOwnProperty("EDA") == false
        || SC_jsonData[kitNumber].hasOwnProperty("EDA_All") == false
        || SC_jsonData[kitNumber].hasOwnProperty("OutOfPA") == false)
    {
        $("#eda a").text("Loading...");
        $("#alleda a").text("Loading...");
        $("#defectCount a").text("Loading...");
        $("#dcr a").text("Loading...");

        var client = new $.RestClient('/rest/');
        client.add("scorecard_Defects");
        var restRequest = client.scorecard_Defects.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_Defects_callBack);
    } else get_ScoreCardData_Defects_callBack(SC_jsonData[kitNumber]);
}

function get_ScoreCardData_Defects_callBack(r) {
    var SC_current_data;
    // first store the data for later use
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty("DefectsClosed") == false
        || SC_jsonData[kitNumber].hasOwnProperty("EDA") == false
        || SC_jsonData[kitNumber].hasOwnProperty("EDA_All") == false
        || SC_jsonData[kitNumber].hasOwnProperty("OutOfPA") == false)
    {
        SC_jsonData[kitNumber]["DefectsClosed"] = r["DefectsClosed"];
        SC_jsonData[kitNumber]["EDA"] = r["EDA"];
        SC_jsonData[kitNumber]["EDA_All"] = r["EDA_All"];
        SC_jsonData[kitNumber]["OutOfPA"] = r["OutOfPA"];
    }
    SC_current_data = r;
    // then store the data for current use
    SC_dataTables["Defects"] = SC_current_data["DefectsClosed"];
    SC_dataTables["EDA"] = SC_current_data["EDA"];
    SC_dataTables["EDA_All"] = SC_current_data["EDA_All"];
    SC_dataTables["OutOfPA"] = SC_current_data["OutOfPA"];

    configureSPCharts_Defects();
    $("#defectPhaseFound_chartdivContainer.kitPageChartContainer").removeClass("loading");

    resetCurrentHash();

    return SC_current_data;
}

function get_ScoreCardData_DefectHistory() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false
        || SC_jsonData[kitNumber].hasOwnProperty("DefectTrends") == false) {
        var client = new $.RestClient('/rest/');
        client.add("scorecard_DefectHistory");
        var restRequest = client.scorecard_DefectHistory.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_DefectHistory_callBack);
    } else get_ScoreCardData_DefectHistory_callBack(SC_jsonData[kitNumber]);
}

function get_ScoreCardData_DefectHistory_callBack(r) {
    var SC_current_data;
    // first store the data for later use
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty(["DefectTrends"]) == false) {
        SC_jsonData[kitNumber]["DefectTrends"] = r["DefectTrends"];
    }
    SC_current_data = r;
    // then store the data for current use
    SC_dataTables["DefectTrends"] = SC_current_data["DefectTrends"];
    configureSPCharts_DefectHistory();
    $("#defectTrend_chartdivContainer.kitPageChartContainer").removeClass("loading");

    resetCurrentHash();

    return SC_current_data;
}

function get_ScoreCardData_KitTimeLine() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false
        || SC_jsonData[kitNumber].hasOwnProperty("KitTimeLine") == false) {
        var client = new $.RestClient('/rest/');
        client.add("scorecard_KitTimeLine");
        var restRequest = client.scorecard_KitTimeLine.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_KitTimeLine_callBack);
    } else get_ScoreCardData_KitTimeLine_callBack(SC_jsonData[kitNumber]);
}

function get_ScoreCardData_KitTimeLine_callBack(r) {

    var SC_current_data;
    // first store the data for later use
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty(["KitTimeLine"]) == false) {
        SC_jsonData[kitNumber]["KitTimeLine"] = r["KitTimeLine"];
    }
    SC_current_data = r;
    // then store the data for current use
    SC_dataTables["KitTimeLine"] = SC_current_data["KitTimeLine"];
    configureSPCharts_KitTimeline();

    setTimeout(function () {
        $("#timeLine_chartdivContainer.kitPageChartContainer").removeClass("loading");
    }, 1000);
    
    resetCurrentHash();

    return SC_current_data;
}

function get_ScoreCardData_JCO() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false
        || SC_jsonData[kitNumber].hasOwnProperty("JCO") == false
        || SC_jsonData[kitNumber].hasOwnProperty("FP_Status") == false)
    {
        $("#jco a").text("Loading...");
        var client = new $.RestClient('/rest/');
        client.add("scorecard_JCO");
        var restRequest = client.scorecard_JCO.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_JCO_callBack);
    } else get_ScoreCardData_JCO_callBack(SC_jsonData[kitNumber]);
}

function get_ScoreCardData_JCO_callBack(r) {
    var SC_current_data;
    // first store the data for later use
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty(["JCO"]) == false
        || SC_jsonData[kitNumber].hasOwnProperty("FP_Status") == false)
    {
        SC_jsonData[kitNumber]["JCO"] = r["JCO"];
        SC_jsonData[kitNumber]["FP_Status"] = r["FP_Status"];
    }
    SC_current_data = r;
    // then store the data for current use
    SC_dataTables["JCO"] = SC_current_data["JCO"];
    SC_dataTables["FP"] = SC_current_data["FP_Status"];
    configureSPCharts_JCO();

    resetCurrentHash();

    return SC_current_data;
}

function get_ScoreCardData_ToolsPerKit() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false
        || SC_jsonData[kitNumber].hasOwnProperty("ToolsPerKit") == false) {        
        $("#toolsUsed a").text( "Loading...");
        var client = new $.RestClient('/rest/');
        client.add("scorecard_ToolsPerKit");
        var restRequest = client.scorecard_ToolsPerKit.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_ToolsPerKit_callBack);
    } else get_ScoreCardData_ToolsPerKit_callBack(SC_jsonData[kitNumber]);
}


function get_ScoreCardData_ToolsPerKit_callBack(r) {
    var SC_current_data;
    // first store the data for later use
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty(["ToolsPerKit"]) == false) {
        SC_jsonData[kitNumber]["ToolsPerKit"] = r["ToolsPerKit"];
    }
    SC_current_data = r;
    // then store the data for current use
    SC_dataTables["ToolsPerKit"] = SC_current_data["ToolsPerKit"];
    // future note: probably could clean this up a bit

    configureSPCharts_ToolsPerKit();

    resetCurrentHash();

    return SC_current_data;
}


function get_ScoreCardData_GameTracker() {
    if (SC_jsonData.hasOwnProperty(kitNumber) == false
        || SC_jsonData[kitNumber].hasOwnProperty("GameTrackerData") == false) {

        var client = new $.RestClient('/rest/');
        client.add("scorecard_GameTracker");
        var restRequest = client.scorecard_GameTracker.create({}, {
            kit: kitNumber
        });
        // POST /rest/scorecard/?kit={kitNumber}
        restRequest.done(get_ScoreCardData_GameTracker_callBack);
    } else get_ScoreCardData_GameTracker_callBack(SC_jsonData[kitNumber]);
}

function get_ScoreCardData_GameTracker_callBack(r) {
    var SC_current_data;
    // first store the data for later use
    if (SC_jsonData.hasOwnProperty(kitNumber) == false) {
        SC_jsonData[kitNumber] = {};
    }
    if (SC_jsonData[kitNumber].hasOwnProperty(["GameTrackerData"]) == false) {
        SC_jsonData[kitNumber]["GameTrackerData"] = r["GameTrackerData"];
    }
    SC_current_data = r;
    // then store the data for current use
    SC_dataTables["GameTrackerData"] = SC_current_data["GameTrackerData"];
    // future note: probably could clean this up a bit

    configureSPCharts_GameTracker();

    resetCurrentHash();

    return SC_current_data;
}

function loadEarlyTestingHours(kit) {
    var databaseAddress = 'http://acloud.insideigt.com/api/aLittleQuery_dev/api/v1/';
    var databaseName = 'wsr_dev/';
    var collection = 'userDataEFT/query';
    var url = databaseAddress + databaseName + collection;
    var query = {
        "kitNumber": kit.toUpperCase()
    };

    xmlHttp.open("POST", url, true);
    xmlHttp.send();
    xmlHttp.addEventListener("readystatechange", eftCallback, false);
}

function eftCallback(e) {
    var response;
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        response = JSON.parse(xmlHttp.responseText);
        for (i = 0; i < response.length; i++) {
            if (response[i].kitNumber == kitNumber) {
                eftHours = response[i].testHoursInput;
            }
        }
    } else {
        eftHours = "No EFT Hours in database";
    }

    updateElementAfterRender("eft", function () {
        var eft_val = "Data Not Available";
        if (eftHours != 0) {
            eft_val = eftHours;
        }
        $("#eft a").text(eft_val);
    });

    resetCurrentHash();

    return eftHours;
}

function loadScoreCardData(prefix, kit, parenturl) {
	kitNumber = kit;
	parent_prefix = prefix;

    if (parent_prefix === "kitPage") {
        parenturl = "/";
        $("#gobacklink_a").attr("href", parenturl);
    }
	if (parenturl == undefined) {
	    	parenturl = "/";
	}

	$("#gobacklink_a").attr("title", parenturl);

    // get Data
	get_ScoreCardData(); // Basic Data and PROS
	get_ScoreCardData_DefectHistory(); // Defect Trends graph
	get_ScoreCardData_Defects(); // defect and eda tables
	get_ScoreCardData_JCO(); // JCO table and First Pass Info
	get_ScoreCardData_KitTimeLine(); // Kit Timeline graph
	get_ScoreCardData_ToolsPerKit(); // Tools Used Table
    get_ScoreCardData_GameTracker(); //Game Tracker data
}

AmCharts.checkEmptyData = function (chart, text) {
	var dataPoint;
	if (chart.dataProvider.length == 0) {

		// add dummy data point
		dataPoint = {
			dummyValue: 0
		};
		dataPoint[chart.categoryField] = '';
		chart.dataProvider = [dataPoint];

		// add label
		chart.addLabel(0, '50%', text, 'center');

		// set opacity of the chart div
		chart.chartDiv.style.opacity = 0.5;

		// redraw it
		chart.validateNow();
	}
	// check sla chart
	else if (chart.dataProvider[0]["days_actual"] === 0 && chart.dataProvider[0]["days_sla"] === 0 &&
		chart.dataProvider[1]["hours_actual"] === 0 && chart.dataProvider[1]["hours_sla"] === 0) {
		// add dummy data point
		dataPoint = {
			dummyValue: 0
		};
		dataPoint[chart.categoryField] = '';
		chart.dataProvider = [dataPoint];

		// add label
		chart.addLabel(0, '50%', text, 'center');

		// set opacity of the chart div
		chart.chartDiv.style.opacity = 0.5;

		// redraw it
		chart.validateNow();
	}
};

function configureSPCharts_BaseData() {

    var kitData = SC_dataTables["BasicData"][0];
    if (kitData == null) {
        alert("No Kit Data Found!");
        removeScoreCard();
        return;
    }

    if (kitData.hasOwnProperty("Component"))
        compName = kitData["Component"];
    else compName = "";
    if (kitData.hasOwnProperty("KitTheme"))
        gameTheme = kitData["KitTheme"];
    else gameTheme = "";
    if (kitData.hasOwnProperty("FIFO"))
        fifoDate = kitData["FIFO"];
    else fifoDate = "";
    if (kitData.hasOwnProperty("OutOfPA"))
        outOfPaDate = kitData["OutOfPA"];
    else outOfPaDate = "";
    if (kitData.hasOwnProperty("KitState"))
        kitState = kitData["KitState"];
    else kitState = "";
    if (kitData.hasOwnProperty("WBS"))
        wbs = kitData["WBS"];
    else wbs = "";
    if (kitData.hasOwnProperty("EQE") && kitData["EQE"]!==null)
        eqe = kitData["EQE"];
    else eqe = "N/A";
    if (kitData.hasOwnProperty("EQE_Date") && kitData["EQE_Date"]!==null)
        eqeDate = kitData["EQE_Date"];        
    else eqeDate = "N/A";

    $("#kitData").ready(function () {

        if (analytics) {
            analytics.setSessionComponents(kitNumber);
        }

        if ($(window).width() < 750) {
            $(".tableLabel").css('text-align', 'left');
        } else
            $(".tableLabel").css('text-align', 'right');

        var GI = compName.slice(compName.indexOf("GI"), compName.indexOf(","));

        $("#kit_td").text(kitNumber);
        $("#theme_td").text(gameTheme);
        $("#wbs_td").text(wbs);
        if (eqeDate != "N/A" && eqeDate != "") {
            var year = eqeDate.slice(0, 4);
            var day = eqeDate.slice(4, 6);
            var month = eqeDate.slice(6, 8);
            $("#eqe_td").text("EQE: Completed on " + day + "/" + month + "/" + year + " (EQ-Score: " + eqe + ")");
            $('#eqe_td').css('text-decoration', 'underline');
            $("#eqe_td").attr('href', "http://acloud.insideigt.com/eqe/#/home/" + GI);
        } else {
            $('#eqe_td').text("N/A");
        }
        if (compName == null) compName = "";
        $("#comp_td").text(compName.replace(/,/g, ", "));
        $("#fifo_td").text(fifoDate);
        $("#outofpa_td").text(outOfPaDate);
        $("#kitstate_td").text(kitState);
        $('#WSRlink').attr('href', "http://acloud.insideigt.com/tools/WeeklySummaryReport/#/" + kitNumber);
        if (SC_dataTables["ThemeLogo"] != "") {
            var logo_src = "data:image/png;base64," + SC_dataTables["ThemeLogo"];
            /*$("#SC_Theme_Logo_Link").css("top", $("#SC_Theme_Logo_Link").parent().offset().top - $("#tabs").offset().top);
			$("#SC_Theme_Logo_Link").css("left", ($("#SC_Theme_Logo_Link").parent().offset().left - $("#tabs").offset().left) + $("#SC_Theme_Logo_Link").parent().width() - 163);*/
            $("#SC_Theme_Logo").attr("alt", gameTheme);
            $("#SC_Theme_Logo").attr("src", logo_src);

        } else $("#SC_Theme_Logo_Link").hide();

        $("#SC_Theme_Logo_LinkContainer.kitPageChartContainer").removeClass("loading");
    });

    config = getTimeSpentInConfig("timeSpentIn", "Time Spent In", currentTheme, SC_dataTables["BasicData"]);
    SC_chartConfigDict["timeSpentIn"] = config;
    chart = AmCharts.makeChart("timeSpentIn_chartdiv", config, 100);
    $("#timeSpentIn_chartdivContainer.kitPageChartContainer").removeClass("loading");
    chart.handleResize();
    chart.addListener("rendered", addSPListeners);
    SC_chartConfigDict["timeSpentIn"] = config;
    SC_chartDict["timeSpentIn"] = chart;
    chart.addLabel(
		'!135', '20',
		"Enlarge",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:EnlargeCellToFillTable( "timeSpentIn");');

    dataTableDataDict["teamTable"] = getTeamTableFromData(SC_dataTables["BasicData"]);

    dataTableClassListDict["teamTable"] = "row-border stripe cell-border compact";
    dataTableOptionsDict["teamTable"] = {
        "scrollY": 500,
        "bScrollCollapse": true,
        "scrollX": false,
        "dom": 'rt'
    };

    config = getSLAvsActualConfig("slaVsActual", "SLA vs Actual", currentTheme, SC_dataTables["PROs"]);
    chart = AmCharts.makeChart("slaVsActual_chartdiv", config, 100);
    $("#slaVsActual_chartdivContainer.kitPageChartContainer").removeClass("loading");
    chart.handleResize();
    chart.addLabel(
		'!135', '20',
		"Enlarge",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:EnlargeCellToFillTable("slaVsActual");');
    chart.addListener("rendered", addSPListeners);
    SC_chartConfigDict["slaVsActual"] = config;
    SC_chartDict["slaVsActual"] = chart;
    AmCharts.checkEmptyData(chart, chartEmptyText["slaVsActual_chartdiv"]);

    updateElementAfterRender("chartLayoutTable", setDefaultChartSizes);

    removeLoadingDiv();
}

function configureSPCharts_DefectHistory() {

    config = getDefectTrendsConfig("defectTrend", "Defects Trends", currentTheme, SC_dataTables["DefectTrends"]);
    SC_chartConfigDict["defectTrend"] = config;
    var chart = AmCharts.makeChart("defectTrend_chartdiv", config, 100);
    chart.handleResize();
    chart.addLabel(
		10, 10,
		"Show Defect Summary",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SwapChart("defectSummary_chartdiv", "defectSummary", "defectTrend_chartdiv", "defectTrend", "Show Defect Trends", "Show Defect Summary");');
    chart.addLabel(
		'!135', '20',
		"Enlarge",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:EnlargeCellToFillTable("defectTrend");');
    chart.addListener("rendered", addSPListeners);
    divsDict["defectTrend"] = $("defectTrend_chartdiv");
    SC_chartDict["defectTrend"] = chart;
    AmCharts.checkEmptyData(chart, chartEmptyText["defectTrend_chartdiv"]);

}

function configureSPCharts_Defects() {

    config = getDefectSummaryConfig("defectSummary", "Defect Summary", currentTheme, SC_dataTables["Defects"]);
    //chart = AmCharts.makeChart("defectSummary_chartdiv", config, 100);
    SC_chartConfigDict["defectSummary"] = config;

    config = getDefectSeverityPieConfig("defectSeverity", "Defect Severity", currentTheme, SC_dataTables["Defects"]);
    SC_chartConfigDict["defectSeverity"] = config;

    config = getDefectPhaseFoundPieConfig("defectPhaseFound", "Defect Phase Found", currentTheme, SC_dataTables["Defects"]);
    chart = AmCharts.makeChart("defectPhaseFound_chartdiv", config, 100);
    chart.handleResize();
    chart.addLabel(
		10, 10,
		"Show Defect Severity",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SwapChart("defectSeverity_chartdiv", "defectSeverity", "defectPhaseFound_chartdiv", "defectPhaseFound", "Show Defect Phase Found", "Show Defect Severity");');
    chart.addLabel(
		'!135', '20',
		"Enlarge",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:EnlargeCellToFillTable("defectPhaseFound");');
    chart.addListener("rendered", addSPListeners);
    SC_chartConfigDict["defectPhaseFound"] = config;
    SC_chartDict["defectPhaseFound"] = chart;
    AmCharts.checkEmptyData(chart, chartEmptyText["defectSeverity_chartdiv"]);

    dataTableDataDict["edaTable"] = SC_dataTables["EDA"];

    // outofpa table is here so we can get the out of pa date before the basic data pull is finished
    updateElementAfterRender("eda", function () {
        if ((SC_dataTables["OutOfPA"])[0].OutOfPA !== "") {
            var edaCount = SC_dataTables["EDA"].length;
            if (edaCount == 0) {
                $("#eda a").removeClass("showTableSC");
                $("#eda a").addClass("emptyTableSC");
            }

            $("#eda a").text(edaCount.toString());
        } else {
            $("#eda a").removeClass("showTableSC");
            $("#eda a").addClass("emptyTableSC");
            $("#eda a").text("N/A");
            $("#eda a").css("cursor", "text");
            $("#eda a").css("text-decoration", "none");
        }
    });

    dataTableDataDict["edaAllTable"] = SC_dataTables["EDA_All"];

    // outofpa table is here so we can get the out of pa date before the basic data pull is finished
    updateElementAfterRender("alleda", function () {
        if ((SC_dataTables["OutOfPA"])[0].OutOfPA !== "") {
            var edaAllCount = SC_dataTables["EDA_All"].length;
            if (edaAllCount == 0) {
                $("#alleda a").removeClass("showTableSC");
                $("#alleda a").addClass("emptyTableSC");
            }

            $("#alleda a").text(edaAllCount.toString());
        } else {
            $("#alleda a").removeClass("showTableSC");
            $("#alleda a").addClass("emptyTableSC");
            $("#alleda a").text("N/A");
            $("#alleda a").css("cursor", "text");
            $("#alleda a").css("text-decoration", "none");
        }
    });

    //Defect Count / (Defect Count + EDA Count)] * 100
    updateElementAfterRender("dcr", function () {
        if ((SC_dataTables["OutOfPA"])[0].OutOfPA !== "") {
            var edaCount = SC_dataTables["EDA"].length;
            var defectCount = SC_dataTables["Defects"].length;
            var dcr_val = 0;
            if (defectCount != 0) {
                dcr_val = (defectCount / (defectCount + edaCount)) * 100;
            }
            $("#dcr a").text(dcr_val.toFixed(2) + "%");
        } else {
            $("#dcr a").text("N/A");
        }
    });

    dataTableDataDict["closedDefectTable"] = SC_dataTables["Defects"];
    updateElementAfterRender("defectCount", function () {
        var defectCount = SC_dataTables["Defects"].length;
        if (defectCount == 0) {
            $("#defectCount a").removeClass("showTableSC");
            $("#defectCount a").addClass("emptyTableSC");
        }
        $("#defectCount a").text(defectCount.toString());
    });    

    dataTableClassListDict["edaTable"] = "row-border stripe cell-border compact hover";
    dataTableOptionsDict["edaTable"] = {
        "scrollY": 200,
        "bScrollCollapse": true,
        "scrollX": true,
        "colReorder": true,
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5'],
        "fnDrawCallback": function (oSettings) {
            if (jQuery('#edaTable_paginate span span.paginate_button').size()) {
                jQuery('#edaTable_paginate')[0].style.display = "block";
            } else {
                jQuery('#edaTable_paginate')[0].style.display = "none";
            }
        }
    };

    dataTableClassListDict["edaAllTable"] = "row-border stripe cell-border compact hover";
    dataTableOptionsDict["edaAllTable"] = {
        "scrollY": 200,
        "bScrollCollapse": true,
        "scrollX": true,
        "colReorder": true,
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5'],
        "fnDrawCallback": function (oSettings) {
            if (jQuery('#edaAllTable_paginate span span.paginate_button').size()) {
                jQuery('#edaAllTable_paginate')[0].style.display = "block";
            } else {
                jQuery('#edaAllTable_paginate')[0].style.display = "none";
            }
        }
    };

    dataTableClassListDict["closedDefectTable"] = "row-border stripe cell-border compact hover";
    dataTableOptionsDict["closedDefectTable"] = {
        "scrollY": 200,
        "bScrollCollapse": true,
        "scrollX": true,
        "colReorder": true,
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5'],
        "fnDrawCallback": function (oSettings) {
            if (jQuery('#closedDefectTable_paginate span .paginate_button').size() > 1) {
                jQuery('#closedDefectTable_paginate')[0].style.display = "block";
            } else {
                jQuery('#closedDefectTable_paginate')[0].style.display = "none";
            }
        }
    };
    hideColumnsDict["closedDefectTable"] = ["Project", "KitTheme", "startdate", "enddate"];

}

function configureSPCharts_KitTimeline() {   

    config = createTimeLineConfig("timeLine", "Kit Timeline", currentTheme, kitNumber, SC_dataTables["KitTimeLine"]);
    chart = AmCharts.makeChart("timeLine_chartdiv", config, 100);
    chart.handleResize();
    /*
	chart.addLabel(
		10, 10,
		"Show Time Spent In",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		false,
		'javascript:SwapChart("timeSpentIn_chartdiv", "timeSpentIn", "timeLine_chartdiv", "timeLine", "Show Kit Timeline", "Show Time Spent In");');
	chart.addLabel(
		'!100', '20',
		"Enlarge",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		false,
		'javascript:EnlargeCellToFillTable( "timeLine");');*/
    chart.addListener("rendered", addSPListeners);
    SC_chartConfigDict["timeLine"] = config;
    SC_chartDict["timeLine"] = chart;
    timeLineChartLen = SC_chartDict["timeLine"].valueAxes[0].gridCount;
    //dealWithLegends();
    chart.handleResize();
   
}


function configureSPCharts_JCO() {

    dataTableDataDict["jurTable"] = SC_dataTables["JCO"];
    updateElementAfterRender("jco", function () {
        var fp_status = SC_dataTables["FP"][0]["Status"];
        if (SC_dataTables["FP"][0]["Status"] === "Approved") {
            fp_status = "First Pass";
        }
        $("#approved_submit_span").text(fp_status);
        if (fp_status == "Pre-Submit") {
            $("#approval_date_span").text("");
            if (SC_dataTables["JCO"].length > 0) {
                $("#jur_label_span").text("; Connected To these ");
                $("#jco a").text("Jurs");
            } else {
                $("#jur_label_span").text("; No Jurs Connected");
                $("#jco a").text("");
            }
        } else {
            var approved_date = SC_dataTables["FP"][0]["On_Date"];
            if (approved_date != "")
                approved_date = " on " + approved_date;
            else approved_date = "";
            $("#approval_date_span").text(approved_date);
            if (SC_dataTables["FP"][0]["Submission_Agency"] != "") {
                var submission_agency = SC_dataTables["FP"][0]["Submission_Agency"];
                if (fp_status == "Submitted") {
                    submission_agency = " to " + submission_agency;
                } else //fp_status == "Approved" or fp_status == "Rejected" 
                {
                    submission_agency = " by " + submission_agency;
                }

                $("#jur_label_span").text(submission_agency + " for Jurisdiction: ");
            } else $("#jur_label_span").text(" for Jurisdiction: ");
            $("#jco a").text(SC_dataTables["FP"][0]["Jur"]);
        }
    });


    dataTableClassListDict["jurTable"] = "row-border stripe cell-border compact hover";
    dataTableOptionsDict["jurTable"] = {
        "scrollY": 200,
        "bScrollCollapse": true,
        "sPaginationType": "full_numbers",
        "scrollX": true,
        "colReorder": true,
        "dom": '<"top">rt<"bottom"fp>',
        "fnDrawCallback": function (oSettings) {
            if (jQuery('#jurTable_paginate span .paginate_button').size() > 1) {
                jQuery('#jurTable_paginate')[0].style.display = "block";
            } else {
                jQuery('#jurTable_paginate')[0].style.display = "none";
            }
        }
    };
}

function configureSPCharts_ToolsPerKit() {

    dataTableDataDict["toolsTableForKit"] = SC_dataTables["ToolsPerKit"];
    updateElementAfterRender("toolsUsed", function () {
        var toolsUsed = SC_dataTables["ToolsPerKit"].length;
        if (toolsUsed == 0) {
            $("#toolsUsed a").removeClass("showTableSC");
            $("#toolsUsed a").addClass("emptyTableSC");
        }
        $("#toolsUsed a").text(toolsUsed.toString());
    });

	dataTableClassListDict["toolsTableForKit"] = "row-border stripe cell-border compact hover";
	dataTableOptionsDict["toolsTableForKit"] = {
	    "scrollY": 200,
	    "bScrollCollapse": true,
	    "scrollX": true,
	    "colReorder": true,
	    "dom": 'rtBfp',
	    buttons: ['copyHtml5', 'csvHtml5'],
	    "fnDrawCallback": function (oSettings) {
	        if (jQuery('#toolsTableForKit_paginate span .paginate_button').size() > 1) {
	            jQuery('#toolsTableForKit_paginate')[0].style.display = "block";
	        } else {
	            jQuery('#toolsTableForKit_paginate')[0].style.display = "none";
	        }
	    }
	};
	hideColumnsDict["toolsTableForKit"] = [];

	//drawTable(dataTableDataDict["toolsTableForKit"], "toolsTableForKit", "100%", dataTableOptionsDict["toolsTableForKit"], dataTableClassListDict["toolsTableForKit"], hideColumnsDict["toolsTableForKit"]);

}


function configureSPCharts_GameTracker() {
    $("#sdk_td").text((SC_dataTables["GameTrackerData"])[0].SdkVersion);

    if ((SC_dataTables["GameTrackerData"])[0].UtpEnabled==="0") {
        $("#utp_td").text("Disabled");
    }
    else if ((SC_dataTables["GameTrackerData"])[0].UtpEnabled === "1") {
        $("#utp_td").text("Enabled");
    }
    else  {
        $("#utp_td").text("Unknown");
    }
}


function removeScoreCard() {
    $.when($("#scoreCard").remove()).then(setTimeout(resetCurrentHash, 100));

    if (parent_prefix === "kitPage") {
        $("#metric_kitPage").removeClass('active');
        $("#metric_kitPage").addClass('hide');
    }
	if (parent_prefix.endsWith("_L2") || parent_prefix.endsWith("_L3")) {

		$("#" + parent_prefix + "_chartdiv").show();
		var tempStr = parent_prefix;
		tempStr = parent_prefix.substr(0, parent_prefix.length - 3);
		$("#" + tempStr + "_dataTable_group").show();
		$("#" + tempStr + "_dataTable_link").show();


		$("#link_" + tempStr + "_4").remove();
		level--;
		$("#link_" + tempStr + "_" + level).addClass('active');
	} else displayTopLevelMetric(parent_prefix);
}

function SwapChart(target_div, target_chart, from_div, from_chart, target_label, from_label) {
	var chart = SC_chartDict[from_chart];
	var labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Enlarge");
	var enlargeShrinkFuncName = "EnlargeCellToFillTable";
	var enlargeShrinkText = "Enlarge";
	if (labelIndex == -1) {
		enlargeShrinkFuncName = "ShrinkCellBack";
		enlargeShrinkText = "Shrink";
	}

	var newDiv = $('<div id="' + target_div + '"  class="col-sm-12 col-md-12 col-lg-6" style="height: 400px; background-color: #'+bgColor+';"></div>');
	$("#" + from_div).replaceWith(newDiv);
	chart = AmCharts.makeChart(target_div, SC_chartConfigDict[target_chart], 100);
	chart.handleResize();
	chart.addLabel(
		10, 10,
		target_label,
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SwapChart("' + from_div + '", "' + from_chart + '", "' + target_div + '", "' + target_chart + '","' + from_label + '", "' + target_label + '");');
	chart.addLabel(
		'!135', '20',
		enlargeShrinkText,
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:' + enlargeShrinkFuncName + '("' + target_chart + '");');
	chart.addListener("rendered", addSPListeners);
	AmCharts.checkEmptyData(chart, chartEmptyText[target_div]);
	SC_chartDict[target_chart] = chart;

	if (from_div == enlargedChart) {
		$("#" + target_div).css('width', '100%');
		enlargedChart = target_div;
	}

	activeCharts[from_div] = 0;
	activeCharts[target_div] = 1;
	chart.animateAgain();
}

function setDefaultChartSizes() {

}

function EnlargeCellToFillTable(target_chart) {


	var chart = SC_chartDict[target_chart];

	var labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Enlarge");
	chart.allLabels[labelIndex].text = "Shrink";
	chart.allLabels[labelIndex].url = 'javascript:ShrinkCellBack("' + target_chart + '");';

	enlargedChart = target_chart + "_chartdiv";
	$("#" + target_chart + "_chartdiv").css('width', $("#tableWrapper").width());
	for (var chart_name in activeCharts) {
		if (chart_name !== target_chart + "_chartdiv")
			$("#" + chart_name).hide();
	}


	//if (target_chart === "timeLine")
	//	dealWithLegends();

	chart.handleResize();
	chart.validateNow();
}

function ShrinkCellBack(target_chart) {

	var chart = SC_chartDict[target_chart];

	var labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Shrink");
	chart.allLabels[labelIndex].text = "Enlarge";
	chart.allLabels[labelIndex].url = 'javascript:EnlargeCellToFillTable("' + target_chart + '");';

	enlargedChart = null;

	if (singleRowCharts)
		$("#" + target_chart + "_chartdiv").css('width', '100%');
	else
		$("#" + target_chart + "_chartdiv").css('width', '50%');

	for (var chart_name in activeCharts) {
		if (chart_name !== target_chart + "_chartdiv")
			$("#" + chart_name).show();
	}

	//if (target_chart === "timeLine")
	//	dealWithLegends();

	chart.validateNow();
}

function addSPListeners(event) {
	$('a[href="http://www.amcharts.com/javascript-charts/"]').remove(); // remove watermark
	chart = event.chart;
	if (chart.titles[0].id == "defectTrend") {
		// zoom to last 5 weeks
		//chart.zoomToIndexes(chart.dataProvider.length - 6, chart.dataProvider.length - 1);
	}
}

function SC_showHideDataTable(dataTableID, width, show) {
	if (show != undefined) {
		dataTableIsVisible[dataTableID] = !show; // set visible to opposite of show so the below code toggles it
	}

	if (dataTableIsVisible.hasOwnProperty(dataTableID) && dataTableIsVisible[dataTableID]) {
		clearTable(dataTableID);
		dataTableIsVisible[dataTableID] = false;
	} else {
		drawTable(dataTableDataDict[dataTableID], dataTableID, width, dataTableOptionsDict[dataTableID], dataTableClassListDict[dataTableID], hideColumnsDict[dataTableID]);
		dataTableIsVisible[dataTableID] = true;
	}
}