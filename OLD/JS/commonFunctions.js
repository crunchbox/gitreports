function GetWebTools(username) {
    var client = new $.RestClient('/rest/');
    client.add("getWebTools");
    var restRequest = client.getWebTools.create({}, {
        user: username
    }); // post request
    restRequest.done(get_GetWebTool_callBack);
}

function get_GetWebTool_callBack(r) {
    var webTools = r["WebTools"];
    var localhost = false;
    if (document.baseURI != null) {
        localhost = document.baseURI.indexOf("localhost") > 1 ? true : false;
    } else if (document.URL != null) {
        localhost = document.URL.indexOf("localhost") > 1 ? true : false;
    }
    var demo_site = false;
    if (document.baseURI != null) {
        demo_site = document.baseURI.indexOf(":81") > 1 ? true : false;
    } else if (document.URL != null) {
        demo_site = document.URL.indexOf(":81") > 1 ? true : false;
    }
    for (var i = 0; i < webTools.length; i++) {
        if (localhost || demo_site) { // if in dev mode, dont worry about dev flad
            var webtool_link = $('<li><a href="' + webTools[i]["url"] + '" target="blank"><i class="fa fa-fw fa-gear"></i>' + webTools[i]["name"] + '</a></li>');
            $('#top_dropdown_ul').prepend(webtool_link);
        } else { // if not in dev mode, check dev flag before displaying
            if (webTools[i].dev !== true) {
                var webtool_link = $('<li><a href="' + webTools[i]["url"] + '" target="blank"><i class="fa fa-fw fa-gear"></i>' + webTools[i]["name"] + '</a></li>');
                $('#top_dropdown_ul').prepend(webtool_link);
            }
        }

    }
    resetCurrentHash();
}


function loadXMLDoc(xml_url, callback_function) {
	$.ajax({
		type: "GET",
		url: xml_url,
		dataType: "text",
		success: function (xml) {
			callback_function(xml);
		}
	});
}

function show_loadingDiv(message, parent_id, isFloating, height, top, left, textonly) {
	if (document.getElementById("loadingdiv") === null) { // only add the loading div if its not already present
		if (parent_id == undefined) {
			parent_id = "#page-wrapper";
		}
		if (height == undefined)
			height = $("#page-wrapper").css("height");
		if (top == undefined)
			top = $("#page-wrapper").offset().top;
		if (left == undefined)
			left = $("#page-wrapper").offset().left;

		var $div = $("<div/>", {
				id: "loadingdiv"
			})
			.css("z-index", 100)
			.css("background-color", bgColor)
			.css("text-align", "center")
			.css("line-height", "300px")
			.css("font-size", "20px")
			.css("color", "#FF0000")
			.css("width", "100%")
			.css("height", height)
			.css("top", top)
			.css("left", left);
		if (isFloating == true)
			$div.css("position", "absolute");

		if (textonly)
			$div.html(message);
		else {
			var $animation_div = $("<div class='loading-animation'><svg xmlns='http://www.w3.org/2000/svg' version='1.1'><defs><filter id='gooey'><feGaussianBlur in='SourceGraphic' stdDeviation='10' result='blur'></feGaussianBlur><feColorMatrix in='blur' mode='matrix' values='1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7' result='goo'></feColorMatrix><feBlend in='SourceGraphic' in2='goo'></feBlend></filter></defs></svg><div class='blob blob-0'></div><div class='blob blob-1'></div><div class='blob blob-2'></div><div class='blob blob-3'></div><div class='blob blob-4'></div><div class='blob blob-5'></div></div>");
			$div.append($animation_div);
		}
		$(parent_id).prepend($div);
	}
}

function removeLoadingDiv() {
	$("#loadingdiv").remove(); // remove loading div
}

function updateElementAfterRender(element_id, postRenderFunction, time) {
	if (time == undefined)
		time = 50;
	if ($('#' + element_id).is(':visible')) { //if the container is visible on the page
		postRenderFunction(); //Adds a grid to the html
	} else {
		setTimeout(updateElementAfterRender, time); //wait 50 ms, then try again
	}
}

Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('');
};

Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf());
  dat.setDate(dat.getDate() + days);
  return dat;
}


function getTomorrowsISODate()
{
    var today = new Date();
    var tomorrowsISODate = today.addDays(1).yyyymmdd();
    return tomorrowsISODate;
}

function getStartOfPrevQuarter(d, v)
{
    var monthsChange = (v * 3) * -1;
    var currentQuarter = Math.floor((d.getMonth() + 3) / 3) - 1;
    var startofQuarter = new Date(d.getFullYear(), currentQuarter * 3, 1);
    return startofQuarter.addMonths(monthsChange);
}