function getFirstPassData(xml) {
    //"<FP_Status><Jur>BAH</Jur><Status>Approved</Status><Date>20160503</Date></FP_Status>" 
    fpData = {};
    $(xml.firstChild).find('FP_Status').children().each(function () {
        var tagName = this.tagName;
        var val = $(this).text();
        fpData[tagName] = val;
    });
    return fpData;
}

function getSLAvsActualConfig(chart_id, chartTitle, theme, data) {
    var reporting_SLA = 0;
    var actual_SLA = 0;
    var reporting_Hours = 0;
    var actual_Hours = 0;

    if (data.length > 0) {
        reporting_SLA = parseFloat(data[0]["Reporting_SLA_Duration"]);
        actual_SLA = parseFloat(data[0]["Actual_SLA_Duration"]);
        reporting_Hours = parseFloat(data[0]["Reporting_Person_Hours"]);
        actual_Hours = parseFloat(data[0]["Actual_Person_Hours"]);
    }

    var config = {
        "type": "serial",
        "theme": theme,
        "startDuration": 1,
        "categoryField": "type",
        "mouseWheelZoomEnabled": false,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "legend": {
            "position": "bottom",
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
                {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
                }
            ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        }
    };
    config.dataProvider = [
        {
            "type": "Business Days",
            "days_sla": reporting_SLA,
            "days_actual": actual_SLA
        },
        {
            "type": "Personnel Hours",
            "hours_sla": reporting_Hours,
            "hours_actual": actual_Hours
        }];
    config.graphs = [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 1,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Actual Days",
            "type": "column",
            "fillColors": "#C58B33",
            "color": "#FFFFFF",
            "labelPosition": "middle",
            "clustered": true,
            "valueAxis": "v1",
            "valueField": "days_actual"
                },
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 1,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "SLA Planned Days",
            "type": "column",
            "clustered": true,
            "fillColors": "#f7e923",
            "color": "#000000",
            "labelPosition": "middle",
            "valueAxis": "v1",
            "valueField": "days_sla"
                },
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 1,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Actual Hours",
            "type": "column",
            "clustered": true,
            "color": "#FFFFFF",
            "fillColors": "#B84346",
            "labelPosition": "middle",
            "valueAxis": "v2",
            "valueField": "hours_actual"
                },
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 1,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "SLA Planned Hours",
            "type": "column",
            "clustered": true,
            "fillColors": "#D1774D",
            "color": "#000000",
            "labelPosition": "middle",
            "valueAxis": "v2",
            "valueField": "hours_sla"
                }];
    config.valueAxes = [{
            "id": "v1",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "axisAlpha": 1,
            "minimum": 0,
            "title": "Days",
            "position": "left",
            "autoGridCount": false
            },
        {
            "id": "v2",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "axisAlpha": 1,
            "gridAlpha": 0,
            "minimum": 0,
            "title": "Hours",
            "position": "right",
            "autoGridCount": false
            }];
    return config;
}

/*function getEFTConfig() {
    var config = {
        "type": "column",
        "theme": theme,
        "startDuration": 1,
        "categoryField": "type",
        "mouseWheelZoomEnabled": false,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "legend": {
            "position": "bottom",
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "export": {
            "enabled": true,
            "backgroundColor": "#222222",
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        }    
    };

}*/

function getDefectSeverityPieConfig(chart_id, chartTitle, theme, data) {
    var dataRow;

    var chartData = [];
    var dataEntry;

    var severityDict = {};
    var severity;

    var tempInt;

    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];

            severity = dataRow["Severity"];
            if (severityDict.hasOwnProperty(severity)) {
                tempInt = severityDict[severity];
                severityDict[severity] = tempInt + 1;
            } else {
                severityDict[severity] = 1;
            }
        }
    }

    for (severity in severityDict) {
        if (severityDict.hasOwnProperty(severity)) {
            if (severityDict.hasOwnProperty(severity)) {
                dataEntry = {};
                dataEntry["severity"] = severity;
                dataEntry["value"] = severityDict[severity];
                chartData.push(dataEntry);
            }
        }
    }

    var config = {
        "type": "pie",
        "theme": theme,
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "legend": {
            "markerType": "circle",
            "position": "right",
            "marginRight": 80,
            "autoMargins": false
        },
        "dataProvider": chartData,
        "valueField": "value",
        "titleField": "severity",
        "balloon": {
            "fixedPosition": true
        },
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 300,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        }
                    }
                }
            ]
        }
    };
    return config;
}

function getDefectPhaseFoundPieConfig(chart_id, chartTitle, theme, data) {
    var dataRow;

    var chartData = [];
    var dataEntry;

    var phaseDict = {};
    var phase;
    var severity;

    var tempInt;

    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];

            phase = dataRow["Phase_Found"];
            severity = dataRow["Severity"];
            if (phaseDict.hasOwnProperty(phase)) {
                tempInt = phaseDict[phase];
                phaseDict[phase] = tempInt + 1;
            } else {
                phaseDict[phase] = 1;
            }
        }
    }

    for (phase in phaseDict) {
        if (phaseDict.hasOwnProperty(phase)) {
            dataEntry = {};
            dataEntry["phase"] = phase;
            dataEntry["value"] = phaseDict[phase];
            chartData.push(dataEntry);
        }
    }

    var config = {
        "type": "pie",
        "theme": theme,
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
                            }
                        ],
        "legend": {
            "markerType": "circle",
            "position": "bottom",
            "marginBottom": 20,
            "autoMargins": false
        },
        "dataProvider": chartData,
        "valueField": "value",
        "titleField": "phase",
        "balloon": {
            "fixedPosition": true
        },
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 200,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        },
                        "balloon": {
                            "enabled": false
                        }
                    }

                }
            ]
        }
    };
    return config;
}

function getDefectSummaryConfig(chart_id, chartTitle, theme, data) {
    var dataRow;

    var chartData = [];
    var dataEntry;

    var phaseDict = {};
    var phase;
    var severity;

    var tempInt;

    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];

            phase = dataRow["Phase_Found"];
            severity = dataRow["Severity"];
            if (phaseDict.hasOwnProperty(phase)) {
                tempInt = phaseDict[phase]["total"];
                phaseDict[phase]["total"] = tempInt + 1;
                if (phaseDict[phase].hasOwnProperty(severity)) {
                    tempInt = phaseDict[phase][severity];
                    phaseDict[phase][severity] = tempInt + 1;
                } else {
                    phaseDict[phase][severity] = 1;
                }
            } else {
                phaseDict[phase] = {};
                phaseDict[phase]["total"] = 1;
                phaseDict[phase]["4-Low"] = 0;
                phaseDict[phase]["3-Medium"] = 0;
                phaseDict[phase]["2-High"] = 0;
                phaseDict[phase]["1-Critical"] = 0;
                phaseDict[phase][severity] = 1;
            }
        }
    }

    for (phase in phaseDict) {
        if (phaseDict.hasOwnProperty(phase)) {
            dataEntry = {};
            dataEntry["phase"] = phase;
            dataEntry["critical"] = phaseDict[phase]["1-Critical"];
            dataEntry["high"] = phaseDict[phase]["2-High"];
            dataEntry["medium"] = phaseDict[phase]["3-Medium"];
            dataEntry["low"] = phaseDict[phase]["4-Low"];
            dataEntry["total"] = phaseDict[phase]["total"];
            chartData.push(dataEntry);
        }
    }

    var graphs = [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "4-Low",
            "type": "column",
            "valueField": "low"
    },
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "3-Medium",
            "type": "column",
            "valueField": "medium"
    },
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "2-High",
            "type": "column",
            "valueField": "high"
    },
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "1-Critical",
            "type": "column",
            "valueField": "critical"
    }];

    var config = {
        "type": "serial",
        "theme": theme,
        "startDuration": 1,
        "categoryField": "phase",
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": false,
        "categoryAxis": {
            "gridPosition": "start",
            "title": "Phase Found",
            "labelOffset": -2
        },
        "dataProvider": chartData,
        "graphs": graphs,
        "valueAxes": [{
            "id": "v1",
            "title": "Defects",
            "stackType": "regular"
        }],
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "chartScrollbar": {
            "autoGridCount": false,
            "scrollbarHeight": 40,
            "selectedBackgroundColor": scrollbarColor,
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": scrollbarLineColor
        },
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 800,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        }
                    }
                },
                {
                    "maxWidth": 500,
                    "overrides": {
                        "valueAxes": {
                            "inside": true
                        }
                    }
                }
            ]
        }
    };

    return config;
}

function getDefectTrendsConfig(chart_id, chartTitle, theme, data) {
    var dataRow;
    var col;

    var chartData = [];
    var dataEntry;

    var weekDict = {};
    var week;

    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];
            week = dataRow["Sundays"];
            if (weekDict.hasOwnProperty(week) == false) {
                weekDict[week] = {};
                weekDict[week]["Open"] = 0;
                weekDict[week]["Closed"] = 0;
            }
            weekDict[week][dataRow["state"]] = dataRow["Count"];
        }
    }

    for (week in weekDict) {
        if (weekDict.hasOwnProperty(week)) {
            dataEntry = {};
            dataEntry["weeks"] = week;
            dataEntry["Open"] = weekDict[week]["Open"];
            dataEntry["Closed"] = weekDict[week]["Closed"];
            chartData.push(dataEntry);
        }
    }

    var trendGraphs = [{
            "id": "g1",
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Open",
            "type": "column",
            "valueField": "Open"
                },
        {
            "id": "g2",
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Closed",
            "type": "column",
            "valueField": "Closed"
                }];

    var config = {
        "type": "serial",
        "theme": theme,
        "startDuration": 1,
        "categoryField": "weeks",
        "mouseWheelZoomEnabled": false,
        "mouseWheelScrollEnabled": true,
        "dataDateFormat": "YYYYMMDD",
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2,
            "parseDates": true,
            "equalSpacing": true
        },
        "dataProvider": chartData,
        "graphs": trendGraphs,
        "valueAxes": [{
            "id": "v1",
            "title": "Defects",
            "stackType": "regular"
        }],
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "chartScrollbar": {
            "autoGridCount": false,
            "scrollbarHeight": 40,
            "selectedBackgroundColor": scrollbarColor,
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": scrollbarLineColor
        },
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: false,
            pan: true
        },
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 800,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        }
                    }
                },
                {
                    "maxWidth": 500,
                    "overrides": {
                        "valueAxes": {
                            "inside": true
                        }
                    }
                }
            ]
        }
    };

    return config;
}

function getTimeSpentInConfig(chart_id, chartTitle, theme, data) {
    var dataRow;
    var col;

    var chartData = [];
    var dataEntry;

    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];
            for (col in dataRow) {
                if (dataRow.hasOwnProperty(col)) {
                    if (col.endsWith("Work_Days")) {
                        dataEntry = {};
                        dataEntry["stategroup"] = col.replaceAll('_', ' ');
                        dataEntry["value"] = dataRow[col];
                        chartData.push(dataEntry);
                    }
                }
            }
        }
    }
    var config = {
        "type": "pie",
        "theme": theme,
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "legend": {
            "markerType": "circle",
            "position": "right",
            "marginRight": 80,
            "autoMargins": false,
        },
        "numberFormatter": {
            "precision": 2,
            "decimalSeparator": '.',
            "thousandsSeparator": ','
        },
        "dataProvider": chartData,
        "valueField": "value",
        "titleField": "stategroup",
        "balloon": {
            "fixedPosition": true
        },
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "responsive": {
            "enabled": true,

            "rules": [
                {
                    "maxWidth": 900,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        },
                    }
                }
            ]
        }
    };
    return config;
}

function getTeamTableFromData(data) {
    var table = [];
    var tableEntry;
    var dataRow;
    var col;
    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];
            for (col in dataRow) {
                if (dataRow.hasOwnProperty(col)) {
                    if (col != "Name" && col != "KitTheme" && col != "KitThemeID" &&
                        col != "OutOfPA" && col != "FIFO" && col != "PAStartDate" &&
                        col != "Component" && col != "KitState" && !col.endsWith("_Days")) {
                        tableEntry = {};
                        tableEntry["attribute"] = col.replaceAll('_', ' ');
                        tableEntry["value"] = dataRow[col];
                        table.push(tableEntry);
                    }
                }
            }
        }
    }
    return table;
}


function getLuma(color) {
    if (color) {
        var c = color.substring(1); // strip #
        var rgb = parseInt(c, 16); // convert rrggbb to decimal
        var r = (rgb >> 16) & 0xff; // extract red
        var g = (rgb >> 8) & 0xff; // extract green
        var b = (rgb >> 0) & 0xff; // extract blue

        //var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
        var luma = (r + g + b) / 3;
        return luma;
    }
    return 0;
}

function isTooDark(color) {
    var luma = getLuma(color);
    if (luma < 128) {
        return false;
    }
    return true;
}

function createTimeLineConfig(chart_id, chartTitle, theme, kitNumber, data) {

    var dataRow;
    var stateDict = {};
    var colorIndex = 0;
    var totalDays = 0;
    var colorArray = ['#e48701', '#a5bc4e', '#1b95d9', '#caca9e', '#6693b0', '#f05e27', '#86d1e4', '#e4f9a0', '#ffd512',
    '#75b000', '#0662b0', '#ede8c6', '#cc3300', '#d1dfe7', '#52d4ca', '#c5e05d', '#e7c174', '#fff797',
    '#c5f68f', '#A8F4B1', '#F1ADFD'];

    var chartData = [{}];
    chartData[0]["kit"] = kitNumber;

    var graphs = [];
    var graphInstace;
    var duration;

    for (var index in data) {
        if (data.hasOwnProperty(index)) {
            dataRow = data[index];
            if (stateDict.hasOwnProperty(dataRow["State"]) == false) {
                stateDict[dataRow["State"]] = colorArray[colorIndex];
                colorIndex++;
            }

            if (dataRow.hasOwnProperty("Duration") == false)
                duration = Math.ceil(totalDays) - totalDays; // if null its the last state so just give it the rest
            else if (!isNaN(dataRow["Duration"])) {
                duration = Number(dataRow["Duration"]);
            } else {
                duration = 0;
            }

            chartData[0][dataRow["StateInstance"]] = duration;
            totalDays += duration;

            graphInstace = {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]] Days</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[title]]",
                "lineAlpha": 0.3,
                "type": "column",
            };
            graphInstace["valueField"] = dataRow["StateInstance"];
            graphInstace["fillColors"] = stateDict[dataRow["State"]];
            // set label color to be white or black depending on the darkness of the fill color
            if (isTooDark(graphInstace["fillColors"]))
                graphInstace["color"] = "#000000";
            else graphInstace["color"] = "#ffffff";

            graphInstace["title"] = dataRow["State"];
            if (dataRow.hasOwnProperty("Duration") == false) // for the last state
                graphInstace["balloonText"] = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>Current State</b></span>";
            graphs.push(graphInstace);
        }
    }
    totalDays = Math.ceil(totalDays);
    var tickFeq = Math.floor(totalDays / 20);
    if (tickFeq < 1) tickFeq = 1;

    var legendData = [];
    var legendEntry;
    for (var state in stateDict) {
        if (stateDict.hasOwnProperty(state)) {
            legendEntry = {};
            legendEntry["title"] = state;
            legendEntry["color"] = stateDict[state];
            legendData.push(legendEntry);
        }
    }


    var config = {
        "type": "serial",
        "theme": theme,
        "titles": [{
            "id": chart_id,
            "size": 15,
            "text": chartTitle
        }],
        "legend": {
            "markerSize": 10,
            "position": "bottom",
            "data": legendData
        },
        "dataProvider": chartData,
        "graphs": graphs,
        "startDuration": 1,
        "rotate": true,
        "categoryField": "kit",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "labelsEnabled": false,
            "position": "left"
        },
        "chartCursor": {
            "zoomable": false,
            "valueZoomable": true,
            "avoidBalloonOverlapping": false,
            "categoryBalloonEnabled": false,
            "oneBalloonOnly": true,
            "cursorAlpha": 0,
            "balloonPointerOrientation": "vertical"
        },
        "mouseWheelZoomEnabled": false,
        "mouseWheelScrollEnabled": false,
        "valueScrollbar": {
            "autoGridCount": true,
            "valueLineEnabled": false,
            "valueLineBalloonEnabled": false,
            "scrollbarHeight": 25,
            "ignoreAxisWidth": true,
            "selectedBackgroundColor": scrollbarColor,
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": scrollbarLineColor
        },
        "valueAxes": [{
            "stackType": "regular",
            "autoGridCount": false,
            "minimum": 0,
            "maximum": totalDays,
            "gridCount": totalDays,
            "minMaxMultiplier": 1,
            "labelFrequency": tickFeq,
            "axisAlpha": 0.5,
            "gridAlpha": 0,
            "title": "Days"
        }],
        "export": {
            "enabled": true,
            "backgroundColor": bgColor,
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "responsive": {
            "enabled": true
        }
    };
    return config;
}