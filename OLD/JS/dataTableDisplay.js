function getLongestRowIndex(data) {
	var max_len = 0;

	var max_len_index = 0;

	for (var index = 0; index < data.length; index++) {
		if (Object.keys(data[index]).length > max_len) {
			max_len = Object.keys(data[index]).length;
			max_len_index = index;
		}
	}

	return max_len_index;
}

function getColumnList(data, hideColumnsList) {
	var colDict = {};
	var dataEntry;
	for (var index = 0; index < data.length; index++) {
		dataEntry = data[index];
		for (var col in dataEntry) {
			if (dataEntry.hasOwnProperty(col)) {
				if (hideColumnsList == undefined || hideColumnsList.indexOf(col) == -1) {
					if (colDict.hasOwnProperty(col) == false)
						colDict[col] = true;
				}
			}
		}
	}
	return Object.keys(colDict);
}

function clearTable(htmlTable) {
	$('#' + htmlTable + "_group").empty();
}

function drawTable(data, htmlTable, width, options, classList, hideColumnsList) {

	if (data.length > 0) {
		$.fn.dataTable.ext.search = []; // clear any global filters

		if (classList == undefined) {
			classList = "row-border stripe cell-border compact";
		}

		var $table = $('<table id="' + htmlTable + '" class="' + classList + '" cellspacing="0" width="100%"></table>');

		if (options == undefined) {
		    options = {
		        dom: 'Blfrtip',
		        buttons: [
		            "excelHtml5",
		            "csvHtml5"
		        ],
				"scrollY": 200,
				"scrollX": true
			};
		}

		if (width) {
			$('#' + htmlTable + "_group").css("width", width);
		}

		var colList = getColumnList(data, hideColumnsList);

		$table.append(drawHeader(colList));

		$table.append(drawRows(data, colList));

		$('#' + htmlTable + "_group").append($table);

		return $table.DataTable(options);
	}
	return null;
}

function drawHeader(colList) {
	var thead = $("<thead/>");
	var row = $("<tr />");
	for (var key in colList) {
		if (colList.hasOwnProperty(key)) {
			row.append($("<th>" + colList[key] + "</th>"));
		}
	}
	thead.append(row);
	return thead;
}

function drawRows(data, colList) {
	var tbody = $("<tbody />");
	var rowData;
	var rows = [];
	var row;

	for (var i = 0; i < data.length; i++) {
		row = $("<tr />");
		rowData = data[i];
		for (var key in colList) {
			if (colList.hasOwnProperty(key)) {
				if (rowData.hasOwnProperty(colList[key]))
					row.append($("<td>" + rowData[colList[key]] + "</td>"));
				else row.append($("<td>0</td>"));
			}
		}
		rows.push(row);
	}
	tbody.append(rows);
	return tbody;
}