function get_DefectsForKitsReleasedMetrics() {

	if (loadedMetrics["defects"] != true && loadedMetrics["kitdefects"] != true) {
	    get_MetricsData("defects", "defectsForKitsReleased");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Defects For Kits Released");

	clearChart("defectsForKitsReleased");
	showHideDataTable("defectsForKitsReleased", "100%", false);

	chartData = parseMetricData(restJsonDict, "DefectsClosedPerKitWBS_Month", studio_alias, "defectsForKitsReleased");
	chartGraphs = GetGraphs(chartData, "month");

	dataTableDataDict["defectsForKitsReleased_dataTable"] = chartData;
    //drawTable(chartData, "defectsForKitsReleased_dataTable");

	var defectReleasedConfig = generateConfig("defectsForKitsReleased", "Defects For Kits Released - Studio", "Defects", "month", currentTheme, false);
	defectReleasedConfig.dataProvider = chartData;
	defectReleasedConfig.graphs = chartGraphs;

	chartConfigDict["defectsForKitsReleased"] = defectReleasedConfig;
	chart = AmCharts.makeChart("defectsForKitsReleased_chartdiv", defectReleasedConfig, 100);
	chartResizerDict["defectsForKitsReleased"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDataDict["defectsForKitsReleased"] = chartData;
	chartDict["defectsForKitsReleased"] = chart; // store chart
}

function parseDefectsReleasedToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempStudio;
	var rollingTotal = 0;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;
				row[tempStudio] = tempInt + parseInt(datarow["Count"]);
				rollingTotal += parseInt(datarow["Count"]);
			}
		}
	}

	if (sub_category == undefined && sub_category != "Total")
		row["Total"] = rollingTotal;

	return row;
}