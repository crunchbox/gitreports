function get_NewThemeOtherSharePointMetrics() {

    if (loadedMetrics["kitdefects_sp"] != true) {
        get_MetricsData("kitdefects_sp", "AllSitesNewOther");
        return;
    }
    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "SharePoint: New Theme / Other");

    clearChart("AllSitesNewOther");
    showHideDataTable("AllSitesNewOther_dataTable", "100%", false);

    chartData = parseMetricData(restJsonDict, "newThemeOtherSharePnt_Month", studio_alias, "AllSitesNewOther");
    chartGraphs = GetStackedGraphs(chartData, "month", "Other", "New Theme");

    dataTableDataDict["AllSitesNewOther_dataTable"] = chartData;

    var newThemeOtherConfig = generateConfig("AllSitesNewOther", "All Sites - New Theme / Other", "Kits", "month", currentTheme, true);
    newThemeOtherConfig.dataProvider = chartData;
    newThemeOtherConfig.graphs = chartGraphs;

    chartConfigDict["AllSitesNewOther"] = newThemeOtherConfig;
    chart = AmCharts.makeChart("AllSitesNewOther_chartdiv", newThemeOtherConfig, 100);
    chartResizerDict["AllSitesNewOther"] = true;

    chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict["AllSitesNewOther"] = chartData;
    chartDict["AllSitesNewOther"] = chart; // store chart
}

function parseByNewThemeOtherSharePntToChartRow(row, viewTable, studio_alias, sub_category) {
    var tempInt = 0;
    var tempString;
    var tempStudio;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category.split('/')[0];
        use_sub_category = true;
    }

    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];

            if (use_sub_category == false || targetStudio == "Total") {
                if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
                    if (row.hasOwnProperty("Total/New Theme"))
                        tempInt = row["Total/New Theme"];
                    else
                        tempInt = 0;
                    row["Total/New Theme"] = tempInt + 1;
                } else {
                    if (row.hasOwnProperty("Total/Other"))
                        tempInt = row["Total/Other"];
                    else
                        tempInt = 0;
                    row["Total/Other"] = tempInt + 1;
                }
            }

            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];

            if (use_sub_category == false || targetStudio == tempStudio) {
                //studio.[name] + '/' + CASE WHEN (dbo.kit_category.[description] = 'New Game Theme' OR dbo.kit_category.[description] = 'Localization' OR dbo.kit_category.[description] = 'New System') THEN 'NewTheme' ELSE 'Other' END AS [NewTheme], 
                switch (datarow["Category"]) {
                    case "New Game Theme":
                    case "Localization":
                    case "New System":
                        tempString = "New Theme";
                        break;
                    default:
                        tempString = "Other";
                        break;
                }

                if (row.hasOwnProperty(tempStudio + "/Other") == false)
                    row[tempStudio + "/Other"] = 0;
                if (row.hasOwnProperty(tempStudio + "/New Theme") == false)
                    row[tempStudio + "/New Theme"] = 0;

                tempString = tempStudio + '/' + tempString;

                if (row.hasOwnProperty(tempString))
                    tempInt = row[tempString];
                else tempInt = 0;
                row[tempString] = tempInt + 1;
            }
        }
    }

    return row;
}