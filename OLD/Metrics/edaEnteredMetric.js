function get_EDAEnteredMetrics() {
    if (loadedMetrics["edas"] != true) {
        get_MetricsData("edas", "edasEntered");
        return;
    }
    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "EDAs by Date Entered");

    clearChart("edasEntered");
    showHideDataTable("edasEntered", "100%", false);

    chartData = parseMetricData(restJsonDict, "EDAEntered_Month", studio_alias, "edasEntered");
    chartGraphs = GetStackedGraphs(chartData, "month", "PA", "Other");

    dataTableDataDict["edasEntered_dataTable"] = chartData;

    var edasEnteredConfig = generateConfig("edasEntered", "EDAs by Date Entered - Studio", "EDA Count", "month", currentTheme, true);
    edasEnteredConfig.dataProvider = chartData;
    edasEnteredConfig.graphs = chartGraphs;

    chartConfigDict["edasEntered"] = edasEnteredConfig;
    chart = AmCharts.makeChart("edasEntered_chartdiv", edasEnteredConfig, 100);
    chartResizerDict["edasEntered"] = true;

    chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict["edasEntered"] = chartData;
    chartDict["edasEntered"] = chart; // store chart
}


function edasEntered_DrillDownToLevel3(prefix, monthname, studio, fpstatus) {


    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var date = new Date();
    var year = date.getFullYear();
    var month = 0;

    if (monthname == "Current Month") {
        month = date.getMonth() + 1;
    }
    else {
        month = getMonthFromString(monthname) + 1;
        if (month > (date.getMonth() + 1)) {
            year--;
        }
    }

    if (month < 10)
        month = "0" + month;

    var yearMonth = year + "" + month;

    // get data
    GetEdasEnteredDataL3(currentPrefix, restJsonDict, yearMonth, studio);
}


function GetEdasEnteredDataL3(prefix, jsonTable, monthname, studio) {

    if (loadedMetrics[prefix  + studio + monthname] != true) {

        get_edasDataL3(prefix.replace("_L3", ""), studio, monthname);
        return;
    }

    var fpTable = restJsonDict[prefix + studio + monthname];
    var L3_id = prefix + "_chartdiv";
    currentPrefix = prefix;
    addLevelChartDiv(prefix, L3_id);

    var jsLink = "$('#edasEntered_dataTable_link').show(); GoUpLevel('" + prefix.replace("_L3", "") + "');";
    $("#" + L3_id).html('<a id="edasEntered_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="edasEntered_L3_dataTable_group">');

    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('metricTitle');
    title_div.html("EDAs by Date Entered for " + monthname + " and " + studio.replaceAll("_", " ") + " - Total: " + fpTable.EDAEnteredL3Info.length);
    $("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict["edasEntered_L3"] = fpTable.EDAEnteredL3Info;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#edasEntered_dataTable_link").hide();

    dataTableOptionsDict["edasEntered_L3_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(fpTable.EDAEnteredL3Info, "edasEntered_L3_dataTable", "100%", dataTableOptionsDict["edasEntered_L3_dataTable"], tableClassList);
    dataTableHTMLTableDict["edasEntered_L3"] = htmlTable;

    if (fpTable.EDAEnteredL3Info.length > 0) {
        dataTableHTMLTableDict["edasEntered_L3"].$("tr").click(function () {
            var data = dataTableHTMLTableDict["edasEntered_L3"].row($(this)).data();
            $("#edasEntered_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }


    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
}