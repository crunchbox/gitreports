function get_BVATestCasesMetrics() {
	if (loadedMetrics["bva"] != true) {
	    get_MetricsData("bva", "BVATestCasesPassFail");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "BVA Test Cases");

	clearChart("BVATestCasesPassFail");
	showHideDataTable("BVATestCasesPassFail_dataTable", "100%", false);

	var bvaConfig;
	var chartData, chartGraphs;
	if (chartConfigDict["BVATestCasesPassFail"] == null) {
	    chartData = parseMetricData(restJsonDict, "bvaTotal_Month", studio_alias, "BVATestCasesPassFail");
		chartGraphs = GetBVATestCasesStackedGraphs(chartData, "month", "Passed", "Failed", "Other");

		dataTableDataDict["BVATestCasesPassFail_dataTable"] = chartData;
		bvaConfig = generateConfig("BVATestCasesPassFail", "BVA - Test Cases Pass/Fail", "Test Cases", "month", currentTheme, true);
		bvaConfig.dataProvider = chartData;
		bvaConfig.graphs = chartGraphs;

		chartConfigDict["BVATestCasesPassFail"] = bvaConfig;
		chartDataDict["BVATestCasesPassFail"] = chartData;
	} else bvaConfig = chartConfigDict["BVATestCasesPassFail"];

	var chart = AmCharts.makeChart("BVATestCasesPassFail_chartdiv", bvaConfig, 100);


	chartResizerDict["BVATestCasesPassFail"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", BVATestCasesaddListeners);

	$("#loadingdiv").remove(); // remove loading div


	//chart.validateNow();
	chartDict["BVATestCasesPassFail"] = chart; // store chart

	if (dataTableIsVisible["BVATestCasesPassFail" + "_dataTable"] !== undefined && dataTableIsVisible["BVATestCasesPassFail" + "_dataTable"] === true)
	    showHideDataTable("BVATestCasesPassFail" + "_dataTable");
}

function BVATestCasesaddListeners(event) {

	$('a[href="http://www.amcharts.com/javascript-charts/"]').remove(); // remove watermark

	var prefix = event.chart.titles[0].id;
	chart = event.chart;
	chart.addListener("clickGraphItem", handleItemClick);

	var categoryAxis = chart.categoryAxis;
	categoryAxis.addListener("clickItem", handleCategoryClick);

	removeLoadingDiv();

	forceChartFillParent(prefix);

	if (hasInitSubCat) {
		var subcat_type = whatIsInitSubCat(prefix); // validates initSubCat and initStackCat and gives type
		hasInitSubCat = false;

		if (subcat_type == "month")
		    BVATestCases_DrillDownToLevel2(prefix, initSubCategory);
		else if (subcat_type == "studio") {
		    if (prefix == "BVATestCasesPassFail")
				BVATestCases_DrillDownToLevel2(prefix, initSubCategory);
		}
	} else resetCurrentHash();


	var startchart = BVATestCasesL1autoZoom(chart);
	chart.zoomToIndexes(startchart, (chartDataDict["BVATestCasesPassFail"]).length - 1);
}

function BVATestCasesL1autoZoom(chart) {
	var start = 0;
	var end = chart.dataProvider.length;

	var zoomStart = start;
	for (var i = start; i < end; i++) {
		var tempTot = (chart.dataProvider[i]).Total;

		if (tempTot === 0 && i <= end)
			zoomStart = i + 1;
		else {
			break;
		}
	}

	return zoomStart;
}

function parseBVATestCases(row, viewTable, studio_alias, sub_category) {
	var tempPass = 0,
		tempFail = 0,
		tempOther = 0;
	var tempString;
	var tempStudio;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category.split('/')[0];
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];
			if (tempStudio !== "") {
				if (use_sub_category == false || targetStudio == "Total") {
                    if (row.hasOwnProperty("Total/Failed"))
                        tempFail = row["Total/Failed"];
                    else tempFail = 0;
                    if (row.hasOwnProperty("Total/Passed"))
                        tempPass = row["Total/Passed"];
                    else tempPass = 0;
                    if (row.hasOwnProperty("Total/Other"))
                        tempOther = row["Total/Other"];
                    else tempOther = 0;
                    
                    if (datarow["Result"] == "Failed")
                    {    
                        row["Total/Failed"] = tempFail + parseInt(datarow["Count"]);
                    }
                    if (datarow["Result"] == "Passed")
                    {
                        row["Total/Passed"] = tempPass + parseInt(datarow["Count"]);
                    }
                    if (datarow["Result"] == "Other")
                    {
                        row["Total/Other"] = tempOther + parseInt(datarow["Count"]);
                    }

				}

				if (row.hasOwnProperty("Total"))
					row["Total"] = row["Total/Failed"] + row["Total/Passed"] + row["Total/Other"];


				if (use_sub_category == false || targetStudio == tempStudio) {
					

                    if (row.hasOwnProperty(tempStudio + "/Passed"))
                        tempPass = row[tempStudio + "/Passed"];
                    else tempPass = 0;
                    if (row.hasOwnProperty(tempStudio + "/Failed"))
                        tempFail = row[tempStudio + "/Failed"];
                    else tempFail = 0;
                    if (row.hasOwnProperty(tempStudio + "/Other"))
                        tempOther = row[tempStudio + "/Other"];
                    else tempOther = 0;
                    

                    if (datarow["Result"] == "Passed")
                    {
					   row[tempStudio + "/Passed"] = tempPass + parseInt(datarow["Count"]);
                    }
                    if (datarow["Result"] == "Failed")
                    {
					   row[tempStudio + "/Failed"] = tempFail + parseInt(datarow["Count"]);
                    }
                    if (datarow["Result"] == "Other")
                    {
					   row[tempStudio + "/Other"] = tempOther + parseInt(datarow["Count"]);
                    }
				}
			}
		}
	}
	return row;
}

function GetBVATestCasesStackedGraphs(data, categoryField, passField, failField, otherField) {
	var graphs = [];
	var ballonText = "[[title]] of [[category]]:[[value]]";

	var colors = ["#5CB85C", "#F0AD4E", "#337AB7", "#a7a737", "#86a965", "#8aabb0", "#d8854f", "#cfd27e",
                  "#9d9888", "#916b8a", "#724887", "#7256bc", // end defaults 
                  "#FF0000", "#008000", "#FFC0CB", "#FFA500", "#FFF0F5", "#FFFF00", "#0000FF", "#CD853F",
                  "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#FFFFE0", "#ADD8E6", "#D2B48C",
			      "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#EEE8AA", "#00BFFF", "#FFDEAD",
			      "#8FBC8B", "#00FF7F", "#FFF0F5", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
			      "#40E0D0", "#66CDAA"];

	var groups = [];
	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		for (var heading in rowData) {
			if (rowData.hasOwnProperty(heading)) {
				if (heading != categoryField) {

					if (groups.indexOf(heading) == -1) {
						groups.push(heading);
					}
				}
			}
		}
	}

	var stacks = {};
	var cat;
	var index;
	var column;
	var key;

	for (index in groups) {
		if (groups.hasOwnProperty(index)) {
			column = {};

			key = groups[index];
			if (key.indexOf(otherField, 0) !== -1 && key.indexOf(otherField, 0) !== 0) {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["newStack"] = false;
				column["type"] = "column";
				column["valueField"] = key;

				cat = key.split("/")[0];
				if (stacks.hasOwnProperty(cat) == false)
					stacks[cat] = {};
				stacks[cat][otherField] = column;
			} else if (key.indexOf(failField, 0) != -1) {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["newStack"] = false;
				column["type"] = "column";
				column["valueField"] = key;

				cat = key.split("/")[0];
				if (stacks.hasOwnProperty(cat) == false)
					stacks[cat] = {};
				stacks[cat][failField] = column;
			} else if (key.indexOf(passField, 0) != -1) {
				column["balloonText"] = ballonText;
				column["fillAlphas"] = 1;
				column["id"] = "AmGraph_" + index;
				column["title"] = key;
				column["newStack"] = true;
				column["type"] = "column";
				column["valueField"] = key;

				cat = key.split("/")[0];
				if (stacks.hasOwnProperty(cat) == false)
					stacks[cat] = {};
				stacks[cat][passField] = column;
			}

		}
	}

	for (cat in stacks) {
		if (stacks.hasOwnProperty(cat)) {
			if (stacks[cat].hasOwnProperty(passField) == false && stacks[cat].hasOwnProperty(failField) == true) {
				stacks[cat][failField]["newStack"] = true;
			} else graphs.push(stacks[cat][passField]);

			if (stacks[cat].hasOwnProperty(failField))
				graphs.push(stacks[cat][failField]);

			if (stacks[cat].hasOwnProperty(otherField))
				graphs.push(stacks[cat][otherField]);
		}
	}

	// set fill colors of bars/columns
	for (i = 0; i < graphs.length; i++) {
		graphs[i].fillColors = colors[i];
		graphs[i].lineColor = colors[i];
	}

	for (index in groups) {
		if (groups.hasOwnProperty(index)) {
			column = {};

			key = groups[index];
			if (key == "Total") {
				column["balloonText"] = ballonText;
				column["id"] = "totalline";
				column["title"] = "Total";
				column["bullet"] = "round";
				column["lineThickness"] = 3;
				column["bulletSize"] = 7;
				column["bulletBorderAlpha"] = 1;
				column["connect"] = true;
				column["useLineColorForBulletBorder"] = true;
				column["bulletBorderThickness"] = 3;
				column["fillAlphas"] = 0;
				column["lineAlpha"] = 1;
				column["valueField"] = key;
				graphs.push(column);
			}
		}
	}

	return graphs;
}

function BVATestCases_DrillDownToLevel2(prefix, itemSelection) {
	currentMonth = "all";
	currentStudio = itemSelection;

	var L2_id;

	if (currentPrefix == "BVATestCasesPassFail") {
		$("#" + prefix + "_chartdiv").hide();
		L2_id = prefix + "_L2_chartdiv";
		currentPrefix = prefix + "_L2";
		addLevelChartDiv(prefix, L2_id);
	}
	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
	    showHideDataTable(prefix + "_dataTable");
	showHideDataTable(prefix, "100%", false);

	var studioName = itemSelection.split('/')[0];
	var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title


	var data = parseMetricData(restJsonDict, "bvaTotal_Month", studio_alias, prefix, itemSelection);
	data = sortBVATestCasesL2Data(data, studioName);
	var levelTwoConfig = generateConfig(prefix, "BVA Test Cases Pass/Fail" + "- " + studioName, yAxisTitle, "month", currentTheme, false);
	levelTwoConfig.dataProvider = data;
	levelTwoConfig.graphs = GetBVATestCasesL2Graphs();

	// Adjust Label Functions 
	for (var i = 0; i < 4; i++) {
		levelTwoConfig.graphs[i].balloonFunction = adjustBVATestCasesL2Text;
		levelTwoConfig.graphs[i].labelFunction = adjustBVATestCasesL2LabelText;
	}


	levelTwoConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
    },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Test Cases",
			"autoGridCount": false
    },
		{
			"id": "v3",
			"axisColor": "#6ca979",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v2",
			"synchronizationMultiplier": 1,
			"position": "right",
			"autoGridCount": false
    },
		{
			"id": "v4",
			"axisColor": "#88D498",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v1",
			"synchronizationMultiplier": 1,
			"position": "left",
			"autoGridCount": false
    }];
	levelTwoConfig.categoryAxis.labelFunction = function (string) {
	    string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
	    string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
	    string = string.replace(/_/g, " ");
	    return string;
	};

	chartConfigDict["BVATestCasesPassFail_L2"] = levelTwoConfig;
	var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
	chartResizerDict[currentPrefix] = true;


	// let's add a label to go back to yearly data
	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');


	//chart.addLabel(
	//	'!140', '30',
	//	"Enable Trend",
	//	undefined,
	//	12,
	//	undefined,
	//	undefined,
	//	undefined,
	//	true,
	//	'javascript:BVATestCasesL2ShowRollingAverages();');


	chart.hideGraph(chart.getGraphById("rollingPassedPercent"));
	chart.hideGraph(chart.getGraphById("rollingFailedPercent"));
	chart.hideGraph(chart.getGraphById("rollingOtherPercent"));

	chart.addListener("clickGraphItem", L2_handleItemClick);
	chart.addListener("init", chartInit);
	chart.addListener("rendered", BVATestCasesL2_handleRendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = data;

	level++; // increase global level indicator

	var dataTable = data; // GetDataForEntry("defectsClosedNewThemes", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
	dataTableDataDict[prefix + "_dataTable"] = dataTable;
	dataTableOptionsDict[prefix + "_dataTable"] = {
		"scrollY": 200,
		"scrollX": true,
		"dom": 'rtBfp',
		"buttons": ['copyHtml5', 'csvHtml5']
	};
}

function BVATestCasesL2_handleRendered(event) {

	if (hasInitSubCatMonth) // if need to go to level 3 
	{
	    var prefix = event.chart.titles[0].id;

	    removeAllActiveClass();
	    //$('#navsidebar li.active').removeClass('active');
	    var $subnav = $("<li id='link_" + prefix + "_3' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + initMonth + "/" + initSubCategory + "' onclick='loadMetricView(\"" + prefix + "\", 3)'>" + initMonth + "</a></li>");
	    $("#link_" + prefix + "_2").after($subnav);

		hasInitSubCatMonth = false;
		BVATestCasesPerBuildsL3(prefix, initMonth, initSubCategory, "");

	} else resetCurrentHash();

	removeLoadingDiv();
	removeLoadingDiv();

	var startchart = BVATestCasesL2autoZoom(chart);
	chart.zoomToIndexes(startchart, (chartDataDict["BVATestCasesPassFail_L2"]).length - 1);
}

function BVATestCasesL2autoZoom(chart) {

	var start = 0;
	var end = chart.dataProvider.length;

	var zoomStart = start;
	for (var i = start; i < end; i++) {
		var tempTot = (chart.dataProvider[i]).Passed + (chart.dataProvider[i]).Failed + (chart.dataProvider[i]).Other;

		if (tempTot === 0 && i <= end)
			zoomStart = i + 1;
		else {
			break;
		}
	}

	return zoomStart;
}

function GetBVATestCasesL2Graphs() {
	var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	var graphsData = [
		{
			"fillColors": "#2ECC71",
			"lineColor": "#2ECC71",
			"color": "#ffffff",
			"title": "Passed %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "Passed_percent",
			"newStack": true
        },
		{
			"fillColors": "#E74C3C",
			"lineColor": "#E74C3C",
			"color": "#ffffff",
			"title": "Failed %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "Failed_percent",
			"newStack": false
        },
		{
			"fillColors": "#3498DB",
			"lineColor": "#3498DB",
			"color": "#ffffff",
			"title": "Other %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "Other_percent",
			"newStack": false
        },
		{
			"fillColors": "#27AE60",
			"lineColor": "#27AE60",
			"color": "#ffffff",
			"title": "Passed",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Passed",
			"newStack": true
        },
		{
			"fillColors": "#C0392B",
			"lineColor": "#C0392B",
			"color": "#ffffff",
			"title": "Failed",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Failed",
			"newStack": false
        },
		{
			"fillColors": "#2980B9",
			"lineColor": "#2980B9",
			"color": "#ffffff",
			"title": "Other",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "Other",
			"newStack": false
        }];
	var graphs = [];
	var graph = {};
	for (var i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}


	graph = {
		"balloonText": ballonText,
		"id": "rollingPassedPercent",
		"title": "Rolling Passed Average",
		"bullet": "square",
		"lineColor": "#51D88C",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v3",
		"valueField": "rollingPassedPercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingFailedPercent",
		"title": "Rolling Failed Average",
		"bullet": "square",
		"lineColor": "#FF7364",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v3",
		"valueField": "rollingFailedPercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingOtherPercent",
		"title": "Rolling Other Average",
		"bullet": "square",
		"lineColor": "#58ACE3",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v3",
		"valueField": "rollingOtherPercent"
	};
	graphs.push(graph);
	return graphs;
}

function adjustBVATestCasesL2Text(graphDataItem, graph) {

	if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
		return "<b style='font-size:14px;'>" + ((graphDataItem.graph).legendTextReal) + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + (graphDataItem.values.percents).toFixed(2) + "%</b>";
	} else
		return "<b style='font-size:14px;'>" + ((graphDataItem.graph).legendTextReal) + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + graphDataItem.values.value + "</b>";

}

function adjustBVATestCasesL2LabelText(graphDataItem, graph) {

	if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
		if (graphDataItem.values.value < 1)
			return "";
		else
			return (graphDataItem.values.percents).toFixed(2) + "%";
	} else {
		if (graphDataItem.values.value === 0)
			return "";
		else
			return graphDataItem.values.value;
	}
}

function sortBVATestCasesL2Data(oriData, studio) {
	var data = [];

	for (var i = 0; i < oriData.length; i++) {
		var monthData = {};

		monthData["month"] = (oriData[i])["month"];
		var tot = 0;

		if ((oriData[i])[studio + "/Passed"] !== undefined) {
			tot = tot + (oriData[i])[studio + "/Passed"];
			monthData["Passed"] = (oriData[i])[studio + "/Passed"];
		} else {
			monthData["Passed"] = 0;
		}

		if ((oriData[i])[studio + "/Failed"] !== undefined) {
			tot = tot + (oriData[i])[studio + "/Failed"];
			monthData["Failed"] = (oriData[i])[studio + "/Failed"];
		} else {
			monthData["Failed"] = 0;
		}

		if ((oriData[i])[studio + "/Other"] !== undefined) {
			tot = tot + (oriData[i])[studio + "/Other"];
			monthData["Other"] = (oriData[i])[studio + "/Other"];
			monthData["Other_percent"] = (((oriData[i])[studio + "/Other"] / tot) * 100).toFixed(2);
		} else {
			monthData["Other"] = 0;
			monthData["Other_percent"] = 0;
		}

		if ((oriData[i])[studio + "/Passed"] !== undefined)
			monthData["Passed_percent"] = (((oriData[i])[studio + "/Passed"] / tot) * 100).toFixed(2);
		else {
			monthData["Passed_percent"] = 0;
		}

		if ((oriData[i])[studio + "/Failed"] !== undefined)
			monthData["Failed_percent"] = (((oriData[i])[studio + "/Failed"] / tot) * 100).toFixed(2);
		else {
			monthData["Failed_percent"] = 0;
		}
		monthData["studio"] = studio;
		data.push(monthData);
	}

	return data;
}

function BVATestCasesL2ShowRollingAverages() {
    var chart = chartDict["BVATestCasesPassFail_L2"];

	//clear Graphs
	for (var i = 0; i < 13; i++) {

		if ((chart.dataProvider[i])["rollingPassedPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPassedPercent"];

		if ((chart.dataProvider[i])["rollingFailedPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingFailedPercent"];

		if ((chart.dataProvider[i])["rollingOtherPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingOtherPercent"];
	}

	//update graphs with new %s
	var start = chart.start;
	var end = chart.end;
	calculateRollingPercentagesBVATestCasesL2(chart, start, end);


	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPassedPercent"));
	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingFailedPercent"));
	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingOtherPercent"));

	chart.invalidateSize();
	chart.validateNow();
}

function calculateRollingPercentagesBVATestCasesL2(chart, start, end) {
	var tot_p = 0,
		tot_f = 0,
		tot_o = 0;
	var month = 0;

	//iterate through the month
	for (var i = start; i <= end; i++) {
		month++;

		tot_p = tot_p + (chart.dataProvider[i]).Passed;
		tot_f = tot_f + (chart.dataProvider[i]).Failed;
		tot_o = tot_o + (chart.dataProvider[i]).Other;

		(chart.dataProvider[i])["rollingPassedPercent"] = (tot_p / month).toFixed(2);
		(chart.dataProvider[i])["rollingFailedPercent"] = (tot_f / month).toFixed(2);
		(chart.dataProvider[i])["rollingOtherPercent"] = (tot_o / month).toFixed(2);
	}
}

function BVARunsPerTestCase(currentPrefix, jsonTable, bva_table_name, monthname, studio) {

    if (loadedMetrics[bva_table_name] != true) {
        if (monthname == "Current Month")
            month = 13;
        else month = getMonthFromString(monthname) + 1;

        if (studio != "Total")
            studio = getStudioKey(studio, "key");
 
        get_bvaTestCaseData(bva_table_name, currentPrefix, studio, month);
        return;
    }

    clearChart("BVATestCasesPassFail_L3");
    var monthText = getStringFromMonth(monthname);
    var chartData = jsonTable[bva_table_name];
    
    var studioName = getStudioKey(studio, "name");

    // get config
    var levelThreeConfig = BVATestCasesL3generateConfig(currentPrefix, "Test Case Runs - " + studio + " for " + monthText, "Runs", "CaseName", currentTheme, false);
    levelThreeConfig.dataProvider = chartData;
    levelThreeConfig.legend.enabled = true;
    levelThreeConfig.legend.marginTop = 10;
    levelThreeConfig.graphs = GetGraphsBVATestCasesL3();

    for (var i = 0; i < 4; i++) {
        levelThreeConfig.graphs[i].balloonFunction = adjustBVATestCasesL2Text;
        levelThreeConfig.graphs[i].labelFunction = adjustBVATestCasesL2LabelText;
    }

    levelThreeConfig.valueAxes = [{
        "id": "v1",
        "axisColor": "#88D498",
        "axisThickness": 2,
        "axisAlpha": 1,
        "title": "Percent",
        "stackType": "100%",
        "position": "left",
        "autoGridCount": false,
        "gridAlpha": 0
    },
       {
           "id": "v2",
           "axisColor": "#6ca979",
           "axisThickness": 2,
           "axisAlpha": 1,
           "gridAlpha": 0,
           "stackType": "regular",
           "position": "right",
           "title": "Runs",
           "autoGridCount": false
       }];


    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };

    levelThreeConfig.categoryAxis.autoWrap = true;

    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    chartConfigDict["BVATestCasesPassFail_L3"] = levelThreeConfig;
    var L3_id = "BVATestCasesPassFail_L3_chartdiv";
    var prefix = "BVATestCasesPassFail";
    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addLabel(
		'!200', '20',
		"Sort by Pass %",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortBVATestCasesL3("' + currentPrefix + '");');

    chart.addListener("clickGraphItem", BVATestCase_L3_handleItemClick);
    chart.addListener("init", BVAchartInit);
    chart.addListener("rendered", BVATc_L3_rendered);
    $("#loadingdiv").remove(); // remove loading div
    chartDict[currentPrefix] = chart;
    var returnData = {
        chart: chartData
    };
    chartDataDict[currentPrefix] = returnData;

    level++; // increase global level indicator

    //resetCurrentHash();


    var dataTable;
    dataTable = returnData.chart; // GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsReleased"], month, subcategory, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    //clearTable(prefix + "_dataTable");
    //drawTable(dataTable, prefix + "_dataTable");
}

function getStudioKey(studioName, returnType) {
    var key, name;
    var studioLookUp = [
        {
            name: "Infinity",
            key: "INT"
        },
        {
            name: "PA Games Foundation",
            key: "PGF"
        },
        {
            name: "Ignite Reno",
            key: "IGR"
        },
        {
            name: "Ignite Las Vegas",
            key: "IGV"
        },
        {
            name: "Games Technology Group",
            key: "GTG"
        },
        {
            name: "Four Kings",
            key: "4KG"
        },
        {
            name: "Mega Jackpots",
            key: "MJP"
        },
        {
            name: "Vortex",
            key: "VOR"
        },
        {
            name: "PA Tools and Automation",
            key: "PTA"
        },
        {
            name: "Ignite",
            key: "IGN"
        },
        {
            name: "Austria",
            key: "AUS"
        },
        {
            name: "Other",
            key: "OTH"
        }

    ];
    
    if (returnType == "key") {
        for (var i = 0; i < studioLookUp.length; i++) {
            if (studioName == studioLookUp[i].name)
                key = studioLookUp[i].key;
        }
        if (key == undefined)
            key = "Invalid Key"
        return key;
    } else if (returnType == "name") {
        for (var i = 0; i < studioLookUp.length; i++) {
            if (studioName == studioLookUp[i].key)
                name = studioLookUp[i].name;
        }
        if (name == undefined)
            name = "Invalid Key"
        return name;
    }
}

function BVATc_L3_rendered(event) {
    chart = event.chart;
    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", BVATestCase_L3_handleCategoryClick);
    resetCurrentHash();

    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo); // zoom into the last 5
    chart.invalidateSize();
}

function BVATestCases_DrillDownToLevel3(prefix, month, subcategory, fpstatus) {
    currentMonth = month;
    currentStudio = subcategory;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);
    
    // get table name
    var monthNum;
    var studioKey;

    if (month == "Current Month")
        monthNum = 13;
    else monthNum = getMonthFromString(month) + 1;

    if (subcategory != "Total")
        studioKey = getStudioKey(subcategory, "key");
    else
        studioKey = "Total";

    var bva_table_name = "BVATestCases_";
    bva_table_name = bva_table_name + studioKey + "_" + monthNum;

    // get data
    BVARunsPerTestCase(currentPrefix, restJsonDict, bva_table_name, month, subcategory);
}

function sortByProperty(array, propertyName) {
    return array.sort(function (a, b) {
        if (a[propertyName] > b[propertyName])
            return -1;
        if (a[propertyName] < b[propertyName])
            return 1;
        return 0;
    });
}

function SortBVATestCasesL3(prefix) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix].chart;
    var labelIndex, newIndex, field;
    var labels = ["Sort by Pass %", "Sort by Fail Count", "Sort by Fail %", "Sort by Other Count", "Sort by Other %", "Sort by Pass Count"];
    var fields = ["PassedPercent", "Failed", "FailedPercent", "Other", "OtherPercent", "Passed"];
    // calculate switch text 
    var currentText = chart.allLabels[1].text;
    var currentIndex = labels.indexOf(currentText);

    if (currentIndex == 5)
        newIndex = 0;
    else newIndex = currentIndex + 1;

    switchText = labels[newIndex];
    field = fields[currentIndex];
   
    chart.dataProvider = (sortByProperty(data, field));

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", currentText);
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortBVATestCasesL3("' + prefix + '");';
    
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}

function BVATestCasesPerBuildsL3(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
    var month;
    var studio;
    var datarow;
    var row;
    var chartTable = [];

    if (monthname == "Current Month")
        month = 12;
    else month = getMonthFromString(monthname);


    var defectTable = jsonTable[defect_table_name + month.toString()];

    studio = sub_category;

    var buildGiAiData = {};
    var groupedByStudio = null;

    if (studio !== "Total") {
        var tempGrouping = _.groupBy(defectTable, "Studio");
        groupedByStudio = tempGrouping[studio];
    }
    else
        groupedByStudio = defectTable;


    var groupedByGiAi = _.groupBy(groupedByStudio, "BuildFile");


    for (var dataindex in groupedByGiAi) {
        if (dataindex !== null) {
            if (buildGiAiData[dataindex] === undefined) {
                buildGiAiData[dataindex] = {
                    "BuildFile": dataindex,
                    "Passed": 0,
                    "Failed": 0,
                    "Other": 0,
                    "passed_percent": 0,
                    "failed_percent": 0,
                    "other_percent": 0
                };
            }

            var list = groupedByGiAi[dataindex];
            for (var i=0; i<list.length; i++) {
                if (parseInt((list[i])["PassCount"]) === undefined)
                    ((buildGiAiData[dataindex])["Passed"]) = 0;
                else 
                    ((buildGiAiData[dataindex])["Passed"]) += parseInt((list[i])["PassCount"]);

                if (parseInt((list[i])["FailedCount"]) === undefined)
                    ((buildGiAiData[dataindex])["Failed"]) = 0;
                else 
                    ((buildGiAiData[dataindex])["Failed"]) += parseInt((list[i])["FailedCount"]);

                if (parseInt((list[i])["OtherCount"]) === undefined)
                    ((buildGiAiData[dataindex])["Other"]) = 0;
                else 
                    ((buildGiAiData[dataindex])["Other"]) += parseInt((list[i])["OtherCount"]);
            }
        }
    }


    for (var build in buildGiAiData) {
        row = [];
        row["buildFile"] = build;
        row["Passed"] = (buildGiAiData[build])["Passed"];
        row["Failed"] = (buildGiAiData[build])["Failed"];
        row["Other"] = (buildGiAiData[build])["Other"];

        var total = (buildGiAiData[build])["Passed"] + (buildGiAiData[build])["Failed"] + (buildGiAiData[build])["Other"];

        row["passed_percent"] = (((buildGiAiData[build])["Passed"] / total) * 100).toFixed(2);
        row["failed_percent"] = (((buildGiAiData[build])["Failed"] / total) * 100).toFixed(2);
        row["other_percent"] = (((buildGiAiData[build])["Other"] / total) * 100).toFixed(2);
        row["Total"] = total;

        chartTable.push(row);
    }

    chartTable.sort(function (a, b) {
        if (a.buildFile < b.buildFile) return -1;
        if (a.buildFile > b.buildFile) return 1;
        return 0;
    });

    return {
        chart: chartTable
    };
}

function BVATestCasesL3generateConfig(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
    var config = {
        "type": "serial",
        "theme": theme,
        "categoryField": catField,
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "valueAxes": [
            {
                "id": "v1",
                "title": yAxisTitle
            }
        ],
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": chartId,
                "size": 15,
                "text": chartTitle
            }
        ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "chartScrollbar": {
            "graph": "totalline",
            "graphType": "line",
            "color": "#FFFFFF",
            "selectedBackgroundColor": "#6F7D8A",
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": "#FFFFFF",
            "autoGridCount": true
        },
        "export": {
            "enabled": true
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 1000,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        }
                    }
                },
                {
                    "maxWidth": 500,
                    "overrides": {
                        "valueAxes": {
                            "inside": true
                        }
                    }
                }
            ]
        }
    };

    config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
    config.export["backgroundColor"] = "#222222";
    config.export.drawing.enabled = true;
    config.export.drawing.color = "#FFFFFF";

    config.export.menu[0].menu.push({
        "label": "Get URL To Chart",
        "click": GetUrlForCurrentChart
    });

    if (useStackCharts) {
        config.valueAxes[0].stackType = "regular";
    }

    return config;
}


function GetGraphsBVATestCasesL3() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
    var graphsData = [
        {
            "fillColors": "#2ECC71",
            "lineColor": "#2ECC71",
            "color": "#ffffff",
            "title": "Passed %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "PassedPercent",
            "newStack": true
        },
		{
		    "fillColors": "#E74C3C",
		    "lineColor": "#E74C3C",
		    "color": "#ffffff",
		    "title": "Failed %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "FailedPercent",
		    "newStack": false
		},
		{
		    "fillColors": "#3498DB",
		    "lineColor": "#3498DB",
		    "color": "#ffffff",
		    "title": "Other %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "OtherPercent",
		    "newStack": false
		},
		{
		    "fillColors": "#27AE60",
		    "lineColor": "#27AE60",
		    "color": "#ffffff",
		    "title": "Passed",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Passed",
		    "newStack": true
		},
		{
		    "fillColors": "#C0392B",
		    "lineColor": "#C0392B",
		    "color": "#ffffff",
		    "title": "Failed",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Failed",
		    "newStack": false
		},
		{
		    "fillColors": "#2980B9",
		    "lineColor": "#2980B9",
		    "color": "#ffffff",
		    "title": "Other",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Other",
		    "newStack": false
		}];
    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "balloonText": ballonText,
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["lineColor"] = graphsData[i]["lineColor"];
        graph["color"] = graphsData[i]["color"];
        graph["title"] = graphsData[i]["title"];
        graph["labelText"] = graphsData[i]["labelText"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);

        // do now show values if they are less than 4
        graph["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }

    return graphs;

}