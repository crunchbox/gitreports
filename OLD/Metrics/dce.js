/*****************************************************************************\

 Javascript "Defect Containment Effectiveness Metric" library

 @version: 0.1 - 2016.05.06
 @author: Sean M McClain, Melanie Neff

\*****************************************************************************/

function get_DceMetrics() {
	var current_JSON_Data_Edas;
	var current_JSON_Data_Defects;

	// Get Metric Data
	if (loadedMetrics["dce"] != true) {
        get_MetricsData("dce", "ContainmentRate");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Defect Containment Rate");

	// Clear anything on the page
	clearChart("ContainmentRate");
	showHideDataTable("ContainmentRate", "100%", false);

	// get chart data and graphs
	chartData = parseMetricDataDCE(restJsonDict, "DCE_Data", studio_alias, "ContainmentRate");
	chartGraphs = GetGraphs(chartData, "month");
	dataTableDataDict["ContainmentRate_dataTable"] = chartData;

	// make graph adjustments
	for (var index = 0; index < chartGraphs.length; index++) {
		chartGraphs[index]["balloonText"] = "[[title]] of [[category]]: [[value]]%";
	}

	// Get chart configuration
	var dceConfig = generateConfig("ContainmentRate", "Defect Containment Rate", "DCR %", "month", currentTheme, false);
	dceConfig.dataProvider = chartData;
	dceConfig.graphs = chartGraphs;

	// make chart adjustments 
	dceConfig.legend.valueText = "[[value]] %";
	dceConfig.valueAxes[0].minimum = 0;
	dceConfig.valueAxes[0].maximum = 100;
	dceConfig.valueAxes[0].guides = [{
	    "value": 98.5,
	    "lineAlpha": 0.9,
	    "lineColor": "#EE4266",
	    "label": "MBO",
	    "position": "right",
	    "boldLabel": true,
	    "lineThickness": 2,
	    "inside": true
	}];
	// make chart
	chartConfigDict["ContainmentRate"] = dceConfig;
	chart = AmCharts.makeChart("ContainmentRate_chartdiv", dceConfig, 100);
	chartResizerDict["ContainmentRate"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDataDict["ContainmentRate"] = chartData; // store chart data
	chartDict["ContainmentRate"] = chart; // store chart

}

function groupByArray(xs, key) {
    return xs.reduce(function (rv, x) {
        var  v = key instanceof Function ? key(x) : x[key];
        //var el = rv.find((r) => r && r.key === v);
        var el = rv.find(function(r) { return r && r.key === v });
        if (el) {
            el.values.push(x);
        } else {
            rv.push({ key: v, values: [x] });
        }
        return rv;
    }, []);
};

function parseMetricDataDCE(jsonTable, tableName, studio_alias, chartSelection, sub_category) {

	var month;
	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    var data = groupByArray(jsonTable[tableName], 'YYYYMM');
    var returnData = [];
    for (month = 0; month < 13; month++) {
        row = data[month];
        if (month == 12)
            row["month"] = "Current Month";
        else
            row["month"] = getMonthFromYYYYMM(row["key"], months);
        for (i = 0; i < row.values.length; i++) {
            var entry = row.values[i];
            if (entry.DCR < 1)
                row[entry.Studio] = (entry.DCR * 100).toFixed(2);
        }
        delete row.key;
        delete row.values;
        returnData.push(row);
    }
    return returnData;
}

function getMonthFromYYYYMM(YYYYMM, months) {
    var month = parseInt(YYYYMM.substr(4, 2));
    return months[month - 1];
}

function parseDCEL2(dict, tableName, studio_alias, prefix, itemSelection) {
    var studioData = [];
    var row;
    var data = dict[tableName];
    for (month = 0; month < 13; month++) {
        row = {};
        row["month"] = data[month].month;
        row[itemSelection] = data[month][itemSelection];
        row[itemSelection + "_NCR"] = (100 - data[month][itemSelection]).toFixed(2);
        row["labelColorField"] = "#ffffff";
        studioData.push(row);
    }
    return studioData;
}

function DrillDownToStudioDce(prefix, itemSelection) {
	currentMonth = "all";
	currentStudio = itemSelection;

	$('#' + prefix + "_chart_link").hide();

	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
		showHideDataTable(prefix + "_dataTable");

	showHideDataTable(prefix, "100%", false);

	var data = parseDCEL2(dataTableDataDict, "ContainmentRate_dataTable", studio_alias, prefix, itemSelection);
	var graphs;
	var stacks;
	var studioName;
	var useStackChart = true; 

	graphs = [
    {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "labelText": "[[value]]%",
        "lineAlpha": 1.0,
        "fillAlphas": 1,
        "title": "DCR %",
        "type": "column",
        "color": "#de4c4f", 
        "valueField": itemSelection,
        "labelColorField": "labelColorField"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 1,
        "labelText": "[[value]]%",
        "lineAlpha": 1,
        "title": "NCR %",
        "type": "column",
        "color": "#eea638",
        "valueField": itemSelection + "_NCR",
        "labelColorField": "labelColorField"
    }];

	studioName = itemSelection;

	var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title
	var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
	var chartTitle_firstpart = chartTitle.split('-')[0];

	var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + " - " + studioName, yAxisTitle, "month", currentTheme, useStackChart);
	levelTwoConfig.dataProvider = data;

	// Create Rolling Viewport Graphs
	var dce_graph = GetRollingGraph("rollingAvg_dcr", "Rolling Average DCR", "#33FFBD");
	var nce_graph = GetRollingGraph("rollingAvg_ncr", "Rolling Average NCR", "#DB178A");
	dce_graph.stackable = false;
	nce_graph.stackable = false;
	graphs.push(dce_graph);
	graphs.push(nce_graph);
	// graphs
	levelTwoConfig.graphs = graphs;

	// make graph adjustments
	for (var index = 0; index < graphs.length; index++) {
		graphs[index]["balloonText"] = "[[title]] of [[category]]: [[value]]%";
	}
	// fix legend text
	levelTwoConfig.legend.valueText = "[[value]] %";
	levelTwoConfig.legend.markerLabelGap = 10;

	// value axes
	levelTwoConfig.valueAxes = [
		{
			"id": "v1",
			"title": yAxisTitle,
			"stackType": "regular",
			"maximum": 100,
			"minimum": 0
        },
		{
			"id": "v2",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"position": "right",
			"autoGridCount": false,
			"synchronizeWith": "v1",
			"synchronizationMultiplier": 1
        }];

	$("#" + prefix + "_chartdiv").hide();

	var L2_id = prefix + "_L2_chartdiv";

	currentPrefix = prefix + "_L2";

	addLevelChartDiv(prefix, L2_id);

	var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
	chartResizerDict[currentPrefix] = true;

	// let's add a label to go back to yearly data
	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

	chart.addListener("clickGraphItem", L2_handleItemClick);

	chart.addListener("init", chartInit);
	chart.addListener("rendered", L2_handleRendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = data;

	level++; // increase global level indicator
	dataTableDataDict[prefix + "_dataTable"] = data;
	removeLoadingDiv();
}

function getDCEMonthData(monthname, data) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].month == monthname) {
            return data[i];
        }
    }
}

function DrillDownToMonthDce(prefix, monthname) {
	currentMonth = monthname;
	currentStudio = "";
	//clearTable(prefix + "_dataTable");
	showHideDataTable(prefix + "_dataTable", "100%", false);

	var data = dataTableDataDict[prefix + "_dataTable"];
	chart = chartDict[prefix];
	currentPrefix = prefix;
	var chartTable = [];
	chartTable.push(getDCEMonthData(monthname, data));


	// Set data provider
	chart.dataProvider = chartTable;
	toggleTotalLine(chart, false);

	var chart_title = chart.titles[0].text;

	// let's add a label to go back to yearly data
	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:resetChart("' + prefix + '","' + chart_title + '");');

	chart.titles[0].text = chart_title + " for " + monthname;

	//chart.startDuration = 1; // enable animation for the drill down
	resetCurrentHash();

	chart.validateData();
	chart.animateAgain();

	dataTableDataDict[prefix + "_dataTable"] = chartTable;
	removeLoadingDiv();
}

function getRollingAvg_dce() {
    var chart = chartDict["ContainmentRate_L2"];
	var tot_dce = 0;
	var tot_nce = 0;
	var i;

	//clear Graphs
	for (i = 0; i < 13; i++) {

	    if ((chart.dataProvider[i])["rollingAvg_dcr"] !== undefined) {
		    delete (chart.dataProvider[i])["rollingAvg_dcr"];
		    delete (chart.dataProvider[i])["rollingAvg_ncr"];
		}
	}

	//update graphs with new %s
	var start = chart.start;
	var end = chart.end;
	var dce_count = 0;
	var nce_count = 0;
	//iterate through the month
	for (i = start; i <= end; i++) {
	    var value;
	    var nvalue;
	    if (chart.dataProvider[i].hasOwnProperty(currentStudio)) {
	        value = parseFloat(chart.dataProvider[i][Object.keys(chart.dataProvider[i])[1]]);
	        if (value) {
	            tot_dce += value;
	            dce_count++;
	        }
		}
	    if (chart.dataProvider[i].hasOwnProperty(currentStudio + "_NCR")) {
	        nvalue = parseFloat(chart.dataProvider[i][Object.keys(chart.dataProvider[i])[2]]);
	        if (nvalue) {
	            tot_nce += nvalue;
	            nce_count++;
	        }
		}
		if (value) (chart.dataProvider[i])["rollingAvg_dcr"] = Math.round((tot_dce / (dce_count)) * 100) / 100;
		if (nvalue) (chart.dataProvider[i])["rollingAvg_ncr"] = (Math.round((tot_nce / (nce_count)) * 100) / 100);
	}

	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAvg_dcr"));
	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAvg_ncr"));

	chart.invalidateSize();
	chart.validateNow();

}

function DrillDownToLevel3_dce(prefix, month, subcategory, stack) {

    currentMonth = month;
    var endIndex = subcategory.length;
    if (subcategory.indexOf("_") != -1) endIndex = subcategory.indexOf("_");
	currentStudio = subcategory.substr(0, endIndex);
	currentStack = stack;

	// turn off show/hide all bars button;
	$('#' + prefix + "_chart_link").hide();
	$("#" + prefix + "_L2_chartdiv").hide();

	var date = new Date();
	var year = date.getFullYear();
	var month = 0;

	if (currentMonth == "Current Month") {
	    month = date.getMonth() + 1;
	}
	else {
	    month = getMonthFromString(currentMonth) + 1;
	    if (month >= (date.getMonth() + 1)) {
	        year--;
	    }
	}
	if (month < 10)
	    month = "0" + month;
	var yearMonth = year + "" + month;

	getDCEL3Data(prefix, restJsonDict, yearMonth, currentStudio); // CURSOR
}

function SortDefectsThemeL3ByEdaCount(prefix, sortByEdas) {
	var chart = chartDict[prefix];
	var data = chartDataDict[prefix];
	var labelIndex;

	var switchText = "Sort By EDAs";

	if (sortByEdas) {
		chart.dataProvider = data.sort(function (a, b) {
			if (a.value > b.value) return -1;
			if (a.value < b.value) return 1;
			return 0;
		});

		labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
		chart.allLabels[labelIndex].text = "Sort By Theme Name";
		chart.allLabels[labelIndex].url = 'javascript:SortDefectsThemeL3ByEdaCount("' + prefix + '", false);';
	} else {
		chart.dataProvider = data.sort(function (a, b) {
			if (a.theme < b.theme) return -1;
			if (a.theme > b.theme) return 1;
			return 0;
		});
		labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort By Theme Name");
		chart.allLabels[labelIndex].text = switchText;
		chart.allLabels[labelIndex].url = 'javascript:SortDefectsThemeL3ByEdaCount("' + prefix + '", true);';
	}
	chart.validateData();
	var zoomTo = chart.dataProvider.length;
	if (zoomTo > 8) zoomTo = 8;
	chart.zoomToIndexes(0, zoomTo);
	chart.invalidateSize();
}

function getDCEL3Data(prefix, jsonTable, yearMonth, studio) {
    if (loadedMetrics[prefix + "_L3" + studio.replaceAll(" ", "_") + yearMonth] != true) {
        get_dceDataL3(prefix, studio, yearMonth);
        return;
    }
    
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);
    clearChart(currentPrefix);

    var data = jsonTable[currentPrefix + studio.replaceAll(" ", "_") + yearMonth].DCR_L3;
    for (i = 0; i < data.length; i++) {
        data[i]["labelColorField"] = "#ffffff";
    }
    // CURSOR
    var monthText = getStringFromMonth(parseInt(yearMonth.substr(4, 2)));
    var levelThreeConfig = generateConfig(prefix, "Defect/EDA Count - " + studio + " for " + monthText, "Defect/EDA Count", "Kit", currentTheme, true);
    levelThreeConfig.graphs = [
        {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "labelText": "[[value]]",
            "lineAlpha": 1.0,
            "fillAlphas": 1,
            "title": "Defect Count",
            "type": "column",
            "color": "#de4c4f",
            "valueField": "DefectCount",
            "labelColorField": "labelColorField"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 1,
            "labelText": "[[value]]",
            "lineAlpha": 1,
            "title": "EDA Count",
            "type": "column",
            "color": "#eea638",
            "valueField": "EDACount",
            "labelColorField": "labelColorField"
        }];
    levelThreeConfig.dataProvider = data;
    levelThreeConfig.legend.enabled = true;
    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        categoryBalloonFunction: function (category) {
            return themeLookUp[category];
        },
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };
    levelThreeConfig.categoryAxis.autoWrap = true;
    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;
    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };
    chartConfigDict["ContainmentRate_L3"] = levelThreeConfig;

    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    var labelText;
   
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addListener("clickGraphItem", L3_handleItemClick);
    chart.addListener("rendered", L3_rendered);

    resetCurrentHash();

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator
}
