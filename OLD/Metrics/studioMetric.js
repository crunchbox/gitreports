/*****************************************************************************\

 Javascript "Kits Released Metric" library

 @version: 0.1 - 2016.05.06
 @author: Sean M McClain

\*****************************************************************************/


function get_StudioGameMetrics() {
	if (loadedMetrics["kitdefects"] != true) {
	    get_MetricsData("kitdefects", "kitsReleased");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Kits Released Out Of PA");

	clearChart("kitsReleased");
	showHideDataTable("kitsReleased", "100%", false);

	chartData = parseMetricData(restJsonDict, "PAReleases_Month", studio_alias, "kitsReleased");
	chartGraphs = GetGraphs(chartData, "month");
	dataTableDataDict["kitsReleased_dataTable"] = chartData;
	//drawTable(chartData, "studio_dataTable");

	var studioConfig = generateConfig("kitsReleased", "Kits Released Out Of PA - Studio", "Kits", "month", currentTheme, false);
	studioConfig.dataProvider = chartData;
	studioConfig.graphs = chartGraphs;

	chartConfigDict["kitsReleased"] = studioConfig;
	chart = AmCharts.makeChart("kitsReleased_chartdiv", studioConfig, 100);
	chartResizerDict["kitsReleased"] = true;

	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		14,
		undefined,
		undefined,
		undefined,
		true,
		'/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDataDict["kitsReleased"] = chartData;
	chartDict["kitsReleased"] = chart; // store chart
	//get_DefectForKitReleased(); // async get of defect data

} // get_StudioGameMetrics()

function parseMetricData_studio(jsonTable, table_name, studio_alias, chartSelection, sub_category) {
	var chartTable = [];
	var currentFiscal;
	var viewTable;
	var row;

	var month;
	var data;
	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	var yearData = {};
	for (month = 0; month < 13; month++) {
		row = {};
		viewTable = jsonTable[table_name + month.toString()];

		if (month == 12)
		    row["month"] = "Current Month";
		else
			row["month"] = months[month];

		if (sub_category == undefined || sub_category.startsWith("Total"))
			row["Total"] = viewTable.length;

		if (chartSelection == "newThemeOther")
			row = parseByNewThemeOtherToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "bugFixOther")
			row = parseByBugFixOtherToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "kitsReleased")
			row = parseByStudioToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "defectsForKitsReleased")
			row = parseDefectsReleasedToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "edas" || chartSelection == "edaEntered")
			row = parseEDAsToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "newThemeFirstPass")
			row = parseNewThemeToChartRow(row, viewTable, studio_alias, sub_category);
		else if (chartSelection == "NewThemeAvgTestTime") {
			data = {};
			row = parseTestTimeToChartRow(row, viewTable, studio_alias, sub_category, data);
			yearData[row["month"]] = data;
		} else if (chartSelection == "NewThemeAvgTTGameType") {
			data = {};
			row = parseTestTimeTypeToChartRow(row, viewTable, studio_alias, sub_category, data);
			yearData[row["month"]] = data;
		}


		if (row["month"] == "Current Month")
			currentFiscal = row;
		else chartTable.push(row);
	}

	if (sub_category === undefined) {
	    if (chartSelection == "NewThemeAvgTestTime")
	        extraGraphData["NewThemeAvgTestTime"] = yearData;
	    else if (chartSelection == "NewThemeAvgTTGameType")
	        extraGraphData["NewThemeAvgTTGameType"] = yearData;
	}


	chartTable = chartTable.concat(chartTable.splice(0, new Date().getMonth()));
	chartTable.push(currentFiscal);

	return chartTable;
}

function parseByStudioToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempStudio;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;
				row[tempStudio] = tempInt + 1;
			}
		}
	}

	return row;
}