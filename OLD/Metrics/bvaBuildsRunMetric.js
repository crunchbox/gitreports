function get_BVABuildsRunMetrics() {

	if (loadedMetrics["bva"] != true) {
		get_MetricsData("bva", "bvaBuildsRun");
		return;
	}

	if (analytics != null)
		analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "BVA Runs");

	clearChart("bvaBuildsRun");
	showHideDataTable("bvaBuildsRun_dataTable", "100%", false);

	var bvaConfig;
	var chartData, chartGraphs, chart;
	if (chartConfigDict["bvaBuildsRun"] == null) {
		chartData = parseMetricData(restJsonDict, "bvaTotal_Month", studio_alias, "bvaBuildsRun");
		chartGraphs = GetGraphs(chartData, "month");

		dataTableDataDict["bvaBuildsRun_dataTable"] = chartData;
		bvaConfig = generateConfig("bvaBuildsRun", "BVA - Builds Run", "Count", "month", currentTheme, false);
		bvaConfig.dataProvider = chartData;
		bvaConfig.graphs = chartGraphs;

		chartConfigDict["bvaBuildsRun"] = bvaConfig;
		chartDataDict["bvaBuildsRun"] = chartData;
	} else bvaConfig = chartConfigDict["bvaBuildsRun"];

	chart = AmCharts.makeChart("bvaBuildsRun_chartdiv", bvaConfig, 100);
	chartResizerDict["bvaBuildsRun"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", BVABuildsRunsaddListeners);

	$("#loadingdiv").remove(); // remove loading div

	chartDict["bvaBuildsRun"] = chart; // store chart
}


function BVABuildsRunsaddListeners(event) {

	$('a[href="http://www.amcharts.com/javascript-charts/"]').remove(); // remove watermark

	var prefix = event.chart.titles[0].id;

	chart.addListener("clickGraphItem", handleItemClick);

	var categoryAxis = chart.categoryAxis;
	categoryAxis.addListener("clickItem", handleCategoryClick);

	removeLoadingDiv();

	forceChartFillParent(prefix);

	if (hasInitSubCat) {
		var subcat_type = whatIsInitSubCat(prefix); // validates initSubCat and initStackCat and gives type
		hasInitSubCat = false;

		if (subcat_type == "month")
		    DrillDownToMonth(prefix, initSubCategory);
		else if (subcat_type == "studio") {
		    DrillDownToStudio(prefix, initSubCategory);
		}
	} else resetCurrentHash();

	var startchart = BVABuildsRunL1autoZoom(chart);
	chart.zoomToIndexes(startchart, (chartDataDict["bvaBuildsRun"]).length - 1);
}


function BVABuildsRunL1autoZoom(chart) {
	var start = 0;
	var end = chart.dataProvider.length;

	var zoomStart = start;
	for (var i = start; i < end; i++) {
		var tempTot = (chart.dataProvider[i]).Total;

		if (tempTot === 0 && i <= end)
			zoomStart = i + 1;
		else {
			break;
		}
	}

	return zoomStart;
}



function parseBVABuildsRun(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempStudio;
	var rollingTotal = 0;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	var tempGrouping = _.groupBy(viewTable, "Studio");

	for (var group in tempGrouping) {
	    if (tempGrouping.hasOwnProperty(group)) {
	        var groupData = tempGrouping[group];
	        var groupedByGiAi = _.groupBy(groupData, "BuildFile");

	        if (studio_alias.hasOwnProperty(group))
	            tempStudio = studio_alias[group];
	        else tempStudio = group;

	        for (var build in groupedByGiAi) {
	            if (use_sub_category == false || targetStudio == tempStudio) {
	                if (row.hasOwnProperty(tempStudio))
	                    tempInt = row[tempStudio];
	                else tempInt = 0;
	                row[tempStudio] = tempInt + 1;
	                rollingTotal += 1;
	            }
	        }
	        
            
		}
	}

	if (sub_category == undefined && sub_category != "Total")
		row["Total"] = rollingTotal;

	return row;
}



function BVABuildsRun_DrillDownToLevel3(prefix, month, subcategory) {
    currentMonth = month;
    currentStudio = subcategory;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var returnData = BVABuildsPerBuildsL3(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
    var levelThreeConfig = BVABuildsL3generateConfig(prefix, "Test Cases by Build - " + subcategory + " for " + month, "Test Cases", "buildFile", currentTheme, false); //DefectsFoundTTL3generateConfig(prefix, "Defects - " + subcategory + " for " + month, "Defects Found", "theme", currentTheme, false);

    levelThreeConfig.dataProvider = returnData.chart;

    levelThreeConfig.legend.enabled = false;
    levelThreeConfig.legend.marginTop = 10;
    levelThreeConfig.graphs = GetGraphsBVABuildsL3();

    // Adjust Label Functions 
    /*
     for (var i = 0; i < 4; i++) {
         levelThreeConfig.graphs[i].balloonFunction = adjustDefectsFoundTTL2Text;
         levelThreeConfig.graphs[i].labelFunction = adjustDefectsFoundTTL2LabelText;
     }*/

    levelThreeConfig = setColumnSpacingFirstPass(levelThreeConfig);

    //levelTwoConfig.columnSpacing = -15;
    levelThreeConfig.valueAxes = [{
        "id": "v1",
        "axisColor": "#88D498",
        "axisThickness": 2,
        "axisAlpha": 1,
        "title": "Percent",
        "stackType": "100%",
        "position": "left",
        "autoGridCount": false,
        "gridAlpha": 0
    },
		{
		    "id": "v2",
		    "axisColor": "#6ca979",
		    "axisThickness": 2,
		    "axisAlpha": 1,
		    "gridAlpha": 0,
		    "stackType": "regular",
		    "position": "right",
		    "title": "Count",
		    "autoGridCount": false
		}];


    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };

    levelThreeConfig.categoryAxis.autoWrap = true;

    delete levelThreeConfig.chartScrollbar.graph;
    delete levelThreeConfig.chartScrollbar.graphType;
    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    chartConfigDict["bvaBuildsRun_L3"] = levelThreeConfig;
    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addLabel(
		'!200', '20',
		"Sort By Test Cases",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		false,
		'javascript:SortBVABuildsL3("' + currentPrefix + '", true);');

    chart.addListener("clickGraphItem", BVA_L3_handleItemClick);
    chart.addListener("init", BVAchartInit);
    chart.addListener("rendered", BVA_L3_rendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = returnData;

    level++; // increase global level indicator

    //resetCurrentHash();


    var dataTable;
    dataTable = returnData.chart; // GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsReleased"], month, subcategory, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    //clearTable(prefix + "_dataTable");
    //drawTable(dataTable, prefix + "_dataTable");
}



function SortBVABuildsL3(prefix, sortByCount) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix].chart;
    var labelIndex;

    var switchText = "Sort By Test Cases";

    if (sortByCount) {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.Total > b.Total) return -1;
            if (a.Total < b.Total) return 1;
            return 0;
        });

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
        chart.allLabels[labelIndex].text = "Sort By Build Name";
        chart.allLabels[labelIndex].url = 'javascript:SortBVABuildsL3("' + prefix + '", false);';
    } else {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.buildFile < b.buildFile) return -1;
            if (a.buildFile > b.buildFile) return 1;
            return 0;
        });
        labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort By Name");
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortBVABuildsL3("' + prefix + '", true);';
    }
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}


function BVABuildsPerBuildsL3(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
    var month;
    var studio;
    var datarow;
    var row;
    var chartTable = [];

    if (monthname == "Current Month")
        month = 12;
    else month = getMonthFromString(monthname);


    var defectTable = jsonTable[defect_table_name + month.toString()];

    studio = sub_category;

    var buildGiAiData = {};
    var groupedByStudio = null;

    if (studio !== "Total") {
        var tempGrouping = _.groupBy(defectTable, "Studio");
        groupedByStudio = tempGrouping[studio];
    }
    else
        groupedByStudio = defectTable;


    var groupedByGiAi = _.groupBy(groupedByStudio, "BuildFile");


    for (var dataindex in groupedByGiAi) {
        if (dataindex !== null) {
            if (buildGiAiData[dataindex] === undefined) {
                buildGiAiData[dataindex] = {
                    "BuildFile": dataindex,
                    "Passed": 0,
                    "Failed": 0,
                    "Other": 0,
                    "passed_percent": 0,
                    "failed_percent": 0,
                    "other_percent": 0
                };
            }

            var list = groupedByGiAi[dataindex];
            for (var i = 0; i < list.length; i++) {
                if (parseInt((list[i])["PassCount"]) === undefined)
                    ((buildGiAiData[dataindex])["Passed"]) = 0;
                else
                    ((buildGiAiData[dataindex])["Passed"]) += parseInt((list[i])["PassCount"]);

                if (parseInt((list[i])["FailedCount"]) === undefined)
                    ((buildGiAiData[dataindex])["Failed"]) = 0;
                else
                    ((buildGiAiData[dataindex])["Failed"]) += parseInt((list[i])["FailedCount"]);

                if (parseInt((list[i])["OtherCount"]) === undefined)
                    ((buildGiAiData[dataindex])["Other"]) = 0;
                else
                    ((buildGiAiData[dataindex])["Other"]) += parseInt((list[i])["OtherCount"]);
            }
        }
    }


    for (var build in buildGiAiData) {
        row = [];
        row["buildFile"] = build;
        row["Passed"] = (buildGiAiData[build])["Passed"];
        row["Failed"] = (buildGiAiData[build])["Failed"];
        row["Other"] = (buildGiAiData[build])["Other"];

        var total = (buildGiAiData[build])["Passed"] + (buildGiAiData[build])["Failed"] + (buildGiAiData[build])["Other"];

        row["passed_percent"] = (((buildGiAiData[build])["Passed"] / total) * 100).toFixed(2);
        row["failed_percent"] = (((buildGiAiData[build])["Failed"] / total) * 100).toFixed(2);
        row["other_percent"] = (((buildGiAiData[build])["Other"] / total) * 100).toFixed(2);
        row["Total"] = total;

        chartTable.push(row);
    }

    chartTable.sort(function (a, b) {
        if (a.buildFile < b.buildFile) return -1;
        if (a.buildFile > b.buildFile) return 1;
        return 0;
    });

    return {
        chart: chartTable
    };
}


function BVABuildsL3generateConfig(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
    var config = {
        "type": "serial",
        "theme": theme,
        "categoryField": catField,
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "valueAxes": [
            {
                "id": "v1",
                "title": yAxisTitle
            }
        ],
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": chartId,
                "size": 15,
                "text": chartTitle
            }
        ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "chartScrollbar": {
            "graph": "totalline",
            "graphType": "line",
            "color": "#FFFFFF",
            "selectedBackgroundColor": "#6F7D8A",
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": "#FFFFFF",
            "autoGridCount": true
        },
        "export": {
            "enabled": true
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 1000,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        }
                    }
                },
                {
                    "maxWidth": 500,
                    "overrides": {
                        "valueAxes": {
                            "inside": true
                        }
                    }
                }
            ]
        }
    };

    config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
    config.export["backgroundColor"] = "#222222";
    config.export.drawing.enabled = true;
    config.export.drawing.color = "#FFFFFF";

    config.export.menu[0].menu.push({
        "label": "Get URL To Chart",
        "click": GetUrlForCurrentChart
    });

    if (useStackCharts) {
        config.valueAxes[0].stackType = "regular";
    }

    return config;
}



function GetGraphsBVABuildsL3() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
    var graphsData = [
        {
            "fillColors": "#2ECC71",
            "lineColor": "#2ECC71",
            "color": "#ffffff",
            "title": "Passed %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "passed_percent",
            "newStack": true
        },
		{
		    "fillColors": "#E74C3C",
		    "lineColor": "#E74C3C",
		    "color": "#ffffff",
		    "title": "Failed %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "failed_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#3498DB",
		    "lineColor": "#3498DB",
		    "color": "#ffffff",
		    "title": "Other %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "other_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#27AE60",
		    "lineColor": "#27AE60",
		    "color": "#ffffff",
		    "title": "Passed",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Passed",
		    "newStack": true
		},
		{
		    "fillColors": "#C0392B",
		    "lineColor": "#C0392B",
		    "color": "#ffffff",
		    "title": "Failed",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Failed",
		    "newStack": false
		},
		{
		    "fillColors": "#2980B9",
		    "lineColor": "#2980B9",
		    "color": "#ffffff",
		    "title": "Other",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Other",
		    "newStack": false
		}];
    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "balloonText": ballonText,
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["lineColor"] = graphsData[i]["lineColor"];
        graph["color"] = graphsData[i]["color"];
        graph["title"] = graphsData[i]["title"];
        graph["labelText"] = graphsData[i]["labelText"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);

        // do now show values if they are less than 4
        graph["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }

    return graphs;

}