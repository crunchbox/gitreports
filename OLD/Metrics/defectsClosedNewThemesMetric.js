var savedData = {};

function get_DefectsFoundNewThemesMetrics() {
	if (loadedMetrics["defectsClosedNT"] != true) {
	    get_MetricsData("defectsClosedNT", "defectsPhaseFound");
		return;
	}

	if (analytics != null)
		analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Defects Closed by Phase Found");

	clearChart("defectsPhaseFound");
	showHideDataTable("defectsPhaseFound", "100%", false);


	var defectsFoundConfig;
	var chartData, chartGraphs, chart;
	if (chartConfigDict["defectsPhaseFound"] == null) {
	    chartData = parseMetricData(restJsonDict, "DefectClosedNewThemes_Month", studio_alias, "defectsPhaseFound");
		chartGraphs = GetGraphs(chartData, "month");

		dataTableDataDict["defectsPhaseFound_dataTable"] = chartData;
		defectsFoundConfig = generateConfig("defectsPhaseFound", "Defects Closed for New Themes", "Defects", "month", currentTheme, false);
		defectsFoundConfig.dataProvider = chartData;
		defectsFoundConfig.graphs = chartGraphs;

		chartConfigDict["defectsPhaseFound"] = defectsFoundConfig;
		chartDataDict["defectsPhaseFound"] = chartData;
	} else defectsFoundConfig = chartConfigDict["defectsPhaseFound"];

	chart = AmCharts.makeChart("defectsPhaseFound_chartdiv", defectsFoundConfig, 100);

	chartResizerDict["defectsPhaseFound"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	$("#loadingdiv").remove(); // remove loading div


	chartDict["defectsPhaseFound"] = chart; // store chart

	var dataTable = chartData; // GetDataForEntry("defectsPhaseFound", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
	dataTableDataDict["defectsPhaseFound_dataTable"] = dataTable;
	dataTableOptionsDict["defectsPhaseFound_dataTable"] = {
		"scrollY": 200,
		"scrollX": true,
		"dom": 'rtBfp',
		"buttons": ['copyHtml5', 'csvHtml5']
	};
} //

function DefectsFoundNT_DrillDownToLevel2(prefix, itemSelection) {
	currentMonth = "all";
	currentStudio = itemSelection;

	var L2_id;

	if (currentPrefix == "defectsPhaseFound") {
		$("#" + prefix + "_chartdiv").hide();
		L2_id = prefix + "_L2_chartdiv";
		currentPrefix = prefix + "_L2";
		addLevelChartDiv(prefix, L2_id);
	}

	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
		showHideDataTable(prefix + "_dataTable");
	showHideDataTable(prefix, "100%", false);

	var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
	var chartTitle_firstpart = chartTitle.split('-')[0];

	var data = GetDefectsFoundNTPercentAndTotal(restJsonDict, "DefectClosedNewThemes_Month", currentStudio, studio_alias);

	var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + "- " + currentStudio, "Defects Found", "month", currentTheme, false);
	levelTwoConfig.dataProvider = data;
	levelTwoConfig.graphs = GetDefectsFoundNTGraphsL2();

	var i;

	// Adjust Label Functions 
	for (i = 0; i < 6; i++) {
		levelTwoConfig.graphs[i].balloonFunction = adjustDefectsFoundNTL2Text;
		levelTwoConfig.graphs[i].labelFunction = adjustDefectsFoundNTL2LabelText;
	}

	for (i = 6; i < 12; i++) {
		levelTwoConfig.graphs[i].balloonFunction = adjustDefectsFoundNTL2Text;
	}


	levelTwoConfig = setColumnSpacingFirstPass(levelTwoConfig);

	//levelTwoConfig.columnSpacing = -15;
	levelTwoConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
    },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Count",
			"autoGridCount": false
        },
		{
			"id": "v3",
			"axisColor": "#6ca979",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v2",
			"synchronizationMultiplier": 1,
			"position": "right",
			"autoGridCount": false
        },
		{
			"id": "v4",
			"axisColor": "#88D498",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v1",
			"synchronizationMultiplier": 1,
			"position": "left",
			"autoGridCount": false
        }];



	chartConfigDict["defectsPhaseFound_L2"] = levelTwoConfig;
	var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
	chartResizerDict[currentPrefix] = true;

	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

        chart.addLabel(
        '!150', '20',
        "Enable Count",
        undefined,
        12,
        undefined,
        undefined,
        undefined,
        true,
        'javascript:DefectsFoundNTL2EnableValues();');

        //chart.addLabel(
        //    '!150', '34',
        //    "Enable Trend",
		//undefined,
		//12,
		//undefined,
		//undefined,
		//undefined,
		//true,
		//'javascript:DefectFoundNTShowRollingPercents();');



	chart.addListener("clickGraphItem", L2_handleItemClick);
	chart.addListener("init", DefectsFoundL2chartInit);
	chart.addListener("rendered", L2_handleRendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = data;


	level++; // increase global level indicator

	var dataTable = data; // GetDataForEntry("defectsPhaseFound", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
	dataTableDataDict[prefix + "_dataTable"] = dataTable;
	dataTableOptionsDict[prefix + "_dataTable"] = {
		"scrollY": 200,
		"scrollX": true,
		"dom": 'rtBfp',
		"buttons": ['copyHtml5', 'csvHtml5']
	};
   
} //

function DefectsFoundNTL2EnableValues() {
	//hide rolling charts
    var chart = chartDict["defectsPhaseFound_L2"];
	chart.showGraph(chart.getGraphById("earlyTesting"));
	chart.showGraph(chart.getGraphById("paytable"));
	chart.showGraph(chart.getGraphById("math"));
	chart.showGraph(chart.getGraphById("pa"));
	chart.showGraph(chart.getGraphById("postPA"));
	chart.showGraph(chart.getGraphById("prePA"));
}

function DefectsFoundL2chartInit(event) {
    chart = event.chart;
    var prefix = event.chart.titles[0].id;

    // calculate trend lines for showing from legend
    calculateRollingPercentagesDefectsFoundNT(chart, chart.start, chart.end);

    // add legend listener
    chart.legend.addListener("showItem", showLegendItem);

    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var max_chart_height = h - 98;

    $('#' + prefix + "_chartdiv").height(max_chart_height);

    var totalLineGraph = chart.getGraphById("totalline");
    if (totalLineGraph != null)
        chart.hideGraph(totalLineGraph); // hide total line by default

    //hide value charts
    chart.hideGraph(chart.getGraphById("earlyTesting"));
    chart.hideGraph(chart.getGraphById("paytable"));
    chart.hideGraph(chart.getGraphById("math"));
    chart.hideGraph(chart.getGraphById("pa"));
    chart.hideGraph(chart.getGraphById("postPA"));
    chart.hideGraph(chart.getGraphById("prePA"));

    //hide rolling charts
    chart.hideGraph(chart.getGraphById("rollingPrePAPercent"));
    chart.hideGraph(chart.getGraphById("rollingPostPAPercent"));
    chart.hideGraph(chart.getGraphById("rollingPaPercent"));
    chart.hideGraph(chart.getGraphById("rollingEarlyTestingPercent"));
	chart.hideGraph(chart.getGraphById("rollingMathPercent"));
	chart.hideGraph(chart.getGraphById("rollingPaytablePercent"));
}

function DefectFoundNTShowRollingPercents() {
    var chart = chartDict["defectsPhaseFound_L2"];

	//clear Graphs
	for (var i = 0; i < 13; i++) {

		if ((chart.dataProvider[i])["rollingPrePAPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPrePAPercent"];

		if ((chart.dataProvider[i])["rollingPostPAPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPostPAPercent"];

		if ((chart.dataProvider[i])["rollingPaPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPaPercent"];

		if ((chart.dataProvider[i])["rollingEarlyTestingPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingEarlyTestingPercent"];

		if ((chart.dataProvider[i])["rollingMathPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingMathPercent"];

		if ((chart.dataProvider[i])["rollingPaytablePercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPaytablePercent"];
	}

	//update graphs with new %s
	var start = chart.start;
	var end = chart.end;
	calculateRollingPercentagesDefectsFoundNT(chart, start, end);

	//show graphs
	if ((chart.legend.legendData[0]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPrePAPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPrePAPercent"));

	//Turn off rejected if  "rejected %" are off
	if ((chart.legend.legendData[1]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPostPAPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPostPAPercent"));

	//Turn off pending if "pending %" are off
	if ((chart.legend.legendData[2]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPaPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPaPercent"));

	if ((chart.legend.legendData[3]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingEarlyTestingPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingEarlyTestingPercent"));

	if ((chart.legend.legendData[4]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingMathPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingMathPercent"));

	if ((chart.legend.legendData[5]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPaytablePercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPaytablePercent"));


	chart.invalidateSize();
	chart.validateNow();
} //

function calculateRollingPercentagesDefectsFoundNT(chart, start, end) {

	var tot_pre = 0,
		tot_post = 0,
		tot_pa = 0,
		tot_early = 0,
		tot_math = 0,
		tot_paytable = 0;
	var tot = 0;

	//iterate through the month
	for (var i = start; i <= end; i++) {

		if ((chart.legend.legendData[0]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).prePA;

		//check if rejected or "rejected %" is active
		if ((chart.legend.legendData[1]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).postPA;

		//check if pending or "pending %" is active
		if ((chart.legend.legendData[2]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).pa;

		//check if rejected or "na %" is active
		if ((chart.legend.legendData[3]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).earlyTesting;

		if ((chart.legend.legendData[4]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).math;

		if ((chart.legend.legendData[5]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).paytable;

		tot_pre += (chart.dataProvider[i]).prePA;
		tot_post += (chart.dataProvider[i]).postPA;
		tot_pa += (chart.dataProvider[i]).pa;
		tot_early += (chart.dataProvider[i]).earlyTesting;
		tot_math += (chart.dataProvider[i]).math;
		tot_paytable += (chart.dataProvider[i]).paytable;


		(chart.dataProvider[i])["rollingPrePAPercent"] = (tot_pre / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingPostPAPercent"] = (tot_post / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingPaPercent"] = (tot_pa / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingEarlyTestingPercent"] = (tot_early / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingMathPercent"] = (tot_math / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingPaytablePercent"] = (tot_paytable / tot * 100).toFixed(2);
	}

} //

function DefectsFoundNT_L2_legendHandler(event) {

	var chart = event.chart;
	var targetGraph = event.dataItem;
	var otherGraph;
	var valueField = targetGraph.valueField;
	var dataSet = chart.dataProvider;
	var maxValue = 0;
	var i;

	var axis = targetGraph.valueAxis;
	if (axis.synchronizeWith)
		axis = axis.synchronizeWith;

	if (targetGraph.id == "fyRollingTotal" || targetGraph.id == "rollingTotal") {

		if (event.type == "showItem") {
			for (i = 0; i < dataSet.length; i++) {
				if (maxValue < dataSet[i][valueField])
					maxValue = dataSet[i][valueField];
			}
			if (axis.maximum == undefined || axis.maximum < maxValue)
				axis.maximum = maxValue;
			chart.invalidateSize();
			chart.validateData();
		} else if (event.type == "hideItem") {
			if (targetGraph.id == "fyRollingTotal") {
				otherGraph = chart.getGraphById("rollingTotal");
				if (otherGraph.hidden) {
					axis.maximum = undefined;
				} else {
					valueField = otherGraph.valueField;
					maxValue = 0;
					for (i = 0; i < dataSet.length; i++) {
						if (maxValue < dataSet[i][valueField])
							maxValue = dataSet[i][valueField];
					}
					if (axis.maximum == undefined || axis.maximum < maxValue)
						axis.maximum = maxValue;
				}
			} else if (targetGraph.id == "rollingTotal") {
				otherGraph = chart.getGraphById("fyRollingTotal");
				if (otherGraph.hidden) {
					axis.maximum = undefined;
				} else {
					valueField = otherGraph.valueField;
					maxValue = 0;
					for (i = 0; i < dataSet.length; i++) {
						if (maxValue < dataSet[i][valueField])
							maxValue = dataSet[i][valueField];
					}
					if (axis.maximum == undefined || axis.maximum < maxValue)
						axis.maximum = maxValue;
				}
			}
			chart.invalidateSize();
			chart.validateData();
		}
	}
} //

function adjustDefectsFoundNTL2Text(graphDataItem, graph) {

    var data = extraGraphData["defectsPhaseFound"];
	var month = graphDataItem.category;
	var section = (graph.legendTextReal).replace(" %", "");

	var percentage = true;
	if ((graph.legendTextReal).search(" %") === -1)
		percentage = false;

	var phaseData = (data[month])[section];

	var tot = ((data[month])["PA"])["Total"] + ((data[month])["Pre PA"])["Total"] + ((data[month])["Post PA"])["Total"] + ((data[month])["Early Testing"])["Total"] + ((data[month])["Paytable"])["Total"] + ((data[month])["Math"])["Total"];

    if (percentage == true)
        graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;
    else
        graphDataItem.values.value = Math.round(graphDataItem.values.value * 100) / 100;

	var test;

	if (percentage === true)
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + Math.round(graphDataItem.values.percents * 100) / 100 + "%</b>";
	else
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + ((data[month])[section])["Total"] + "</b>";

	var property = 0;
	for (property in phaseData) {
		if (property !== "Total") {
			text += "<br>";

			if (percentage === true)
				text += property.substring(3) + ": <b style='font-size:13px;'>" + ((phaseData[property] / tot) * 100).toFixed(2) + "%</b>";
			else
				text += property.substring(3) + ": <b style='font-size:13px;'>" + phaseData[property] + "</b>";
		}
	}

	return text;
} //

function adjustDefectsFoundNTL2LabelText(graphDataItem, graph) {
	if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
		//   graphDataItem.values.value = (graphDataItem.values.percents).toFixed(2);

		//graphDataItem.values.value = (graphDataItem.values.percents).toFixed(2);
		if (graphDataItem.values.value < 1)
			return "";
		else
			return (graphDataItem.values.percents).toFixed(2) + "%";
	} else
		return graphDataItem.values.value;
} //

function DefectsFoundNT_DrillDownToLevel3(prefix, month, subcategory, fpstatus) {

	currentMonth = month;
	currentStudio = subcategory;

	$("#" + prefix + "_L2_chartdiv").hide();

	var L3_id = prefix + "_L3_chartdiv";
	currentPrefix = prefix + "_L3";
	addLevelChartDiv(prefix, L3_id);

	var returnData = GetDefectsFoundPerTheme(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
	var levelThreeConfig = DefectsFoundL3generateConfig(prefix, "Defects - " + subcategory + " for " + month, "Defects Found", "theme", currentTheme, false);

	levelThreeConfig.dataProvider = returnData.chart;

	levelThreeConfig.legend.enabled = true;
	levelThreeConfig.legend.marginTop = 10;
	levelThreeConfig.legend.valueFunction = adjustDefectsFoundNTL3LegendText;
	levelThreeConfig.graphs = GetGraphsDefectsFoundL3();

	var i;

	// Adjust Label Functions 
	for (i = 0; i < 6; i++) {
		levelThreeConfig.graphs[i].balloonFunction = adjustDefectsFoundNTL3Text;
		levelThreeConfig.graphs[i].labelFunction = adjustDefectsFoundNTL2LabelText;
	}

	for (i = 6; i < 12; i++) {
		levelThreeConfig.graphs[i].balloonFunction = adjustDefectsFoundNTL3Text;
	}

	levelThreeConfig = setColumnSpacingFirstPass(levelThreeConfig);

	//levelTwoConfig.columnSpacing = -15;
	levelThreeConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
        },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Count",
			"autoGridCount": false
        }];


	levelThreeConfig.chartCursor = {
		valueBalloonsEnabled: false,
		/*
		categoryBalloonFunction: function (category) {
		    return themeLookUp[category];
		},*/
		fullWidth: true,
		cursorAlpha: 0.1,
		zoomable: false,
		pan: true
	};

	levelThreeConfig.categoryAxis.autoWrap = true;

	delete levelThreeConfig.chartScrollbar.graph;
	delete levelThreeConfig.chartScrollbar.graphType;
	levelThreeConfig.chartScrollbar.autoGridCount = false;
	levelThreeConfig.chartScrollbar.hideResizeGrips = true;
	levelThreeConfig.chartScrollbar.resizeEnabled = false;
	levelThreeConfig.zoomOutText = "";
	levelThreeConfig.mouseWheelZoomEnabled = false;
	levelThreeConfig.mouseWheelScrollEnabled = true;

	chartConfigDict["defectsPhaseFound_L3"] = levelThreeConfig;
	var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
	chartResizerDict[currentPrefix] = true;

	chart.addLabel(
		'!160', '20',
		"Enable Count",
    undefined,
    12,
    undefined,
    undefined,
    undefined,
    true,
    'javascript:DefectsFoundNTL3EnableValues();');

	chart.addLabel(
		'!170', '34',
		"Sort By Defects",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortDefectsThemeL3ByDefectCount("' + currentPrefix + '", true);');
	
	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

	chart.addListener("init", DefectsFoundL3chartInit);
	chart.addListener("clickGraphItem", L3_handleItemClick);
	chart.addListener("rendered", L3_rendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = returnData.chart;

	level++; // increase global level indicator

	/*
	//OLD: Pull request for tester information ///////////////////////
	//get the data
	var monthInt = 0;
	if (month == "Current Month")
	    monthInt = 12;
	else monthInt = getMonthFromString(month);
	monthInt++;

	get_defectsFoundPerStudioMonth(subcategory, monthInt, DefectsFoundCallbackFunc);
	*/

	resetCurrentHash();

	var dataTable = returnData.chart; // GetDataForEntry("defectsPhaseFound", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
	dataTableDataDict["defectsPhaseFound_dataTable"] = dataTable;
	dataTableOptionsDict["defectsPhaseFound_dataTable"] = {
		"scrollY": 200,
		"scrollX": true,
		"dom": 'rtBfp',
		"buttons": ['copyHtml5', 'csvHtml5']
	};


	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
	    showHideDataTable(prefix + "_dataTable");
}

function DefectsFoundNTL3EnableValues() {
    var chart = chartDict["defectsPhaseFound_L3"];
	chart.showGraph(chart.getGraphById("earlyTesting"));
	chart.showGraph(chart.getGraphById("paytable"));
	chart.showGraph(chart.getGraphById("math"));
	chart.showGraph(chart.getGraphById("pa"));
	chart.showGraph(chart.getGraphById("postPA"));
	chart.showGraph(chart.getGraphById("prePA"));
}

function DefectsFoundL3chartInit(event) {
    chart = event.chart;

    //hide value charts
    chart.hideGraph(chart.getGraphById("earlyTesting"));
    chart.hideGraph(chart.getGraphById("paytable"));
    chart.hideGraph(chart.getGraphById("math"));
    chart.hideGraph(chart.getGraphById("pa"));
    chart.hideGraph(chart.getGraphById("postPA"));
    chart.hideGraph(chart.getGraphById("prePA"));

}

function adjustDefectsFoundNTL3LegendText(graphDataItem) {
	//return graphDataItem.values.value;
	//return "balh";
	if (graphDataItem.values)
		return graphDataItem.values.value;
	else
		return "";
}

function GetGraphsDefectsFoundL3() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";

    var graphsData = [
        {
            "id": "prePA_percent",
            "fillColors": "#e4c23e",
            "lineColor": "#e4c23e",
            "color": "#ffffff",
            "title": "Pre PA %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "prePA_percent",
            "newStack": true
        },
		{
		    "id": "earlyTesting_percent",
            "fillColors": "#ea5a66",
            "lineColor": "#ea5a66",
			"color": "#ffffff",
			"title": "Early Testing %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "earlyTesting_percent",
			"newStack": false
        },
        {
            "id": "paytable_percent",
            "fillColors": "#2686b9",
            "lineColor": "#2686b9",
            "color": "#ffffff",
            "title": "Paytable %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "paytable_percent",
            "newStack": false
        },
        {
            "id": "math_percent",
            "fillColors": "#56d37a",
            "lineColor": "#56d37a",
            "color": "#ffffff",
            "title": "Math %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "math_percent",
            "newStack": false
        },
        {
            "id": "pa_percent",
            "fillColors": "#922a93",
            "lineColor": "#922a93",
			"color": "#ffffff",
			"title": "PA %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "pa_percent",
			"newStack": false
        },
		{
		    "id": "postPA_percent",
            "fillColors": "#e46f3e",
            "lineColor": "#e46f3e",
			"color": "#ffffff",
			"title": "Post PA %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "postPA_percent",
			"newStack": false
		},
		{
		    "id": "prePA",
		    "fillColors": "#e1bc29",
		    "lineColor": "#e1bc29",
		    "color": "#ffffff",
		    "title": "Pre PA",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "prePA",
		    "newStack": true
		},
		{
		    "id": "earlyTesting",
            "fillColors": "#E84855",
            "lineColor": "#E84855",
			"color": "#ffffff",
			"title": "Early Testing",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "earlyTesting",
			"newStack": false
        },
        {
            "id": "paytable",
            "fillColors": "#0e79b2",
            "lineColor": "#0e79b2",
            "color": "#ffffff",
            "title": "Paytable",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "paytable",
            "newStack": false
        },
        {
            "id": "math",
            "fillColors": "#44cf6c",
            "lineColor": "#44cf6c",
            "color": "#ffffff",
            "title": "Math",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "math",
            "newStack": false
        },
        {
            "id": "pa",
            "fillColors": "#861388",
            "lineColor": "#861388",
			"color": "#ffffff",
			"title": "PA",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "pa",
			"newStack": false
        },
		{
		    "id": "postPA",
            "fillColors": "#e16029",
            "lineColor": "#e16029",
			"color": "#ffffff",
			"title": "Post PA",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "postPA",
			"newStack": false
        }];
	var graphs = [];
	var graph = {};
	for (var i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["id"] = graphsData[i]["id"];
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}

	/*
	// Add Label functions to round percentages
	for (var i = 0; i < 6; i++) {
	    graphs[i]["labelFunction"] = function (item) {
	        if (item.values.value < 1)
	            return "";
	        else
	            return Math.round(item.values.value).toString() + "%";
	    };
	}
	*/


	return graphs;

}

function adjustDefectsFoundNTL3Text(graphDataItem, graph) {

    var data = savedData["defectsPhaseFound_L3"];
	var kitTheme = graphDataItem.category;
	var section = (graph.legendTextReal).replace(" %", "");
	var kit;

	var kits;
	for (kits in data) {
		if ((data[kits])["theme"] === kitTheme) {
			kit = kits;
		}
	}

	var percentage = true;
	if ((graph.legendTextReal).search(" %") === -1)
		percentage = false;

	var phaseData = (data[kit])[section];

	var tot = ((data[kit])[section])["Total"]; //((data[kit])["PA"])["Total"] + ((data[kit])["Pre PA"])["Total"] + ((data[kit])["Post PA"])["Total"] + ((data[kit])["Early Testing"])["Total"] +
	//((data[kit])["Paytable"])["Total"] + ((data[kit])["Math"])["Total"];


	graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;

	var test;

	if (percentage === true)
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + Math.round(graphDataItem.values.percents * 100) / 100 + "%</b>";
	else
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + ((data[kit])[section])["Total"] + "</b>";

	var property = 0;
	for (property in phaseData) {
		if (property !== "Total") {
			text += "<br>";

			if (percentage === true)
				text += property.substring(3) + ": <b style='font-size:13px;'>" + ((phaseData[property] / tot) * 100).toFixed(2) + "%</b>";
			else
				text += property.substring(3) + ": <b style='font-size:13px;'>" + phaseData[property] + "</b>";
		}
	}

	return text;
}

function GetDefectsFoundPerTheme(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
	var month;
	var tempArray;
	var tempInt;
	var studio;
	var cat;
	var dataStudio;
	var dataTheme;
	var dataThemeKey;
	var dataKit;
	var dataPhase;
	var datarow;
	var row;
	var isValidKit;
	var chartTable = [];
	var themeDict = {};
	var themeLookUp = {}; // for the cursor category balloon
	var tempTheme = "";
	var compareVal;

	if (monthname == "Current Month")
		month = 12;
	else month = getMonthFromString(monthname);


	var defectTable = jsonTable[defect_table_name + month.toString()];

	studio = sub_category;

	var kitChartData = {};


	for (var dataindex in defectTable) {
		if (defectTable.hasOwnProperty(dataindex)) {
			datarow = defectTable[dataindex];
			dataStudio = datarow["Studio"];
			dataTheme = datarow["KitTheme"];
			dataKit = datarow["Kit"];
			dataThemeKey = datarow["SAP_Theme_ID"];
			dataPhase = datarow["PhaseFoundGroup"];

			compareVal = dataStudio;

			if (studio_alias) {
				if (studio_alias.hasOwnProperty(compareVal))
					compareVal = studio_alias[compareVal];
			}

			isValidKit = false;
			if (studio === "Total" || studio === compareVal) {
				isValidKit = true;
			}



			//(kitChartData[dataKit])["theme"] = dataTheme;


			if (isValidKit) {


				if (kitChartData[dataKit] === undefined) {
					kitChartData[dataKit] = {
						"PA": {
							"Total": 0
						},
						"Early Testing": {
							"Total": 0
						},
						"Pre PA": {
							"Total": 0
						},
						"Math": {
							"Total": 0
						},
						"Paytable": {
							"Total": 0
						},
						"Post PA": {
							"Total": 0
						},
						"pa_percent": 0,
						"earlyTesting_percent": 0,
						"prePA_percent": 0,
						"math_percent": 0,
						"paytable_percent": 0,
						"postPA_percent": 0
					};
				}


				if ((kitChartData[dataKit])["theme"] === undefined) {
					if (dataTheme.length >= 27) {
						tempTheme = dataTheme.substring(0, 26);
						if (themeDict.hasOwnProperty(tempTheme))
							themeDict[tempTheme] = themeDict[tempTheme] + 1;
						else themeDict[tempTheme] = 1;
						(kitChartData[dataKit])["theme"] = tempTheme + "_" + themeDict[tempTheme]; // max out                     
					} else {
						(kitChartData[dataKit])["theme"] = dataTheme;
					}

					themeLookUp[(kitChartData[dataKit])["theme"]] = dataTheme;
					(kitChartData[dataKit])["fulltheme"] = dataTheme;
					(kitChartData[dataKit])["themekey"] = dataThemeKey;
					(kitChartData[dataKit])["month"] = month;
				}

				if (dataPhase === "PA") {
					((kitChartData[dataKit])["PA"])["Total"] += parseInt(datarow["Count"]);
					if (((kitChartData[dataKit])["PA"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["PA"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["PA"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "PRE PA") {
					((kitChartData[dataKit])["Pre PA"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Pre PA"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Pre PA"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Pre PA"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "EARLY TESTING") {
					((kitChartData[dataKit])["Early Testing"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Early Testing"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Early Testing"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Early Testing"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "POST PA") {
					((kitChartData[dataKit])["Post PA"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Post PA"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Post PA"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Post PA"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "MATH") {
					((kitChartData[dataKit])["Math"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Math"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Math"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Math"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "PAYTABLE") {
					((kitChartData[dataKit])["Paytable"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Paytable"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Paytable"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Paytable"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				}

			}
		}
	}

	savedData["defectsPhaseFound_L3"] = kitChartData;

	var kits;

	for (kits in kitChartData) {
		row = [];

		row["theme"] = (kitChartData[kits])["theme"];
		//themeLookUp[row["theme"]] = row["theme"];
		row["kit"] = kits;
		row["pa"] = ((kitChartData[kits])["PA"])["Total"];
		row["prePA"] = ((kitChartData[kits])["Pre PA"])["Total"];
		row["postPA"] = ((kitChartData[kits])["Post PA"])["Total"];
		row["paytable"] = ((kitChartData[kits])["Paytable"])["Total"];
		row["math"] = ((kitChartData[kits])["Math"])["Total"];
		row["earlyTesting"] = ((kitChartData[kits])["Early Testing"])["Total"];

		var tot = ((kitChartData[kits])["PA"])["Total"] + ((kitChartData[kits])["Pre PA"])["Total"] + ((kitChartData[kits])["Post PA"])["Total"] + ((kitChartData[kits])["Paytable"])["Total"] + ((kitChartData[kits])["Math"])["Total"] + ((kitChartData[kits])["Early Testing"])["Total"];
		row["pa_percent"] = ((((kitChartData[kits])["PA"])["Total"] / tot) * 100).toFixed(2);
		row["prePA_percent"] = ((((kitChartData[kits])["Pre PA"])["Total"] / tot) * 100).toFixed(2);
		row["postPA_percent"] = ((((kitChartData[kits])["Post PA"])["Total"] / tot) * 100).toFixed(2);
		row["paytable_percent"] = ((((kitChartData[kits])["Paytable"])["Total"] / tot) * 100).toFixed(2);
		row["math_percent"] = ((((kitChartData[kits])["Math"])["Total"] / tot) * 100).toFixed(2);
		row["earlyTesting_percent"] = ((((kitChartData[kits])["Early Testing"])["Total"] / tot) * 100).toFixed(2);

		row["total_defects"] = tot;
		chartTable.push(row);
	}

	chartTable.sort(function (a, b) {
		if (a.theme < b.theme) return -1;
		if (a.theme > b.theme) return 1;
		return 0;
	});

	return {
		chart: chartTable,
		themes: themeLookUp
	};
}

function DefectsFoundL3generateConfig(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
	var config = {
		"type": "serial",
		"theme": theme,
		"categoryField": catField,
		"mouseWheelZoomEnabled": true,
		"mouseWheelScrollEnabled": true,
		"categoryAxis": {
			"gridPosition": "start",
			"labelOffset": -2
		},
		"valueAxes": [
			{
				"id": "v1",
				"title": yAxisTitle
            }
        ],
		"legend": {
			"enabled": true,
			"useGraphSettings": true
		},
		"titles": [
			{
				"id": chartId,
				"size": 15,
				"text": chartTitle
            }
        ],
		"chartCursor": {
			valueBalloonsEnabled: false,
			fullWidth: true,
			cursorAlpha: 0.1,
			zoomable: true,
			pan: false
		},
		"chartScrollbar": {
			"graph": "totalline",
			"graphType": "line",
			"color": "#FFFFFF",
			"selectedBackgroundColor": "#6F7D8A",
			"selectedBackgroundAlpha": "0.75",
			"selectedGraphLineColor": "#FFFFFF",
			"autoGridCount": true
		},
		"export": {
			"enabled": true
		},
		"responsive": {
			"enabled": true,
			"rules": [
				{
					"maxWidth": 1000,
					"overrides": {
						"legend": {
							"enabled": false
						}
					}
                },
				{
					"maxWidth": 500,
					"overrides": {
						"valueAxes": {
							"inside": true
						}
					}
                }
            ]
		}
	};

	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});

	if (useStackCharts) {
		config.valueAxes[0].stackType = "regular";
	}

	return config;
}

// OLD L3 PIE CHART (SDET/PA) CODE //////////////////
/*
function DefectsFoundCallbackFunc(r) {
    var passedData = null;
    var prefix = "defectsPhaseFound";
    var sdets = ["donavaa", "stevenc", "clarkr", "whiterob", "westera", "donahos", "cains", "patelr", "sajidq", "neffm",
            "balogke"];
    if (r["DefectsFoundPerStudioMonth"] !== undefined) {
        var data = r["DefectsFoundPerStudioMonth"];
        var studio = r["studioName"];
        var month = r["month"];
        var phaseData = {};
        var saveData = { "studio": studio, "month": month, "data": null };

        //sort data and count defects based on the test
        for (var i = 0; i < data.length; i++) {
            var tester = (data[i])["EnteredBy"];
            var phase = (data[i])["Phase"];

            if (phaseData[phase] === undefined)
                phaseData[phase] = { "sdet": 0, "pa": 0 };

            if (sdets.indexOf(tester) !== -1) {
                (phaseData[phase])["sdet"] += 1;
            } else {
                (phaseData[phase])["pa"] += 1;
            }
        }

        saveData["data"] = phaseData;
        defectsFoundStudioMonth[studio + month] = saveData;
        passedData = saveData;
    }
    else {
        passedData = r;
    }

    var monthName = "Current Month";
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    if (passedData["month"] !== "13") {

        monthName = months[(passedData["month"] - 1)];
    }
    //create the graph
    var config = getDefectFoundNTL3PieChart("defectsClosedNTL3pie", "Defects Found For " + passedData["studio"] + " - " + monthName, "black", passedData);
    var chart = AmCharts.makeChart(prefix + "_L3_chartdiv", config, 100);
    chartResizerDict["defectsPhaseFound_L3"] = true;

    chart.addLabel(
        35, 20,
        "< Go back",
        undefined,
        15,
        undefined,
        undefined,
        undefined,
        true,
        'javascript:GoUpLevel("' + prefix + '");');

    chart.addListener("init", function () {
        //L3_rendered();
        removeLoadingDiv();
        addSPListeners();
    });


    chartDict["defectsPhaseFound_L3"] = chart;
    chartDataDict["defectsPhaseFound_L3"] = passedData;


    level++; // increase global level indicator

    dataTableDataDict[prefix + "_dataTable"] = DefectsFoundNTL3GetDataForEntry(prefix, passedData["data"]);

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");

    var listSdets = "";
    for (var i = 0; i < sdets.length; i++) {
        if (i !== 0)
            listSdets += ", ";
        listSdets += sdets[i];
    }
    $("#" + prefix + "_disclaimer").text("This data uses the following SDETs: " + listSdets + ". If this list is old, please update it in the function DefectsFoundCallbackFunc().");


    removeLoadingDiv();
}

function DefectsFoundNTL3GetDataForEntry(prefix, data) {

    var viewTable = [];

    //var totals = {};
    var tot = 0;
    var property;
    for (property in data) {
        tot += (data[property])["pa"] + (data[property])["sdet"];
    }

    //add data to datatable
    for (property in data) {
        var paPlusSdet = (data[property])["pa"] + (data[property])["sdet"];
        var perDefects = ((paPlusSdet / tot) * 100).toFixed(2);
        var perPA = (((data[property])["pa"] / paPlusSdet) * 100).toFixed(2);
        var perSDET = (((data[property])["sdet"] / paPlusSdet) * 100).toFixed(2);
        viewTable.push({ "Phase": property.substring(3), "% Defects": perDefects + "%", "PA Game Test": (data[property])["pa"], "PA Game Test %": perPA + "%", "SDETs": (data[property])["sdet"], "% SDETs": perSDET + "%" });
    }

    return viewTable;
}


function getDefectsFoundNTL3ChartData(phaseData, selected) {

    var chartData = [];
    var property;

    for (property in phaseData) {
        var dataEntry = {};

        //sort data in chartData for the graph; 
        if (property.substring(0, 2) === selected) { //selected part of piechart
            dataEntry["phase"] = property.substring(3) + " - SDET";
            dataEntry["value"] = (phaseData[property])["sdet"];

            dataEntry["pulled"] = true;

            chartData.push(dataEntry);

            dataEntry = {};
            dataEntry["phase"] = property.substring(3) + " - PA Game Test";
            dataEntry["value"] = (phaseData[property])["pa"];

            dataEntry["pulled"] = true;
            chartData.push(dataEntry);
        }
        else {
            dataEntry["phase"] = property.substring(3);
            dataEntry["value"] = (phaseData[property])["sdet"] + (phaseData[property])["pa"];
            dataEntry["id"] = property.substring(0, 2);
            chartData.push(dataEntry);
        }
    }

    return chartData;
}



function getDefectFoundNTL3PieChart(chart_id, chartTitle, theme, data) {
    var config = {
        "type": "pie",
        "theme": theme,
        "titles": [
            {
                "id": chart_id,
                "size": 15,
                "text": chartTitle
            }
        ],
        "legend": {
            "markerType": "circle",
            "position": "bottom",
            "autoMargins": false
        },
        "dataProvider": getDefectsFoundNTL3ChartData(data["data"], null),
        "valueField": "value",
        "titleField": "phase",
        "pulledField": "pulled",
        "groupedTitle": "group",
        "radius": "30%",
        "marginBottom": "30",
        "marginTop": "70",
        "labelRadius": "50",
        "pullOutRadius": "45%",
        "hideLabelsPercent": "1",
        "hoverAlpha": ".8",
        "balloon": {
            "fixedPosition": true
        },
        "export": {
            "enabled": true,
            "backgroundColor": "#222222",
            "drawing": {
                "enabled": true,
                "color": "#FFFFFF"
            }
        },
        "listeners": [{
            "event": "clickSlice",
            "method": function (event) {
                var chart = event.chart;
                var selected;
                if (event.dataItem.dataContext.id != undefined) {
                    selected = event.dataItem.dataContext.id;
                } else {
                    selected = undefined;
                }
                chart.dataProvider = getDefectsFoundNTL3ChartData(data["data"], selected);
                chart.validateData();
            }
        }],
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 200,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        },
                        "balloon": {
                            "enabled": false
                        }
                    }

                }
            ]
        }
    };
    return config;
}
*/



// CHARTS CONFIG ////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

function GetDefectsFoundNTGraphsL2() {
	var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	var graphsData = [
        {
            "id": "prePA_percent",
            "fillColors": "#e4c23e",
            "lineColor": "#e4c23e",
            "color": "#ffffff",
            "title": "Pre PA %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "prePA_percent",
            "newStack": true
        },
		{
		    "id": "earlyTesting_percent",
            "fillColors": "#ea5a66",
            "lineColor": "#ea5a66",
			"color": "#ffffff",
			"title": "Early Testing %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "earlyTesting_percent",
			"newStack": false
        },
        {
            "id": "paytable_percent",
            "fillColors": "#2686b9",
            "lineColor": "#2686b9",
            "color": "#ffffff",
            "title": "Paytable %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "paytable_percent",
            "newStack": false
        },
        {
            "id": "math_percent",
            "fillColors": "#56d37a",
            "lineColor": "#56d37a",
            "color": "#ffffff",
            "title": "Math %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "math_percent",
            "newStack": false
        },
        {
            "id": "pa_percent",
            "fillColors": "#922a93",
            "lineColor": "#922a93",
            "color": "#ffffff",
            "title": "PA %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "pa_percent",
            "newStack": false
        },
        {
            "id": "postPA_percent",
            "fillColors": "#e46f3e",
            "lineColor": "#e46f3e",
            "color": "#ffffff",
            "title": "Post PA %",
            "labelText": "[[percents]]%",
            "columnWidth": 1,
            "valueAxis": "v1",
            "valueField": "postPA_percent",
            "newStack": false
        },
        {
            "id": "prePA",
            "fillColors": "#e1bc29",
            "lineColor": "#e1bc29",
            "color": "#ffffff",
            "title": "Pre PA",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "prePA",
            "newStack": true
        },
        {
            "id": "earlyTesting",
            "fillColors": "#E84855",
            "lineColor": "#E84855",
            "color": "#ffffff",
            "title": "Early Testing",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "earlyTesting",
            "newStack": false
        },
        {
            "id": "paytable",
            "fillColors": "#0e79b2",
            "lineColor": "#0e79b2",
            "color": "#ffffff",
            "title": "Paytable",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "paytable",
            "newStack": false
        },
        {
            "id": "math",
            "fillColors": "#44cf6c",
            "lineColor": "#44cf6c",
            "color": "#ffffff",
            "title": "Math",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "math",
            "newStack": false
        },
        {
            "id": "pa",
            "fillColors": "#861388",
            "lineColor": "#861388",
            "color": "#ffffff",
            "title": "PA",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "pa",
            "newStack": false
        },
        {
            "id": "postPA",
            "fillColors": "#e16029",
            "lineColor": "#e16029",
            "color": "#ffffff",
            "title": "Post PA",
            "labelText": "[[value]]",
            "columnWidth": 0.85,
            "valueAxis": "v2",
            "valueField": "postPA",
            "newStack": false
        }];
    var graphs = [];
    var graph = {};
	var i;

	for (i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
	    //graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["id"] = graphsData[i]["id"];
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}

	/*jshint loopfunc: true */ // this function in a loop is fine

	// Add Label functions to round percentages
	for (i = 0; i < 6; i++) {
		graphs[i]["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return Math.round(item.values.value).toString() + "%";
		};
	}

	// start line graphs (rolling totals)
	graph = {
		"balloonText": ballonText,
		"id": "rollingEarlyTestingPercent",
		"title": "Rolling Early Testing %",
		"bullet": "square",
        "lineColor": "#f19199",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingEarlyTestingPercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingMathPercent",
		"title": "Rolling Math Testing %",
		"bullet": "square",
        "lineColor": "#8ee2a6",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingMathPercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingPaytablePercent",
		"title": "Rolling Paytable Testing %",
		"bullet": "square",
		"lineColor": "#66cac0",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPaytablePercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingPaPercent",
		"title": "Rolling PA %",
		"bullet": "square",
        "lineColor": "#b671b7",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPaPercent",
		"stackable": false
	};
	graphs.push(graph);


	graph = {
		"balloonText": ballonText,
		"id": "rollingPostPAPercent",
		"title": "Rolling Post PA %",
		"bullet": "square",
        "lineColor": "#ed9f7e",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPostPAPercent",
		"stackable": false
	};
	graphs.push(graph);


	graph = {
		"balloonText": ballonText,
		"id": "rollingPrePAPercent",
		"title": "Rolling Pre PA %",
		"bullet": "square",
        "lineColor": "#edd67e",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPrePAPercent",
		"stackable": false
	};
	graphs.push(graph);


	return graphs;
} //

function GetDefectsFoundNTPercentAndTotal(jsonTable, table_name, sub_category, studio_alias) {
	var kitTable;
	var fpTable;
	var chartTable = [];
	var currentFiscal;
	var row;
	var dataStudio;
	var studio = sub_category;

	var months = [{
		name: "January",
		num: 0
    }, {
		name: "February",
		num: 1
    }, {
		name: "March",
		num: 2
    }, {
		name: "April",
		num: 3
    }, {
		name: "May",
		num: 4
    }, {
		name: "June",
		num: 5
    }, {
		name: "July",
		num: 6
    }, {
		name: "August",
		num: 7
    }, {
		name: "September",
		num: 8
    }, {
		name: "October",
		num: 9
    }, {
		name: "November",
		num: 10
    }, {
		name: "December",
		num: 11
    }];
	var rotated_months = months.concat(months.splice(0, new Date().getMonth()));
	rotated_months.push({
	    name: "Current Month",
		num: 12
	});

	var twelveMonthRollingTotal = 0;
	var yearToDateRollingTotal;

	var allmonthPhases = {};
	for (month = 0; month < 13; month++) {
		row = {};

		row["month"] = rotated_months[month]["name"];
		if (rotated_months[month]["name"] == "January")
			yearToDateRollingTotal = 0;

		row["prePA"] = 0;
		row["earlyTesting"] = 0;
		row["postPA"] = 0;
		row["pa"] = 0;
		row["math"] = 0;
		row["paytable"] = 0;
		row["prePA_percent"] = 0;
		row["earlyTesting_percent"] = 0;
		row["postPA_percent"] = 0;
		row["pa_percent"] = 0;
		row["math_percent"] = 0;
		row["paytable_percent"] = 0;
		row["studio"] = studio;

		fpTable = jsonTable[table_name + rotated_months[month]["num"]];

		var totPhasesPA = {};
		var totPhasesPre = {};
		var totPhasesPost = {};
		var totPhasesEarly = {};
		var totPhasesMath = {};
		var totPhasesPaytable = {};

		var total_TempVal = 0;
		for (var dataindex in fpTable) {
			if (fpTable.hasOwnProperty(dataindex)) {
				datarow = fpTable[dataindex];
				dataStudio = datarow["Studio"];

				if (studio_alias) {
					if (studio_alias.hasOwnProperty(dataStudio))
						dataStudio = studio_alias[dataStudio];
				}

				if (studio == "Total" || studio == dataStudio) {
					if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
						twelveMonthRollingTotal += datarow["Count"];
						total_TempVal += datarow["Count"];

						if (yearToDateRollingTotal != undefined)
							yearToDateRollingTotal += datarow["Count"];


						if (datarow["PhaseFoundGroup"] === "PA") {
							row["pa"] += datarow["Count"];

							if (totPhasesPA[datarow["Phase"]] === undefined)
								totPhasesPA[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPA[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "PRE PA") {
							row["prePA"] += datarow["Count"];

							if (totPhasesPre[datarow["Phase"]] === undefined)
								totPhasesPre[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPre[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "EARLY TESTING") {
							row["earlyTesting"] += datarow["Count"];

							if (totPhasesEarly[datarow["Phase"]] === undefined)
								totPhasesEarly[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesEarly[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "POST PA") {
							row["postPA"] += datarow["Count"];

							if (totPhasesPost[datarow["Phase"]] === undefined)
								totPhasesPost[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPost[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "MATH") {
							row["math"] += datarow["Count"];

							if (totPhasesMath[datarow["Phase"]] === undefined)
								totPhasesMath[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesMath[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "PAYTABLE") {
							row["paytable"] += datarow["Count"];

							if (totPhasesPaytable[datarow["Phase"]] === undefined)
								totPhasesPaytable[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPaytable[datarow["Phase"]] += datarow["Count"];
						}
					}
				}
			}
		}

		totPhasesPA["Total"] = row["pa"];
		totPhasesPre["Total"] = row["prePA"];
		totPhasesPost["Total"] = row["postPA"];
		totPhasesEarly["Total"] = row["earlyTesting"];
		totPhasesMath["Total"] = row["math"];
		totPhasesPaytable["Total"] = row["paytable"];

		var allphaseData = {
			"PA": totPhasesPA,
			"Pre PA": totPhasesPre,
			"Post PA": totPhasesPost,
			"Early Testing": totPhasesEarly,
			"Math": totPhasesMath,
			"Paytable": totPhasesPaytable
		};

		allmonthPhases[row["month"]] = allphaseData;

		var total = row["pa"] + row["prePA"] + row["earlyTesting"] + row["postPA"] + row["math"] + row["paytable"];
		if (total > 0) {
			row["pa_percent"] = ((row["pa"] / total) * 100).toFixed(2);
			row["prePA_percent"] = ((row["prePA"] / total) * 100).toFixed(2);
			row["earlyTesting_percent"] = ((row["earlyTesting"] / total) * 100).toFixed(2);
			row["postPA_percent"] = ((row["postPA"] / total) * 100).toFixed(2);
			row["math_percent"] = ((row["math"] / total) * 100).toFixed(2);
			row["paytable_percent"] = ((row["paytable"] / total) * 100).toFixed(2);
		}
		row["rollingTotal"] = twelveMonthRollingTotal;
		if (yearToDateRollingTotal != undefined)
			row["yearToDateRollingDate"] = yearToDateRollingTotal;

		chartTable.push(row);
	}


	extraGraphData["defectsPhaseFound"] = allmonthPhases;

	return chartTable;
} //

function parseDefectsFoundNTToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempStudio;
	var rollingTotal = 0;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;
				row[tempStudio] = tempInt + parseInt(datarow["Count"]);
				rollingTotal += parseInt(datarow["Count"]);
			}
		}
	}

	if (sub_category == undefined && sub_category != "Total")
		row["Total"] = rollingTotal;

	return row;
} //