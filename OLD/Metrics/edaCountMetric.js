thisMetricName = "edas";


function get_EDAForKitsReleasedMetrics() {

	if (loadedMetrics["edas"] != true) {
	    get_MetricsData("edas", "edas");
		return;
	}

	if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "EDAs by Kit Relase Date");

	clearChart("edas");
	showHideDataTable("edas", "100%", false);

	chartData = parseMetricData(restJsonDict, "EDACountPerKit_Month", studio_alias, "edas");
	chartGraphs = GetStackedGraphs(chartData, "month", "PA", "Other");

    
	dataTableDataDict["edas_dataTable"] = chartData;

	var edaReleasedConfig = generateConfig("edas", "EDAs by Kit Release Date - Studio", "EDA Count", "month", currentTheme, true);
	edaReleasedConfig.dataProvider = chartData;
	edaReleasedConfig.graphs = chartGraphs;

    // put kits released in the balloon function
	for (var i = 0; i < edaReleasedConfig.graphs.length; i++) {
        chartGraphs[i].balloonFunction = function (item, graph) {
            var monthName = item.category;
            var studio = graph.valueField;
            var kitsReleasedValue = getKitsForBaloon(monthName, studio);
            return graph.title + " of " + item.category + ": " + item.values.value +  "</br> Kits Released: " + kitsReleasedValue;
        }
    }
    
	chartConfigDict["edas"] = edaReleasedConfig;
	chart = AmCharts.makeChart("edas_chartdiv", edaReleasedConfig, 100);
	chartResizerDict["edas"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDataDict["edas"] = chartData;
	chartDict["edas"] = chart; // store chart
}

function parseEDAsToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempStudio;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
	    targetStudio = sub_category.split('/')[0];
	    use_sub_category = true;
	}
    /*
	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;
				row[tempStudio] = tempInt + parseInt(datarow["Count"]);
				rollingTotal += parseInt(datarow["Count"]);
			}
		}
	}

	if (sub_category == undefined && sub_category != "Total")
		row["Total"] = rollingTotal;
    */


	for (var dataindex in viewTable) {
	    if (viewTable.hasOwnProperty(dataindex)) {
	        datarow = viewTable[dataindex];


	        if (studio_alias.hasOwnProperty(datarow["Studio"]))
	            tempStudio = studio_alias[datarow["Studio"]];
	        else tempStudio = datarow["Studio"];
	        if (tempStudio !== "") {

	            if (use_sub_category == false || targetStudio == "Total") {
	                row["studio"] = "Total";
	                if (!row.hasOwnProperty("Total/PA"))
	                    row["Total/PA"] = 0;

	                if (!row.hasOwnProperty("Total/Other"))
	                    row["Total/Other"] = 0;


	                if (datarow.hasOwnProperty("Responsible_Group")) {
	                    if (datarow["Responsible_Group"] === "PA") {
	                        row["Total/PA"] += datarow["Count"];
	                    }
	                    else /*if (datarow["Result"] === "N")*/ {
	                        row["Total/Other"] += datarow["Count"];
	                    }
	                }

	            }

	            if (row.hasOwnProperty("Total"))
	                row["Total"] = row["Total/PA"] + row["Total/Other"];


	            if (use_sub_category == false || targetStudio == tempStudio) {
	                row["studio"] = tempStudio;
	                if (!row.hasOwnProperty(tempStudio + "/PA"))
	                    row[tempStudio + "/PA"] = 0;

	                if (!row.hasOwnProperty(tempStudio + "/Other"))
	                    row[tempStudio + "/Other"] = 0;

	                if (datarow.hasOwnProperty("Responsible_Group")) {
	                    if (datarow["Responsible_Group"] === "PA") {
	                        row[tempStudio + "/PA"] += datarow["Count"];
	                    }
	                    else {
	                        row[tempStudio + "/Other"] += datarow["Count"];
	                    }
	                }

	            }
	        }
	    }
	}
	return row;
}

function edaCountDrillDownToLevel3(prefix, monthname, studio, fpstatus) {
    thisMetricName = "edas";
    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);


    var splitCat = fpstatus.split("/");

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var date = new Date();
    var year = date.getFullYear();
    var month = 0;

    if (monthname == "Current Month") {
        month = date.getMonth() + 1;
    }
    else {
        month = getMonthFromString(monthname) + 1;
        if (month > (date.getMonth() + 1)) {
            year--;
        }
    }

    if (month < 10)
        month = "0" + month;

    var yearMonth = year + "" + month;
    /*
    var paCount = "Y";
    if (splitCat[1] === "Other")
        paCount = "N"; 
    */
    // get data
    edaCountL3(currentPrefix, restJsonDict, yearMonth, studio);
}

function edaCountL3(prefix, jsonTable, yearMonth, studio) {

    if (loadedMetrics[prefix + studio + yearMonth] != true) {
        //get_CN_IN_KitList(studio, yearMonth, paCount);
        get_edasDataL3(thisMetricName, studio, yearMonth);
        return;
    }

    var fpTable = restJsonDict[prefix + studio + yearMonth];
    var L3_id = prefix + "_chartdiv";
    currentPrefix = prefix;
    addLevelChartDiv(prefix, L3_id);

    var jsLink = "$('#" + thisMetricName + "_dataTable_link').show(); GoUpLevel('" + thisMetricName + "');";
    $("#" + L3_id).html('<a id="' + thisMetricName + '_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="' + thisMetricName + '_L3_dataTable_group">');

    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('metricTitle');
    title_div.html("EDAs Kits Released for " + yearMonth + " and " + studio.replaceAll("_", " ") + " - Total: " + fpTable.EDAEnteredL3Info.length);
    $("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict[prefix] = fpTable.EDAEnteredL3Info;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#" + thisMetricName + "_dataTable_link").hide();

    dataTableOptionsDict[prefix + "_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(fpTable.EDAEnteredL3Info, prefix + "_dataTable", "100%", dataTableOptionsDict[prefix + "_dataTable"], tableClassList);
    dataTableHTMLTableDict[prefix] = htmlTable;

    if (fpTable.EDAEnteredL3Info.length > 0) {
        dataTableHTMLTableDict[prefix].$("tr").click(function () {
            var data = dataTableHTMLTableDict[prefix].row($(this)).data();
            $("#" + thisMetricName + "_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }

    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
}

function getKitsForBaloon(monthName, studio) {
    studio = studio.substr(0, studio.indexOf('/')).toUpperCase();
    var data = restJsonDict["PAReleasesCounts_PerMonth"];
    var months = [
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November",
        "December"
    ];
    var YYYYMM;

    var currentMonth = new Date().getMonth() + 1;
    if (currentMonth < 10) currentMonth = "0" + currentMonth;
    var currentYear = new Date().getFullYear();
    YYYYMM = currentYear + '' + currentMonth;

    if (studio === "TOTAL") {
        var totalCount = 0;
        if (monthName === "Current Month") {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Month === YYYYMM) {
                    totalCount += data[i].count;
                }
            }
            return totalCount;
        } else {
            var month = months.indexOf(monthName) + 1;
            if (month < 10) month = "0" + month;
            else month = "" + month;
            for (var i = 0; i < data.length; i++) {
                if (data[i].Month.substr(4, 2) === month && data[i].Month !== YYYYMM) {
                    totalCount += data[i].count;
                }
            }
            return totalCount;
        }
    } else {

        if (monthName === "Current Month") {
            YYYYMM = currentYear + '' + currentMonth;
            for (var i = 0; i < data.length; i++) {
                if (data[i].Month === YYYYMM && data[i].studio === studio) {
                    return data[i].count;
                }
            }
        } else {
            var month = months.indexOf(monthName) + 1;
            if (month < 10) month = "0" + month;
            else month = "" + month;
            for (var i = 0; i < data.length; i++) {
                if (data[i].Month.substr(4, 2) === month && data[i].Studio === studio && data[i].Month !== YYYYMM) {
                    return data[i].count;
                }
            }
        }
    }
}
