function get_mathDefectsFoundMetric() {
    var metricName = "mathDefectsFound";
    if (loadedMetrics[metricName] != true) {
        get_MetricsData(metricName, metricName);
        return;
    }
    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Math Defects Found");

    clearChart(metricName);
    showHideDataTable(metricName, "100%", false);

    chartData = parseMetricData(restJsonDict, "MathDefectsFound_Month", studio_alias, metricName);
    chartGraphs = GetGraphs(chartData, "month");
    dataTableDataDict[metricName + "_dataTable"] = chartData;
    //drawTable(chartData, "studio_dataTable");

    var studioConfig = generateConfig(metricName, "Math Defects Found", "Defects", "month", currentTheme, false);
    studioConfig.dataProvider = chartData;
    studioConfig.graphs = chartGraphs;

    chartConfigDict[metricName] = studioConfig;
    chart = AmCharts.makeChart(metricName + "_chartdiv", studioConfig, 100);
    chartResizerDict[metricName] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		14,
		undefined,
		undefined,
		undefined,
		true,
		'/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict[metricName] = chartData;
    chartDict[metricName] = chart;
}

function parseMathDefectsFound(row, viewTable, studio_alias, sub_category) {
    var tempInt = 0;
    var tempStudio;
    var rollingTotal = 0;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category;
        use_sub_category = true;
    }

    //var groupedByStudio = _.groupBy(viewTable, 'Studio');

    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];

            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];

            if (use_sub_category == false || targetStudio == tempStudio) {
                if (row.hasOwnProperty(tempStudio))
                    tempInt = row[tempStudio];
                else tempInt = 0;
                row[tempStudio] = tempInt + parseInt(datarow["Errors"]);
                rollingTotal += parseInt(datarow["Errors"]);
            }
        }
    }

    if (sub_category == undefined && sub_category != "Total")
        row["Total"] = rollingTotal;

    return row;
}


function MathDefectsFound_DrillDownToLevel3(prefix, monthname, studio, fpstatus) {


    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    var date = new Date();
    var year = date.getFullYear();
    var month = 0;

    if (monthname == "Current Month") {
        month = 13;
    }
    else {
        month = getMonthFromString(monthname) + 1;
    }

    if (month < 10)
        month = "0" + month;


    // get data
    MathDefectsFoundDataL3(prefix, restJsonDict, month, studio);
}


function MathDefectsFoundDataL3(prefix, jsonTable, month, studio) {

    if (loadedMetrics[prefix + studio + month] != true) {

        get_mathThemesL3(prefix, studio, month);
        return;
    }

    var fpTable = restJsonDict[prefix + studio + month];
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);
    var data = fpTable.MathThemesReleasedMetric_L3;
    var monthName = getStringFromMonth(parseInt(month));
    var levelThreeConfig = generateConfig(prefix, "Released Themes - " + " for " + monthName, "Errors", "Theme", currentTheme, false);
    levelThreeConfig.dataProvider = data;
    levelThreeConfig.legend.enabled = false;

    levelThreeConfig.graphs = [{
        balloonFunction: function (graphDataItem, graph) {
            var fulltheme = graphDataItem.dataContext.Theme;
            var value = graphDataItem.dataContext.Errors;
            return fulltheme + ": " + value;
        },
        fillAlphas: 1,
        id: "AmGraph_1",
        type: "column",
        valueField: "Errors"
    }];
    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };
    levelThreeConfig.categoryAxis.autoWrap = true;

    delete levelThreeConfig.chartScrollbar.graph;
    delete levelThreeConfig.chartScrollbar.graphType;
    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    chart.addLabel(
        35, 20,
        "< Go back",
        undefined,
        15,
        undefined,
        undefined,
        undefined,
        true,
        'javascript:GoUpLevel("' + prefix + '");');

    chart.addListener("init", chartInit);
    //chart.addListener("clickGraphItem", L3_handleItemClick);
    chart.addListener("rendered", L3_rendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    var dataTable = GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["mathDefectsFound"], month, null, studio_alias);


    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
}