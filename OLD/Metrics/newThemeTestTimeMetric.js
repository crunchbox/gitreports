function get_NewThemeTestTimeMetrics() {
	if (loadedMetrics["testTime"] != true) {
	    get_MetricsData("testTime", "NewThemeAvgTestTime");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "New Theme Test Time");

	clearChart("NewThemeAvgTestTime");
	showHideDataTable("NewThemeAvgTestTime", "100%", false);

	var NewThemeAvgTestTimeConfig;
	if (chartConfigDict["NewThemeAvgTestTime"] == null) {
	    chartData = parseMetricData(restJsonDict, "NewThemeTestTime_Month", studio_alias, "NewThemeAvgTestTime");
		chartGraphs = GetGraphs(chartData, "month");

		chartGraphs.push(GetRollingGraph("rollingAvg", "Rolling Average", "#33FFBD"));

		dataTableDataDict["NewThemeAvgTestTime_dataTable"] = chartData;
		NewThemeAvgTestTimeConfig = generateConfig("NewThemeAvgTestTime", "New Theme Test Time - Studio", "Work Days", "month", currentTheme, false);
		NewThemeAvgTestTimeConfig.dataProvider = chartData;
		//chartGraphs.balloonFunction = adjustNewThemeFirstPassL2Text; 
		NewThemeAvgTestTimeConfig.graphs = chartGraphs;

		chartConfigDict["NewThemeAvgTestTime"] = NewThemeAvgTestTimeConfig;
		chartDataDict["NewThemeAvgTestTime"] = chartData;
	} else NewThemeAvgTestTimeConfig = chartConfigDict["NewThemeAvgTestTime"];

	chart = AmCharts.makeChart("NewThemeAvgTestTime_chartdiv", NewThemeAvgTestTimeConfig, 100);
	chartResizerDict["NewThemeAvgTestTime"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addLabel(
		'!150', '30',
		"Enable Trend",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:getRollingAverageTestTime();');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDict["NewThemeAvgTestTime"] = chart; // store chart
}

function parseTestTimeToMonth(row, table, studio_alias, chart) {

	var data = {};
	var chartData = (extraGraphData[chart])[row["month"]];
	var property;
	for (property in chartData) {
		data[property] = ((chartData[property])[1] / (chartData[property])[0]).toFixed(2);
	}
	data["month"] = row["month"];

	return data;
}

function parseTestTimeToChartRow(row, viewTable, studio_alias, sub_category, data) {
	var tempInt = 0;
	var tempStudio;
	var rollingTotal = 0;
	var rollingTotalSub = 0;
	var countStudioAmt = {};
	var studioName = [];


	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	var tot = 0;
	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];
			tot = tot + 1;

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;

				row[tempStudio] = tempInt + parseFloat(datarow["Testing_Time"]);
				rollingTotal = rollingTotal + parseFloat(datarow["Testing_Time"]);

				if (countStudioAmt.hasOwnProperty(tempStudio))
					countStudioAmt[tempStudio] = countStudioAmt[tempStudio] + 1;
				else
					countStudioAmt[tempStudio] = 1;

				if (studioName.indexOf(tempStudio) == -1)
					studioName.push(tempStudio);
			} else if (use_sub_category === true && targetStudio === "Total") {
				rollingTotalSub = rollingTotalSub + parseFloat(datarow["Testing_Time"]);
			}
		}
	}


	for (var i = 0; i < studioName.length; i++) {
		data[studioName[i]] = [countStudioAmt[studioName[i]], row[studioName[i]]];
		row[studioName[i]] = row[studioName[i]] / countStudioAmt[studioName[i]];
		row[studioName[i]] = Math.round(row[studioName[i]] * 100) / 100;
		// tot += countStudioAmt[studioName[i]];
	}

	if (sub_category == undefined && sub_category != "Total") {
		data["Total"] = [tot, rollingTotal];
		row["Total"] = Math.round((rollingTotal / tot) * 100) / 100;
	} else if (sub_category === "Total") {
		row["Total"] = Math.round((rollingTotalSub / tot) * 100) / 100;
	}

	return row;
}

function GetTestTimeCountPerTheme(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
	var month;
	var studio;
	var dataStudio;
	var dataTheme;
	var compareVal;
	var dataThemeKey;
	var dataKit;
	var datarow;
	var row;
	var isValidKit;
	var chartTable = [];
	var themeDict = {};
	var themeLookUp = {}; // for the cursor category balloon
	var tempTheme = "";

	if (monthname == "Current Month")
		month = 12;
	else month = getMonthFromString(monthname);

	var defectTable = jsonTable[defect_table_name + month.toString()];


	studio = sub_category;


	for (var dataindex in defectTable) {
		if (defectTable.hasOwnProperty(dataindex)) {
			datarow = defectTable[dataindex];
			dataStudio = datarow["Studio"];
			dataTheme = datarow["KitTheme"];
			dataKit = datarow["Kit"];
			dataThemeKey = datarow["SAP_Theme_ID"];

			if (chartSelection === "NewThemeAvgTestTime")
				compareVal = dataStudio;
			else {
				compareVal = datarow["Type"];
			}

			if (studio_alias && chartSelection !== "NewThemeAvgTTGameType") {
				if (studio_alias.hasOwnProperty(compareVal))
					compareVal = studio_alias[compareVal];
			}

			isValidKit = false;
			if (studio == "Total" || studio == compareVal) {
				isValidKit = true;
			}
			if (isValidKit) {
				row = [];
				if (dataTheme.length >= 27) {
					tempTheme = dataTheme.substring(0, 26);
					if (themeDict.hasOwnProperty(tempTheme))
						themeDict[tempTheme] = themeDict[tempTheme] + 1;
					else themeDict[tempTheme] = 1;
					row["theme"] = tempTheme + "_" + themeDict[tempTheme]; // max out                     
				} else {
					row["theme"] = dataTheme;
				}
				themeLookUp[row["theme"]] = dataTheme; // for the cursor balloon
				row["fullTheme"] = dataTheme;
				row["value"] = parseFloat(datarow["Testing_Time"]);
				row["kit"] = dataKit;
				row["month"] = month;
				row["themekey"] = dataThemeKey;
				chartTable.push(row);
			}
		}
	}

	chartTable.sort(function (a, b) {
		if (a.theme < b.theme) return -1;
		if (a.theme > b.theme) return 1;
		return 0;
	});

	return {
		chart: chartTable,
		themes: themeLookUp
	};
}