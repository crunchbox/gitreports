function get_mathEdasMetric() {
    var metricName = "mathEdas";
    if (loadedMetrics[metricName] != true) {
        get_MetricsData(metricName, metricName);
        return;
    }
    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Math EDAs");

    clearChart(metricName);
    showHideDataTable(metricName, "100%", false);

    chartData = parseMetricData(restJsonDict, "MathEdas_Month", studio_alias, metricName);
    chartGraphs = GetGraphs(chartData, "month");
    dataTableDataDict[metricName + "_dataTable"] = chartData;
    //drawTable(chartData, "studio_dataTable");

    var studioConfig = generateConfig(metricName, "Math EDAs", "EDAs", "month", currentTheme, false);
    studioConfig.dataProvider = chartData;
    studioConfig.graphs = chartGraphs;

    chartConfigDict[metricName] = studioConfig;
    chart = AmCharts.makeChart(metricName + "_chartdiv", studioConfig, 100);
    chartResizerDict[metricName] = true;

    chart.addLabel(
        35, 20,
        "< Go back",
        undefined,
        14,
        undefined,
        undefined,
        undefined,
        true,
        '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict[metricName] = chartData;
    chartDict[metricName] = chart;
}

function parseMathEdas(row, viewTable, studio_alias, sub_category) {
    var tempInt = 0;
    var tempStudio;
    var rollingTotal = 0;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category;
        use_sub_category = true;
    }

    //var groupedByStudio = _.groupBy(viewTable, 'Studio');

    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];

            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];

            if (use_sub_category == false || targetStudio.toUpperCase() == tempStudio.toUpperCase()) {
                if (row.hasOwnProperty(tempStudio))
                    tempInt = row[tempStudio];
                else tempInt = 0;
                row[tempStudio] = tempInt + 1;
                rollingTotal += 1;
            }
        }
    }

    if (sub_category == undefined && sub_category != "Total")
        row["Total"] = rollingTotal;

    return row;
}

function MathEdas_DrillDownToLevel3(prefix, monthname, studio, fpstatus) {
    thisMetricName = "mathEdas";
    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);

    var splitCat = fpstatus.split("/");

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var date = new Date();
    var year = date.getFullYear();
    var currentMonth = date.getMonth();
    var monthNum;
    var monthNames = ["January", "February", "March", "April", "May",
        "June", "July", "August", "September", "October", "November", "December"];


    if (monthname === "Current Month") {
        monthNum = 12;
    } else {
        monthNum = monthNames.indexOf(monthname);
    }

    var data = restJsonDict["MathEdas_Month" + monthNum];

    var jsLink = "$('#" + thisMetricName + "_dataTable_link').show(); GoUpLevel('" + thisMetricName + "');";
    $("#" + L3_id).html('<a id="' + thisMetricName + '_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="' + thisMetricName + '_L3_dataTable_group">');

    var title_div = $("<div/>",
            {
                id: currentPrefix + "_Title"
            })
        .addClass('metricTitle');
    title_div.html("Math EDAs for " + monthname + " and " + studio.replaceAll("_", " ") + " - Total: " + data.length);
    $("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict[currentPrefix] = data;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#" + thisMetricName + "_dataTable_link").hide();

    dataTableOptionsDict[currentPrefix + "_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(data, currentPrefix + "_dataTable", "100%", dataTableOptionsDict[prefix + "_dataTable"], tableClassList);
    dataTableHTMLTableDict[currentPrefix] = htmlTable;

    if (data.length > 0) {
        dataTableHTMLTableDict[currentPrefix].$("tr").click(function () {
            var data = dataTableHTMLTableDict[currentPrefix].row($(this)).data();
            $("#" + thisMetricName + "_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }

    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
}


