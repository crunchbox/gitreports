function get_edaNewOnlyMetrics() {
    if (loadedMetrics["edaNewOnly"] != true) {
        get_MetricsData("edaNewOnly", "edaNewOnly");
        return;
    }

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "EDA/CN/IN for New Themes Only");

    clearChart("edaNewOnly");
    showHideDataTable("edaNewOnly", "100%", false);

    chartData = parseMetricData(restJsonDict, "EdaNewOnly_Month", studio_alias, "edaNewOnly");
    chartGraphs = GetStackedGraphs(chartData, "month", "PA", "Other");

    dataTableDataDict["edaNewOnly_dataTable"] = chartData;

    var edaNewOnlyConfig = generateConfig("edaNewOnly",
        "EDA/CN/IN for New Themes Only",
        "EDA Count",
        "month",
        currentTheme,
        true);
    edaNewOnlyConfig.dataProvider = chartData;
    edaNewOnlyConfig.graphs = chartGraphs;

    chartConfigDict["edaNewOnly"] = edaNewOnlyConfig;
    chart = AmCharts.makeChart("edaNewOnly_chartdiv", edaNewOnlyConfig, 100);
    chartResizerDict["edaNewOnly"] = true;

    chart.addLabel(
        35, 20,
        "< Go back",
        undefined,
        14,
        undefined,
        undefined,
        undefined,
        true,
        '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict["edaNewOnly"] = chartData;
    chartDict["edaNewOnly"] = chart; // store chart
}

function edaNewOnlyDrillDownToLevel3(prefix, monthname, studio, fpstatus) {

    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);

    var splitCat = fpstatus.split("/");

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var date = new Date();
    var year = date.getFullYear();
    var month = 0;

    if (monthname == "Current Month") {
        month = date.getMonth() + 1;
    }
    else {
        month = getMonthFromString(monthname) + 1;
        if (month > (date.getMonth() + 1)) {
            year--;
        }
    }

    if (month < 10)
        month = "0" + month;

    var yearMonth = year + "" + month;
    /*
    var paCount = "Y";
    if (splitCat[1] === "Other")
        paCount = "N"; 
    */
    // get data
    edaNewOnlyL3(prefix, restJsonDict, yearMonth, studio);
}

function edaNewOnlyL3(prefix, jsonTable, yearMonth, studio) {

    if (loadedMetrics[prefix + studio + yearMonth] != true) {
        //get_CN_IN_KitList(studio, yearMonth, paCount);
        get_edasNewOnlyDataL3(prefix, studio, yearMonth);
        return;
    }

    var fpTable = restJsonDict[prefix + studio + yearMonth];
    var L3_id = prefix + "_L3" + "_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(currentPrefix, L3_id);

    var jsLink = "$('#" + prefix + "_dataTable_link').show(); GoUpLevel('" + prefix + "');";
    $("#" + L3_id).html('<a id="' + prefix + '_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="' + prefix + '_L3_dataTable_group">');

    var title_div = $("<div/>",
            {
                id: currentPrefix + "_Title"
            })
        .addClass('metricTitle');
    title_div.html("EDA/CN/IN for New Game Themes for " + yearMonth + " and " + studio.replaceAll("_", " ") + " - Total: " + fpTable.EDAEnteredL3Info.length);
    $("#" + L3_id).prepend(title_div);

    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict[prefix] = fpTable.EDAEnteredL3Info;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#" + prefix + "_dataTable_link").hide();

    dataTableOptionsDict[currentPrefix + "_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(fpTable.EDAEnteredL3Info, currentPrefix + "_dataTable", "100%", dataTableOptionsDict[currentPrefix + "_dataTable"], tableClassList);
    dataTableHTMLTableDict[currentPrefix] = htmlTable;

    if (fpTable.EDAEnteredL3Info.length > 0) {
        dataTableHTMLTableDict[currentPrefix].$("tr").click(function () {
            var data = dataTableHTMLTableDict[currentPrefix].row($(this)).data();
            $("#" + prefix + "_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }

    var dataTable = [];
    dataTableDataDict[currentPrefix + "_dataTable"] = dataTable;
}