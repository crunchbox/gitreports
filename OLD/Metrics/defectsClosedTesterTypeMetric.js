var savedDataDefectsFoundTester = {};

function get_DefectsFoundTesterTypesMetrics() {
	if (loadedMetrics["defectsClosedTT"] != true) {
	    get_MetricsData("defectsClosedTT", "defectsClosedbyTesterType");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Defects Closed Tester Type");

	clearChart("defectsClosedbyTesterType");
	showHideDataTable("defectsClosedbyTesterType_dataTable", "100%", false);

	var defectsFoundConfig;
	var chartData, chartGraphs, chart;
	if (chartConfigDict["defectsClosedbyTesterType"] == null) {
	    chartData = parseMetricData(restJsonDict, "DefectsClosedTesterType_Month", studio_alias, "defectsClosedbyTesterType");
		chartGraphs = GetStackedGraphs(chartData, "month", "Game Tester", "SDET");

		dataTableDataDict["defectsClosedbyTesterType_dataTable"] = chartData;
		defectsFoundConfig = generateConfig("defectsClosedbyTesterType", "Defects Closed by Tester Type - SDET/Game Tester", "Defects", "month", currentTheme, true);
		defectsFoundConfig.dataProvider = chartData;
		defectsFoundConfig.graphs = chartGraphs;

		chartConfigDict["defectsClosedbyTesterType"] = defectsFoundConfig;
		chartDataDict["defectsClosedbyTesterType"] = chartData;
	} else defectsFoundConfig = chartConfigDict["defectsClosedbyTesterType"];

	chart = AmCharts.makeChart("defectsClosedbyTesterType_chartdiv", defectsFoundConfig, 100);
	chartResizerDict["defectsClosedbyTesterType"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDict["defectsClosedbyTesterType"] = chart; // store chart
}


function parseDefectsFoundTesterToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempString;
	var tempStudio;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category.split('/')[0];
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (use_sub_category == false || targetStudio == "Total") {
				if (datarow["TesterType"] == 'SDET') {
					if (row.hasOwnProperty("Total/SDET"))
						tempInt = row["Total/SDET"];
					else
						tempInt = 0;
					row["Total/SDET"] = tempInt + parseInt(datarow["Count"]);
				} else {
					if (row.hasOwnProperty("Total/Game Tester"))
						tempInt = row["Total/Game Tester"];
					else
						tempInt = 0;
					row["Total/Game Tester"] = tempInt + parseInt(datarow["Count"]);
				}
			}

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				row["studio"] = tempStudio;
				if (datarow["TesterType"] === "SDET") {
					tempString = "SDET";
				} else {
					tempString = "Game Tester";
				}

				if (row.hasOwnProperty(tempStudio + "/Game Tester") == false)
					row[tempStudio + "/Game Tester"] = 0;
				if (row.hasOwnProperty(tempStudio + "/SDET") == false)
					row[tempStudio + "/SDET"] = 0;

				tempString = tempStudio + '/' + tempString;

				if (row.hasOwnProperty(tempString))
					tempInt = row[tempString];
				else tempInt = 0;
				row[tempString] = tempInt + parseInt(datarow["Count"]);
			}
		}
	}

	return row;
}



function DefectsFoundTT_DrillDownToLevel2(prefix, itemSelection) {
	currentMonth = "all";
	currentStudio = itemSelection;

	var L2_id;

	if (currentPrefix == "defectsClosedbyTesterType") {
		$("#" + prefix + "_chartdiv").hide();
		L2_id = prefix + "_L2_chartdiv";
		currentPrefix = prefix + "_L2";
		addLevelChartDiv(prefix, L2_id);
	}

	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
		showHideDataTable(prefix + "_dataTable");
	showHideDataTable(prefix, "100%", false);

	var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
	var chartTitle_firstpart = chartTitle.split('-')[0];


	var studioName = itemSelection.split('/')[0];
	var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title


	var data = parseMetricData(restJsonDict, "DefectsClosedTesterType_Month", studio_alias, prefix, itemSelection);
	data = sortL2Data(data, studioName);

	var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + "- " + studioName, yAxisTitle, "month", currentTheme, false);
	levelTwoConfig.dataProvider = data;
	levelTwoConfig.graphs = GetDefectsFoundTTGraphs();

	// Adjust Label Functions 
	for (var i = 0; i < 4; i++) {
		levelTwoConfig.graphs[i].balloonFunction = adjustDefectsFoundTTL2Text;
		levelTwoConfig.graphs[i].labelFunction = adjustDefectsFoundTTL2LabelText;
	}


	levelTwoConfig = setColumnSpacingFirstPass(levelTwoConfig);

	levelTwoConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
    },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Count",
			"autoGridCount": false
    },
		{
			"id": "v3",
			"axisColor": "#6ca979",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v2",
			"synchronizationMultiplier": 1,
			"position": "right",
			"autoGridCount": false
    },
		{
			"id": "v4",
			"axisColor": "#88D498",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v1",
			"synchronizationMultiplier": 1,
			"position": "left",
			"autoGridCount": false
    }];

	chartConfigDict["defectsClosedbyTesterType_L2"] = levelTwoConfig;
	var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
	chartResizerDict[currentPrefix] = true;

	// let's add a label to go back to yearly data
	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

	chart.addLabel(
		'!150', '20',
		"Enable Count",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:DefectsFoundTTL2EnableValues();');

	chart.addLabel(
		'!150', '34',
		"Enable Trend",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:DefectFoundTTShowRollingAverages();');


	chart.addListener("clickGraphItem", L2_handleItemClick);
	chart.addListener("init", DefectsFoundTT_L2_chartInit);
	chart.addListener("rendered", L2_handleRendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = data;

	level++; // increase global level indicator

	var dataTable = data; // GetDataForEntry("defectsClosedNewThemes", restJsonDict, "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
	dataTableDataDict[prefix + "_dataTable"] = dataTable;
	dataTableOptionsDict[prefix + "_dataTable"] = {
		"scrollY": 200,
		"scrollX": true,
		"dom": 'rtBfp',
		"buttons": ['copyHtml5', 'csvHtml5']
	};
}



function DefectsFoundTTL2EnableValues() {
	//hide rolling charts
    var chart = chartDict["defectsClosedbyTesterType_L2"];

	chart.showGraph(chart.getGraphById("game_tester"));
	chart.showGraph(chart.getGraphById("sdet"));
}

function DefectsFoundTT_L2_chartInit(event) {
	chart = event.chart;

	chart.legend.addListener("showItem", DefectsFoundNT_L2_legendHandler);
	chart.legend.addListener("hideItem", DefectsFoundNT_L2_legendHandler);

	chart.hideGraph(chart.getGraphById("rollingGamePercent"));
	chart.hideGraph(chart.getGraphById("rollingSDETPercent"));

	chart.hideGraph(chart.getGraphById("game_tester"));
	chart.hideGraph(chart.getGraphById("sdet"));
}




function DefectFoundTTShowRollingAverages() {
    var chart = chartDict["defectsClosedbyTesterType_L2"];

	//clear Graphs
	for (var i = 0; i < 13; i++) {

		if ((chart.dataProvider[i])["rollingGamePercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingGamePercent"];

		if ((chart.dataProvider[i])["rollingSDETPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingSDETPercent"];
	}

	//update graphs with new %s
	var start = chart.start;
	var end = chart.end;
	calculateRollingPercentagesDefectsFoundTT(chart, start, end);


	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingGamePercent"));
	chartDict[currentPrefix].showGraph(chart.getGraphById("rollingSDETPercent"));


	chart.invalidateSize();
	chart.validateNow();
}




function calculateRollingPercentagesDefectsFoundTT(chart, start, end) {
	var tot_sdet = 0,
		tot_game = 0;
	var month = 0;

	//iterate through the month
	for (var i = start; i <= end; i++) {
		month++;

		tot_sdet = tot_sdet + (chart.dataProvider[i]).sdet;
		tot_game = tot_game + (chart.dataProvider[i]).game_tester;

		(chart.dataProvider[i])["rollingGamePercent"] = (tot_game / month).toFixed(2);
		(chart.dataProvider[i])["rollingSDETPercent"] = (tot_sdet / month).toFixed(2);
	}
}



function sortL2Data(oriData, studio) {
	var data = [];

	for (var i = 0; i < oriData.length; i++) {
		var monthData = {};

		monthData["month"] = (oriData[i])["month"];
		var tot = 0;

		if ((oriData[i])[studio + "/Game Tester"] !== undefined) {
			tot = tot + (oriData[i])[studio + "/Game Tester"];
			monthData["game_tester"] = (oriData[i])[studio + "/Game Tester"];
		} else {
			monthData["game_tester"] = 0;
		}

		if ((oriData[i])[studio + "/SDET"] !== undefined) {
			tot = tot + (oriData[i])[studio + "/SDET"];
			monthData["sdet"] = (oriData[i])[studio + "/SDET"];
			monthData["sdet_percent"] = (((oriData[i])[studio + "/SDET"] / tot) * 100).toFixed(2);
		} else {
			monthData["sdet"] = 0;
			monthData["sdet_percent"] = 0;
		}

		if ((oriData[i])[studio + "/Game Tester"] !== undefined)
			monthData["game_percent"] = (((oriData[i])[studio + "/Game Tester"] / tot) * 100).toFixed(2);
		else {
			monthData["game_percent"] = 0;
		}

		monthData["studio"] = studio;
		data.push(monthData);
	}

	return data;
}




function GetDefectsFoundTTGraphs() {
	var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	var graphsData = [
		{
			"id": "game_percent",
			"fillColors": "#05668D",
			"lineColor": "#05668D",
			"color": "#ffffff",
			"title": "Game Tester %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "game_percent",
			"newStack": true
        },
		{
			"id": "sdet_percent",
			"fillColors": "#DBD56E",
			"lineColor": "#DBD56E",
			"color": "#ffffff",
			"title": "SDET %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "sdet_percent",
			"newStack": false
        },
		{
			"id": "game_tester",
			"fillColors": "#045170",
			"lineColor": "#045170",
			"color": "#ffffff",
			"title": "Game Tester",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "game_tester",
			"newStack": true
        },
		{
			"id": "sdet",
			"fillColors": "#afaa58",
			"lineColor": "#afaa58",
			"color": "#ffffff",
			"title": "SDET",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "sdet",
			"newStack": false
        }];
	var graphs = [];
	var graph = {};
	for (var i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["id"] = graphsData[i]["id"];
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}


	graph = {
		"balloonText": ballonText,
		"id": "rollingGamePercent",
		"title": "Rolling Game Tester Average",
		"bullet": "square",
		"lineColor": "#6edbd5",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v3",
		"valueField": "rollingGamePercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingSDETPercent",
		"title": "Rolling SDET Average",
		"bullet": "square",
		"lineColor": "#abdb6e",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v3",
		"valueField": "rollingSDETPercent"
	};
	graphs.push(graph);

	return graphs;
}


function adjustDefectsFoundTTL2LabelText(graphDataItem, graph) {

	if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
		if (graphDataItem.values.value < 1)
			return "";
		else
			return (graphDataItem.values.percents).toFixed(2) + "%";
	} else {
		if (graphDataItem.values.value === 0)
			return "";
		else
			return graphDataItem.values.value;
	}
}


function adjustDefectsFoundTTL2Text(graphDataItem, graph) {

	if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
		return "<b style='font-size:14px;'>" + ((graphDataItem.graph).legendTextReal) + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + (graphDataItem.values.percents).toFixed(2) + "%</b>";
	} else
		return "<b style='font-size:14px;'>" + ((graphDataItem.graph).legendTextReal) + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + graphDataItem.values.value + "</b>";

}


function DefectsFoundTT_DrillDownToLevel3(prefix, month, subcategory, fpstatus) {
	currentMonth = month;
	currentStudio = subcategory;
	showHideDataTable(prefix + "_chart", "100%", false);

	$("#" + prefix + "_L2_chartdiv").hide();

	// hide show/hide all bars button
	$('#' + prefix + "_chart_link").hide();

	var L3_id = prefix + "_L3_chartdiv";
	currentPrefix = prefix + "_L3";
	addLevelChartDiv(prefix, L3_id);

	var returnData = GetDefectsFoundTTPerTheme(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
	var levelThreeConfig = generateConfig(prefix, "Defects Found by Tester Type - " + subcategory + " for " + month, "Defects", "theme", currentTheme, false); //DefectsFoundTTL3generateConfig(prefix, "Defects - " + subcategory + " for " + month, "Defects Found", "theme", currentTheme, false);

	levelThreeConfig.dataProvider = returnData.chart;

	levelThreeConfig.legend.enabled = true;
	levelThreeConfig.legend.marginTop = 10;
	levelThreeConfig.graphs = GetGraphsDefectsFoundTTL3();

	// Adjust Label Functions 
	for (var i = 0; i < 4; i++) {
		levelThreeConfig.graphs[i].balloonFunction = adjustDefectsFoundTTL2Text;
		levelThreeConfig.graphs[i].labelFunction = adjustDefectsFoundTTL2LabelText;
	}


	levelThreeConfig = setColumnSpacingFirstPass(levelThreeConfig);

	//levelTwoConfig.columnSpacing = -15;
	levelThreeConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
    },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Count",
			"autoGridCount": false
    }];


	levelThreeConfig.chartCursor = {
		valueBalloonsEnabled: false,
		/*categoryBalloonFunction: function (category) {
		    return themeLookUp[category];
		},*/
		fullWidth: true,
		cursorAlpha: 0.1,
		zoomable: false,
		pan: true
	};

	levelThreeConfig.categoryAxis.autoWrap = true;

	delete levelThreeConfig.chartScrollbar.graph;
	delete levelThreeConfig.chartScrollbar.graphType;
	levelThreeConfig.chartScrollbar.autoGridCount = false;
	levelThreeConfig.chartScrollbar.hideResizeGrips = true;
	levelThreeConfig.chartScrollbar.resizeEnabled = false;
	levelThreeConfig.zoomOutText = "";
	levelThreeConfig.mouseWheelZoomEnabled = false;
	levelThreeConfig.mouseWheelScrollEnabled = true;

	chartConfigDict["defectsClosedbyTesterType_L3"] = levelThreeConfig;
	var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
	chartResizerDict[currentPrefix] = true;

	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

	chart.addLabel(
		'!160', '20',
		"Enable Count",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:DefectsFoundTTL3EnableValues();');

	chart.addLabel(
		'!170', '34',
		"Sort By Defects",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortDefectsThemeL3ByDefectFoundTT("' + currentPrefix + '", true);');

	chart.addListener("init", DefectsFoundTTL3chartInit);
	chart.addListener("clickGraphItem", L3_handleItemClick);
	chart.addListener("rendered", L3_rendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = returnData;

	level++; // increase global level indicator

	resetCurrentHash();


	var dataTable;
	dataTable = returnData.chart; // GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsReleased"], month, subcategory, studio_alias);


	dataTableDataDict[prefix + "_dataTable"] = dataTable;

	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
		showHideDataTable(prefix + "_dataTable");
	//clearTable(prefix + "_dataTable");
	//drawTable(dataTable, prefix + "_dataTable");
}

function SortDefectsThemeL3ByDefectFoundTT(prefix, sortByDefects) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix].chart;
    var labelIndex;

    var switchText = "Sort By Defects";

    if (sortByDefects) {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.total_defects > b.total_defects) return -1;
            if (a.total_defects < b.total_defects) return 1;
            return 0;
        });

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
        chart.allLabels[labelIndex].text = "Sort By Theme Name";
        chart.allLabels[labelIndex].url = 'javascript:SortDefectsThemeL3ByDefectFoundTT("' + prefix + '", false);';
    } else {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.theme < b.theme) return -1;
            if (a.theme > b.theme) return 1;
            return 0;
        });
        labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort By Theme Name");
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortDefectsThemeL3ByDefectFoundTT("' + prefix + '", true);';
    }
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}

function DefectsFoundTTL3EnableValues() {
	//hide rolling charts
    var chart = chartDict["defectsClosedbyTesterType_L3"];

	chart.showGraph(chart.getGraphById("game_tester"));
	chart.showGraph(chart.getGraphById("sdet"));
}




function DefectsFoundTTL3chartInit(event) {
	chart = event.chart;

	//hide value charts
	chart.hideGraph(chart.getGraphById("game_tester"));
	chart.hideGraph(chart.getGraphById("sdet"));

}


function GetGraphsDefectsFoundTTL3() {
	var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	var graphsData = [
		{
			"id": "game_percent",
			"fillColors": "#05668D",
			"lineColor": "#05668D",
			"color": "#ffffff",
			"title": "Game Tester %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "game_percent",
			"newStack": true
        },
		{
			"id": "sdet_percent",
			"fillColors": "#DBD56E",
			"lineColor": "#DBD56E",
			"color": "#ffffff",
			"title": "SDET %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "sdet_percent",
			"newStack": false
        },
		{
			"id": "game_tester",
			"fillColors": "#045170",
			"lineColor": "#045170",
			"color": "#ffffff",
			"title": "Game Tester",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "game_tester",
			"newStack": true
        },
		{
			"id": "sdet",
			"fillColors": "#afaa58",
			"lineColor": "#afaa58",
			"color": "#ffffff",
			"title": "SDET",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "sdet",
			"newStack": false
        }];
	var graphs = [];
	var graph = {};
	for (var i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["id"] = graphsData[i]["id"];
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}

	return graphs;
}


function GetDefectsFoundTTPerTheme(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
	var month;
	var tempArray;
	var tempInt;
	var studio;
	var cat;
	var dataStudio;
	var dataTheme;
	var dataThemeKey;
	var dataKit;
	var testerType;
	var datarow;
	var row;
	var isValidKit;
	var chartTable = [];
	var themeDict = {};
	var themeLookUp = {}; // for the cursor category balloon
	var tempTheme = "";
	var compareVal;

	if (monthname == "Current Month")
		month = 12;
	else month = getMonthFromString(monthname);


	var defectTable = jsonTable[defect_table_name + month.toString()];

	studio = sub_category;

	var kitChartData = {};


	for (var dataindex in defectTable) {
		if (defectTable.hasOwnProperty(dataindex)) {
			datarow = defectTable[dataindex];
			dataStudio = datarow["Studio"];
			dataTheme = datarow["KitTheme"];
			dataKit = datarow["Kit"];
			dataThemeKey = datarow["SAP_Theme_ID"];
			testerType = datarow["TesterType"];

			compareVal = dataStudio;

			if (studio_alias) {
				if (studio_alias.hasOwnProperty(compareVal))
					compareVal = studio_alias[compareVal];
			}

			isValidKit = false;
			if (studio === "Total" || studio === compareVal) {
				isValidKit = true;
			}

			if (isValidKit) {

				if (kitChartData[dataKit] === undefined) {
					kitChartData[dataKit] = {
						"studio": compareVal,
						"sdet": 0,
						"game_tester": 0,
						"sdet_percent": 0,
						"earlyTesting_percent": 0
					};
				}

				if ((kitChartData[dataKit])["theme"] === undefined) {
					if (dataTheme.length >= 27) {
						tempTheme = dataTheme.substring(0, 26);
						if (themeDict.hasOwnProperty(tempTheme))
							themeDict[tempTheme] = themeDict[tempTheme] + 1;
						else themeDict[tempTheme] = 1;
						(kitChartData[dataKit])["theme"] = tempTheme + "_" + themeDict[tempTheme]; // max out                     
					} else {
						(kitChartData[dataKit])["theme"] = dataTheme;
					}

					themeLookUp[(kitChartData[dataKit])["theme"]] = dataTheme;
					(kitChartData[dataKit])["fulltheme"] = dataTheme;
					(kitChartData[dataKit])["themekey"] = dataThemeKey;
					(kitChartData[dataKit])["month"] = month;

				}

				if (testerType === "SDET") {
					(kitChartData[dataKit])["sdet"] = (kitChartData[dataKit])["sdet"] + parseInt(datarow["Count"]);
				} else if (testerType === "Game Tester") {
					(kitChartData[dataKit])["game_tester"] = (kitChartData[dataKit])["game_tester"] + parseInt(datarow["Count"]);
				}
			}
		}
	}

	savedDataDefectsFoundTester["defectsClosedbyTesterType_L3"] = kitChartData;

	var kits;

	for (kits in kitChartData) {
		row = [];

		row["theme"] = (kitChartData[kits])["theme"];
		//themeLookUp[row["theme"]] = row["theme"];
		row["kit"] = kits;
		row["sdet"] = ((kitChartData[kits])["sdet"]);
		row["game_tester"] = ((kitChartData[kits])["game_tester"]);

		var tot = (kitChartData[kits])["sdet"] + (kitChartData[kits])["game_tester"];
		row["sdet_percent"] = (((kitChartData[kits])["sdet"] / tot) * 100).toFixed(2);
		row["game_percent"] = (((kitChartData[kits])["game_tester"] / tot) * 100).toFixed(2);

		row["studio"] = (kitChartData[kits])["studio"];
		row["total_defects"] = tot;
		row["fulltheme"] = (kitChartData[kits])["fulltheme"];
		row["themekey"] = (kitChartData[kits])["themekey"];
		row["month"] = (kitChartData[kits])["month"];
		chartTable.push(row);
	}

	chartTable.sort(function (a, b) {
		if (a.theme < b.theme) return -1;
		if (a.theme > b.theme) return 1;
		return 0;
	});

	return {
		chart: chartTable,
		themes: themeLookUp
	};
}


function DefectsFoundTTL3generateConfig(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
	var config = {
		"type": "serial",
		"theme": theme,
		"categoryField": catField,
		"mouseWheelZoomEnabled": true,
		"mouseWheelScrollEnabled": true,
		"categoryAxis": {
			"gridPosition": "start",
			"labelOffset": -2
		},
		"valueAxes": [
			{
				"id": "v1",
				"title": yAxisTitle
            }
        ],
		"legend": {
			"enabled": true,
			"useGraphSettings": true
		},
		"titles": [
			{
				"id": chartId,
				"size": 15,
				"text": chartTitle
            }
        ],
		"chartCursor": {
			valueBalloonsEnabled: false,
			fullWidth: true,
			cursorAlpha: 0.1,
			zoomable: true,
			pan: false
		},
		"chartScrollbar": {
			"graph": "totalline",
			"graphType": "line",
			"color": "#FFFFFF",
			"selectedBackgroundColor": "#6F7D8A",
			"selectedBackgroundAlpha": "0.75",
			"selectedGraphLineColor": "#FFFFFF",
			"autoGridCount": true
		},
		"export": {
			"enabled": true
		},
		"responsive": {
			"enabled": true,
			"rules": [
				{
					"maxWidth": 1000,
					"overrides": {
						"legend": {
							"enabled": false
						}
					}
                },
				{
					"maxWidth": 500,
					"overrides": {
						"valueAxes": {
							"inside": true
						}
					}
                }
            ]
		}
	};

	config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
	config.export["backgroundColor"] = "#222222";
	config.export.drawing.enabled = true;
	config.export.drawing.color = "#FFFFFF";

	config.export.menu[0].menu.push({
		"label": "Get URL To Chart",
		"click": GetUrlForCurrentChart
	});

	if (useStackCharts) {
		config.valueAxes[0].stackType = "regular";
	}

	return config;
}