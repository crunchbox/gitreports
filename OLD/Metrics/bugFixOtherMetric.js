function get_BugFixOtherGameMetrics() {

	if (loadedMetrics["kitdefects"] != true) {
		get_MetricsData("kits", "bugFixOther");
		return;
	}

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Bug Fix / Other");

	clearChart("bugFixOther");
	showHideDataTable("bugFixOther_dataTable", "100%", false);

	chartData = parseMetricData(restJsonDict, "PAReleases_Month", studio_alias, "bugFixOther");
	chartGraphs = GetStackedGraphs(chartData, "month", "Other", "Bug Fix");

	dataTableDataDict["bugFixOther_dataTable"] = chartData;
	//drawTable(chartData, "bugFixOther_dataTable");

	var bugFixOtherConfig = generateConfig("bugFixOther", "Kits Released Out Of PA - Bug Fix / Other", "Kits", "month", currentTheme, true);
	bugFixOtherConfig.dataProvider = chartData;
	bugFixOtherConfig.graphs = chartGraphs;

	chartConfigDict["bugFixOther"] = bugFixOtherConfig;
	chart = AmCharts.makeChart("bugFixOther_chartdiv", bugFixOtherConfig, 100);
	chartResizerDict["bugFixOther"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDataDict["bugFixOther"] = chartData;
	chartDict["bugFixOther"] = chart; // store chart
}

function parseByBugFixOtherToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempString;
	var tempStudio;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category.split('/')[0];
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (use_sub_category == false || targetStudio == "Total") {
				if (datarow["Category"] == 'Bug Fix') {
					if (row.hasOwnProperty("Total/Bug Fix"))
						tempInt = row["Total/Bug Fix"];
					else
						tempInt = 0;
					row["Total/Bug Fix"] = tempInt + 1;
				} else {
					if (row.hasOwnProperty("Total/Other"))
						tempInt = row["Total/Other"];
					else
						tempInt = 0;
					row["Total/Other"] = tempInt + 1;
				}
			}

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (datarow["Category"] == "Bug Fix") {
					tempString = "Bug Fix";
				} else {
					tempString = "Other";
				}

				if (row.hasOwnProperty(tempStudio + "/Other") == false)
					row[tempStudio + "/Other"] = 0;
				if (row.hasOwnProperty(tempStudio + "/Bug Fix") == false)
					row[tempStudio + "/Bug Fix"] = 0;

				tempString = tempStudio + '/' + tempString;

				if (row.hasOwnProperty(tempString))
					tempInt = row[tempString];
				else tempInt = 0;
				row[tempString] = tempInt + 1;
			}
		}
	}

	return row;
}