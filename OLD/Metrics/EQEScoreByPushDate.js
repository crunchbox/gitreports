function get_EQEScoreByPushDateMetrics() {
    if (loadedMetrics["EQEScoreByPushDate"] !== true) {
        get_EQEScoreData("EQEScoreByPushDate");
        return;
    }

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', 'EQE Score by Push Date');

    clearChart("EQEScoreByPushDate");
    showHideDataTable("EQEScoreByPushDate", "100%", false);

    chartData = parseEQEScoreData(restJsonDict, "EQEScoreByPushDate", "EQEScoreByPushDate");
    chartGraphs = GetEQEL1Graphs(chartData, "month");
    for (var i = 0; i < chartGraphs.length; i++) {
        chartGraphs[i]["balloonText"] = "[[title]] Avg EQ-Score: [[value]]";
        chartGraphs[i]["balloonFunction"] = function (item, graph) {
            var studio = item.graph.title;
            var eqe = item.values.value;
            var count = item.dataContext[studio + "_EQEcount"];
            return studio + " Avg EQ-Score: " + eqe + "<br>" + count + " EQE Projects";
        }
    }

    dataTableDataDict["EQEScoreByPushDate_dataTable"] = chartData;

    var config = generateConfig("EQEScoreByPushDate", "Monthly Avg EQ-Score by Studio", "Avg EQ-Score", "month", currentTheme, false);
    config.dataProvider = chartData;
    config.graphs = chartGraphs;

    chartConfigDict["EQEScoreByPushDate"] = config;
    chart = AmCharts.makeChart("EQEScoreByPushDate_chartdiv", config, 100);
    chartResizerDict["EQEScoreByPushDate"] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		14,
		undefined,
		undefined,
		undefined,
		true,
		'/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict["EQEScoreByPushDate"] = chartData;
    chartDict["EQEScoreByPushDate"] = chart; // store chart
}

function get_EQEScoreData(chart_type) {
    if(loadedMetrics[chart_type]) {
        get_MetricsData_callBack(restJsonDict);
    } else {
        var date = new Date();
        var month = (date.getMonth() + 1);
        if (month < 10)
            month = "0" + month;

        var day = date.getDate();
        if (day < 10)
            day = "0" + day;

        var startdate = (date.getFullYear() - 1) + month + day;
        var enddate = (date.getFullYear()) + month + day;

        show_loadingDiv("Loading Data...");

        $.ajax({
            url: '/rest/EQEScoreByPushDate/',
            dataType: 'json',
            data: "startDate=" + startdate + "&endDate=" + enddate,
            success: function (result) {
                get_EQEScoreData_callBack(result["EQEScore"]);
            },
            error: function (request, textStatus, errorThrown) {
                alert(textStatus);
            }
        });     
    }
}

function get_EQEScoreData_callBack(r) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("EQEScoreByPushDate") == false) {
            restJsonDict["EQEScoreByPushDate"] = r;
        }

        loadedMetrics["EQEScoreByPushDate"] = true;
        get_EQEScoreByPushDateMetrics();

    } else {
        alert("No Data From Server");
    }
}

function parseEQEScoreData(restJsonDict, data, metric) {
    var results = _.groupBy(restJsonDict[data],
        function (item) {
            return item.YYYYMM;
        });

    var EQEscores = [];
    var total = 0;
    var months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

    var firstMonth = new Date().getMonth();
    var adjustedMonths = months.slice(firstMonth, months.length).concat(months.slice(0, firstMonth));
    adjustedMonths.push(months[firstMonth]);

    for (i = 0; i < 13; i++) {
        var dataRow = {};
        dataRow["month"] = adjustedMonths[i];
        var dataRowYear = getYear(adjustedMonths, i);

        for (var month in results) {
            var dataMonth = months[(results[month][0].YYYYMM).slice(4, 6) - 1];
            var dataYear = results[month][0].YYYYMM.slice(0, 4);

            if (dataMonth == adjustedMonths[i] && dataYear == dataRowYear) {
                for (j = 0; j < results[month].length; j++) {
                    var studio = results[month][j].Studio;
                    dataRow[results[month][j].Studio] = results[month][j].AvgEQE;
                    dataRow[studio + "_EQEcount"] = results[month][j].EQE_Count;
                }
            }
        }
        EQEscores.push(dataRow);
    }
    // change last entry to current Month?
    EQEscores[12].month = "Current Month";

    return EQEscores;
}

function getYear(monthsArray, month) {
    var currentYear = new Date().getFullYear();
    var lastYear = currentYear - 1;

    var pivotIndex = monthsArray.indexOf("January");

    if (month < pivotIndex)
        return lastYear;
    else
        return currentYear;

}

function EQEScoreByPushDate_DrillDownToLevel2(prefix, itemSelection) {
    
    currentMonth = "all";
    currentStudio = itemSelection;

    // turn off show/hide all bars button;
    $('#' + prefix + "_chart_link").hide();

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");

    showHideDataTable(prefix, "100%", false);

    var data = parseEQEScoreData_L2(chartDataDict, chartJSONTableDict[prefix], studio_alias, prefix, itemSelection);

    var graphs = GetEQEL1Graphs(data, "month");
    graphs.push(GetRollingGraph("rollingAvg", "Rolling Average", "#33FFBD"));
    graphs[0]["labelText"] = "[[value]]";
    graphs[0]["labelPosition"] = "middle";
    graphs[0]["balloonText"] = "Avg EQ-Score: [[value]]";
    graphs[0]["balloonFunction"] = function (item, graph) {
        var count = item.dataContext["count"];
        return "Avg EQ-Score: " + item.values.value + "<br>Count: " + count;
    };
    graphs[1]["hidden"] = true;

    var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title
    var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
    var chartTitle_firstpart = chartTitle.split(' - ')[0];
    
    var levelTwoConfig = generateConfig(prefix, "Monthly Avg EQ-Score" + " - " + itemSelection, yAxisTitle, "month", currentTheme, false);

    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = graphs;
    levelTwoConfig.valueAxes = [
		{
		    "id": "v1",
		    "title": yAxisTitle,
            "minimum": 0
		},
		{
		    "id": "v2",
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false,
		    "position": "right",
		    "autoGridCount": false,
		    "synchronizeWith": "v1",
		    "synchronizationMultiplier": 1
		}];

    $("#" + prefix + "_chartdiv").hide();

    var L2_id = prefix + "_L2_chartdiv";

    currentPrefix = prefix + "_L2";

    addLevelChartDiv(prefix, L2_id);

    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;

    // let's add a label to go back to yearly data
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    // rolling average function for level 2

    //total_Label = "Enable Trend";
    //total_link = 'javascript:getRollingAverageTestTimeEQEL2();';

    //chart.addLabel(
	//	'!150', '34',
	//	total_Label,
	//	undefined,
	//	12,
	//	undefined,
	//	undefined,
	//	undefined,
	//	true,
	//	total_link);

    chart.addListener("clickGraphItem", L2_handleItemClick);

    chart.addListener("init", chartInit);
    chart.addListener("rendered", L2_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    dataTableDataDict[prefix + "_dataTable"] = data;

    removeLoadingDiv();
}

function parseEQEScoreData_L2(jsonTable, table_name, studio_alias, prefix, itemSelection) {
    var chartTable = [];
    var currentFiscal;
    var viewTable;
    var row;

    var month;
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    var parsedData = [];
    for (month = 0; month < 13; month++) {
        row = {};
        viewTable = jsonTable[table_name][month];

        row["month"] = jsonTable[table_name][month].month;

        if (jsonTable[table_name][month][itemSelection]) {
            row["EQEScore"] = jsonTable[table_name][month][itemSelection];
            row["count"] = jsonTable[table_name][month][itemSelection + "_EQEcount"];
        } else {
            row["EQEScore"] = 0;
        }
        parsedData.push(row);
    }

    return parsedData;
}

function EQEScoreByPushDate_DrillDownToLevel3(prefix, studio) {
    var data = GetEQEScoreDataL3();
    if (data != undefined) {

        currentStudio = studio;
        showHideDataTable(prefix + "_chart", "100%", false);

        // hide show/hide all bars button
        $('#' + prefix + "_chart_link").hide();

        $("#" + prefix + "_L2_chartdiv").hide();

        var L3_id = prefix + "_L3_chartdiv";
        currentPrefix = prefix + "_L3";
        addLevelChartDiv(prefix, L3_id);

        var levelThreeConfig = generateConfig(currentPrefix, "EQE Projects - " + studio, "EQ-Score", "KitNumber", currentTheme, false);

        // sort data
        data = data.sort(function(a, b) {
            return (b.FIFO - a.FIFO);
        });

        levelThreeConfig.valueAxes[0].minimum = 0;
        levelThreeConfig.dataProvider = data;
        levelThreeConfig.legend.enabled = true;
        levelThreeConfig.legend.marginTop = 10;
        levelThreeConfig.graphs = [{
            "balloonFunction": function (item, graph) {
                var year = item.dataContext.FIFO.slice(0, 4);
                var month = item.dataContext.FIFO.slice(4, 6);
                var day = item.dataContext.FIFO.slice(6, 8);
                var kit = item.dataContext.KitNumber;
                var score = item.dataContext.EQE;
                return "EQ-Score: " + score + "<br>EQE Date: " + month + "/" + day + "/" + year;
            },
            "fillAlphas": 1,
            "fillColors": "#de4c4f",
            "id": "AmGraph_0",
            "lineColor": "#de4c4f",
            "title": "EQ-Score",
            "type": "column",
            "valueField": "EQE",
            "labelText": "[[value]]",
            "labelPosition": "middle"
        }];
        levelThreeConfig.chartCursor = {
            valueBalloonsEnabled: false,
            categoryBalloonFunction: function (category) {
                return category;
            },
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: false,
            pan: true
        };
        levelThreeConfig.categoryAxis.autoWrap = true;

        levelThreeConfig.chartScrollbar.autoGridCount = false;
        levelThreeConfig.chartScrollbar.hideResizeGrips = true;
        levelThreeConfig.chartScrollbar.resizeEnabled = false;
        levelThreeConfig.zoomOutText = "";
        levelThreeConfig.mouseWheelZoomEnabled = false;
        levelThreeConfig.mouseWheelScrollEnabled = true;

        var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
        chartResizerDict[currentPrefix] = true;

        chart.addLabel(
            35, 20,
            "< Go back",
            undefined,
            15,
            undefined,
            undefined,
            undefined,
            true,
            'javascript:GoUpLevel("' + prefix + '");');

        chart.addListener("init", chartInit);
        chart.addListener("clickGraphItem", L3_EQEhandleItemClick);
        chart.addListener("rendered", L3_rendered);

        chartDict[currentPrefix] = chart;
        chartDataDict[currentPrefix] = data;

        level++; // increase global level indicator

        dataTableDataDict[prefix + "_dataTable"] = data;

        if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
            showHideDataTable(prefix + "_dataTable");
    }
}

function L3_EQEhandleItemClick(event) {
    show_loadingDiv("Loading Data...");

    var prefix = event.chart.titles[0].id;

    var dataItem = event.item.dataContext;
    //var targetTheme = dataItem.theme;
    var targetKit = dataItem.KitNumber;
    var month = dataItem.month;
    //var defectCount = dataItem.value;

    currentPrefix = prefix;
    level = 4;
    showAndLoadScoreCard(currentPrefix, targetKit, month);
}

function GetEQEScoreDataL3() {
    if (loadedMetrics["EQEScoreL3"] !== true) {
        get_EQEScoreDataL3("EQEScoreL3", currentStudio);
        return;
    }

    return restJsonDict["EQEScoreL3"];
}

function get_EQEScoreDataL3(chart_type, studio) {
    if (loadedMetrics[chart_type]) {
        return loadedMetrics[chart_type];
    } else {
        var date = new Date();
        var month = (date.getMonth() + 1);
        if (month < 10)
            month = "0" + month;

        var day = date.getDate();
        if (day < 10)
            day = "0" + day;

        var startdate = (date.getFullYear() - 1) + month + day;
        var enddate = (date.getFullYear()) + month + day;

        show_loadingDiv("Loading Data...");

        $.ajax({
            url: '/rest/EQEScoreL3/',
            dataType: 'json',
            data: "startDate=" + startdate + "&endDate=" + enddate + "&studio=" + studio,
            success: function (result) {
                get_EQEScoreDataL3_callBack(result["EQEScoreL3_" + studio]);
            },
            error: function (request, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }
}

function get_EQEScoreDataL3_callBack(r) {
    if (r != null && r != undefined) {
        if (restJsonDict.hasOwnProperty("EQEScoreL3") == false) {
            restJsonDict["EQEScoreL3"] = r;
        }

        loadedMetrics["EQEScoreL3"] = true;
        EQEScoreByPushDate_DrillDownToLevel3("EQEScoreByPushDate", currentStudio);

    } else {
        alert("No Data From Server");
    }
}

function getRollingAverageTestTimeEQEL2() {
    var chart = chartDict["EQEScoreByPushDate_L2"];

    //clear Graphs
    for (i = 0; i < 13; i++) {

        if ((chart.dataProvider[i])["rollingAvg"] !== undefined) {
            delete (chart.dataProvider[i])["rollingAvg"];
            delete (chart.dataProvider[i])["rollingAvg"];
        }
    }

    var start = chart.start;
    var end = chart.end;
    var avg = 0;
    var count = 0;

    for (i = start; i <= end; i++) {
        if (chart.dataProvider[i].hasOwnProperty('EQEScore')) {
            var score = (chart.dataProvider[i][Object.keys(chart.dataProvider[i])[1]])
            avg += score;
            if (score != 0) {
                count++;
            }
        }

        (chart.dataProvider[i])["rollingAvg"] = Math.round((avg / (count)) * 100) / 100;
    }

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAvg"));
    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingAvg"));

    chart.invalidateSize();
    chart.validateNow();
}

function GetEQEL1Graphs(data, categoryField) {
    var graphs = [];
    var ballonText = "[[title]] of [[category]]:[[value]]";
    var max_len_index = getLongestRowIndex(data);

    var colors = ["#de4c4f", "#eea638", "#69c8ff", "#a7a737", "#86a965", "#8aabb0", "#d8854f", "#cfd27e",
                  "#9d9888", "#916b8a", "#724887", "#7256bc", // end defaults 
                  "#FF0000", "#008000", "#FFC0CB", "#FFA500", "#FFF0F5", "#FFFF00", "#0000FF", "#CD853F",
                  "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#FFFFE0", "#ADD8E6", "#D2B48C",
			      "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#EEE8AA", "#00BFFF", "#FFDEAD",
			      "#8FBC8B", "#00FF7F", "#FFF0F5", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
			      "#40E0D0", "#66CDAA"];
    /*
	var rowData = data[max_len_index]; // get row with the most cols    
	var groups = [];
    
	for (var heading in rowData) {
	    if (rowData.hasOwnProperty(heading)) {
	        if (heading != categoryField) {
	            groups.push(heading);
	        }
	    }
	}
	*/

    var groups = [];
    for (var i = 0; i < data.length; i++) {
        rowData = data[i];
        for (var heading in rowData) {
            if (rowData.hasOwnProperty(heading)) {
                if (heading != categoryField && !heading.includes("count")) {

                    if (groups.indexOf(heading) == -1) {
                        groups.push(heading);
                    }
                }
            }
        }
    }


    var stacks = {};
    var cat;
    var totalline_col;

    for (var index in groups) {
        if (groups.hasOwnProperty(index)) {
            var column = {};
            var key = groups[index];
            if (key == "Total") {
                column["balloonText"] = ballonText;
                column["id"] = "totalline";
                column["title"] = "Total";
                column["bullet"] = "round";
                column["lineThickness"] = 3;
                column["bulletSize"] = 7;
                column["bulletBorderAlpha"] = 1;
                column["connect"] = true;
                column["useLineColorForBulletBorder"] = true;
                column["bulletBorderThickness"] = 3;
                column["fillAlphas"] = 0;
                column["lineAlpha"] = 1;
                column["valueField"] = key;
                graphs.push(column);

                column = {};
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["type"] = "column";
                column["valueField"] = key;
                graphs.push(column);
            } else {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["type"] = "column";
                column["valueField"] = key;

                graphs.push(column);
            }

        }
    }
    // set fill colors of bars/columns
    for (i = 0; i < graphs.length; i++) {
        graphs[i].fillColors = colors[i];
        graphs[i].lineColor = colors[i];
    }

    return graphs;
}