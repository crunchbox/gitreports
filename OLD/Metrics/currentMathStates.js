function get_MathStatesGrid() {
    var metricName = "currentMathStates"; 
    if (loadedMetrics[metricName] != true) {
        get_MetricsData(metricName, metricName);
        return;
    }

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Current Math States");

    var dataTableID = metricName+"_dataTable";

    dataTableDataDict[dataTableID] = restJsonDict[chartJSONTableDict[metricName]];
    dataTableOptionsDict[dataTableID] = {
        "scrollY": "80vh",
        "scrollX": true,
        "scrollCollapse": true,
        "pageLength": 15,
        "paging": true,
        "dom": 'frtBp',
        "buttons": ['copyHtml5', 'csvHtml5'],
        "responsive": true,
        "columnDefs": [{ responsivePriority: 1, targets: 6 },
                       { responsivePriority: 2, targets: 0 },
		               { responsivePriority: 3, targets: 1 },
		               { responsivePriority: 4, targets: 2 },
		               { responsivePriority: 5, targets: 3 },
		               { responsivePriority: 6, targets: 4 },
		               { responsivePriority: 7, targets: 5 }],
        "order": [[6, "desc"]]
    };

    dataTableIsVisible[dataTableID] = true; // set visible to opposite of show so the below code toggles it

    var classList = "row-border stripe cell-border compact hover";

    currentPrefix = metricName;
    currentMonth = "all";

    if ($("#" + currentPrefix + "_Title").length == 0) {
        var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('searchMetricTitle');

        title_div.html("Search Kits in Test - " + dataTableDataDict[dataTableID].length + " Kits Total");
        var goBackLink = "<div style='padding-bottom:20px;'><a href='/' style='font-size:14px; color:#fff; font-weight:700;'>< Go back</a></div>";
        $("#metric_currentMathStates_header").prepend(goBackLink);
        $("#metric_currentMathStates_header").prepend(title_div);

    }


    var sortedMonths = getSortedMonthList();

    var index;
    var monNum = 1;
    for (index = sortedMonths.length - 1; index >= 0; index--) {
        $("#currentMathStates_MonthDropDown").append($('<option>', {
            value: monNum
        }).text(sortedMonths[index]));
        monNum++;
    }

    var studios = getStudioAliasList(studio_alias);
    for (index = 0; index < studios.length; index++) {
        $("#currentMathStates_StudioDropDown").append($('<option>', {
            value: studios[index]
        }).text(studios[index]));
    }


    var table = drawTable(dataTableDataDict[dataTableID], dataTableID, "100%", dataTableOptionsDict[dataTableID], classList);
    dataTableHTMLTableDict[metricName] = table;

    $('#currentMathStates_dataTable_filter label').css("width", "100%");
    $('#currentMathStates_dataTable_filter label input').css("width", "90%");
    $('#currentMathStates_dataTable_filter').css("width", "inherit");
    $('#currentMathStates_dataTable_filter').css("margin", "auto");
    $('#currentMathStates_dataTable_filter').css("float", "none");

    /*
    table.$("td:not(:first-child)").click(function () {
        var data = dataTableHTMLTableDict[metricName].row($(this).closest('tr')).data();
        $.fn.dataTable.ext.search = [];
        showAndLoadScoreCard(currentPrefix, data[0], currentMonth);
    });*/

    table.$("td:not(:first-child)").css('cursor', 'pointer');

    $("#currentMathStates_StudioDropDown_Div").css("visibility", "visible");
    $("#currentMathStates_MonthDropDown_Div").css("visibility", "visible");

    $.fn.dataTable.ext.search = [];
    $.fn.dataTable.ext.search.push(currentMathStates_emptyFilter);
    $.fn.dataTable.ext.search.push(currentMathStates_emptyFilter);

    $("#currentMathStates_StudioDropDown").change(currentMathStates_UpdateStudioList);
    $("#currentMathStates_MonthDropDown").change(currentMathStates_UpdateMonthList);

    removeLoadingDiv();

}



var currentMathStates_currentMonthFilter = 0;
var currentMathStates_currentStudioFilter = "All Studios";

function currentMathStates_UpdateStudioList() {
    var selectedValue = jQuery(this).val();
    currentMathStates_currentStudioFilter = selectedValue;

    if (selectedValue != "All Studios") {
        $.fn.dataTable.ext.search[0] = currentMathStates_studioBaseFilter;
    } else $.fn.dataTable.ext.search[0] = currentMathStates_emptyFilter;
    dataTableHTMLTableDict["currentMathStates"].draw();
}

function currentMathStates_UpdateMonthList() {
    var selectedValue = jQuery(this).val();
    currentMathStates_currentMonthFilter = selectedValue;

    if (selectedValue > 0) {
        $.fn.dataTable.ext.search[1] = currentMathStates_dateBaseFilter;
    } else $.fn.dataTable.ext.search[1] = currentMathStates_emptyFilter;
    dataTableHTMLTableDict["currentMathStates"].draw();
}

function currentMathStates_dateBaseFilter(settings, data, dataIndex) {
    var date = ISODateParse(data[5]);
    var targetDate = new Date();
    targetDate.setMonth(targetDate.getMonth() - (currentMathStates_currentMonthFilter - 1));
    if (date.getMonth() == targetDate.getMonth() && date.getYear() == targetDate.getYear())
        return true;
    return false;
}

function currentMathStates_studioBaseFilter(settings, data, dataIndex) {
    if (data[4] == currentMathStates_currentStudioFilter)
        return true;
    return false;
}

function currentMathStates_emptyFilter(settings, data, dataIndex) {
    return true;
}