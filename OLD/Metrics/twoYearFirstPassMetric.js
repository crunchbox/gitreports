function GetFirstPassNewThemePercentAndTotal_Q(jsonTable, firstpass_table_name, sub_category, studio_alias) {
    var kitTable;
    var fpTable;
    var chartTable = [];
    var currentFiscal;
    var row;
    var dataStudio;
    var studio = sub_category;
    var thisYear = new Date().getFullYear();
    var rotated_quarters = getQuarterListObject();

    var twelveMonthApprovedRollingTotal = 0;
    var yearToDateApprovedRollingTotal;

    for (quarter = 0; quarter < 8; quarter++) {
        row = {};

        row["quarter"] = rotated_quarters[quarter]["quarter"];
        if (rotated_quarters[quarter]["quarter"] == "Q1 " + thisYear)
            yearToDateApprovedRollingTotal = 0;

        row["approved"] = 0;
        row["rejected"] = 0;
        row["pending"] = 0;
        row["na"] = 0;
        row["approved_percent"] = 0;
        row["rejected_percent"] = 0;
        row["pending_percent"] = 0;
        row["na_percent"] = 0;
        row["studio"] = studio;

        fpTable = jsonTable[firstpass_table_name + rotated_quarters[quarter]["num"]];

        for (var dataindex in fpTable) {
            if (fpTable.hasOwnProperty(dataindex)) {
                datarow = fpTable[dataindex];
                dataStudio = datarow["Studio"];

                if (studio_alias) {
                    if (studio_alias.hasOwnProperty(dataStudio))
                        dataStudio = studio_alias[dataStudio];
                }

                if (studio == "Total" || studio == dataStudio) {
                    if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
                        if (datarow["FP_Status"] == "Approved") {
                            row["approved"] += 1;
                            twelveMonthApprovedRollingTotal += 1;
                            if (yearToDateApprovedRollingTotal != undefined)
                                yearToDateApprovedRollingTotal += 1;
                        } else if (datarow["FP_Status"] == "Rejected") {
                            row["rejected"] += 1;
                        } else if (datarow["FP_Status"] == "Pending") {
                            row["pending"] += 1;
                        } else if (datarow["FP_Status"] == "Not Applicable") {
                            row["na"] += 1;
                        }
                    }
                }
            }
        }

        var total = row["approved"] + row["rejected"] + row["pending"] + row["na"];
        if (total > 0) {
            row["approved_percent"] = ((row["approved"] / total) * 100).toFixed(2);
            row["rejected_percent"] = ((row["rejected"] / total) * 100).toFixed(2);
            row["pending_percent"] = ((row["pending"] / total) * 100).toFixed(2);
            row["na_percent"] = ((row["na"] / total) * 100).toFixed(2);
        }
        row["rollingTotal"] = twelveMonthApprovedRollingTotal;
        if (yearToDateApprovedRollingTotal != undefined)
            row["yearToDateRollingDate"] = yearToDateApprovedRollingTotal;

        chartTable.push(row);
    }

    return chartTable;
}

function GetFirstPassTwoYearKitListTable(prefix, jsonTable, firstpass_table_name, quartername, sub_category, fpstatus, studio_alias) {
    var quarter;
    var row;
    var fpTable;
    var dataStudio;
    var studio = sub_category;
    var kitListTable = [];
    var kitListRow;
    var fpStatusValue;
    if (fpstatus.endsWith("_percent"))
        fpStatusValue = fpstatus.substr(0, fpstatus.indexOf('_')).toLowerCase();
    else fpStatusValue = fpstatus.toLowerCase();

    if (quartername == "Current Month")
        quarter = 8;
    else quarter = getQuarterFromString(quartername);

    fpTable = jsonTable[firstpass_table_name + quarter];

    for (var dataindex in fpTable) {
        if (fpTable.hasOwnProperty(dataindex)) {
            datarow = fpTable[dataindex];
            dataStudio = datarow["Studio"];
            dataFPStatus = datarow["FP_Status"].toLowerCase();
            if (dataFPStatus == "not applicable")
                dataFPStatus = "na";

            if (studio_alias) {
                if (studio_alias.hasOwnProperty(dataStudio))
                    dataStudio = studio_alias[dataStudio];
            }

            if (studio == "Total" || studio == dataStudio) {
                if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
                    if (dataFPStatus == fpStatusValue) {
                        kitListRow = {};
                        kitListRow["Logo"] = "";
                        kitListRow["KitNumber"] = datarow["KitNumber"];
                        kitListRow["FP Status"] = datarow["FP_Status"];
                        kitListRow["OutOfPA"] = datarow["OutOfPA"];
                        kitListRow["Category"] = datarow["Category"];
                        kitListRow["Studio"] = datarow["Studio"];
                        kitListRow["Theme"] = datarow["KitTheme"];
                        kitListTable.push(kitListRow);
                    }
                }
            }
        }
    }

    return kitListTable;
}

function get_TwoYearFirstPassMetrics() {
    if (loadedMetrics["kitfpQ"] != true && ((loadedMetrics["kits"] != true && loadedMetrics["kitdefects"] != true) || loadedMetrics["fp_Q"] != true)) {
        if (loadedMetrics["fp_Q"])
            get_MetricsData("kits", "QuartersFirstPass", true);
        else if (loadedMetrics["kits"] || loadedMetrics["kitdefects"])
            get_MetricsData("fp_Q", "QuartersFirstPass", true);
        else get_MetricsData("kitfpQ", "QuartersFirstPass", true);
        return;
    }

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Two Year First Pass");

    clearChart("QuartersFirstPass");
    showHideDataTable("QuartersFirstPass_datatable", "100%", false);

    var firstPassConfig;
    if (chartConfigDict["QuartersFirstPass"] == null) {
        chartData = parseMetricDataByQuarter(restJsonDict, "FirstPass_Quarter", studio_alias, "QuartersFirstPass");
        chartGraphs = GetGraphs(chartData, "quarter");

        dataTableDataDict["QuartersFirstPass_dataTable"] = chartData;
        firstPassConfig = generateConfig("QuartersFirstPass", "First Pass - New Themes", "Kits", "quarter", currentTheme, false);
        firstPassConfig.dataProvider = chartData;
        // chartGraphs.balloonFunction = adjustNewthemeFirstPassL2Text;
        firstPassConfig.graphs = chartGraphs;

        chartConfigDict["QuartersFirstPass"] = firstPassConfig;
    } else firstPassConfig = chartConfigDict["QuartersFirstPass"];

    chart = AmCharts.makeChart("QuartersFirstPass_chartdiv", firstPassConfig, 100);
    chartResizerDict["QuartersFirstPass"] = true;

    chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict["QuartersFirstPass"] = chartData;
    chartDict["QuartersFirstPass"] = chart; // store chart
}

function TwoYearFirstPassDrillDownToLevel2(prefix, itemSelection) {
    currentMonth = "all";
    currentStudio = itemSelection;

    var L2_id;

    if (currentPrefix == "QuartersFirstPass") {
        $("#" + prefix + "_chartdiv").hide();
        L2_id = prefix + "_L2_chartdiv";
        currentPrefix = prefix + "_L2";
        addLevelChartDiv(prefix, L2_id);
    }

    var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
    var chartTitle_firstpart = chartTitle.split('-')[0];

    var data = GetFirstPassNewThemePercentAndTotal_Q(restJsonDict, "FirstPass_Quarter", currentStudio, studio_alias);

    var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + "- " + currentStudio, "First Pass Kits", "quarter", currentTheme, false);
    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = GetFirstPassForFirstThemesGraphs_2Y();

    // Adjust Label Functions 
    for (i = 0; i < 4; i++) {
        levelTwoConfig.graphs[i].balloonFunction = adjustTwoYearFirstPass;
        levelTwoConfig.graphs[i].labelFunction = adjustTwoYearFirstPassL2LabelText;
    }

    levelTwoConfig = setColumnSpacingFirstPass_2Y(levelTwoConfig);

    levelTwoConfig.valueAxes = [{
        "id": "v1",
        "axisColor": "#009900",
        "axisThickness": 2,
        "axisAlpha": 1,
        "title": "Percent",
        "stackType": "100%",
        "position": "left",
        "autoGridCount": false,
        "gridAlpha": 0
    },
		{
		    "id": "v2",
		    "axisColor": "#00FF00",
		    "axisThickness": 2,
		    "axisAlpha": 1,
		    "gridAlpha": 0,
		    "stackType": "regular",
		    "position": "right",
		    "title": "Count",
		    "autoGridCount": false
		},
		{
		    "id": "v3",
		    "axisColor": "#00FF00",
		    "ignoreAxisWidth": true,
		    "axisThickness": 0,
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false,
		    "offset": 50,
		    "synchronizeWith": "v2",
		    "synchronizationMultiplier": 1,
		    "position": "right",
		    "autoGridCount": false
		},
		{
		    "id": "v4",
		    "axisColor": "#009900",
		    "ignoreAxisWidth": true,
		    "axisThickness": 0,
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false,
		    "offset": 50,
		    "synchronizeWith": "v1",
		    "synchronizationMultiplier": 1,
		    "position": "left",
		    "autoGridCount": false
		}];

    chartConfigDict["QuartersFirstPass_L2"] = levelTwoConfig;

    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    //chart.addLabel(
	//	'!150', '30',
	//	"Enable Trend",
	//	undefined,
	//	12,
	//	undefined,
	//	undefined,
	//	undefined,
	//	true,
	//	'javascript:ShowRollingTotal(true);');


    chart.hideGraph(chart.getGraphById("rollingAppPercent"));
    chart.hideGraph(chart.getGraphById("rollingRejPercent"));
    chart.hideGraph(chart.getGraphById("rollingPenPercent"));
    chart.hideGraph(chart.getGraphById("rollingNAPercent"));

    chart.addListener("clickGraphItem", L2_handleItemClick);
    chart.addListener("init", QuartersFirstPass_L2_chartInit);
    chart.addListener("rendered", L2_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;


    level++; // increase global level indicator

    var dataTable = data;//GetDataForEntry_Quarters("QuartersFirstPass", restJsonDict, "FirstPass_Quarter", "all", currentStudio, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
    dataTableOptionsDict[prefix + "_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
}

function QuartersFirstPass_L2_chartInit(event) {
    chart = event.chart;

    calculateRollingPercentages(chart, chart.start, chart.end);

    chart.legend.addListener("showItem", TwoYearFirstPass_L2_legendHandler);
    chart.legend.addListener("hideItem", TwoYearFirstPass_L2_legendHandler);

    // hide all line graphs
    for (i = 0; i < chart.graphs.length; i++) {
        if (chart.graphs[i].type == "line" && !chart.graphs[i].hidden)
            event.chart.hideGraph(chart.graphs[i]);
    }
}

function TwoYearFirstPass_L2_legendHandler(event) {

    var chart = event.chart;
    var targetGraph = event.dataItem;
    var otherGraph;
    var valueField = targetGraph.valueField;
    var graphId = targetGraph.id;
    var dataSet = chart.dataProvider;
    var maxValue = 0;
    var i;

    var axis = targetGraph.valueAxis;
    if (axis.synchronizeWith)
        axis = axis.synchronizeWith;

    ShowTrendLine(false, graphId, event.type); // (quarters, id, show/hide)

    if (targetGraph.id == "fyRollingTotal" || targetGraph.id == "rollingTotal") {

        if (event.type == "showItem") {
            for (i = 0; i < dataSet.length; i++) {
                if (maxValue < dataSet[i][valueField])
                    maxValue = dataSet[i][valueField];
            }
            if (axis.maximum == undefined || axis.maximum < maxValue)
                axis.maximum = maxValue;
            chart.invalidateSize();
            chart.validateData();
        } else if (event.type == "hideItem") {
            if (targetGraph.id == "fyRollingTotal") {
                otherGraph = chart.getGraphById("rollingTotal");
                if (otherGraph.hidden) {
                    axis.maximum = undefined;
                } else {
                    valueField = otherGraph.valueField;
                    maxValue = 0;
                    for (i = 0; i < dataSet.length; i++) {
                        if (maxValue < dataSet[i][valueField])
                            maxValue = dataSet[i][valueField];
                    }
                    if (axis.maximum == undefined || axis.maximum < maxValue)
                        axis.maximum = maxValue;
                }
            } else if (targetGraph.id == "rollingTotal") {
                otherGraph = chart.getGraphById("fyRollingTotal");
                if (otherGraph.hidden) {
                    axis.maximum = undefined;
                } else {
                    valueField = otherGraph.valueField;
                    maxValue = 0;
                    for (i = 0; i < dataSet.length; i++) {
                        if (maxValue < dataSet[i][valueField])
                            maxValue = dataSet[i][valueField];
                    }
                    if (axis.maximum == undefined || axis.maximum < maxValue)
                        axis.maximum = maxValue;
                }
            }
            chart.invalidateSize();
            chart.validateData();
        }
    }

    calculateRollingPercentages(chart, chart.start, chart.end);
}

function adjustTwoYearFirstPass(graphDataItem, graph) {
    graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;

    return "<b>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + Math.round(graphDataItem.values.percents * 100) / 100 + "%</b>";
}

function adjustTwoYearFirstPassL2LabelText(graphDataItem, graph) {
    graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;
    if (graphDataItem.values.value < 1)
        return "";
    else
        return Math.round((graphDataItem.values.percents * 100) / 100) + "%";
}

function TwoYearFirstPassDrillDownToLevel3(prefix, quarter, subcategory, fpstatus) {
    currentQuarter = quarter;
    currentStudio = subcategory;
    showHideDataTable(prefix + "_dataTable", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var jsLink = "$('#QuartersFirstPass_dataTable_link').show(); GoUpLevel('" + prefix + "');";
    $("#" + L3_id).html('<a id="FP_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="QuartersFirstPass_L3_dataTable_group">');

	var title_div = $("<div/>", {
			id: currentPrefix + "_Title"
		})
		.addClass('metricTitle')
	title_div.html("New Theme First Pass Data for " + currentMonth + " and " + currentStudio);
	$("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator
    // CURSOR YOU ARE HERE
    var kitListTable = GetFirstPassTwoYearKitListTable(prefix, restJsonDict, "FirstPass_Quarter", quarter, subcategory, fpstatus, studio_alias);
    var kitlist = "";
    for (var i = 0; i < kitListTable.length; i++) {
        kitlist += "," + kitListTable[i]["KitNumber"];
    }
    kitlist = kitlist.substr(1);

    chartDataDict["QuartersFirstPass_L3"] = kitListTable;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#QuartersFirstPass_dataTable_link").hide();

    dataTableOptionsDict["QuartersFirstPass_L3_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5'],
        "fnDrawCallback": function (oSettings) {
            if (jQuery('#QuartersFirstPass_L3_dataTable_paginate span .paginate_button').size() > 1) {
                jQuery('#QuartersFirstPass_L3_dataTable_paginate')[0].style.display = "block";
            } else {
                jQuery('#QuartersFirstPass_L3_dataTable_paginate')[0].style.display = "none";
            }
        },
        "columnDefs": [{
            "targets": 0,
            "data": "Logo",
            "render": function (data, type, full, meta) {
                if (data == "")
                    return "No Image Found";
                return '<img id="SC_Theme_Logo" src="' + data + '" width="150" />';
            }
        }]
    };

    show_loadingDiv("Loading Data...");

    get_ThemeLogo(kitlist, UpdateFPKitListTableWithThemeLogo_2Y);


    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    resetCurrentHash();
}

function FirstPass_L3_rendered_2Y(event) {
    chart = event.chart;
    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", FirstPass_L3_handleCategoryClick_2Y);
    resetCurrentHash();

    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}


function FirstPass_L3_handleItemClick_2Y(event) {
    var prefix = event.chart.titles[0].id;
    var dataItem = event.item.dataContext;
    var quarter = dataItem.quarter;
    var studio = dataItem.studio;

    TwoYearFirstPassDrillDownToLevel3(prefix, month, studio);
}

function GetFirstPassForFirstThemesGraphs_2Y() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
    var graphsData = [
		{
		    "fillColors": "#009900",
		    "title": "Approved %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "approved_percent",
		    "newStack": true
		},
		{
		    "fillColors": "#990000",
		    "title": "Rejected %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "rejected_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#163488",
		    "title": "Pending %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "pending_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#898989",
		    "title": "Not Applicable %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "na_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#00ff00",
		    "title": "Approved",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "approved",
		    "newStack": true
		},
		{
		    "fillColors": "#FF0000",
		    "title": "Rejected",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "rejected",
		    "newStack": false,
		},
		{
		    "fillColors": "#0000FF",
		    "title": "Pending",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "pending",
		    "newStack": false
		},
		{
		    "fillColors": "#8e8e8e",
		    "title": "Not Applicable",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "na",
		    "newStack": false
		}];
    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "balloonText": ballonText,
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text_SP;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["title"] = graphsData[i]["title"];
        graph["labelText"] = graphsData[i]["labelText"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);

        // do now show values if they are less than 4
        graph["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }

    // start line graphs (rolling totals)
    graph = {
        "balloonText": ballonText,
        "id": "fyRollingTotal",
        "title": "Fiscal Year Rolling Total",
        "bullet": "round",
        "lineColor": "#cc9900",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v3",
        "valueField": "yearToDateRollingDate",
        "stackable": false
    };
    graphs.push(graph);


    graph = {
        "balloonText": ballonText,
        "id": "rollingTotal",
        "title": "2 Year Rolling Total",
        "bullet": "triangleUp",
        "lineColor": "#993399",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v3",
        "valueField": "rollingTotal",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingAppPercent",
        "title": "Rolling Approved %",
        "bullet": "square",
        "lineColor": "#1aff1a",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingAppPercent",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingRejPercent",
        "title": "Rolling Rejected %",
        "bullet": "square",
        "lineColor": "#ff0000",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingRejPercent",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingPenPercent",
        "title": "Rolling Pending %",
        "bullet": "square",
        "lineColor": "#1a1aff",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingPenPercent",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingNAPercent",
        "title": "Rolling NA %",
        "bullet": "square",
        "lineColor": "#bfbfbf",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingNAPercent"
    };
    graphs.push(graph);

    return graphs;
}

function GetTwoYearFirstPassGraphs() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
    var graphsData = [
		{
		    "fillColors": "#009900",
		    "title": "Approved %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "approved_percent",
		    "newStack": true
		},
		{
		    "fillColors": "#990000",
		    "title": "Rejected %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "rejected_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#163488",
		    "title": "Pending %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "pending_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#898989",
		    "title": "Not Applicable %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "na_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#00ff00",
		    "title": "Approved",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "approved",
		    "newStack": true
		},
		{
		    "fillColors": "#FF0000",
		    "title": "Rejected",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "rejected",
		    "newStack": false,
		},
		{
		    "fillColors": "#0000FF",
		    "title": "Pending",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "pending",
		    "newStack": false
		},
		{
		    "fillColors": "#8e8e8e",
		    "title": "Not Applicable",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "na",
		    "newStack": false
		}];
    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "balloonText": ballonText,
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text_SP;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["title"] = graphsData[i]["title"];
        graph["labelText"] = graphsData[i]["labelText"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);

        // do now show values if they are less than 4
        graph["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }

    // start line graphs (rolling totals)
    graph = {
        "balloonText": ballonText,
        "id": "fyRollingTotal",
        "title": "Two Year Rolling Total",
        "bullet": "round",
        "lineColor": "#cc9900",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v3",
        "valueField": "yearToDateRollingDate",
        "stackable": false
    };
    graphs.push(graph);


    graph = {
        "balloonText": ballonText,
        "id": "rollingTotal",
        "title": "Two Year Rolling Total",
        "bullet": "triangleUp",
        "lineColor": "#993399",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v3",
        "valueField": "rollingTotal",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingAppPercent",
        "title": "Rolling Approved %",
        "bullet": "square",
        "lineColor": "#1aff1a",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingAppPercent",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingRejPercent",
        "title": "Rolling Rejected %",
        "bullet": "square",
        "lineColor": "#ff0000",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingRejPercent",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingPenPercent",
        "title": "Rolling Pending %",
        "bullet": "square",
        "lineColor": "#1a1aff",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingPenPercent",
        "stackable": false
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingNAPercent",
        "title": "Rolling NA %",
        "bullet": "square",
        "lineColor": "#bfbfbf",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v4",
        "valueField": "rollingNAPercent"
    };
    graphs.push(graph);

    return graphs;
}

function parseByTwoYearFirstPassToChartRow(row, viewTable, studio_alias, sub_category) {
    var tempInt = 0;
    var tempStudio;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category;
        use_sub_category = true;
    }

    row["Total"] = 0; // clear out kits total, to be filled in with new theme total

    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];

            if (use_sub_category == false || targetStudio == "Total") {
                if ((datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') && datarow["FP_Status"] == "Approved") {
                    if (row.hasOwnProperty("Total"))
                        tempInt = row["Total"];
                    else tempInt = 0;
                    row["Total"] = tempInt + 1;
                }
            }

            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];

            if (use_sub_category == false || targetStudio == tempStudio) {
                if ((datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System')&& datarow["FP_Status"] == "Approved") {
                    if (row.hasOwnProperty(tempStudio))
                        tempInt = row[tempStudio];
                    else tempInt = 0;
                    row[tempStudio] = tempInt + 1;
                }
            }
        }
    }

    return row;
}

function UpdateFPKitListTableWithThemeLogo_2Y(r) {
    var kit;
    for (var i = 0; i < r.length; i++) {
        kit = r[i]["Kit"];
        themeLogoDict[kit] = r[i]["ThemeLogo"];
    }

    var data = chartDataDict["QuartersFirstPass_L3"];
    for (var c = 0; c < data.length; c++) {
        kit = data[c]["KitNumber"];
        if (themeLogoDict[kit] == "")
            data[c]["Logo"] = "";
        else data[c]["Logo"] = "data:image/png;base64," + themeLogoDict[kit];
    }

    //table.rows().invalidate().draw();
    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(data, "QuartersFirstPass_L3_dataTable", "100%", dataTableOptionsDict["QuartersFirstPass_L3_dataTable"], tableClassList);
    dataTableHTMLTableDict["QuartersFirstPass_L3"] = htmlTable;
    dataTableHTMLTableDict["QuartersFirstPass_L3"].$("tr").click(function () {
        var data = dataTableHTMLTableDict["QuartersFirstPass_L3"].row($(this)).data();
        $("#QuartersFirstPass_dataTable_link").show();
        showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
    });
    updateElementAfterRender("QuartersFirstPass_L3_dataTable_filter", function () {
        removeLoadingDiv();
    });
}

function setColumnSpacingFirstPass_2Y(config) {
    // get width of the chart div
    divWidth = document.getElementById(currentPrefix + "_chartdiv").offsetWidth;
    //console.log("Div Width: " + divWidth);

    // set column spacing based on width of div
    //if (divWidth >= 1350)
    //    config.columnSpacing = -15;
    //else if (divWidth < 1350 && divWidth >= 1000)
    //    config.columnSpacing = -10;
    //else if (divWidth < 1000 && divWidth >= 900)
    //    config.columnSpacing = -8;
    //else if (divWidth < 900 && divWidth > 500)
    //    config.columnSpacing = -5;
    //else
        config.columnSpacing = 0;

    //console.log(config.columnSpacing);

    return config;
}

function setColumnSpacingFirstPassInt_2Y() {
    // get width of the chart div
    divWidth = document.getElementById(currentPrefix + "_chartdiv").offsetWidth;

    //// set column spacing based on width of div
    //if (divWidth >= 1350)
    //    return -15;
    //else if (divWidth < 1350 && divWidth >= 1000)
    //    return -10;
    //else if (divWidth < 1000 && divWidth >= 900)
    //    return -8;
    //else if (divWidth < 900 && divWidth > 500)
    //    return -5;
    //else
        return 0;
}