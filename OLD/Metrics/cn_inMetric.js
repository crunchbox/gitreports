thisMetricName = "cn_in";

function get_cn_inMetrics() {
    thisMetricName = "cn_in";
    if (loadedMetrics[thisMetricName] != true) {
        get_MetricsData("cn_in", thisMetricName);
        return;
    }

    if (analytics != null)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "CN/IN by Kit Release Date");

    clearChart(thisMetricName);
    showHideDataTable(thisMetricName, "100%", false);


    var config;
    var chartData, chartGraphs, chart;
    if (chartConfigDict[thisMetricName] == null) {
        chartData = parseMetricData(restJsonDict, (thisMetricName + "_Month"), studio_alias, thisMetricName);
        chartGraphs = GetStackedGraphs(chartData, "month", "PA", "Other");

        dataTableDataDict[thisMetricName+"_dataTable"] = chartData;
        config = generateConfig(thisMetricName, "CN/IN  by Kit Release Date - PA/Other Breakdown", "CN/IN Count", "month", currentTheme, true);
        config.dataProvider = chartData;
        config.graphs = chartGraphs;

        chartConfigDict[thisMetricName] = config;
        chartDataDict[thisMetricName] = chartData;
    } else config = chartConfigDict[thisMetricName];

    chart = AmCharts.makeChart(thisMetricName+"_chartdiv", config, 100);

    chartResizerDict[thisMetricName] = true;

    chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    $("#loadingdiv").remove(); // remove loading div


    chartDict[thisMetricName] = chart; // store chart

    var dataTable = chartData; 
    dataTableDataDict[thisMetricName+"_dataTable"] = dataTable;
    dataTableOptionsDict[thisMetricName+"_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
} //


function parseCN_INdata(row, viewTable, studio_alias, sub_category) {
    var tempStudio;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category.split('/')[0];
        use_sub_category = true;
    }

    //var tempGrouping = _.groupBy(viewTable, "StudioName");

    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];


            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];
            if (tempStudio !== "") {
                
                if (use_sub_category == false || targetStudio == "Total") {
                    row["studio"] = "Total";
                    if (!row.hasOwnProperty("Total/PA"))
                        row["Total/PA"] = 0;

                    if (!row.hasOwnProperty("Total/Other"))
                        row["Total/Other"] = 0;


                    if (datarow.hasOwnProperty("Responsible_Group")) {
                        if (datarow["Responsible_Group"] === "PA") {
                            row["Total/PA"] += datarow["Count"];
                        }
                        else /*if (datarow["Result"] === "N")*/ {
                            row["Total/Other"]+= datarow["Count"];
                        }
                    }

                }

                if (row.hasOwnProperty("Total"))
                    row["Total"] = row["Total/PA"] + row["Total/Other"];


                if (use_sub_category == false || targetStudio == tempStudio) {
                    row["studio"] = tempStudio;
                    if (!row.hasOwnProperty(tempStudio + "/PA"))
                        row[tempStudio + "/PA"] = 0;

                    if (!row.hasOwnProperty(tempStudio + "/Other"))
                        row[tempStudio + "/Other"] = 0;

                    if (datarow.hasOwnProperty("Responsible_Group")) {
                        if (datarow["Responsible_Group"] === "PA") {
                            row[tempStudio + "/PA"] += datarow["Count"];
                        }
                        else {
                            row[tempStudio + "/Other"] += datarow["Count"];
                        }
                    }

                }
            }
        }
    }
    return row;
} //

function Getcn_inStackedGraphs(data, categoryField, bottomField, topField) {
    var graphs = [];
    var ballonText = "[[title]] of [[category]]:[[value]]";

    var colors = ["#5CB85C", "#F0AD4E", "#337AB7", "#a7a737", "#86a965", "#8aabb0", "#d8854f", "#cfd27e",
                  "#9d9888", "#916b8a", "#724887", "#7256bc", // end defaults 
                  "#FF0000", "#008000", "#FFC0CB", "#FFA500", "#FFF0F5", "#FFFF00", "#0000FF", "#CD853F",
                  "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#FFFFE0", "#ADD8E6", "#D2B48C",
			      "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#EEE8AA", "#00BFFF", "#FFDEAD",
			      "#8FBC8B", "#00FF7F", "#FFF0F5", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
			      "#40E0D0", "#66CDAA"];

    var groups = [];
    for (var i = 0; i < data.length; i++) {
        rowData = data[i];
        for (var heading in rowData) {
            if (rowData.hasOwnProperty(heading)) {
                if (heading != categoryField) {

                    if (groups.indexOf(heading) == -1) {
                        groups.push(heading);
                    }
                }
            }
        }
    }

    var stacks = {};
    var cat;
    var index;
    var column;
    var key;

    for (index in groups) {
        if (groups.hasOwnProperty(index)) {
            column = {};

            key = groups[index];
            if (key.indexOf(topField, 0) != -1) {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["newStack"] = false;
                column["type"] = "column";
                column["valueField"] = key;

                cat = key.split("/")[0];
                if (stacks.hasOwnProperty(cat) == false)
                    stacks[cat] = {};
                stacks[cat][topField] = column;
            } else if (key.indexOf(bottomField, 0) != -1) {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["newStack"] = true;
                column["type"] = "column";
                column["valueField"] = key;

                cat = key.split("/")[0];
                if (stacks.hasOwnProperty(cat) == false)
                    stacks[cat] = {};
                stacks[cat][bottomField] = column;
            }
        }
    }

    for (cat in stacks) {
        if (stacks.hasOwnProperty(cat)) {
            if (stacks[cat].hasOwnProperty(bottomField) == false && stacks[cat].hasOwnProperty(topField) == true) {
                stacks[cat][topField]["newStack"] = true;
            } else graphs.push(stacks[cat][bottomField]);

            if (stacks[cat].hasOwnProperty(topField))
                graphs.push(stacks[cat][topField]);
        }
    }

    // set fill colors of bars/columns
    for (i = 0; i < graphs.length; i++) {
        graphs[i].fillColors = colors[i];
        graphs[i].lineColor = colors[i];
    }

    for (index in groups) {
        if (groups.hasOwnProperty(index)) {
            column = {};

            key = groups[index];
            if (key == "Total") {
                column["balloonText"] = ballonText;
                column["id"] = "totalline";
                column["title"] = "Total";
                column["bullet"] = "round";
                column["lineThickness"] = 3;
                column["bulletSize"] = 7;
                column["bulletBorderAlpha"] = 1;
                column["connect"] = true;
                column["useLineColorForBulletBorder"] = true;
                column["bulletBorderThickness"] = 3;
                column["fillAlphas"] = 0;
                column["lineAlpha"] = 1;
                column["valueField"] = key;
                graphs.push(column);
            }
        }
    }

    return graphs;
}


function CN_IN_DrillDownToLevel2(prefix, itemSelection) {
    currentMonth = "all";
    currentStudio = (itemSelection.split("/"))[0];

    var L2_id;

    if (currentPrefix == thisMetricName) {
        $("#" + prefix + "_chartdiv").hide();
        L2_id = prefix + "_L2_chartdiv";
        currentPrefix = prefix + "_L2";
        addLevelChartDiv(prefix, L2_id);
    }
    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    showHideDataTable(prefix, "100%", false);

    var studioName = itemSelection.split('/')[0];
    var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title


    var data = parseMetricData(restJsonDict, thisMetricName+"_Month", studio_alias, prefix, itemSelection);

    var levelTwoConfig = generateConfig(prefix, "CN/IN by Kit Release Date" + " - " + studioName, yAxisTitle, "month", currentTheme, true);
    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = GetStackedGraphs(data, "month", "PA", "Other");


    var stacks = ["PA", "Other"];
    levelTwoConfig.graphs = GetStackedRollingTotal(levelTwoConfig.graphs, data, "month", stacks);
    levelTwoConfig.valueAxes = [
    {
        "id": "v1",
        "title": yAxisTitle,
        "stackType": "regular"
    },
    {
        "id": "v2",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "labelsEnabled": false,
        "position": "right",
        "autoGridCount": false,
        "synchronizeWith": "v1",
        "synchronizationMultiplier": 1
    }];

    chartConfigDict[thisMetricName+"_L2"] = levelTwoConfig;
    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;


    // let's add a label to go back to yearly data
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    var total_Label = "Enable Trend";
    var total_link = 'javascript:getRollingTotal();';
    chart.addLabel(
		'!150', '34',
		total_Label,
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		total_link);

    chart.addListener("clickGraphItem", L2_handleItemClick);
    chart.addListener("init", chartInit);
    chart.addListener("rendered", L2_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    var dataTable = data; 
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
    dataTableOptionsDict[prefix + "_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
}


function CN_IN_DrillDownToLevel3(prefix, monthname, studio, fpstatus) {
    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);

    
    var splitCat = fpstatus.split("/");

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var date = new Date();
    var year = date.getFullYear();
    var month = 0;

    if (monthname == "Current Month") {
        month = date.getMonth() + 1;
    }
    else {
        month = getMonthFromString(monthname) + 1;
        if (month > (date.getMonth() + 1)) {
            year--;
        }
    }

    if (month < 10)
        month = "0" + month;

    var yearMonth = year +""+ month;
    /*
    var paCount = "Y";
    if (splitCat[1] === "Other")
        paCount = "N"; 
    */
    // get data
    CN_INForKits(currentPrefix, restJsonDict, yearMonth, studio);
}

function CN_INForKits(prefix, jsonTable, yearMonth, studio) {

    if (loadedMetrics[prefix + studio + yearMonth] != true) {
        //get_CN_IN_KitList(studio, yearMonth, paCount);
        get_edasDataL3(thisMetricName, studio, yearMonth);
        return;
    }

    var fpTable = restJsonDict[prefix + studio + yearMonth];
    var L3_id = prefix + "_chartdiv";
    currentPrefix = prefix;
    addLevelChartDiv(prefix, L3_id);

    var jsLink = "$('#" + thisMetricName + "_dataTable_link').show(); GoUpLevel('" + thisMetricName + "');";
    $("#" + L3_id).html('<a id="' + thisMetricName + '_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="' + thisMetricName + '_L3_dataTable_group">');

    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('metricTitle');
    title_div.html("CN/IN for " + yearMonth + " and " + studio.replaceAll("_", " ") + " - Total: " + fpTable.EDAEnteredL3Info.length);
    $("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict[prefix] = fpTable.EDAEnteredL3Info;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#"+thisMetricName+"_dataTable_link").hide();

    dataTableOptionsDict[prefix + "_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(fpTable.EDAEnteredL3Info, prefix + "_dataTable", "100%", dataTableOptionsDict[prefix + "_dataTable"], tableClassList);
    dataTableHTMLTableDict[prefix] = htmlTable;
    if (fpTable.EDAEnteredL3Info.length > 0) {
        dataTableHTMLTableDict[prefix].$("tr").click(function () {
            var data = dataTableHTMLTableDict[prefix].row($(this)).data();
            $("#" + thisMetricName + "_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }

    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
}


function SortCN_INL3ByCount(prefix, sortByDefects) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix];
    var labelIndex;

    var switchText = "Sort by CN/IN Count";

    if (sortByDefects) {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.Count > b.Count) return -1;
            if (a.Count < b.Count) return 1;
            return 0;
        });

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
        chart.allLabels[labelIndex].text = "Sort By Kit Number";
        chart.allLabels[labelIndex].url = 'javascript:SortCN_INL3ByCount("' + prefix + '", false);';
    } else {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.KitNumber < b.KitNumber) return -1;
            if (a.KitNumber > b.KitNumber) return 1;
            return 0;
        });
        labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort By Kit Number");
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortCN_INL3ByCount("' + prefix + '", true);';
    }
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}


function getL3GraphsCN_IN(data, categoryField) {
    var graphs = [];

    var column = {
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "columnWidth": .8,
        "fontSize": 10,
        "showAllValueLabels": true,
        "valueField": "Count"
    };

    graphs.push(column);


    return graphs;
}