/*****************************************************************************\

 Javascript "Kit Release LIst" library

 @version: 0.1 - 2016.05.06
 @author: Sean M McClain

\*****************************************************************************/


function get_KitReleaseGrid() {
	if (loadedMetrics["kitreleaselist"] != true) {
		get_MetricsData("kitreleaselist", "kitreleaselist");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Kit Release Grid");

	var dataTableID = "kitreleaselist_dataTable";

	dataTableDataDict[dataTableID] = restJsonDict[chartJSONTableDict["kitreleaselist"]];
	dataTableOptionsDict[dataTableID] = {
		"scrollY": "80vh",
		"scrollX": true,
		"scrollCollapse": true,
		"pageLength": 15,
		"paging": true,
		"dom": 'frtBp',
		"buttons": ['copyHtml5', 'csvHtml5'],
		"responsive": true,
		"columnDefs": [{ responsivePriority: 1, targets: 6 },
                       { responsivePriority: 2, targets: 0 },
		               { responsivePriority: 3, targets: 1 },
		               { responsivePriority: 4, targets: 2 },
		               { responsivePriority: 5, targets: 3 },
		               { responsivePriority: 6, targets: 4 },
		               { responsivePriority: 7, targets: 5 },
		               { className: "none", "targets": [-9, -8, -7, -6, -5, -4, -3, -2, -1] }],
		"order": [[6, "desc"]]
	};

	dataTableIsVisible[dataTableID] = true; // set visible to opposite of show so the below code toggles it

	var classList = "row-border stripe cell-border compact hover";

	currentPrefix = "kitreleaselist";
	currentMonth = "all";

	if ($("#" + currentPrefix + "_Title").length == 0) {
	    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('searchMetricTitle');

	    title_div.html("Search Kits Released - " + dataTableDataDict[dataTableID].length + " Kits Total");
	    var goBackLink = "<div style='padding-bottom:20px;'><a href='/' style='font-size:14px; color:#" + scrollbarLineColor + "; font-weight:700;'>< Go back</a></div>";
	    $("#metric_kitreleaselist_header").prepend(goBackLink);
	    $("#metric_kitreleaselist_header").prepend(title_div);
	   
	}


	var sortedMonths = getSortedMonthList();

	var index;
	var monNum = 1;
	for (index = sortedMonths.length - 1; index >= 0; index--) {
		$("#kitreleaselist_MonthDropDown").append($('<option>', {
			value: monNum
		}).text(sortedMonths[index]));
		monNum++;
	}

	var studios = getStudioAliasList(studio_alias);
	for (index = 0; index < studios.length; index++) {
		$("#kitreleaselist_StudioDropDown").append($('<option>', {
			value: studios[index]
		}).text(studios[index]));
	}

	for (var i = 0; i < dataTableDataDict[dataTableID].length; i++) {
	    ((dataTableDataDict[dataTableID])[i])["Components"].replaceAll(",", ", ");
	}

	var table = drawTable(dataTableDataDict[dataTableID], dataTableID, "100%", dataTableOptionsDict[dataTableID], classList);
	dataTableHTMLTableDict["kitreleaselist"] = table;

	$('#kitreleaselist_dataTable_filter label').css("width", "100%");
	$('#kitreleaselist_dataTable_filter label input').css("width", "90%");
	$('#kitreleaselist_dataTable_filter').css("width", "inherit");
	$('#kitreleaselist_dataTable_filter').css("margin", "auto");
	$('#kitreleaselist_dataTable_filter').css("float", "none");

	table.$("td:not(:first-child)").click(function () {
	    var data = dataTableHTMLTableDict["kitreleaselist"].row($(this).closest('tr')).data();
		$.fn.dataTable.ext.search = [];
		showAndLoadScoreCard(currentPrefix, data[0], currentMonth);
	});

	table.$("td:not(:first-child)").css('cursor', 'pointer');

	$("#kitreleaselist_StudioDropDown_Div").css("visibility", "visible");
	$("#kitreleaselist_MonthDropDown_Div").css("visibility", "visible");

	$.fn.dataTable.ext.search = [];
	$.fn.dataTable.ext.search.push(kitrelease_emptyFilter);
	$.fn.dataTable.ext.search.push(kitrelease_emptyFilter);


	$("#kitreleaselist_StudioDropDown").change(kitrelease_UpdateStudioList);
	$("#kitreleaselist_MonthDropDown").change(kitrelease_UpdateMonthList);

	removeLoadingDiv();

}

var kitrelease_currentMonthFilter = 0;
var kitrelease_currentStudioFilter = "All Studios";

function kitrelease_UpdateStudioList() {
	var selectedValue = jQuery(this).val();
	kitrelease_currentStudioFilter = selectedValue;

	if (selectedValue != "All Studios") {
		$.fn.dataTable.ext.search[0] = kitrelease_studioBaseFilter;
	} else $.fn.dataTable.ext.search[0] = kitrelease_emptyFilter;
	dataTableHTMLTableDict["kitreleaselist"].draw();
}

function kitrelease_UpdateMonthList() {
	var selectedValue = jQuery(this).val();
	kitrelease_currentMonthFilter = selectedValue;

	if (selectedValue > 0) {
		$.fn.dataTable.ext.search[1] = kitrelease_dateBaseFilter;
	} else $.fn.dataTable.ext.search[1] = kitrelease_emptyFilter;
	dataTableHTMLTableDict["kitreleaselist"].draw();
}

function kitrelease_dateBaseFilter(settings, data, dataIndex) {
	var date = ISODateParse(data[5]);
	var targetDate = new Date();
	targetDate.setMonth(targetDate.getMonth() - (kitrelease_currentMonthFilter - 1));
	if (date.getMonth() == targetDate.getMonth() && date.getYear() == targetDate.getYear())
		return true;
	return false;
}

function kitrelease_studioBaseFilter(settings, data, dataIndex) {
	if (data[4] == kitrelease_currentStudioFilter)
		return true;
	return false;
}

function kitrelease_emptyFilter(settings, data, dataIndex) {
	return true;
}