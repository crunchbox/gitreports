thisMetricName = "cn_inEntered";

function get_cn_inEnteredMetrics() {
    thisMetricName = "cn_inEntered";
    // load metric data if not already loaded
    if (loadedMetrics["cn_inEntered"] != true) {
        get_MetricsData("cn_inEntered", thisMetricName);
        return;
    }
    // set analytics event
    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "CN/IN by Date Entered");

    // clear chart and table
    clearChart(thisMetricName);
    showHideDataTable(thisMetricName, "100%", false);
    // set chart config and graphs if not already set
    var config;
    var chartData, chartGraphs, chart;
    if (chartConfigDict[thisMetricName] == null) {
        chartData = parseMetricData(restJsonDict, (thisMetricName + "_Month"), studio_alias, thisMetricName);
        chartGraphs = GetStackedGraphs(chartData, "month", "PA", "Other");

        dataTableDataDict[thisMetricName + "_dataTable"] = chartData;
        config = generateConfig(thisMetricName, "CN/IN by Date Entered - PA/Other Breakdown", "CN/IN Count", "month", currentTheme, true);
        config.dataProvider = chartData;
        config.graphs = chartGraphs;

        chartConfigDict[thisMetricName] = config;
        chartDataDict[thisMetricName] = chartData;
    } else config = chartConfigDict[thisMetricName];
    // draw chart
    chart = AmCharts.makeChart(thisMetricName + "_chartdiv", config, 100);

    chartResizerDict[thisMetricName] = true;
    // add go back label
    chart.addLabel(
        35, 20,
        "< Go back",
        undefined,
        14,
        undefined,
        undefined,
        undefined,
        true,
        '/');
    // apply init and rendered listeners
    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    $("#loadingdiv").remove(); // remove loading div
    
    chartDict[thisMetricName] = chart; // store chart
    // store dataTable data and options for export
    var dataTable = chartData;
    dataTableDataDict[thisMetricName + "_dataTable"] = dataTable;
    dataTableOptionsDict[thisMetricName + "_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
}

function parseCN_INEnteredData(row, viewTable, studio_alias, sub_category) {
    var tempStudio;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category.split('/')[0];
        use_sub_category = true;
    }

    //var tempGrouping = _.groupBy(viewTable, "StudioName");

    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];


            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];
            if (tempStudio !== "") {

                if (use_sub_category == false || targetStudio == "Total") {
                    row["studio"] = "Total";
                    if (!row.hasOwnProperty("Total/PA"))
                        row["Total/PA"] = 0;

                    if (!row.hasOwnProperty("Total/Other"))
                        row["Total/Other"] = 0;


                    if (datarow.hasOwnProperty("Responsible_Group")) {
                        if (datarow["Responsible_Group"] === "PA") {
                            row["Total/PA"] += datarow["Count"];
                        }
                        else /*if (datarow["Result"] === "N")*/ {
                            row["Total/Other"] += datarow["Count"];
                        }
                    }

                }

                if (row.hasOwnProperty("Total"))
                    row["Total"] = row["Total/PA"] + row["Total/Other"];


                if (use_sub_category == false || targetStudio == tempStudio) {
                    row["studio"] = tempStudio;
                    if (!row.hasOwnProperty(tempStudio + "/PA"))
                        row[tempStudio + "/PA"] = 0;

                    if (!row.hasOwnProperty(tempStudio + "/Other"))
                        row[tempStudio + "/Other"] = 0;

                    if (datarow.hasOwnProperty("Responsible_Group")) {
                        if (datarow["Responsible_Group"] === "PA") {
                            row[tempStudio + "/PA"] += datarow["Count"];
                        }
                        else {
                            row[tempStudio + "/Other"] += datarow["Count"];
                        }
                    }

                }
            }
        }
    }
    return row;
}

function CN_INEntered_DrillDownToLevel2(prefix, itemSelection) {
    currentMonth = "all";
    currentStudio = (itemSelection.split("/"))[0];

    var L2_id;

    if (currentPrefix == thisMetricName) {
        $("#" + prefix + "_chartdiv").hide();
        L2_id = prefix + "_L2_chartdiv";
        currentPrefix = prefix + "_L2";
        addLevelChartDiv(prefix, L2_id);
    }
    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    showHideDataTable(prefix, "100%", false);

    var studioName = itemSelection.split('/')[0];
    var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title


    var data = parseMetricData(restJsonDict, thisMetricName + "_Month", studio_alias, prefix, itemSelection);

    var levelTwoConfig = generateConfig(prefix, "CN/IN by Date Entered" + " - " + studioName, yAxisTitle, "month", currentTheme, true);
    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = GetStackedGraphs(data, "month", "PA", "Other");


    var stacks = ["PA", "Other"];
    levelTwoConfig.graphs = GetStackedRollingTotal(levelTwoConfig.graphs, data, "month", stacks);
    levelTwoConfig.valueAxes = [
        {
            "id": "v1",
            "title": yAxisTitle,
            "stackType": "regular"
        },
        {
            "id": "v2",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "labelsEnabled": false,
            "position": "right",
            "autoGridCount": false,
            "synchronizeWith": "v1",
            "synchronizationMultiplier": 1
        }];

    chartConfigDict[thisMetricName + "_L2"] = levelTwoConfig;
    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;


    // let's add a label to go back to yearly data
    chart.addLabel(
        35, 20,
        "< Go back",
        undefined,
        15,
        undefined,
        undefined,
        undefined,
        true,
        'javascript:GoUpLevel("' + prefix + '");');

    var total_Label = "Enable Trend";
    var total_link = 'javascript:getRollingTotal();';
    chart.addLabel(
        '!150', '34',
        total_Label,
        undefined,
        12,
        undefined,
        undefined,
        undefined,
        true,
        total_link);

    chart.addListener("clickGraphItem", L2_handleItemClick);
    chart.addListener("init", chartInit);
    chart.addListener("rendered", L2_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    var dataTable = data;
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
    dataTableOptionsDict[prefix + "_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
}


function CN_IN_EnteredDrillDownToLevel3(prefix, monthname, studio, fpstatus) {
    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);


    var splitCat = fpstatus.split("/");

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var date = new Date();
    var year = date.getFullYear();
    var month = 0;

    if (monthname == "Current Month") {
        month = date.getMonth() + 1;
    }
    else {
        month = getMonthFromString(monthname) + 1;
        if (month > (date.getMonth() + 1)) {
            year--;
        }
    }

    if (month < 10)
        month = "0" + month;

    var yearMonth = year + "" + month;
    /*
    var paCount = "Y";
    if (splitCat[1] === "Other")
        paCount = "N"; 
    */
    // get data
    CN_INEnteredForKits(currentPrefix, restJsonDict, yearMonth, studio);
}

function CN_INEnteredForKits(prefix, jsonTable, yearMonth, studio) {

    if (loadedMetrics[prefix + studio + yearMonth] != true) {
        //get_CN_IN_KitList(studio, yearMonth, paCount);
        get_edasDataL3(thisMetricName, studio, yearMonth);
        return;
    }

    var fpTable = restJsonDict[prefix + studio + yearMonth];
    var L3_id = prefix + "_chartdiv";
    currentPrefix = prefix;
    addLevelChartDiv(prefix, L3_id);

    var jsLink = "$('#" + thisMetricName + "_dataTable_link').show(); GoUpLevel('" + thisMetricName + "');";
    $("#" + L3_id).html('<a id="' + thisMetricName + '_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="' + thisMetricName + '_L3_dataTable_group">');

    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('metricTitle');
    title_div.html("CN/IN Entered for " + yearMonth + " and " + studio.replaceAll("_", " ") + " - Total: " + fpTable.EDAEnteredL3Info.length);
    $("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict[prefix] = fpTable.EDAEnteredL3Info;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#" + thisMetricName + "_dataTable_link").hide();

    dataTableOptionsDict[prefix + "_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(fpTable.EDAEnteredL3Info, prefix + "_dataTable", "100%", dataTableOptionsDict[prefix + "_dataTable"], tableClassList);
    dataTableHTMLTableDict[prefix] = htmlTable;

    if (fpTable.EDAEnteredL3Info.length > 0) {
        dataTableHTMLTableDict[prefix].$("tr").click(function () {
            var data = dataTableHTMLTableDict[prefix].row($(this)).data();
            $("#" + thisMetricName + "_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }


    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
}
