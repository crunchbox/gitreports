function get_GameTrackerMetrics() {
    if (loadedMetrics["GameTracker"] != true) {
        get_GameTrackerList();
        return;
    }

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Game Tracker");

    clearChart("GameTracker");
    showHideDataTable("GameTracker_dataTable", "100%", false);

    var trackerConfig;
    var chartData, chartGraphs;
    if (chartConfigDict["GameTracker"] == null) {
        chartData = parseMetricData(restJsonDict, "gameTracker_Month", studio_alias, "GameTracker");
        chartGraphs = GetGameTrackerStackedGraphs(chartData, "month", "Enabled", "Disabled");

        dataTableDataDict["GameTracker_dataTable"] = chartData;
        trackerConfig = generateConfig("GameTracker", "Game Tracker - UTP Games", "Games", "month", currentTheme, true);
        trackerConfig.dataProvider = chartData;
        trackerConfig.graphs = chartGraphs;

        chartConfigDict["GameTracker"] = trackerConfig;
        chartDataDict["GameTracker"] = chartData;
    } else trackerConfig = chartConfigDict["GameTracker"];

    var chart = AmCharts.makeChart("GameTracker_chartdiv", trackerConfig, 100);


    chartResizerDict["GameTracker"] = true;

    chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", GameTrackeraddListeners);

    $("#loadingdiv").remove(); // remove loading div


    //chart.validateNow();
    chartDict["GameTracker"] = chart; // store chart

    if (dataTableIsVisible["GameTracker" + "_dataTable"] !== undefined && dataTableIsVisible["GameTracker" + "_dataTable"] === true)
        showHideDataTable("GameTracker" + "_dataTable");
}

function formatTitle(str) {
    var newStr = str[0].toUpperCase() + str.toLowerCase().substring(1, str.length);

    for (var i = 1; i < newStr.length; i++) {
        if (newStr[i] === " " && newStr[i+1]!==undefined) {
            newStr = newStr.substring(0, i+1) + newStr[i + 1].toUpperCase() + newStr.substring(i+2, newStr.length);
        }
    }

    return newStr;
}

function parseGameTrackerData(row, viewTable, studio_alias, sub_category) {
    var tempStudio;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category.split('/')[0];
        use_sub_category = true;
    }

    //var tempGrouping = _.groupBy(viewTable, "Studio");


    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            var datarow = viewTable[dataindex];


            if (studio_alias.hasOwnProperty(datarow["Studio"].toUpperCase()))
                tempStudio = studio_alias[datarow["Studio"].toUpperCase()];
            else tempStudio = formatTitle(datarow["Studio"]);
            if (tempStudio !== "") {
                if (use_sub_category == false || targetStudio == "Total") {

                    if (!row.hasOwnProperty("Total/Enabled"))
                        row["Total/Enabled"] = 0;

                    if (!row.hasOwnProperty("Total/Disabled"))
                        row["Total/Disabled"] = 0;


                    if (datarow.Builds[datarow.Builds.length - 1].UtpEnabled !== undefined) {
                        if (datarow.Builds[datarow.Builds.length - 1].UtpEnabled === "1") {
                            row["Total/Enabled"]++;
                        }
                        else {
                            row["Total/Disabled"]++;
                        }
                    }

                }

                if (row.hasOwnProperty("Total"))
                    row["Total"] = row["Total/Enabled"] + row["Total/Disabled"];


                if (use_sub_category == false || targetStudio == tempStudio) {
                    if (!row.hasOwnProperty(tempStudio + "/Enabled"))
                        row[tempStudio + "/Enabled"] = 0;

                    if (!row.hasOwnProperty(tempStudio + "/Disabled"))
                        row[tempStudio + "/Disabled"] = 0;

                    if (datarow.Builds[datarow.Builds.length - 1].UtpEnabled !== undefined) {
                        if (datarow.Builds[datarow.Builds.length - 1].UtpEnabled === "1") {
                            row[tempStudio + "/Enabled"]++;
                        }
                        else {
                            row[tempStudio + "/Disabled"]++;
                        }
                    }

                }
            }
        }
    }

    return row;
}

function GameTrackeraddListeners(event) {

    $('a[href="http://www.amcharts.com/javascript-charts/"]').remove(); // remove watermark

    var prefix = event.chart.titles[0].id;
    chart = event.chart;
    chart.addListener("clickGraphItem", handleItemClick);

    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", handleCategoryClick);

    removeLoadingDiv();

    forceChartFillParent(prefix);

    if (hasInitSubCat) {
        var subcat_type = whatIsInitSubCat(prefix); // validates initSubCat and initStackCat and gives type
        hasInitSubCat = false;

        if (subcat_type == "month")
            GameTracker_DrillDownToLevel2(prefix, initSubCategory);
        else if (subcat_type == "studio") {
            if (prefix == "GameTracker")
                GameTracker_DrillDownToLevel2(prefix, initSubCategory);
        }
    } else resetCurrentHash();
}

function GetGameTrackerStackedGraphs(data, categoryField, passField, failField) {
    var graphs = [];
    var ballonText = "[[title]] of [[category]]:[[value]]";

    var colors = ["#5CB85C", "#c15353", "#a7a737", "#86a965", "#8aabb0", "#d8854f", "#cfd27e",
                  "#9d9888", "#916b8a", "#724887", "#7256bc", // end defaults 
                  "#b85c8a", "#008000", "#FFC0CB", "#FFA500", "#FFF0F5", "#FFFF00", "#0000FF", "#CD853F",
                  "#F08080", "#90EE90", "#FF69B4", "#FF6347", "#FF00FF", "#FFFFE0", "#ADD8E6", "#D2B48C",
			      "#DC143C", "#808000", "#FF1493", "#7FFFD4", "#9400D3", "#EEE8AA", "#00BFFF", "#FFDEAD",
			      "#8FBC8B", "#00FF7F", "#FFF0F5", "#1E90FF", "#7B68EE", "#BDB76B", "#00FFFF", "#9ACD32",
			      "#40E0D0", "#66CDAA"];

    var groups = [];
    for (var i = 0; i < data.length; i++) {
        rowData = data[i];
        for (var heading in rowData) {
            if (rowData.hasOwnProperty(heading)) {
                if (heading != categoryField) {

                    if (groups.indexOf(heading) == -1) {
                        groups.push(heading);
                    }
                }
            }
        }
    }

    var stacks = {};
    var cat;
    var index;
    var column;
    var key;

    for (index in groups) {
        if (groups.hasOwnProperty(index)) {
            column = {};

            key = groups[index];
            if (key.indexOf(failField, 0) !== -1 && key.indexOf(failField, 0) !== 0) {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["newStack"] = false;
                column["type"] = "column";
                column["valueField"] = key;

                cat = key.split("/")[0];
                if (stacks.hasOwnProperty(cat) == false)
                    stacks[cat] = {};
                stacks[cat][failField] = column;
            }/* else if (key.indexOf(failField, 0) != -1) {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["newStack"] = false;
                column["type"] = "column";
                column["valueField"] = key;

                cat = key.split("/")[0];
                if (stacks.hasOwnProperty(cat) == false)
                    stacks[cat] = {};
                stacks[cat][failField] = column;
            }*/ else if (key.indexOf(passField, 0) != -1) {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["newStack"] = true;
                column["type"] = "column";
                column["valueField"] = key;

                cat = key.split("/")[0];
                if (stacks.hasOwnProperty(cat) == false)
                    stacks[cat] = {};
                stacks[cat][passField] = column;
            }

        }
    }

    for (cat in stacks) {
        if (stacks.hasOwnProperty(cat)) {
            if (stacks[cat].hasOwnProperty(passField) == false && stacks[cat].hasOwnProperty(failField) == true) {
                stacks[cat][failField]["newStack"] = true;
            } else graphs.push(stacks[cat][passField]);

            if (stacks[cat].hasOwnProperty(failField))
                graphs.push(stacks[cat][failField]);

           // if (stacks[cat].hasOwnProperty(otherField))
             //   graphs.push(stacks[cat][otherField]);
        }
    }

    // set fill colors of bars/columns
    for (i = 0; i < graphs.length; i++) {
        graphs[i].fillColors = colors[i];
        graphs[i].lineColor = colors[i];
    }

    for (index in groups) {
        if (groups.hasOwnProperty(index)) {
            column = {};

            key = groups[index];
            if (key == "Total") {
                column["balloonText"] = ballonText;
                column["id"] = "totalline";
                column["title"] = "Total";
                column["bullet"] = "round";
                column["lineThickness"] = 3;
                column["bulletSize"] = 7;
                column["bulletBorderAlpha"] = 1;
                column["connect"] = true;
                column["useLineColorForBulletBorder"] = true;
                column["bulletBorderThickness"] = 3;
                column["fillAlphas"] = 0;
                column["lineAlpha"] = 1;
                column["valueField"] = key;
                graphs.push(column);
            }
        }
    }

    return graphs;
}

function GameTracker_DrillDownToLevel2(prefix, itemSelection) {
    currentMonth = "all";
    currentStudio = itemSelection;

    var L2_id;

    if (currentPrefix == "GameTracker") {
        $("#" + prefix + "_chartdiv").hide();
        L2_id = prefix + "_L2_chartdiv";
        currentPrefix = prefix + "_L2";
        addLevelChartDiv(prefix, L2_id);
    }
    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    showHideDataTable(prefix, "100%", false);

    var studioName = itemSelection.split('/')[0];
    var yAxisTitle = chartDict[prefix].valueAxes[0].title; // get parent yAxis title


    var data = parseMetricData(restJsonDict, "gameTracker_Month", studio_alias, prefix, itemSelection);
    data = sortGameTrackerL2Data(data, studioName);
    var levelTwoConfig = generateConfig(prefix, "Game Tracker UTP Games" + "- " + studioName, yAxisTitle, "month", currentTheme, false);
    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = GetGameTrackerL2Graphs();

    // Adjust Label Functions 
    for (var i = 0; i < 4; i++) {
        levelTwoConfig.graphs[i].balloonFunction = adjustGameTrackerL2Text;
        levelTwoConfig.graphs[i].labelFunction = adjustGameTrackerL2LabelText;
    }


    levelTwoConfig.valueAxes = [{
        "id": "v1",
        "axisColor": "#88D498",
        "axisThickness": 2,
        "axisAlpha": 1,
        "title": "Percent",
        "stackType": "100%",
        "position": "left",
        "autoGridCount": false,
        "gridAlpha": 0
    },
		{
		    "id": "v2",
		    "axisColor": "#6ca979",
		    "axisThickness": 2,
		    "axisAlpha": 1,
		    "gridAlpha": 0,
		    "stackType": "regular",
		    "position": "right",
		    "title": "Games",
		    "autoGridCount": false
		},
		{
		    "id": "v3",
		    "axisColor": "#6ca979",
		    "ignoreAxisWidth": true,
		    "axisThickness": 0,
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false,
		    "offset": 50,
		    "synchronizeWith": "v2",
		    "synchronizationMultiplier": 1,
		    "position": "right",
		    "autoGridCount": false
		},
		{
		    "id": "v4",
		    "axisColor": "#88D498",
		    "ignoreAxisWidth": true,
		    "axisThickness": 0,
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false,
		    "offset": 50,
		    "synchronizeWith": "v1",
		    "synchronizationMultiplier": 1,
		    "position": "left",
		    "autoGridCount": false
		}];
    levelTwoConfig.categoryAxis.labelFunction = function (string) {
        string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
        string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2');
        string = string.replace(/_/g, " ");
        return string;
    };

    chartConfigDict["GameTracker_L2"] = levelTwoConfig;
    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;


    // let's add a label to go back to yearly data
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');


    //chart.addLabel(
	//	'!140', '30',
	//	"Enable Trend",
	//	undefined,
	//	12,
	//	undefined,
	//	undefined,
	//	undefined,
	//	true,
	//	'javascript:GameTrackerL2ShowRollingAverages();');


    chart.hideGraph(chart.getGraphById("rollingPassedPercent"));
    chart.hideGraph(chart.getGraphById("rollingFailedPercent"));

    chart.addListener("clickGraphItem", L2_handleItemClick);
    chart.addListener("init", chartInit);
    chart.addListener("rendered", L2_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    var dataTable = data; // GetDataForEntry("defectsClosedNewThemes", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
    dataTableOptionsDict[prefix + "_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
}

/*
function GameTrackerL2_handleRendered(event) {

    if (hasInitSubCatMonth) // if need to go to level 3 
    {
        var prefix = event.chart.titles[0].id;

        removeAllActiveClass();
        //$('#navsidebar li.active').removeClass('active');
        var $subnav = $("<li id='link_" + prefix + "_3' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + initMonth + "/" + initSubCategory + "' onclick='loadMetricView(\"" + prefix + "\", 3)'>" + initMonth + "</a></li>");
        $("#link_" + prefix + "_2").after($subnav);

        hasInitSubCatMonth = false;
        //UTPTestCasesPerBuildsL3(prefix, initMonth, initSubCategory, "");

    } else resetCurrentHash();

    removeLoadingDiv();
    removeLoadingDiv();
}
*/

function GetGameTrackerL2Graphs() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
    var graphsData = [
		{
		    "fillColors": "#2ECC71",
		    "lineColor": "#2ECC71",
		    "color": "#ffffff",
		    "title": "Enabled %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "Enabled_percent",
		    "newStack": true
		},
		{
		    "fillColors": "#E74C3C",
		    "lineColor": "#E74C3C",
		    "color": "#ffffff",
		    "title": "Disabled %",
		    "labelText": "[[percents]]%",
		    "columnWidth": 1,
		    "valueAxis": "v1",
		    "valueField": "Disabled_percent",
		    "newStack": false
		},
		{
		    "fillColors": "#27AE60",
		    "lineColor": "#27AE60",
		    "color": "#ffffff",
		    "title": "Enabled",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Enabled",
		    "newStack": true
		},
		{
		    "fillColors": "#C0392B",
		    "lineColor": "#C0392B",
		    "color": "#ffffff",
		    "title": "Disabled",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Disabled",
		    "newStack": false
		}];
    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "balloonText": ballonText,
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["lineColor"] = graphsData[i]["lineColor"];
        graph["color"] = graphsData[i]["color"];
        graph["title"] = graphsData[i]["title"];
        graph["labelText"] = graphsData[i]["labelText"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);

        // do now show values if they are less than 4
        graph["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }


    graph = {
        "balloonText": ballonText,
        "id": "rollingPassedPercent",
        "title": "Rolling Enabled Average",
        "type": "line",
        "bullet": "square",
        "lineColor": "#51D88C",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v3",
        "valueField": "rollingPassedPercent"
    };
    graphs.push(graph);

    graph = {
        "balloonText": ballonText,
        "id": "rollingFailedPercent",
        "title": "Rolling Disabled Average",
        "type": "line",
        "bullet": "square",
        "lineColor": "#FF7364",
        "lineThickness": 3,
        "bulletSize": 7,
        "bulletBorderAlpha": 1,
        "connect": true,
        "useLineColorForBulletBorder": true,
        "bulletBorderThickness": 3,
        "fillAlphas": 0,
        "lineAlpha": 1,
        "valueAxis": "v3",
        "valueField": "rollingFailedPercent"
    };
    graphs.push(graph);

    return graphs;
}

function adjustGameTrackerL2Text(graphDataItem, graph) {

    if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
        return "<b style='font-size:14px;'>" + ((graphDataItem.graph).legendTextReal) + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + (graphDataItem.values.percents).toFixed(2) + "%</b>";
    } else
        return "<b style='font-size:14px;'>" + ((graphDataItem.graph).legendTextReal) + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + graphDataItem.values.value + "</b>";

}

function adjustGameTrackerL2LabelText(graphDataItem, graph) {

    if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
        if (graphDataItem.values.value < 1)
            return "";
        else
            return (graphDataItem.values.percents).toFixed(2) + "%";
    } else {
        if (graphDataItem.values.value === 0)
            return "";
        else
            return graphDataItem.values.value;
    }
}

function sortGameTrackerL2Data(oriData, studio) {
    var data = [];

    for (var i = 0; i < oriData.length; i++) {
        var monthData = {};

        monthData["month"] = (oriData[i])["month"];
        var tot = 0;

        if ((oriData[i])[studio + "/Enabled"] !== undefined) {
            tot = tot + (oriData[i])[studio + "/Enabled"];
            monthData["Enabled"] = (oriData[i])[studio + "/Enabled"];
        } else {
            monthData["Enabled"] = 0;
        }

        if ((oriData[i])[studio + "/Disabled"] !== undefined) {
            tot = tot + (oriData[i])[studio + "/Disabled"];
            monthData["Disabled"] = (oriData[i])[studio + "/Disabled"];
        } else {
            monthData["Disabled"] = 0;
        }


        if ((oriData[i])[studio + "/Enabled"] !== undefined)
            monthData["Enabled_percent"] = (((oriData[i])[studio + "/Enabled"] / tot) * 100).toFixed(2);
        else {
            monthData["Enabled_percent"] = 0;
        }

        if ((oriData[i])[studio + "/Disabled"] !== undefined)
            monthData["Disabled_percent"] = (((oriData[i])[studio + "/Disabled"] / tot) * 100).toFixed(2);
        else {
            monthData["Disabled_percent"] = 0;
        }
        monthData["studio"] = studio;
        data.push(monthData);
    }

    return data;
}

function GameTrackerL2ShowRollingAverages() {
    var chart = chartDict["GameTracker_L2"];

    //clear Graphs
    for (var i = 0; i < 13; i++) {

        if ((chart.dataProvider[i])["rollingPassedPercent"] !== undefined)
            delete (chart.dataProvider[i])["rollingPassedPercent"];

        if ((chart.dataProvider[i])["rollingFailedPercent"] !== undefined)
            delete (chart.dataProvider[i])["rollingFailedPercent"];

    }

    //update graphs with new %s
    var start = chart.start;
    var end = chart.end;
    calculateRollingPercentagesGameTrackerL2(chart, start, end);

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPassedPercent"));
    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingFailedPercent"));

    chart.invalidateSize();
    chart.validateNow();
}

function calculateRollingPercentagesGameTrackerL2(chart, start, end) {
    var tot_p = 0,
		tot_f = 0,
		tot_o = 0;
    var month = 0;

    //iterate through the month
    for (var i = start; i <= end; i++) {
        month++;

        tot_p = tot_p + (chart.dataProvider[i]).Enabled;
        tot_f = tot_f + (chart.dataProvider[i]).Disabled;

        (chart.dataProvider[i])["rollingPassedPercent"] = (tot_p / month).toFixed(2);
        (chart.dataProvider[i])["rollingFailedPercent"] = (tot_f / month).toFixed(2);
        (chart.dataProvider[i])["rollingOtherPercent"] = (tot_o / month).toFixed(2);
    }
}

function GameTracker_DrillDownToLevel3(prefix, month, subcategory, fpstatus) {//currentPrefix, jsonTable, utp_table_name, monthname, studio) {
    currentMonth = month;
    currentStudio = subcategory;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    var returnData = GetGameTrackerDataL3(prefix, restJsonDict, chartJSONTableDict[prefix], month, subcategory, studio_alias);
    var levelThreeConfig = GameTrackerL3GenerateConfig(currentPrefix, "Game Tracker UTP Files - " + subcategory + " for " + currentMonth, "builds", "game", currentTheme, false);
    //var levelThreeConfig = generateConfig(prefix, "Defects Found by Tester Type - " + subcategory + " for " + month, "builds", "game", currentTheme, false);
    levelThreeConfig.dataProvider = returnData;
    levelThreeConfig.legend.enabled = true;
    levelThreeConfig.legend.marginTop = 10;
    levelThreeConfig.graphs =  GetGraphsGameTrackerL3();
  
    levelThreeConfig.valueAxes = [
       {
           "id": "v2",
           "axisColor": "#6ca979",
           "axisThickness": 2,
           "axisAlpha": 1,
           "gridAlpha": 0,
           "stackType": "regular",
           "position": "left",
           "title": "Builds",
           "autoGridCount": false
       }];


    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };

    levelThreeConfig.categoryAxis.autoWrap = true;

    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    chartConfigDict[prefix+"_L3"] = levelThreeConfig;
    //var L3_id = prefix+"_L3_chartdiv";
    //var prefix = "utpTestCases";
    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addLabel(
		'!200', '20',
		"Sort by Builds",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortGameTrackerL3("' + currentPrefix + '", true);');

    chart.addListener("clickGraphItem", GameTracker_L3_handleItemClick);
    chart.addListener("init", UTPchartInit);
    chart.addListener("rendered", GameTrackerTc_L3_rendered);
    $("#loadingdiv").remove(); // remove loading div
    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = returnData;

    level++; // increase global level indicator

    //resetCurrentHash();


    var dataTable;
    dataTable = returnData; // GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsReleased"], month, subcategory, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    //clearTable(prefix + "_dataTable");
    //drawTable(dataTable, prefix + "_dataTable");
}

function SortGameTrackerL3(prefix, sortByDefects) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix];
    var labelIndex;

    var switchText = "Sort by Builds";

    if (sortByDefects) {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.builds > b.builds) return -1;
            if (a.builds < b.builds) return 1;
            return 0;
        });

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
        chart.allLabels[labelIndex].text = "Sort By Game";
        chart.allLabels[labelIndex].url = 'javascript:SortGameTrackerL3("' + prefix + '", false);';
    } else {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.game < b.game) return -1;
            if (a.game > b.game) return 1;
            return 0;
        });
        labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort By Game");
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortGameTrackerL3("' + prefix + '", true);';
    }
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}

function GameTrackerTc_L3_rendered(event) {
    chart = event.chart;
    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", UTPTestCase_L3_handleCategoryClick);
    resetCurrentHash();

    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo); // zoom into the last 5
    chart.invalidateSize();
}

function GetGameTrackerDataL3(chartSelection, jsonTable, defect_table_name, monthname, sub_category, studio_alias) {
    var month;
    var studio;
    var datarow;
    var row;
    var chartTable = [];

    if (monthname == "Current Month")
        month = 12;
    else month = getMonthFromString(monthname);


    var defectTable = jsonTable[defect_table_name + month.toString()];

    for (var i=0; i<defectTable.length;i++) {
        if (defectTable[i] !== null) {
            datarow = defectTable[i];
            var studioVal = datarow.Studio;

            if (studio_alias) {
                if (studio_alias.hasOwnProperty(datarow.Studio)) {
                    studioVal = studio_alias[datarow.Studio];
                }
            }

            if (studioVal === sub_category || sub_category==="Total") {
                row = [];
                row["game"] = "GI" + datarow.Family + datarow.Number + datarow.Revision;
                row["Enabled"] = 0;
                row["Disabled"] = 0;
                row["builds"] = datarow.Builds.length;
                for (var j = 0; j < datarow.Builds.length; j++) {
                    if (datarow.Builds[j].UtpEnabled === "1") {
                        row["Enabled"]++;
                    } else {
                        row["Disabled"]++;

                    }
                }

                chartTable.push(row);
            }

        }
    }

    return chartTable;
}

function GameTrackerL3GenerateConfig(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
    var config = {
        "type": "serial",
        "theme": theme,
        "categoryField": catField,
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "valueAxes": [
            {
                "id": "v1",
                "title": yAxisTitle
            }
        ],
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": chartId,
                "size": 15,
                "text": chartTitle
            }
        ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "chartScrollbar": {
            "graph": "totalline",
            "graphType": "line",
            "color": "#FFFFFF",
            "selectedBackgroundColor": "#6F7D8A",
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": "#FFFFFF",
            "autoGridCount": true
        },
        "export": {
            "enabled": true
        },
        "responsive": {
            "enabled": true,
            "rules": [
                {
                    "maxWidth": 1000,
                    "overrides": {
                        "legend": {
                            "enabled": false
                        }
                    }
                },
                {
                    "maxWidth": 500,
                    "overrides": {
                        "valueAxes": {
                            "inside": true
                        }
                    }
                }
            ]
        }
    };

    config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
    config.export["backgroundColor"] = "#222222";
    config.export.drawing.enabled = true;
    config.export.drawing.color = "#FFFFFF";

    config.export.menu[0].menu.push({
        "label": "Get URL To Chart",
        "click": GetUrlForCurrentChart
    });

    if (useStackCharts) {
        config.valueAxes[0].stackType = "regular";
    }

    return config;
}

function GetGraphsGameTrackerL3() {
    var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
    var graphsData = [

		{
		    "fillColors": "#27AE60",
		    "lineColor": "#27AE60",
		    "color": "#ffffff",
		    "title": "Enabled",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Enabled",
		    "newStack": true
		},
        {
		    "fillColors": "#C0392B",
		    "lineColor": "#C0392B",
		    "color": "#ffffff",
		    "title": "Disabled",
		    "labelText": "[[value]]",
		    "columnWidth": 0.85,
		    "valueAxis": "v2",
		    "valueField": "Disabled",
		    "newStack": false
		}];
    var graphs = [];
    var graph = {};
    for (var i = 0; i < graphsData.length; i++) {
        graph = {
            "balloonText": ballonText,
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "type": "column",
            "labelPosition": "middle",
            "columnWidth": 1,
            "fontSize": 10,
            "showAllValueLabels": true
        };
        //graph.balloonFunction = adjustNewThemeFirstPassL2Text;
        graph["fillColors"] = graphsData[i]["fillColors"];
        graph["lineColor"] = graphsData[i]["lineColor"];
        graph["color"] = graphsData[i]["color"];
        graph["title"] = graphsData[i]["title"];
        graph["labelText"] = graphsData[i]["labelText"];
        graph["valueAxis"] = graphsData[i]["valueAxis"];
        graph["valueField"] = graphsData[i]["valueField"];
        graph["newStack"] = graphsData[i]["newStack"];
        graph["columnWidth"] = graphsData[i]["columnWidth"];
        graphs.push(graph);

        // do now show values if they are less than 4
        graph["labelFunction"] = function (item) {
            if (item.values.value < 1)
                return "";
            else
                return item.values.value;
        };
    }

    return graphs;

}