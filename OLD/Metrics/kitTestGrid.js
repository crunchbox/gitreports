/*****************************************************************************\

 Javascript "Kit Release LIst" library

 @version: 0.1 - 2016.05.06
 @author: Sean M McClain

\*****************************************************************************/


function get_KitTestGrid() {
	if (loadedMetrics["kittestlist"] != true) {
		get_MetricsData("kittestlist", "kittestlist");
		return;
	}

	if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Kit Test Grid");

	var dataTableID = "kittestlist_dataTable";

	dataTableDataDict[dataTableID] = restJsonDict[chartJSONTableDict["kittestlist"]];
	dataTableOptionsDict[dataTableID] = {
		"scrollY": "80vh",
		"scrollX": true,
		"scrollCollapse": true,
		"pageLength": 15,
		"paging": true,
		"dom": 'frtBp',
		"buttons": ['copyHtml5', 'csvHtml5'],
		"responsive": true,
		"columnDefs": [{ responsivePriority: 1, targets: 6 },
                       { responsivePriority: 2, targets: 0 },
		               { responsivePriority: 3, targets: 1 },
		               { responsivePriority: 4, targets: 2 },
		               { responsivePriority: 5, targets: 3 },
		               { responsivePriority: 6, targets: 4 },
		               { responsivePriority: 7, targets: 5 },
		               { className: "none", "targets": [-9, -8, -7, -6, -5, -4, -3, -2, -1] }],
		"order": [[6, "desc"]]
	};

	dataTableIsVisible[dataTableID] = true; // set visible to opposite of show so the below code toggles it

	var classList = "row-border stripe cell-border compact hover";

	currentPrefix = "kittestlist";
	currentMonth = "all";

	if ($("#" + currentPrefix + "_Title").length == 0) {
	    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('searchMetricTitle');

	    title_div.html("Search Kits in Test - " + dataTableDataDict[dataTableID].length + " Kits Total");
	    var goBackLink = "<div style='padding-bottom:20px;'><a href='/' style='font-size:14px; color:#" + scrollbarLineColor + "; font-weight:700;'>< Go back</a></div>";
	    $("#metric_kittestlist_header").prepend(goBackLink);
	    $("#metric_kittestlist_header").prepend(title_div);

	}
    

	var sortedMonths = getSortedMonthList();

	var index;
	var monNum = 1;
	for (index = sortedMonths.length - 1; index >= 0; index--) {
		$("#kittestlist_MonthDropDown").append($('<option>', {
			value: monNum
		}).text(sortedMonths[index]));
		monNum++;
	}

	var studios = getStudioAliasList(studio_alias);
	for (index = 0; index < studios.length; index++) {
		$("#kittestlist_StudioDropDown").append($('<option>', {
			value: studios[index]
		}).text(studios[index]));
	}

	for (var i = 0; i < dataTableDataDict[dataTableID].length; i++) {
	    ((dataTableDataDict[dataTableID])[i])["Components"].replaceAll(",", ", ");
	}

	var table = drawTable(dataTableDataDict[dataTableID], dataTableID, "100%", dataTableOptionsDict[dataTableID], classList);
	dataTableHTMLTableDict["kittestlist"] = table;

	$('#kittestlist_dataTable_filter label').css("width", "100%");
	$('#kittestlist_dataTable_filter label input').css("width", "90%");
	$('#kittestlist_dataTable_filter').css("width", "inherit");
	$('#kittestlist_dataTable_filter').css("margin", "auto");
	$('#kittestlist_dataTable_filter').css("float", "none");

	table.$("td:not(:first-child)").click(function () {
	    var data = dataTableHTMLTableDict["kittestlist"].row($(this).closest('tr')).data();
		$.fn.dataTable.ext.search = [];
		showAndLoadScoreCard(currentPrefix, data[0], currentMonth);
	});

	table.$("td:not(:first-child)").css('cursor', 'pointer');

	$("#kittestlist_StudioDropDown_Div").css("visibility", "visible");
	$("#kittestlist_MonthDropDown_Div").css("visibility", "visible");

	$.fn.dataTable.ext.search = [];
	$.fn.dataTable.ext.search.push(kittest_emptyFilter);
	$.fn.dataTable.ext.search.push(kittest_emptyFilter);

	$("#kittestlist_StudioDropDown").change(kittest_UpdateStudioList);
	$("#kittestlist_MonthDropDown").change(kittest_UpdateMonthList);

	removeLoadingDiv();

}

var kittest_currentMonthFilter = 0;
var kittest_currentStudioFilter = "All Studios";

function kittest_UpdateStudioList() {
	var selectedValue = jQuery(this).val();
	kittest_currentStudioFilter = selectedValue;

	if (selectedValue != "All Studios") {
		$.fn.dataTable.ext.search[0] = kittest_studioBaseFilter;
	} else $.fn.dataTable.ext.search[0] = kittest_emptyFilter;
	dataTableHTMLTableDict["kittestlist"].draw();
}

function kittest_UpdateMonthList() {
	var selectedValue = jQuery(this).val();
	kittest_currentMonthFilter = selectedValue;

	if (selectedValue > 0) {
		$.fn.dataTable.ext.search[1] = kittest_dateBaseFilter;
	} else $.fn.dataTable.ext.search[1] = kittest_emptyFilter;
	dataTableHTMLTableDict["kittestlist"].draw();
}

function kittest_dateBaseFilter(settings, data, dataIndex) {
	var date = ISODateParse(data[5]);
	var targetDate = new Date();
	targetDate.setMonth(targetDate.getMonth() - (kittest_currentMonthFilter - 1));
	if (date.getMonth() == targetDate.getMonth() && date.getYear() == targetDate.getYear())
		return true;
	return false;
}

function kittest_studioBaseFilter(settings, data, dataIndex) {
	if (data[4] == kittest_currentStudioFilter)
		return true;
	return false;
}

function kittest_emptyFilter(settings, data, dataIndex) {
	return true;
}