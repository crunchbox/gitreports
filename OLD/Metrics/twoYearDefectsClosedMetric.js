var savedData = {};

function get_TwoYearDefectsFoundMetrics() {
	if (loadedMetrics["defectsClosedQ"] != true) {
	    get_MetricsData("defectsClosedQ", "QuartersPhaseFound", true);
		return;
	}

	if (analytics != null)
		analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Two Year Defects Closed");

	clearChart("QuartersPhaseFound");
	showHideDataTable("QuartersPhaseFound_dataTable", "100%", false);

	var config;
	if (chartConfigDict["QuartersPhaseFound"] == null) {
	    chartData = parseMetricDataByQuarter(restJsonDict, "DefectsClosed_Quarter", studio_alias, "QuartersPhaseFound");
		chartGraphs = GetGraphs(chartData, "quarter");
		dataTableDataDict["QuartersPhaseFound_dataTable"] = chartData;
		config = generateConfig("QuartersPhaseFound", "Two Year Defects Closed", "Defects", "quarter", currentTheme, false);
		config.dataProvider = chartData;
		config.graphs = chartGraphs;

		chartConfigDict["QuartersPhaseFound"] = config;
		chartDataDict["QuartersPhaseFound"] = chartData;
	} else config = chartConfigDict["QuartersPhaseFound"];

	chart = AmCharts.makeChart("QuartersPhaseFound_chartdiv", config, 100);

	chartResizerDict["QuartersPhaseFound"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	$("loadingdiv").remove(); // remove loading div

	chartDict["QuartersPhaseFound"] = chart; // store chart

	if (dataTableIsVisible["QuartersPhaseFound" + "_dataTable"] !== undefined && dataTableIsVisible["QuartersPhaseFound" + "_dataTable"] === true)
	    showHideDataTable("QuartersPhaseFound" + "_dataTable");
}

function parseByTwoYearDefectsFoundToChartRow(row, viewTable, studio_alias, sub_category) {
	var tempInt = 0;
	var tempStudio;
	var rollingTotal = 0;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];

			if (studio_alias.hasOwnProperty(datarow["Studio"]))
				tempStudio = studio_alias[datarow["Studio"]];
			else tempStudio = datarow["Studio"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;
				row[tempStudio] = tempInt + parseInt(datarow["Count"]);
				rollingTotal += parseInt(datarow["Count"]);
			}
		}
	}

	if (sub_category == undefined && sub_category != "Total")
		row["Total"] = rollingTotal;

	return row;
}

function TwoYearDefectsFound_DrillDownToLevel2(prefix, itemSelection) {
	currentQuarter = "all";
	currentStudio = itemSelection;

	var L2_id;

	if (currentPrefix == "QuartersPhaseFound") {
		$("#" + prefix + "_chartdiv").hide();
		L2_id = prefix + "_L2_chartdiv";
		currentPrefix = prefix + "_L2";
		addLevelChartDiv(prefix, L2_id);
	}

	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
	    showHideDataTable(prefix + "_dataTable");
	showHideDataTable(prefix, "100%", false);

	var chartTitle = chartDict[prefix].titles[0].text; // get parent chart title
	var chartTitle_firstpart = chartTitle.split('-')[0];

	var data = GetDefectsFound2YPercentAndTotal(restJsonDict, "DefectsClosed_Quarter", currentStudio, studio_alias);

	var levelTwoConfig = generateConfig(prefix, chartTitle_firstpart + "- " + currentStudio, "Defects Found", "quarter", currentTheme, false);
	levelTwoConfig.dataProvider = data;
	levelTwoConfig.graphs = GetDefectsFound2YGraphsL2();

	var i;

	// Adjust Label Functions 
	for (i = 0; i < 6; i++) {
		levelTwoConfig.graphs[i].balloonFunction = adjustDefectsFound2YL2Text;
		levelTwoConfig.graphs[i].labelFunction = adjustDefectsFound2YL2LabelText;
	}

	for (i = 6; i < 12; i++) {
		levelTwoConfig.graphs[i].balloonFunction = adjustDefectsFound2YL2Text;
	}


	levelTwoConfig = setColumnSpacingFirstPass(levelTwoConfig);

	//levelTwoConfig.columnSpacing = -15;
	levelTwoConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
    },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Count",
			"autoGridCount": false
		},
		{
			"id": "v3",
			"axisColor": "#6ca979",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v2",
			"synchronizationMultiplier": 1,
			"position": "right",
			"autoGridCount": false
		},
		{
			"id": "v4",
			"axisColor": "#88D498",
			"ignoreAxisWidth": true,
			"axisThickness": 0,
			"axisAlpha": 0,
			"gridAlpha": 0,
			"labelsEnabled": false,
			"offset": 50,
			"synchronizeWith": "v1",
			"synchronizationMultiplier": 1,
			"position": "left",
			"autoGridCount": false
		}];



	chartConfigDict["QuartersPhaseFound_L2"] = levelTwoConfig;
	var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
	chartResizerDict[currentPrefix] = true;

	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

	//chart.addLabel(
	//	'!150', '30',
	//	"Enable Trend",
	//	undefined,
	//	12,
	//	undefined,
	//	undefined,
	//	undefined,
	//	true,
	//	'javascript:DefectFound2YShowRollingPercents();');


	chart.hideGraph(chart.getGraphById("rollingPrePAPercent"));
	chart.hideGraph(chart.getGraphById("rollingPostPAPercent"));
	chart.hideGraph(chart.getGraphById("rollingPaPercent"));
	chart.hideGraph(chart.getGraphById("rollingEarlyTestingPercent"));
	chart.hideGraph(chart.getGraphById("rollingMathPercent"));
	chart.hideGraph(chart.getGraphById("rollingPaytablePercent"));

	chart.addListener("clickGraphItem", L2_handleItemClick);
	chart.addListener("init", chartInit);
	chart.addListener("rendered", L2_handleRendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = data;


	level++; // increase global level indicator

	var dataTable = data; // GetDataForEntry("QuartersPhaseFound", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
	dataTableDataDict[prefix + "_dataTable"] = dataTable;
	dataTableOptionsDict[prefix + "_dataTable"] = {
		"scrollY": 200,
		"scrollX": true,
		"dom": 'rtBfp',
		"buttons": ['copyHtml5', 'csvHtml5']
	};
}

function GetDefectsFound2YPercentAndTotal(jsonTable, table_name, sub_category, studio_alias) {
	var kitTable;
	var fpTable;
	var chartTable = [];
	var currentFiscal;
	var row;
	var dataStudio;
	var studio = sub_category;
	var thisYear = new Date().getFullYear();
	var rotated_quarters = getQuarterListObject();

	var twelveMonthRollingTotal = 0;
	var yearToDateRollingTotal;

	var allquarterPhases = {};
	for (quarter = 0; quarter < 8; quarter++) {
		row = {};

		row["quarter"] = rotated_quarters[quarter]["quarter"];
		if (rotated_quarters[quarter]["quarter"] == "Q1 " + thisYear)
			yearToDateRollingTotal = 0;

		row["prePA"] = 0;
		row["earlyTesting"] = 0;
		row["postPA"] = 0;
		row["pa"] = 0;
		row["math"] = 0;
		row["paytable"] = 0;
		row["prePA_percent"] = 0;
		row["earlyTesting_percent"] = 0;
		row["postPA_percent"] = 0;
		row["pa_percent"] = 0;
		row["math_percent"] = 0;
		row["paytable_percent"] = 0;
		row["studio"] = studio;

		fpTable = jsonTable[table_name + rotated_quarters[quarter]["num"]];

		var totPhasesPA = {};
		var totPhasesPre = {};
		var totPhasesPost = {};
		var totPhasesEarly = {};
		var totPhasesMath = {};
		var totPhasesPaytable = {};

		var total_TempVal = 0;
		for (var dataindex in fpTable) {
			if (fpTable.hasOwnProperty(dataindex)) {
				datarow = fpTable[dataindex];
				dataStudio = datarow["Studio"];

				if (studio_alias) {
					if (studio_alias.hasOwnProperty(dataStudio))
						dataStudio = studio_alias[dataStudio];
				}

				if (studio == "Total" || studio == dataStudio) {
					if (datarow["Category"] == 'New Game Theme' || datarow["Category"] == 'Localization' || datarow["Category"] == 'New System') {
						twelveMonthRollingTotal += datarow["Count"];
						total_TempVal += datarow["Count"];

						if (yearToDateRollingTotal != undefined)
							yearToDateRollingTotal += datarow["Count"];


						if (datarow["PhaseFoundGroup"] === "PA") {
							row["pa"] += datarow["Count"];

							if (totPhasesPA[datarow["Phase"]] === undefined)
								totPhasesPA[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPA[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "PRE PA") {
							row["prePA"] += datarow["Count"];

							if (totPhasesPre[datarow["Phase"]] === undefined)
								totPhasesPre[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPre[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "EARLY TESTING") {
							row["earlyTesting"] += datarow["Count"];

							if (totPhasesEarly[datarow["Phase"]] === undefined)
								totPhasesEarly[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesEarly[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "POST PA") {
							row["postPA"] += datarow["Count"];

							if (totPhasesPost[datarow["Phase"]] === undefined)
								totPhasesPost[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPost[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "MATH") {
							row["math"] += datarow["Count"];

							if (totPhasesMath[datarow["Phase"]] === undefined)
								totPhasesMath[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesMath[datarow["Phase"]] += datarow["Count"];
						} else if (datarow["PhaseFoundGroup"] === "PAYTABLE") {
							row["paytable"] += datarow["Count"];

							if (totPhasesPaytable[datarow["Phase"]] === undefined)
								totPhasesPaytable[datarow["Phase"]] = datarow["Count"];
							else
								totPhasesPaytable[datarow["Phase"]] += datarow["Count"];
						}
					}
				}
			}
		}

		totPhasesPA["Total"] = row["pa"];
		totPhasesPre["Total"] = row["prePA"];
		totPhasesPost["Total"] = row["postPA"];
		totPhasesEarly["Total"] = row["earlyTesting"];
		totPhasesMath["Total"] = row["math"];
		totPhasesPaytable["Total"] = row["paytable"];

		var allphaseData = {
			"PA": totPhasesPA,
			"Pre PA": totPhasesPre,
			"Post PA": totPhasesPost,
			"Early Testing": totPhasesEarly,
			"Math": totPhasesMath,
			"Paytable": totPhasesPaytable
		};

		allquarterPhases[row["quarter"]] = allphaseData;

		var total = row["pa"] + row["prePA"] + row["earlyTesting"] + row["postPA"] + row["math"] + row["paytable"];
		if (total > 0) {
			row["pa_percent"] = ((row["pa"] / total) * 100).toFixed(2);
			row["prePA_percent"] = ((row["prePA"] / total) * 100).toFixed(2);
			row["earlyTesting_percent"] = ((row["earlyTesting"] / total) * 100).toFixed(2);
			row["postPA_percent"] = ((row["postPA"] / total) * 100).toFixed(2);
			row["math_percent"] = ((row["math"] / total) * 100).toFixed(2);
			row["paytable_percent"] = ((row["paytable"] / total) * 100).toFixed(2);
		}
		row["rollingTotal"] = twelveMonthRollingTotal;
		if (yearToDateRollingTotal != undefined)
			row["yearToDateRollingDate"] = yearToDateRollingTotal;

		chartTable.push(row);
	}


	extraGraphData["QuartersPhaseFound"] = allquarterPhases;

	return chartTable;
}

function GetDefectsFound2YGraphsL2() {
	var ballonText = "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	var graphsData = [
		{
			"fillColors": "#ea5a66",
			"lineColor": "#ea5a66",
			"color": "#ffffff",
			"title": "Early Testing %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "earlyTesting_percent",
			"newStack": true
		},
		{
			"fillColors": "#56d37a",
			"lineColor": "#56d37a",
			"color": "#ffffff",
			"title": "Math %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "math_percent",
			"newStack": false
		},
		{
			"fillColors": "#2686b9",
			"lineColor": "#2686b9",
			"color": "#ffffff",
			"title": "Paytable %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "paytable_percent",
			"newStack": false
        },
		{
			"fillColors": "#922a93",
			"lineColor": "#922a93",
			"color": "#ffffff",
			"title": "PA %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "pa_percent",
			"newStack": false
        },
		{
			"fillColors": "#e46f3e",
			"lineColor": "#e46f3e",
			"color": "#ffffff",
			"title": "Post PA %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "postPA_percent",
			"newStack": false
        },
		{
			"fillColors": "#e4c23e",
			"lineColor": "#e4c23e",
			"color": "#ffffff",
			"title": "Pre PA %",
			"labelText": "[[percents]]%",
			"columnWidth": 1,
			"valueAxis": "v1",
			"valueField": "prePA_percent",
			"newStack": false
        },
		{
			"fillColors": "#E84855",
			"lineColor": "#E84855",
			"color": "#ffffff",
			"title": "Early Testing",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "earlyTesting",
			"newStack": true
        },
		{
			"fillColors": "#44cf6c",
			"lineColor": "#44cf6c",
			"color": "#ffffff",
			"title": "Math",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "math",
			"newStack": false
        },
		{
			"fillColors": "#0e79b2",
			"lineColor": "#0e79b2",
			"color": "#ffffff",
			"title": "Paytable",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "paytable",
			"newStack": false
        },
		{
			"fillColors": "#861388",
			"lineColor": "#861388",
			"color": "#ffffff",
			"title": "PA",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "pa",
			"newStack": false
        },
		{
			"fillColors": "#e16029",
			"lineColor": "#e16029",
			"color": "#ffffff",
			"title": "Post PA",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "postPA",
			"newStack": false
        },
		{
			"fillColors": "#e1bc29",
			"lineColor": "#e1bc29",
			"color": "#ffffff",
			"title": "Pre PA",
			"labelText": "[[value]]",
			"columnWidth": 0.85,
			"valueAxis": "v2",
			"valueField": "prePA",
			"newStack": false
        }];
	var graphs = [];
	var graph = {};
	var i;

	for (i = 0; i < graphsData.length; i++) {
		graph = {
			"balloonText": ballonText,
			"fillAlphas": 0.8,
			"lineAlpha": 0.3,
			"type": "column",
			"labelPosition": "middle",
			"columnWidth": 1,
			"fontSize": 10,
			"showAllValueLabels": true
		};
		//graph.balloonFunction = adjustNewThemeFirstPassL2Text;
		graph["fillColors"] = graphsData[i]["fillColors"];
		graph["lineColor"] = graphsData[i]["lineColor"];
		graph["color"] = graphsData[i]["color"];
		graph["title"] = graphsData[i]["title"];
		graph["labelText"] = graphsData[i]["labelText"];
		graph["valueAxis"] = graphsData[i]["valueAxis"];
		graph["valueField"] = graphsData[i]["valueField"];
		graph["newStack"] = graphsData[i]["newStack"];
		graph["columnWidth"] = graphsData[i]["columnWidth"];
		graphs.push(graph);

		// do now show values if they are less than 4
		graph["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return item.values.value;
		};
	}

	/*jshint loopfunc: true */ // this function in a loop is fine

	// Add Label functions to round percentages
	for (i = 0; i < 6; i++) {
		graphs[i]["labelFunction"] = function (item) {
			if (item.values.value < 1)
				return "";
			else
				return Math.round(item.values.value).toString() + "%";
		};
	}
	// start line graphs (rolling totals)
	/*
	graph = {
	    "balloonText": ballonText,
	    "id": "fyRollingTotal",
	    "title": "Fiscal Year Rolling Total",
	    "bullet": "round",
	    "lineColor": "#cc9900",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "connect": true,
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "valueAxis": "v3",
	    "valueField": "yearToDateRollingDate",
	    "stackable": false
	};
	graphs.push(graph);


	graph = {
	    "balloonText": ballonText,
	    "id": "rollingTotal",
	    "title": "12 Month Rolling Total",
	    "bullet": "triangleUp",
	    "lineColor": "#993399",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "connect": true,
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "valueAxis": "v3",
	    "valueField": "rollingTotal",
	    "stackable": false
	};
	graphs.push(graph);
	*/
	graph = {
		"balloonText": ballonText,
		"id": "rollingEarlyTestingPercent",
		"title": "Rolling Early Testing %",
		"bullet": "square",
		"lineColor": "#f19199",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingEarlyTestingPercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingMathPercent",
		"title": "Rolling Math Testing %",
		"bullet": "square",
		"lineColor": "#8ee2a6",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingMathPercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingPaytablePercent",
		"title": "Rolling Paytable Testing %",
		"bullet": "square",
		"lineColor": "#66cac0",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPaytablePercent"
	};
	graphs.push(graph);

	graph = {
		"balloonText": ballonText,
		"id": "rollingPaPercent",
		"title": "Rolling PA %",
		"bullet": "square",
		"lineColor": "#b671b7",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPaPercent",
		"stackable": false
	};
	graphs.push(graph);


	graph = {
		"balloonText": ballonText,
		"id": "rollingPostPAPercent",
		"title": "Rolling Post PA %",
		"bullet": "square",
		"lineColor": "#ed9f7e",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPostPAPercent",
		"stackable": false
	};
	graphs.push(graph);


	graph = {
		"balloonText": ballonText,
		"id": "rollingPrePAPercent",
		"title": "Rolling Pre PA %",
		"bullet": "square",
		"lineColor": "#edd67e",
		"lineThickness": 3,
		"bulletSize": 7,
		"bulletBorderAlpha": 1,
		"connect": true,
		"useLineColorForBulletBorder": true,
		"bulletBorderThickness": 3,
		"fillAlphas": 0,
		"lineAlpha": 1,
		"valueAxis": "v4",
		"valueField": "rollingPrePAPercent",
		"stackable": false
	};
	graphs.push(graph);


	return graphs;
}

function adjustDefectsFound2YL2Text(graphDataItem, graph) {
    var data = extraGraphData["QuartersPhaseFound"];
	var quarter = graphDataItem.category;
	var section = (graph.legendTextReal).replace(" %", "");

	var percentage = true;
	if ((graph.legendTextReal).search(" %") === -1)
		percentage = false;

	var phaseData = (data[quarter])[section];

	var tot = ((data[quarter])["PA"])["Total"] + ((data[quarter])["Pre PA"])["Total"] + ((data[quarter])["Post PA"])["Total"] + ((data[quarter])["Early Testing"])["Total"] + ((data[quarter])["Paytable"])["Total"] + ((data[quarter])["Math"])["Total"];

    if (percentage === true)
        graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;
    else
        graphDataItem.values.value = Math.round(graphDataItem.values.value * 100) / 100;

	var test;

	if (percentage === true)
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + Math.round(graphDataItem.values.percents * 100) / 100 + "%</b>";
	else
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + ((data[quarter])[section])["Total"] + "</b>";

	var property = 0;
	for (property in phaseData) {
		if (property !== "Total") {
			text += "<br>";

			if (percentage === true)
				text += property.substring(3) + ": <b style='font-size:13px;'>" + ((phaseData[property] / tot) * 100).toFixed(2) + "%</b>";
			else
				text += property.substring(3) + ": <b style='font-size:13px;'>" + phaseData[property] + "</b>";
		}
	}

	return text;
}

function adjustDefectsFound2YL2LabelText(graphDataItem, graph) {
	if (((graphDataItem.graph).legendTextReal).search(" %") !== -1) {
		//   graphDataItem.values.value = (graphDataItem.values.percents).toFixed(2);

		//graphDataItem.values.value = (graphDataItem.values.percents).toFixed(2);
		if (graphDataItem.values.value < 1)
			return "";
		else
			return (graphDataItem.values.percents).toFixed(2) + "%";
	} else
		return graphDataItem.values.value;
}

function DefectFound2YShowRollingPercents() {
    var chart = chartDict["QuartersPhaseFound_L2"];

	//clear Graphs
	for (var i = 0; i < 8; i++) {

		if ((chart.dataProvider[i])["rollingPrePAPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPrePAPercent"];

		if ((chart.dataProvider[i])["rollingPostPAPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPostPAPercent"];

		if ((chart.dataProvider[i])["rollingPaPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPaPercent"];

		if ((chart.dataProvider[i])["rollingEarlyTestingPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingEarlyTestingPercent"];

		if ((chart.dataProvider[i])["rollingMathPercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingMathPercent"];

		if ((chart.dataProvider[i])["rollingPaytablePercent"] !== undefined)
			delete(chart.dataProvider[i])["rollingPaytablePercent"];
	}

	//update graphs with new %s
	var start = chart.start;
	var end = chart.end;
	calculateRollingPercentagesDefectsFoundNT(chart, start, end);

	//show graphs
	if ((chart.legend.legendData[0]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPrePAPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPrePAPercent"));

	//Turn off rejected if  "rejected %" are off
	if ((chart.legend.legendData[1]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPostPAPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPostPAPercent"));

	//Turn off pending if "pending %" are off
	if ((chart.legend.legendData[2]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPaPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPaPercent"));

	if ((chart.legend.legendData[3]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingEarlyTestingPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingEarlyTestingPercent"));

	if ((chart.legend.legendData[4]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingMathPercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingMathPercent"));

	if ((chart.legend.legendData[5]).columnsArray.length > 0)
		chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPaytablePercent"));
	else
		chartDict[currentPrefix].hideGraph(chart.getGraphById("rollingPaytablePercent"));


	chart.invalidateSize();
	chart.validateNow();
}

function calculateRollingPercentagesDefectsFound2Y(chart, start, end) {

	var tot_pre = 0,
		tot_post = 0,
		tot_pa = 0,
		tot_early = 0,
		tot_math = 0,
		tot_paytable = 0;
	var tot = 0;

	//iterate through the quarter
	for (var i = start; i <= end; i++) {

		if ((chart.legend.legendData[0]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).prePA;

		//check if rejected or "rejected %" is active
		if ((chart.legend.legendData[1]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).postPA;

		//check if pending or "pending %" is active
		if ((chart.legend.legendData[2]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).pa;

		//check if rejected or "na %" is active
		if ((chart.legend.legendData[3]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).earlyTesting;

		if ((chart.legend.legendData[4]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).math;

		if ((chart.legend.legendData[5]).columnsArray.length > 0)
			tot += (chart.dataProvider[i]).paytable;

		tot_pre += (chart.dataProvider[i]).prePA;
		tot_post += (chart.dataProvider[i]).postPA;
		tot_pa += (chart.dataProvider[i]).pa;
		tot_early += (chart.dataProvider[i]).earlyTesting;
		tot_math += (chart.dataProvider[i]).math;
		tot_paytable += (chart.dataProvider[i]).paytable;


		(chart.dataProvider[i])["rollingPrePAPercent"] = (tot_pre / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingPostPAPercent"] = (tot_post / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingPaPercent"] = (tot_pa / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingEarlyTestingPercent"] = (tot_early / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingMathPercent"] = (tot_math / tot * 100).toFixed(2);
		(chart.dataProvider[i])["rollingPaytablePercent"] = (tot_paytable / tot * 100).toFixed(2);
	}

}

function DefectsFound2Y_L2_chartInit(event) {
	chart = event.chart;

	chart.legend.addListener("showItem", DefectsFound2Y_L2_legendHandler);
	chart.legend.addListener("hideItem", DefectsFound2Y_L2_legendHandler);

	var fyRollingTotalGraph = chart.getGraphById("fyRollingTotal");
	if (fyRollingTotalGraph != null)
		chart.hideGraph(fyRollingTotalGraph); // hide total line by default

	var rollingTotalGraph = chart.getGraphById("rollingTotal");
	if (rollingTotalGraph != null)
		chart.hideGraph(rollingTotalGraph); // hide total line by default    

}

function DefectsFound2Y_L2_legendHandler(event) {

	var chart = event.chart;
	var targetGraph = event.dataItem;
	var otherGraph;
	var valueField = targetGraph.valueField;
	var dataSet = chart.dataProvider;
	var maxValue = 0;
	var i;

	var axis = targetGraph.valueAxis;
	if (axis.synchronizeWith)
		axis = axis.synchronizeWith;

	if (targetGraph.id == "fyRollingTotal" || targetGraph.id == "rollingTotal") {

		if (event.type == "showItem") {
			for (i = 0; i < dataSet.length; i++) {
				if (maxValue < dataSet[i][valueField])
					maxValue = dataSet[i][valueField];
			}
			if (axis.maximum == undefined || axis.maximum < maxValue)
				axis.maximum = maxValue;
			chart.invalidateSize();
			chart.validateData();
		} else if (event.type == "hideItem") {
			if (targetGraph.id == "fyRollingTotal") {
				otherGraph = chart.getGraphById("rollingTotal");
				if (otherGraph.hidden) {
					axis.maximum = undefined;
				} else {
					valueField = otherGraph.valueField;
					maxValue = 0;
					for (i = 0; i < dataSet.length; i++) {
						if (maxValue < dataSet[i][valueField])
							maxValue = dataSet[i][valueField];
					}
					if (axis.maximum == undefined || axis.maximum < maxValue)
						axis.maximum = maxValue;
				}
			} else if (targetGraph.id == "rollingTotal") {
				otherGraph = chart.getGraphById("fyRollingTotal");
				if (otherGraph.hidden) {
					axis.maximum = undefined;
				} else {
					valueField = otherGraph.valueField;
					maxValue = 0;
					for (i = 0; i < dataSet.length; i++) {
						if (maxValue < dataSet[i][valueField])
							maxValue = dataSet[i][valueField];
					}
					if (axis.maximum == undefined || axis.maximum < maxValue)
						axis.maximum = maxValue;
				}
			}
			chart.invalidateSize();
			chart.validateData();
		}
	}
}

function TwoYearDefectsFoundDrillDownToLevel3(prefix, quarter, subcategory, fpstatus) {
	currentQuarter = quarter;
	currentStudio = subcategory;
	showHideDataTable(prefix + "_chart", "100%", false);

	$("#" + prefix + "_L2_chartdiv").hide();

	// hide show/hide all bars button
	$('#' + prefix + "_chart_link").hide();

	var L3_id = prefix + "_L3_chartdiv";
	currentPrefix = prefix + "_L3";
	addLevelChartDiv(prefix, L3_id);

	var returnData = GetDefectsFoundPerThemeByQuarter(prefix, restJsonDict, chartJSONTableDict[prefix], quarter, subcategory, studio_alias);
	var levelThreeConfig = DefectsFoundL3generateConfig(prefix, "Defects - " + subcategory + " for " + quarter, "Defects Found", "theme", currentTheme, false);

	levelThreeConfig.dataProvider = returnData.chart;

	levelThreeConfig.legend.enabled = true;
	levelThreeConfig.legend.marginTop = 10;
	levelThreeConfig.legend.valueFunction = adjustDefectsFound2YL3LegendText;
	levelThreeConfig.graphs = GetGraphsDefectsFoundL3();

	var i;

	// Adjust Label Functions 
	for (i = 0; i < 6; i++) {
		levelThreeConfig.graphs[i].balloonFunction = adjustDefectsFound2YL3Text;
		levelThreeConfig.graphs[i].labelFunction = adjustDefectsFound2YL2LabelText;
	}

	for (i = 6; i < 12; i++) {
		levelThreeConfig.graphs[i].balloonFunction = adjustDefectsFound2YL3Text;
	}

	levelThreeConfig = setColumnSpacingFirstPass(levelThreeConfig);

	//levelTwoConfig.columnSpacing = -15;
	levelThreeConfig.valueAxes = [{
			"id": "v1",
			"axisColor": "#88D498",
			"axisThickness": 2,
			"axisAlpha": 1,
			"title": "Percent",
			"stackType": "100%",
			"position": "left",
			"autoGridCount": false,
			"gridAlpha": 0
    },
		{
			"id": "v2",
			"axisColor": "#6ca979",
			"axisThickness": 2,
			"axisAlpha": 1,
			"gridAlpha": 0,
			"stackType": "regular",
			"position": "right",
			"title": "Count",
			"autoGridCount": false
		}];


	levelThreeConfig.chartCursor = {
		valueBalloonsEnabled: false,
		/*
		categoryBalloonFunction: function (category) {
		    return themeLookUp[category];
		},*/
		fullWidth: true,
		cursorAlpha: 0.1,
		zoomable: false,
		pan: true
	};

	levelThreeConfig.categoryAxis.autoWrap = true;

	delete levelThreeConfig.chartScrollbar.graph;
	delete levelThreeConfig.chartScrollbar.graphType;
	levelThreeConfig.chartScrollbar.autoGridCount = false;
	levelThreeConfig.chartScrollbar.hideResizeGrips = true;
	levelThreeConfig.chartScrollbar.resizeEnabled = false;
	levelThreeConfig.zoomOutText = "";
	levelThreeConfig.mouseWheelZoomEnabled = false;
	levelThreeConfig.mouseWheelScrollEnabled = true;

	chartConfigDict["defectsClosedNewThemes_L3"] = levelThreeConfig;
	var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
	chartResizerDict[currentPrefix] = true;

	chart.addLabel(
		'!180', '20',
		"Sort By Defects",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortDefectsThemeL3ByDefectFound("' + currentPrefix + '", true);');
	chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

	chart.addListener("init", chartInit);
	chart.addListener("clickGraphItem", L3_handleItemClick);
	chart.addListener("rendered", L3_rendered);

	chartDict[currentPrefix] = chart;
	chartDataDict[currentPrefix] = returnData;

	level++; // increase global level indicator


	var dataTable;
	dataTable = returnData.chart; // GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsReleased"], month, subcategory, studio_alias);
	dataTableDataDict[prefix + "_dataTable"] = dataTable;

	if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
	    showHideDataTable(prefix + "_dataTable");

	showHideDataTable(prefix, "100%", false);

	resetCurrentHash();
}

function GetDefectsFoundPerThemeByQuarter(chartSelection, jsonTable, defect_table_name, quartername, sub_category, studio_alias) {
	var quarter;
	var tempArray;
	var tempInt;
	var studio;
	var cat;
	var dataStudio;
	var dataTheme;
	var dataThemeKey;
	var dataKit;
	var dataPhase;
	var datarow;
	var row;
	var isValidKit;
	var chartTable = [];
	var themeDict = {};
	var themeLookUp = {}; // for the cursor category balloon
	var tempTheme = "";
	var compareVal;

	if (quartername == "Current Month")
		quarter = 8;
	else quarter = getQuarterFromString(quartername);


	var defectTable = jsonTable[defect_table_name + quarter.toString()];

	studio = sub_category;

	var kitChartData = {};


	for (var dataindex in defectTable) {
		if (defectTable.hasOwnProperty(dataindex)) {
			datarow = defectTable[dataindex];
			dataStudio = datarow["Studio"];
			dataTheme = datarow["KitTheme"];
			dataKit = datarow["Kit"];
			dataThemeKey = datarow["SAP_Theme_ID"];
			dataPhase = datarow["PhaseFoundGroup"];

			compareVal = dataStudio;

			if (studio_alias) {
				if (studio_alias.hasOwnProperty(compareVal))
					compareVal = studio_alias[compareVal];
			}

			isValidKit = false;
			if (studio === "Total" || studio === compareVal) {
				isValidKit = true;
			}



			//(kitChartData[dataKit])["theme"] = dataTheme;


			if (isValidKit) {


				if (kitChartData[dataKit] === undefined) {
					kitChartData[dataKit] = {
						"PA": {
							"Total": 0
						},
						"Early Testing": {
							"Total": 0
						},
						"Pre PA": {
							"Total": 0
						},
						"Math": {
							"Total": 0
						},
						"Paytable": {
							"Total": 0
						},
						"Post PA": {
							"Total": 0
						},
						"pa_percent": 0,
						"earlyTesting_percent": 0,
						"prePA_percent": 0,
						"math_percent": 0,
						"paytable_percent": 0,
						"postPA_percent": 0
					};
				}


				if ((kitChartData[dataKit])["theme"] === undefined) {
					if (dataTheme.length >= 27) {
						tempTheme = dataTheme.substring(0, 26);
						if (themeDict.hasOwnProperty(tempTheme))
							themeDict[tempTheme] = themeDict[tempTheme] + 1;
						else themeDict[tempTheme] = 1;
						(kitChartData[dataKit])["theme"] = tempTheme + "_" + themeDict[tempTheme]; // max out                     
					} else {
						(kitChartData[dataKit])["theme"] = dataTheme;
					}

					themeLookUp[(kitChartData[dataKit])["theme"]] = dataTheme;
					(kitChartData[dataKit])["fulltheme"] = dataTheme;
					(kitChartData[dataKit])["themekey"] = dataThemeKey;
					(kitChartData[dataKit])["quarter"] = quarter;
				}

				if (dataPhase === "PA") {
					((kitChartData[dataKit])["PA"])["Total"] += parseInt(datarow["Count"]);
					if (((kitChartData[dataKit])["PA"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["PA"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["PA"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "PRE PA") {
					((kitChartData[dataKit])["Pre PA"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Pre PA"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Pre PA"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Pre PA"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "EARLY TESTING") {
					((kitChartData[dataKit])["Early Testing"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Early Testing"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Early Testing"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Early Testing"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "POST PA") {
					((kitChartData[dataKit])["Post PA"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Post PA"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Post PA"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Post PA"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "MATH") {
					((kitChartData[dataKit])["Math"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Math"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Math"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Math"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				} else if (dataPhase === "PAYTABLE") {
					((kitChartData[dataKit])["Paytable"])["Total"] += parseInt(datarow["Count"]);

					if (((kitChartData[dataKit])["Paytable"])[datarow["Phase"]] === undefined) {
						((kitChartData[dataKit])["Paytable"])[datarow["Phase"]] = 0;
					}
					((kitChartData[dataKit])["Paytable"])[datarow["Phase"]] += parseInt(datarow["Count"]);
				}

			}
		}
	}

	savedData["QuartersPhaseFound_L3"] = kitChartData;

	var kits;

	for (kits in kitChartData) {
		row = [];

		row["theme"] = (kitChartData[kits])["theme"];
		//themeLookUp[row["theme"]] = row["theme"];
		row["kit"] = kits;
		row["pa"] = ((kitChartData[kits])["PA"])["Total"];
		row["prePA"] = ((kitChartData[kits])["Pre PA"])["Total"];
		row["postPA"] = ((kitChartData[kits])["Post PA"])["Total"];
		row["paytable"] = ((kitChartData[kits])["Paytable"])["Total"];
		row["math"] = ((kitChartData[kits])["Math"])["Total"];
		row["earlyTesting"] = ((kitChartData[kits])["Early Testing"])["Total"];

		var tot = ((kitChartData[kits])["PA"])["Total"] + ((kitChartData[kits])["Pre PA"])["Total"] + ((kitChartData[kits])["Post PA"])["Total"] + ((kitChartData[kits])["Paytable"])["Total"] + ((kitChartData[kits])["Math"])["Total"] + ((kitChartData[kits])["Early Testing"])["Total"];
		row["pa_percent"] = ((((kitChartData[kits])["PA"])["Total"] / tot) * 100).toFixed(2);
		row["prePA_percent"] = ((((kitChartData[kits])["Pre PA"])["Total"] / tot) * 100).toFixed(2);
		row["postPA_percent"] = ((((kitChartData[kits])["Post PA"])["Total"] / tot) * 100).toFixed(2);
		row["paytable_percent"] = ((((kitChartData[kits])["Paytable"])["Total"] / tot) * 100).toFixed(2);
		row["math_percent"] = ((((kitChartData[kits])["Math"])["Total"] / tot) * 100).toFixed(2);
		row["earlyTesting_percent"] = ((((kitChartData[kits])["Early Testing"])["Total"] / tot) * 100).toFixed(2);

		row["total_defects"] = tot;
		chartTable.push(row);
	}

	chartTable.sort(function (a, b) {
		if (a.theme < b.theme) return -1;
		if (a.theme > b.theme) return 1;
		return 0;
	});

	return {
		chart: chartTable,
		themes: themeLookUp
	};
}

function adjustDefectsFound2YL3LegendText(graphDataItem) {
	//return graphDataItem.values.value;
	//return "balh";
	if (graphDataItem.values)
		return graphDataItem.values.value;
	else
		return "";
}

function adjustDefectsFound2YL3Text(graphDataItem, graph) {

    var data = savedData["QuartersPhaseFound_L3"];
	var kitTheme = graphDataItem.category;
	var section = (graph.legendTextReal).replace(" %", "");
	var kit;

	var kits;
	for (kits in data) {
		if ((data[kits])["theme"] === kitTheme) {
			kit = kits;
		}
	}

	var percentage = true;
	if ((graph.legendTextReal).search(" %") === -1)
		percentage = false;

	var phaseData = (data[kit])[section];

	var tot = ((data[kit])[section])["Total"]; //((data[kit])["PA"])["Total"] + ((data[kit])["Pre PA"])["Total"] + ((data[kit])["Post PA"])["Total"] + ((data[kit])["Early Testing"])["Total"] +
	//((data[kit])["Paytable"])["Total"] + ((data[kit])["Math"])["Total"];

    if(percentage == true)
        graphDataItem.values.value = Math.round(graphDataItem.values.percents * 100) / 100;
    else
        graphDataItem.values.value = Math.round(graphDataItem.values.value * 100) / 100;

	var test;

	if (percentage === true)
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + Math.round(graphDataItem.values.percents * 100) / 100 + "%</b>";
	else
		text = "<b style='font-size:14px;'>" + graph.legendTextReal + "</b><br>" + graphDataItem.category + ": <b style='font-size:13px;'>" + ((data[kit])[section])["Total"] + "</b>";

	var property = 0;
	for (property in phaseData) {
		if (property !== "Total") {
			text += "<br>";

			if (percentage === true)
				text += property.substring(3) + ": <b style='font-size:13px;'>" + ((phaseData[property] / tot) * 100).toFixed(2) + "%</b>";
			else
				text += property.substring(3) + ": <b style='font-size:13px;'>" + phaseData[property] + "</b>";
		}
	}

	return text;
}