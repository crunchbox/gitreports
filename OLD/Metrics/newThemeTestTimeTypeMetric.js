function get_NewThemeTestTimeMetricsType() {
	if (loadedMetrics["testTime"] != true) {
		get_MetricsData("testTime", "NewThemeAvgTTGameType");
		return;
	}
    if (analytics)
	    analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "New Theme Test Time Type");

	clearChart("NewThemeAvgTTGameType");
	showHideDataTable("NewThemeAvgTTGameType", "100%", false);

	var NewThemeAvgTTGameTypeConfig;
	if (chartConfigDict["NewThemeAvgTTGameType"] == null) {
	    chartData = parseMetricData(restJsonDict, "NewThemeTestTime_Month", studio_alias, "NewThemeAvgTTGameType");
		chartGraphs = GetGraphs(chartData, "month");

		chartGraphs.push(GetRollingGraph("rollingAvg", "Rolling Average", "#33FFBD"));

		dataTableDataDict["NewThemeAvgTTGameType_dataTable"] = chartData;
		NewThemeAvgTTGameTypeConfig = generateConfig("NewThemeAvgTTGameType", "New Theme Test Time - Game Type", "Work Days", "month", currentTheme, false);
		NewThemeAvgTTGameTypeConfig.dataProvider = chartData;
		//chartGraphs.balloonFunction = adjustNewThemeFirstPassL2Text; 
		NewThemeAvgTTGameTypeConfig.graphs = chartGraphs;

		chartConfigDict["NewThemeAvgTTGameType"] = NewThemeAvgTTGameTypeConfig;
		chartDataDict["NewThemeAvgTTGameType"] = chartData;
	} else NewThemeAvgTTGameTypeConfig = chartConfigDict["NewThemeAvgTTGameType"];

	chart = AmCharts.makeChart("NewThemeAvgTTGameType_chartdiv", NewThemeAvgTTGameTypeConfig, 100);

	chartResizerDict["NewThemeAvgTTGameType"] = true;

	chart.addLabel(
    35, 20,
    "< Go back",
    undefined,
    14,
    undefined,
    undefined,
    undefined,
    true,
    '/');

	chart.addLabel(
		'!150', '30',
		"Enable Trend",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:getRollingAverageTestTime();');

	chart.addListener("init", chartInit);
	chart.addListener("rendered", addListeners);

	chartDict["NewThemeAvgTTGameType"] = chart; // store chart
}

function parseTestTimeTypeToChartRow(row, viewTable, studio_alias, sub_category, data) {
	var tempInt = 0;
	var tempStudio;
	var tempTot = 0;
	var rollingTotal = 0;
	var countStudioAmt = {};
	var studioName = [];
	var tot = 0;
	var rollingTotalSub = 0;

	var targetStudio;
	var use_sub_category = false;
	if (sub_category != undefined) {
		targetStudio = sub_category;
		use_sub_category = true;
	}

	for (var dataindex in viewTable) {
		if (viewTable.hasOwnProperty(dataindex)) {
			datarow = viewTable[dataindex];
			tot = tot + 1;


			tempStudio = datarow["Type"];

			if (use_sub_category == false || targetStudio == tempStudio) {
				if (row.hasOwnProperty(tempStudio))
					tempInt = row[tempStudio];
				else tempInt = 0;

				row[tempStudio] = tempInt + parseFloat(datarow["Testing_Time"]);
				rollingTotal = rollingTotal + parseFloat(datarow["Testing_Time"]);

				if (countStudioAmt.hasOwnProperty(tempStudio))
					countStudioAmt[tempStudio] = countStudioAmt[tempStudio] + 1;
				else
					countStudioAmt[tempStudio] = 1;

				if (studioName.indexOf(tempStudio) == -1)
					studioName.push(tempStudio);
			} else if (use_sub_category === true && targetStudio === "Total") {
				rollingTotalSub = rollingTotalSub + parseFloat(datarow["Testing_Time"]);
			}
		}
	}


	for (var i = 0; i < studioName.length; i++) {
		data[studioName[i]] = [countStudioAmt[studioName[i]], row[studioName[i]]];
		row[studioName[i]] = row[studioName[i]] / countStudioAmt[studioName[i]];
		row[studioName[i]] = Math.round(row[studioName[i]] * 100) / 100;
		//tot += countStudioAmt[studioName[i]];
	}

	if (sub_category == undefined && sub_category != "Total") {
		data["Total"] = [tot, rollingTotal];
		row["Total"] = Math.round((rollingTotal / tot) * 100) / 100;
	} else if (sub_category === "Total") {
		row["Total"] = Math.round((rollingTotalSub / tot) * 100) / 100;
	}


	return row;
}