function get_avgDefectsNewThemeMetric() {
    var metricName = "avgDefects";
    if (loadedMetrics[metricName] != true) {
        get_MetricsData(metricName, metricName);
        return;
    }
    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', analyticsDict[metricName]);

    clearChart(metricName);
    showHideDataTable(metricName, "100%", false);

    chartData = parseMetricData(restJsonDict, chartJSONTableDict[metricName], studio_alias, metricName);
    chartGraphs = GetGraphs(chartData, "month");
    dataTableDataDict[metricName + "_dataTable"] = chartData;
    //drawTable(chartData, "studio_dataTable");

    var studioConfig = generateConfig(metricName, analyticsDict[metricName], "Defects", "month", currentTheme, false);
    studioConfig.dataProvider = chartData;
    studioConfig.graphs = chartGraphs;

    chartConfigDict[metricName] = studioConfig;
    chart = AmCharts.makeChart(metricName + "_chartdiv", studioConfig, 100);
    chartResizerDict[metricName] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		14,
		undefined,
		undefined,
		undefined,
		true,
		'/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", addListeners);

    chartDataDict[metricName] = chartData;
    chartDict[metricName] = chart;
}

function get_DefectsData(chart_type) {
    if (loadedMetrics[chart_type]===undefined) {

        var rest_params = { chart: chart_type, metric: "defectsForKitsReleased", date: getTomorrowsISODate(), quarters: false };

        var cache_filename = compose_cache_filename("kitPAReleased", rest_params);
        get_CachedData(cache_filename, function(){}, cacheData_errorHandler);
    }
}

function parseAvgDefectsNewThemesL1(row, viewTable, studio_alias, sub_category) {
    var tempInt = 0;
    var tempStudio;
    var rollingTotal = 0;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category;
        use_sub_category = true;
    }

    var count = 0;
    var defects = 0;
    var kitCount = 0; 
    for (var dataindex in viewTable) {
        if (viewTable.hasOwnProperty(dataindex)) {
            datarow = viewTable[dataindex];

            if (studio_alias.hasOwnProperty(datarow["Studio"]))
                tempStudio = studio_alias[datarow["Studio"]];
            else tempStudio = datarow["Studio"];

            if (use_sub_category == false || targetStudio == tempStudio) {
                var avg = parseFloat((parseFloat(datarow["Avg"])).toFixed(2));
                row[tempStudio] = avg;
                defects += parseInt(datarow["DefectCount"]);
                kitCount += parseInt(datarow["KitCount"]);
            }
        }
    }

    rollingTotal = defects/kitCount;
    rollingTotal = parseFloat(rollingTotal.toFixed(2));

    if (sub_category == undefined && sub_category != "Total")
        row["Total"] = rollingTotal;

    return row;
}

function parseAvgDefectsNewThemesL2(jsonTable, table_name, studio_alias, chartSelection, sub_category) {
    var tempInt = 0;
    var tempStudio;
    var rollingTotal = 0;

    var targetStudio;
    var use_sub_category = false;
    if (sub_category != undefined) {
        targetStudio = sub_category;
        use_sub_category = true;
    }

    var parsedData = [];
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var currentFiscal = {}; 
    for (var month = 0; month < 13; month++) {

        var viewTable = jsonTable[table_name + month.toString()];
        var row = {};
        var defects = 0;
        var kitCount = 0;
        for (var dataindex in viewTable) {
            if (viewTable.hasOwnProperty(dataindex)) {
                if (month == 12)
                    row["month"] = "Current Month";
                else
                    row["month"] = months[month];
                var datarow = viewTable[dataindex];

                if (studio_alias.hasOwnProperty(datarow["Studio"]))
                    tempStudio = studio_alias[datarow["Studio"]];
                else tempStudio = datarow["Studio"];

                if (targetStudio == tempStudio || sub_category == "Total") {
                    if (sub_category == "Total") {
                        defects += parseInt(datarow["DefectCount"]);
                        kitCount += parseInt(datarow["KitCount"]);
                    } else {
                        var avg = parseFloat((parseFloat(datarow["Avg"])).toFixed(2));
                        row[tempStudio] = avg;
                    }

                    var kits = 0;
                    if (row["KitCount"] !== undefined)
                        kits = row["KitCount"];

                    row["KitCount"] = kits + parseInt(datarow["KitCount"]);

                }

            }
           
        }
        if (sub_category == "Total") {
            row["Total"] = defects / kitCount;
            row["Total"] = parseFloat(row["Total"].toFixed(2));
        }

        if (row["month"] !== "Current Month")
            parsedData.push(row);
        else
            currentFiscal = row; 
    }


    parsedData = parsedData.concat(parsedData.splice(0, new Date().getMonth()));
    parsedData.push(currentFiscal);
    return parsedData;
}


function avgDefectsGetGraphsL2(data, categoryField) {
    var graphs = [];
    var ballonText = "[[title]]:[[value]] \n KitCount: [[KitCount]]";
    var max_len_index = getLongestRowIndex(data);

    var colors = chartColors;

    var groups = [];
    for (var i = 0; i < 1; i++) {
        var rowData = data[i];
        for (var heading in rowData) {
            if (rowData.hasOwnProperty(heading)) {
                if (heading != categoryField && heading !== "KitCount") {

                    if (groups.indexOf(heading) == -1) {
                        groups.push(heading);
                    }
                }
            }
        }
    }

    for (var index in groups) {
        if (groups.hasOwnProperty(index)) {
            var column = {};
            var key = groups[index];
            if (key == "Total") {
                column["balloonText"] = ballonText;
                column["id"] = "totalline";
                column["title"] = "Total";
                column["bullet"] = "round";
                column["lineThickness"] = 3;
                column["bulletSize"] = 7;
                column["bulletBorderAlpha"] = 1;
                column["connect"] = true;
                column["useLineColorForBulletBorder"] = true;
                column["bulletBorderThickness"] = 3;
                column["fillAlphas"] = 0;
                column["lineAlpha"] = 1;
                column["valueField"] = key;
                graphs.push(column);

                column = {};
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["type"] = "column";
                column["valueField"] = key;
                graphs.push(column);
            } else {
                column["balloonText"] = ballonText;
                column["fillAlphas"] = 1;
                column["id"] = "AmGraph_" + index;
                column["title"] = key;
                column["type"] = "column";
                column["valueField"] = key;

                graphs.push(column);
            }

        }
    }
    // set fill colors of bars/columns
    for (i = 0; i < graphs.length; i++) {
        graphs[i].fillColors = colors[i];
        graphs[i].lineColor = colors[i];
    }

    return graphs;
}


function avgDefectsL2ShowRollingAverages() {
    var chart = chartDict["avgDefects_L2"];

    //clear Graphs
    for (var i = 0; i < 13; i++) {

        if ((chart.dataProvider[i])["rollingPassedPercent"] !== undefined)
            delete (chart.dataProvider[i])["rollingPassedPercent"];

        if ((chart.dataProvider[i])["rollingFailedPercent"] !== undefined)
            delete (chart.dataProvider[i])["rollingFailedPercent"];

    }

    //update graphs with new %s
    var start = chart.start;
    var end = chart.end;
    calculateRollingPercentagesGameTrackerL2(chart, start, end);

    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingPassedPercent"));
    chartDict[currentPrefix].showGraph(chart.getGraphById("rollingFailedPercent"));

    chart.invalidateSize();
    chart.validateNow();
}


function avgDefectsGenerateConfigL2(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
    var config = {
        "type": "serial",
        "theme": theme,
        "categoryField": catField,
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "valueAxes": [
			{
			    "id": "v1",
			    "title": yAxisTitle
			}
        ],
        "legend": {
            "enabled": true,
            "useGraphSettings": true,
            "valueFunction": function (item, value) {
                if (value == "")
                    return 0;
                else
                    return value;
            }
        },
        "titles": [
			{
			    "id": chartId,
			    "size": 15,
			    "text": chartTitle
			}
        ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "chartScrollbar": {
            "graph": "totalline",
            "graphType": "line",
            "color": "#FFFFFF",
            "selectedBackgroundColor": "#6F7D8A",
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": "#FFFFFF",
            "autoGridCount": true
        },
        "export": {
            "enabled": true
        },
        "responsive": {
            "enabled": true,
            "rules": [
				{
				    "maxWidth": 1000,
				    "overrides": {
				        "legend": {
				            "enabled": false
				        }
				    }
				},
				{
				    "maxWidth": 500,
				    "overrides": {
				        "valueAxes": {
				            "inside": true
				        }
				    }
				}
            ]
        }
    };

    config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
    config.export["backgroundColor"] = "#222222";
    config.export.drawing.enabled = true;
    config.export.drawing.color = "#FFFFFF";

    config.export.menu[0].menu.push({
        "label": "Get URL To Chart",
        "click": GetUrlForCurrentChart
    });

    if (useStackCharts) {
        config.valueAxes[0].stackType = "regular";
    }

    return config;
}








function avgDefects_DrillDownToLevel3(prefix, monthname, studio, fpstatus) {


    currentMonth = monthname;
    currentStudio = studio;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);


    // get data
    avgDefectsDataL3(currentPrefix, restJsonDict, studio, monthname);
}


function avgDefectsDataL3(prefix, jsonTable, studio, monthname) {
    var myMetric = "avgDefects";
    if (loadedMetrics["DefectsKitsReleasedL3"] != true) {

        get_DefectsKitsReleasedL3(myMetric, studio, monthname);
        return;
    }

    var returnData = GetDefectCountPerTheme(myMetric, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], monthname, studio, studio_alias);
    var data = returnData.chart;
    //var themeLookUp = returnData.themes;

    var L3_id = prefix + "_chartdiv";
    currentPrefix = prefix;
    addLevelChartDiv(prefix, L3_id);

    var jsLink = "$('#" + myMetric + "_dataTable_link').show(); GoUpLevel('" + myMetric + "');";
    $("#" + L3_id).html('<a id="' + myMetric + '_L3_GoBackLink" class="gobacklink_manual" href="#" onclick="' + jsLink + '">&lt; Go back</a><div id="' + myMetric + '_L3_dataTable_group">');

    var title_div = $("<div/>",
        {
            id: currentPrefix + "_Title"
        })
        .addClass('metricTitle');
    title_div.html("Average Defects for " + monthname + " and " + studio.replaceAll("_", " ") + " - Total: " + data.length);
    $("#" + L3_id).prepend(title_div);


    level++; // increase global level indicator

    removeLoadingDiv();
    chartDataDict[prefix] = data;

    var calcHeight = $("#" + L3_id).innerHeight() - 110;
    $("#" + myMetric + "_dataTable_link").hide();

    dataTableOptionsDict[prefix + "_dataTable"] = {
        "bScrollCollapse": true,
        "scrollX": true,
        "scrollY": calcHeight,
        "colReorder": true,
        "processing": true,
        "language": {
            "processing": "DataTable is Loading..."
        },
        "dom": 'rtBfp',
        buttons: ['copyHtml5', 'csvHtml5']
    };

    var tableClassList = "row-border stripe cell-border hover";
    var htmlTable = drawTable(data, prefix + "_dataTable", "100%", dataTableOptionsDict[prefix + "_dataTable"], tableClassList);
    dataTableHTMLTableDict[prefix] = htmlTable;
    if (data.length > 0) {
        dataTableHTMLTableDict[prefix].$("tr").click(function () {
            var data = dataTableHTMLTableDict[prefix].row($(this)).data();
            $("#" + myMetric + "_dataTable_link").show();
            if (data[1] !== "")
                showAndLoadScoreCard(currentPrefix, data[1], currentMonth);
        });
    }

    var dataTable = [];
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
    /*
    var myMetric = "avgDefects";
    if (loadedMetrics["DefectsKitsReleasedL3"] != true) {

        get_DefectsKitsReleasedL3(myMetric, studio, monthname);
        return;
    }
    clearChart(myMetric);
    showHideDataTable(myMetric, "100%", false);
    var returnData = GetDefectCountPerTheme(myMetric, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], monthname, studio, studio_alias);
    var data = returnData.chart;
    themeLookUp = returnData.themes;

    $("#" + myMetric + "_L2_chartdiv").hide();
    var L3_id = myMetric + "_L3_chartdiv";
    currentPrefix = myMetric + "_L3";

    //var levelThreeConfig = generateConfig(prefix, "Released Themes - " + subcategory + " for " + month, "Defects", "theme", currentTheme, false);

    var levelThreeConfig = generateConfig(myMetric, "Released Themes - " + studio + " for " + monthname, "Defects", "theme", currentTheme, false);
    levelThreeConfig.dataProvider = data;

    levelThreeConfig.legend.enabled = false;
    levelThreeConfig.graphs = [{
        balloonFunction: function (graphDataItem, graph) {
            var fulltheme = graphDataItem.dataContext.fullTheme;
            var value = graphDataItem.dataContext.value;
            return fulltheme + ": " + value;
        },
        fillAlphas: 1,
        id: "AmGraph_1",
        type: "column",
        valueField: "value"
    }];
    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        categoryBalloonFunction: function (category) {
            return themeLookUp[category];
        },
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };
    levelThreeConfig.categoryAxis.autoWrap = true;

    delete levelThreeConfig.chartScrollbar.graph;
    delete levelThreeConfig.chartScrollbar.graphType;
    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    var labelText = "Sort By Defects";


    chart.addLabel(
		'!180', '20',
		labelText,
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortDefectsThemeL3ByDefectCount("' + currentPrefix + '", true);');
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + myMetric + '");');

    chart.addListener("init", chartInit);
    chart.addListener("clickGraphItem", L3_handleItemClick);
    chart.addListener("rendered", L3_rendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    //var dataTable = GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], month, subcategory, studio_alias);

    var dataTable = GetDataForEntry(myMetric, restJsonDict, chartJSONTableDict["defectsForKitsReleased"], monthname, studio, studio_alias);


    dataTableDataDict[myMetric + "_dataTable"] = dataTable;

    if (dataTableIsVisible[myMetric + "_dataTable"] !== undefined && dataTableIsVisible[myMetric + "_dataTable"] === true)
        showHideDataTable(myMetric + "_dataTable");*/
}