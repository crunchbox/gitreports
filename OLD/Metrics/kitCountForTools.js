function get_KitCountForToolsMetrics() {
    if (loadedMetrics["KitCountForTools"] !== true) {
        get_KitCountForToolsList("KitCountForTools");
        return;
    }

    if (analytics)
        analytics.addEntry(aCA.sessionEntryType.CONFIGURATION, 'Metric', "Kit Count for Tools");

    clearChart("KitCountForTools");
    showHideDataTable("KitCountForTools", "100%", false);

    var chartData = parseKitCountForToolsData(restJsonDict, "KitCountAllTools", "KitCountForTools");
    var chartGraphs = getL1GraphKitCountForToolsData(chartData, "tool");
    dataTableDataDict["KitCountForTools_dataTable"] = chartData;
    //drawTable(chartData, "studio_dataTable");

    var studioConfig = generateConfig("KitCountForTools", "Kit Count for Tools (Rolling 12 Months)", "count", "tool", currentTheme, false);
    studioConfig.dataProvider = chartData;
    studioConfig.graphs = chartGraphs;

    chartConfigDict["KitCountForTools"] = studioConfig;
    var chart = AmCharts.makeChart("KitCountForTools_chartdiv", studioConfig, 100);
    chartResizerDict["KitCountForTools"] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		14,
		undefined,
		undefined,
		undefined,
		true,
		'/');

    chart.addListener("init", chartInit);
    chart.addListener("rendered", KitCountForToolsAddListeners);

    chartDataDict["KitCountForTools"] = chartData;
    chartDict["KitCountForTools"] = chart; // store chart
    //get_DefectForKitReleased(); // async get of defect data

}

function KitCountForToolsAddListeners(event) {

    $('a[href="http://www.amcharts.com/javascript-charts/"]').remove(); // remove watermark

    var prefix = event.chart.titles[0].id;
    chart = event.chart;
    chart.addListener("clickGraphItem", handleItemClick);

    var categoryAxis = chart.categoryAxis;
    categoryAxis.addListener("clickItem", handleCategoryClick);

    removeLoadingDiv();

    forceChartFillParent(prefix);

    if (hasInitSubCat) {
        var subcat_type = whatIsInitSubCat(prefix); // validates initSubCat and initStackCat and gives type
        hasInitSubCat = false;

        if (subcat_type == "month")
            KitCountForTools_DrillDownToLevel2(prefix, initSubCategory);
        else if (subcat_type == "studio") {
            if (prefix == "KitCountForTools")
                KitCountForTools_DrillDownToLevel2(prefix, initSubCategory);
        }
    } else resetCurrentHash();


    var startchart = BVATestCasesL1autoZoom(chart);
    chart.zoomToIndexes(startchart, (chartDataDict["KitCountForTools"]).length - 1);
}

function parseKitCountForToolsData(restJsonDict,data,metric)
{
    var results = _.groupBy(restJsonDict[data],
                   function (item) {
                       return item.ProductName;
                   });

    var tools = [];
    var total = 0; 
    for (var tool in results) {
        var count = 0; 
        for (var i = 0; i < results[tool].length; i++) {
            count += (results[tool])[i].KitCount;
        }
        total += count; 
        tools.push({
            tool: tool,
            count:count
        });
    }
    /*
    tools.push({
        tool: "Total",
        count: total
    });*/

    tools = _.sortBy(tools,
            function (item) {
                return item["count"];
            });

    tools.reverse(); 

    return tools;
}

function getL1GraphKitCountForToolsData(data, categoryField) {
    var graphs = [];

    var column = {};
    column["fillAlphas"] = 1;
    column["type"] = "column";
    column["valueField"] = "count";

    graphs.push(column);


    return graphs;
}

function generateConfigKitCountForToolsData(chartId, chartTitle, yAxisTitle, catField, theme, useStackCharts) {
    var config = {
        "type": "serial",
        "theme": theme,
        "categoryField": catField,
        "mouseWheelZoomEnabled": true,
        "mouseWheelScrollEnabled": true,
        "categoryAxis": {
            "gridPosition": "start",
            "labelOffset": -2
        },
        "valueAxes": [
			{
			    "id": "v1",
			    "title": yAxisTitle
			}
        ],
        "legend": {
            "enabled": true,
            "useGraphSettings": true,
            "valueFunction": function (item, value) {
                if (value == "")
                    return 0;
                else
                    return value;
            }
        },
        "titles": [
			{
			    "id": chartId,
			    "size": 15,
			    "text": chartTitle
			}
        ],
        "chartCursor": {
            valueBalloonsEnabled: false,
            fullWidth: true,
            cursorAlpha: 0.1,
            zoomable: true,
            pan: false
        },
        "chartScrollbar": {
            "graph": "totalline",
            "graphType": "line",
            "color": "#FFFFFF",
            "selectedBackgroundColor": "#6F7D8A",
            "selectedBackgroundAlpha": "0.75",
            "selectedGraphLineColor": "#FFFFFF",
            "autoGridCount": true
        },
        "export": {
            "enabled": true
        },
        "responsive": {
            "enabled": true,
            "rules": [
				{
				    "maxWidth": 1000,
				    "overrides": {
				        "legend": {
				            "enabled": false
				        }
				    }
				},
				{
				    "maxWidth": 500,
				    "overrides": {
				        "valueAxes": {
				            "inside": true
				        }
				    }
				}
            ]
        }
    };

    config.export = jQuery.extend(true, {}, DefaultExportOptions.exportCFG); // clone json
    config.export["backgroundColor"] = "#222222";
    config.export.drawing.enabled = true;
    config.export.drawing.color = "#FFFFFF";

    config.export.menu[0].menu.push({
        "label": "Get URL To Chart",
        "click": GetUrlForCurrentChart
    });

    if (useStackCharts) {
        config.valueAxes[0].stackType = "regular";
    }

    return config;
}

function KitCountForTools_DrillDownToLevel2(prefix, itemSelection) {
    
    currentMonth = "all";
    currentStudio = itemSelection;

    var L2_id;

    if (currentPrefix == "KitCountForTools") {
        $("#" + prefix + "_chartdiv").hide();
        L2_id = prefix + "_L2_chartdiv";
        currentPrefix = prefix + "_L2";
        addLevelChartDiv(prefix, L2_id);
    }
    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");
    showHideDataTable(prefix, "100%", false);

    var studioName = itemSelection.split('/')[0];
    var data = parseKitCountForToolsDataL2(restJsonDict, "KitCountAllTools", itemSelection);
 
    var levelTwoConfig = generateConfig(prefix, "Kit Count for Tools - " + itemSelection, "count", "month", currentTheme, false);
    levelTwoConfig.dataProvider = data;
    levelTwoConfig.graphs = getL1GraphKitCountForToolsData();


    chartConfigDict["KitCountAllTools_L2"] = levelTwoConfig;
    var chart = AmCharts.makeChart(L2_id, levelTwoConfig, 50);
    chartResizerDict[currentPrefix] = true;


    // let's add a label to go back to yearly data
    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');


    chart.addListener("clickGraphItem", L2_handleItemClick);
    chart.addListener("init", chartInit);
    chart.addListener("rendered", KitCountForTools_handleRendered);

    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = data;

    level++; // increase global level indicator

    var dataTable = data; // GetDataForEntry("defectsClosedNewThemes", restJsonDict[chartTypeDict[prefix]], "DefectClosedNewThemes_Month", "all", currentStudio, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;
    dataTableOptionsDict[prefix + "_dataTable"] = {
        "scrollY": 200,
        "scrollX": true,
        "dom": 'rtBfp',
        "buttons": ['copyHtml5', 'csvHtml5']
    };
}

function parseKitCountForToolsDataL2(restJsonDict, data, itemSelection) {
    var results = _.groupBy(restJsonDict[data],
                   function (item) {
                       return item.ProductName;
                   });

    var parsedData = [];
    var total = 0;

    if (results[itemSelection] !== undefined) {
        results[itemSelection] = _.sortBy(results[itemSelection],
            function(item) {
                return item["YYYYMM"];
            });

        for (var i = 0; i < results[itemSelection].length; i++) {
            total += (results[itemSelection])[i].KitCount;

            parsedData.push({
                month: parseDate((results[itemSelection])[i].YYYYMM),
                count: (results[itemSelection])[i].KitCount,
                tool: itemSelection
            });
        }

        /*
        parsedData.push({
            month: "Total",
            count: total
        });*/
    } else {
        results = _.groupBy(restJsonDict[data],
                    function (item) {
                        return item.YYYYMM;
                    });

        for (var month in results) {

            var count = 0; 
            for (var i = 0; i < results[month].length; i++) {

                count += (results[month])[i].KitCount;

            }
            total += count;
            parsedData.push({
                month: parseDate(month),
                count: count,
                tool: "Total"
            });
        }

    }

    return parsedData;
}

function parseDate(str) {
    var year = parseInt(str.substring(0, 4));
    var month = parseInt(str.substring(4, 6));

    var date = new Date();
    var thisYear = date.getFullYear();
    var thisMonth = date.getMonth() + 1;

    if (thisYear === year && thisMonth === month) {
        return "Current Month";
    } else {
        var months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];
        return months[(month-1)];
    }


}

function KitCountForTools_handleRendered(event) {

    if (hasInitSubCatMonth) // if need to go to level 3 
    {
        var prefix = event.chart.titles[0].id;

        removeAllActiveClass();
        //$('#navsidebar li.active').removeClass('active');
        var $subnav = $("<li id='link_" + prefix + "_3' class='subnav active'><a href='#' title='#/metric/" + prefix + "/" + initMonth + "/" + initSubCategory + "' onclick='loadMetricView(\"" + prefix + "\", 3)'>" + initMonth + "</a></li>");
        $("#link_" + prefix + "_2").after($subnav);

        hasInitSubCatMonth = false;
        BVATestCasesPerBuildsL3(prefix, initMonth, initSubCategory, "");

    } else resetCurrentHash();

    removeLoadingDiv();
    removeLoadingDiv();

}

function KitCountForTools_DrillDownToLevel3(prefix, month, tool, fpstatus) {
    currentMonth = month;
    currentStudio = tool;
    showHideDataTable(prefix + "_chart", "100%", false);

    $("#" + prefix + "_L2_chartdiv").hide();

    // hide show/hide all bars button
    $('#' + prefix + "_chart_link").hide();

    // create new div
    var L3_id = prefix + "_L3_chartdiv";
    currentPrefix = prefix + "_L3";
    addLevelChartDiv(prefix, L3_id);

    // get data
    KitListForTools(currentPrefix, restJsonDict, month, tool);
}

function KitListForTools(currentPrefix, jsonTable, monthname, tool) {

    if (loadedMetrics["KitCountForTools_L3" + tool + monthname] != true) {
        var date = new Date();
        var year = date.getFullYear();
        var prevYear = date.getFullYear(); 
        var lastDay = 0;
        var month = 0;
        if (monthname == "Current Month") {
            month = date.getMonth()+1;
        }/*
        else if (monthname === "Total") {
            month = date.getMonth()+1;
            prevYear--; 
        }*/
        else {
            month =  getMonthFromString(monthname) + 1;
            if (month > (date.getMonth() + 1)) {
                year--;
                prevYear--; 
            }
        }

        lastDay = new Date(year, month, 1);
        lastDay.setMonth(lastDay.getMonth() + 1);
        var nextMonth = lastDay.getMonth();
        if (nextMonth < 10)
            nextMonth = "0" + nextMonth;

        if (month < 10)
            month = "0" + month;

        var startDate = prevYear + month + "01";
        var endDate = year + nextMonth + "01";

        get_KitListForTool(currentPrefix, tool, month, startDate, endDate, monthname);
        return;
    }

    clearChart("KitCountForTools_L3");
    var chartData = jsonTable["KitCountForTools_L3" + tool + monthname];


    // get config
    var levelThreeConfig = generateConfig(currentPrefix, "Kit Sessions - " + tool + " for " + monthname, "Count", "KitNumber", currentTheme, false);
    levelThreeConfig.dataProvider = chartData;
    levelThreeConfig.legend.enabled = false;
    levelThreeConfig.graphs = getL3GraphKitCountForToolsData();

    levelThreeConfig.chartCursor = {
        valueBalloonsEnabled: false,
        fullWidth: true,
        cursorAlpha: 0.1,
        zoomable: false,
        pan: true
    };

    levelThreeConfig.categoryAxis.autoWrap = true;

    delete levelThreeConfig.chartScrollbar.graph;
    delete levelThreeConfig.chartScrollbar.graphType;
    levelThreeConfig.chartScrollbar.autoGridCount = false;
    levelThreeConfig.chartScrollbar.hideResizeGrips = true;
    levelThreeConfig.chartScrollbar.resizeEnabled = false;
    levelThreeConfig.zoomOutText = "";
    levelThreeConfig.mouseWheelZoomEnabled = false;
    levelThreeConfig.mouseWheelScrollEnabled = true;

    var prefix = "KitCountForTools";

    chartConfigDict["KitCountForTools_L3"] = levelThreeConfig;
    var L3_id = "KitCountForTools_L3_chartdiv";
    var chart = AmCharts.makeChart(L3_id, levelThreeConfig, 50);
    chartResizerDict[currentPrefix] = true;

    chart.addLabel(
		35, 20,
		"< Go back",
		undefined,
		15,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:GoUpLevel("' + prefix + '");');

    chart.addLabel(
		'!200', '20',
		"Sort by Sessions",
		undefined,
		12,
		undefined,
		undefined,
		undefined,
		true,
		'javascript:SortKitCountsL3ByCount("' + currentPrefix + '", true);');


    chart.addListener("clickGraphItem", KitCountForTools_L3_handleItemClick);
    chart.addListener("init", BVAchartInit);
    chart.addListener("rendered", L3_rendered);

    $("#loadingdiv").remove(); // remove loading div
    chartDict[currentPrefix] = chart;
    chartDataDict[currentPrefix] = chartData;

    level++; // increase global level indicator


    var dataTable;
    dataTable = chartData; // GetDataForEntry(prefix, restJsonDict, chartJSONTableDict["defectsReleased"], month, subcategory, studio_alias);
    dataTableDataDict[prefix + "_dataTable"] = dataTable;

    if (dataTableIsVisible[prefix + "_dataTable"] !== undefined && dataTableIsVisible[prefix + "_dataTable"] === true)
        showHideDataTable(prefix + "_dataTable");

}


function SortKitCountsL3ByCount(prefix, sortByDefects) {
    var chart = chartDict[prefix];
    var data = chartDataDict[prefix];
    var labelIndex;

    var switchText = "Sort by Sessions";

    if (sortByDefects) {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.Count > b.Count) return -1;
            if (a.Count < b.Count) return 1;
            return 0;
        });

        labelIndex = chart.allLabels.indexOfAttrValuePair("text", switchText);
        chart.allLabels[labelIndex].text = "Sort by Kit Number";
        chart.allLabels[labelIndex].url = 'javascript:SortKitCountsL3ByCount("' + prefix + '", false);';
    } else {
        chart.dataProvider = data.sort(function (a, b) {
            if (a.KitNumber < b.KitNumber) return -1;
            if (a.KitNumber > b.KitNumber) return 1;
            return 0;
        });
        labelIndex = chart.allLabels.indexOfAttrValuePair("text", "Sort by Kit Number");
        chart.allLabels[labelIndex].text = switchText;
        chart.allLabels[labelIndex].url = 'javascript:SortKitCountsL3ByCount("' + prefix + '", true);';
    }
    chart.validateData();
    var zoomTo = chart.dataProvider.length;
    if (zoomTo > 8) zoomTo = 8;
    chart.zoomToIndexes(0, zoomTo);
    chart.invalidateSize();
}


function getL3GraphKitCountForToolsData(data, categoryField) {
    var graphs = [];

    var column = {
        "fillAlphas": 0.8,
        "lineAlpha": 0.3,
        "type": "column",
        "labelPosition": "middle",
        "columnWidth": .8,
        "fontSize": 10,
        "showAllValueLabels": true,
       "valueField": "Count"
    };

    graphs.push(column);


    return graphs;
}