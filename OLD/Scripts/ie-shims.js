// Some common IE shims... indexOf, startsWith, trim

// Just redirect if IE version is 8 or older
if (navigator.appName.indexOf("Internet Explorer") != -1) { //yeah, he's using IE
	var badBrowser = (
		navigator.appVersion.indexOf("MSIE 9") == -1 && //v9 is ok
		navigator.appVersion.indexOf("MSIE 1") == -1 //v10, 11, 12, etc. is fine too
	);

	if (badBrowser) {
		if (confirm("Your Browser is out of date, and does not support this page. Continue anyway?")) {

		} else {
			window.location = "https://browser-update.org/update.html";
		}
	}
}

/*
  Really? IE8 Doesn't have .indexOf
*/
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
		"use strict";
		if (this === null) {
			throw new TypeError();
		}
		var t = Object(this);
		var len = t.length >>> 0;

		if (len === 0) {
			return -1;
		}
		var n = 0;
		if (arguments.length > 1) {
			n = Number(arguments[1]);
			if (n != n) { // shortcut for verifying if it's NaN
				n = 0;
			} else if (n !== 0 && n != Infinity && n != -Infinity) {
				n = (n > 0 || -1) * Math.floor(Math.abs(n));
			}
		}
		if (n >= len) {
			return -1;
		}
		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) {
				return k;
			}
		}
		return -1;
	};
}

/*
  Useful for scanning json data
*/
if (!Array.prototype.indexOfAttrValuePair) {
	Array.prototype.indexOfAttrValuePair = function (searchAttr, searchValue) {
		"use strict";
		if (this === null) {
			throw new TypeError();
		}
		var t = Object(this);
		var len = t.length >>> 0;

		if (len === 0) {
			return -1;
		}
		for (var i = 0; i < len; i++) {
			if (t[i][searchAttr] === searchValue)
				return i;
		}
		return -1;
	};
}


/*
  IE Doesn't have a .startsWith either?
*/
if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (str) {
		return this.lastIndexOf(str, 0) === 0;
	};
}

/*
  IE Doesn't have a .endsWith either?
*/
if (!String.prototype.endsWith) {
	String.prototype.endsWith = function (suffix) {
		return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

// IE < 9 doesn't have a trim() for strings
if (!String.prototype.trim) {
	String.prototype.trim = function () {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

// Adding replaceAll() to all browsers
if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function (search, replacement) {
		var target = this;
		return target.split(search).join(replacement);
	};
}

// IE 8 or older needs forEach
if (!('forEach' in Array.prototype)) {
	Array.prototype.forEach = function (action, that /*opt*/ ) {
		for (var i = 0, n = this.length; i < n; i++)
			if (i in this)
				action.call(that, this[i], i, this);
	};
}

// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
	Object.keys = (function () {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
			hasDontEnumBug = !({
				toString: null
			}).propertyIsEnumerable('toString'),
			dontEnums = [
              'toString',
              'toLocaleString',
              'valueOf',
              'hasOwnProperty',
              'isPrototypeOf',
              'propertyIsEnumerable',
              'constructor'
            ],
			dontEnumsLength = dontEnums.length;

		return function (obj) {
			if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}

			var result = [],
				prop, i;

			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					result.push(prop);
				}
			}

			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						result.push(dontEnums[i]);
					}
				}
			}
			return result;
		};
	}());
}