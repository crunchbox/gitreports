<html>
<head>
<meta charset="ISO-8859-1">
<title>Moncton Metrics Report</title>

    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/amcharts/amcharts.js"></script>
    <script type="text/javascript" src="js/amcharts/serial.js"></script>
    <script type="text/javascript" src="js/amcharts/pie.js"></script>
    <script type="text/javascript" src="js/amcharts/gauge.js"></script>
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script>
        var chart = AmCharts.makeChart("chartdiv",
            {
                    "type": "serial",
                    "categoryField": "issuetypename",
                    "startDuration": 1,
                    "categoryAxis": {
                            "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                            {
                                    "fillAlphas": 1,
                                    "id": "AmGraph-3",
                                    "title": "graph 3",
                                    "type": "column",
                                    "valueField": "count",
                                    "xField": "count"
                            }
                    ],
                    "guides": [],
                    "valueAxes": [
                            {
                                    "axisFrequency": -2,
                                    "id": "ValueAxis-1",
                                    "zeroGridAlpha": -1,
                                    "minHorizontalGap": 73,
                                    "title": "Axis title",
                                    "titleFontSize": 0
                            }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                            "enabled": true,
                            "useGraphSettings": true
                    },
                    "titles": [
                            {
                                    "id": "Title-1",
                                    "size": 15,
                                    "text": "Chart Title"
                            }
                    ],
//                    "dataProvider": [
//                            {
//                                    "issuetypename": "Task",
//                                    "issuenum": "18",
//                                    "issuecount": "3"
//                            },
//                            {
//                                    "issuetypename": "Enhancement",
//                                    "issuenum": "25",
//                                    "issuecount": "23"
//                            },
//                            {
//                                    "issuetypename": "TIR - Game Integration Test",
//                                    "issuenum": "42",
//                                    "issuecount": "157"
//                            },
//                            {
//                                    "issuetypename": "TIR - Developer",
//                                    "issuenum": "6",
//                                    "issuecount": "21"
//                            }
//                    ]
//                    ,
                    "dataLoader": {
            "url": "./data/totalDefects.php?projectID=28921",
            "format":"json"
          }
            }
    );
  </script>
    
</head>
<body>
      <div id="chartdiv" style="width: 100%; height: 500px;"></div>
    
    
</body>

</html>
    