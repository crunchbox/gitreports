
<?php
//header('Content-Type: application/json');
//<!-- Copyright(c) 2014 GTECH Canada ULC and its affiliates -->
//<!--Author: Patrice Richard (patrice.richard@GTECH.com)-->
include '../config/db_conn.php';
$projectID = $_GET['projectID'];

$query = "SET NOCOUNT ON
    select j.issuetype, REPLACE(CAST(i.pname AS varchar(40)),'–','-') as issuetypename, Count(j.issuenum) AS count FROM jiraissue AS j
INNER JOIN issuetype AS i ON i.ID = j.issuetype 
WHERE project = ".$projectID." GROUP BY j.issuetype, i.pname";
//echo $query;


$result = odbc_exec( $metDBConn , $query);

$array = array();

    while ($row = odbc_fetch_array($result)){
        $array[] = array('issuetypeid'=>$row['issuetype'],'issuetypename'=>$row['issuetypename'],'count'=>$row['count']);
    };
   // print_r($array);
echo json_encode($array);
//print_r(json_encode($array))
?>