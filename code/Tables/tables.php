<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>IGT Moncton Metrics</title>
        
        <!-- DB Connection with Jira -->
         <?php require '../config/db_conn.php' ?> 
        
        <!-- Jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="../JQuery/popper.js"></script>

        
        <!-- boostrap link -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
        <script src="../bootstrap/js/bootstrap.js"></script>
        <link rel="stylesheet" href="../topDashboard/CSS/topDashboard.css" />

      
        
        <!--TypeAhead Twitter-->
        <script src="../GameList/typeahead.bundle.js"></script>
        <script src="../GameList/typeaheadCode.js"></script>
        <link rel="stylesheet" href="../GameList/typeaheadjs.css" />
        
        <!-- DATATABLES -->
<!--        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.css"/>-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/af-2.2.2/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.css"/>
 
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.js"></script>
<!--        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.js"></script>-->
        <script>
        $(function(){
          $("#nav-content").load("../config/nav.html");
          
          
          $('#totalDefectTbl').DataTable( {
        "ajax": "totalDefectsTbl.php?projectID=27320&issType=42",
        "columns": [
            {"mData":"Title",
                "mRender":function(data,type,full){
                    return '<a href="'+ full.isslink +'">'+full.pkey+'</a>';
                }, width:'90'},
            { "data": "summary" },
            { "data": "issuetypename", width:'150' }
        ],
        "pageLength":50
    } );
          
        });
        </script>

            
    </head>
<body>
    <div id="nav-content"></div>
    

    <div class="container">
        <br><br><br><br>
        <table id="totalDefectTbl" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Issue Key</th>
                <th>Summary</th>
                <th>Issue Type Name</th>
            </tr>
        </thead>
    </table>

            




      
    </div>
 
</body>
</html>