
<?php
//header('Content-Type: application/json');
//<!-- Copyright(c) 2014 GTECH Canada ULC and its affiliates -->
//<!--Author: Patrice Richard (patrice.richard@GTECH.com)-->
include '../config/db_conn.php';
$projectID = $_GET['projectID'];
$issType = $_GET['issType'];
//echo $projectID;

//$query = "select j.issuetype, REPLACE(CAST(i.pname AS varchar(40)),'-','–','-') as issuetypename, Count(j.issuenum) AS count FROM jiraissue AS j 
//INNER JOIN issuetype AS i ON i.ID = j.issuetype 
//WHERE project = ".$projectID." GROUP BY j.issuetype, i.pname";

$query = "SET NOCOUNT ON SELECT  p.pkey,  j.issuenum, CONCAT(p.pkey, '-',j.issuenum) AS issKey, j.SUMMARY, j.issuetype, 
IIF(j.issuetype = 42, 'Game Integration Test', i.pname) AS issuetypename
FROM jiraissue AS j 
INNER JOIN issuetype AS i ON i.ID = j.issuetype 
INNER JOIN project AS p ON p.ID = j.project
WHERE project = ".$projectID." AND issuetype = ".$issType."";

//echo $query;


$result = odbc_exec( $DBConn , $query);

$array = array();

    while ($row = odbc_fetch_array($result)){
        $summary = utf8_encode($row['SUMMARY']);
        $summaryRep = str_replace('', '_', $summary);
       // echo $summaryRep . "<br>";
       
       
        $array['data'][] = array('pkey'=>$row['issKey'],'isslink'=>"http://cspjira:8080/jira/browse/".$row['issKey'],'issuenum'=> $row['issuenum'],'summary'=>$summaryRep,'issuetype'=>$row['issuetype'],'issuetypename'=>$row['issuetypename']);
    };
    //print_r($array);
    echo json_encode($array);
//print_r(json_encode($array))
    
    
?>