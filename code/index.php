<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>IGT Moncton Metrics</title>
        
        <!-- DB Connection with Jira -->
         <?php require './config/db_conn.php' ?> 
        
        <!-- Jquery -->
<!--        <script src="JQuery/jquery.js"></script>-->
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="JQuery/popper.js"></script>
        
<!--        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.4.1.min.js"></script>-->
     
        
        
        
        <!-- boostrap link -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
        <script src="bootstrap/js/bootstrap.js"></script>
        <link rel="stylesheet" href="topDashboard/CSS/topDashboard.css" />
        
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
      
        
        <!--TypeAhead Twitter-->
        <script src="GameList/typeahead.bundle.js"></script>
        <script src="GameList/typeaheadCode.js"></script>
        <link rel="stylesheet" href="GameList/typeaheadjs.css" />
        
        
        <!-- JavaScript -->

        <script src="GameList/AjaxConnect.js"></script>
        

        <!-- amcharts -->
        <script type="text/javascript" src="amcharts/amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/amcharts/pie.js"></script>
        <script type="text/javascript" src="amcharts/amcharts/gauge.js"></script>
        <script type="text/javascript" src="http://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
        <script type="text/javascript" src="amcharts/chartLoaders.js"></script>
        <script type="text/javascript" src="amcharts/GITvsQA.js"></script>
        <script type="text/javascript" src="amcharts/TestWasteGITQA.js"></script>


      <!-- Slick Caroussel-->
<!--        <script type="text/javascript" src="slick/slick.js"></script>
        <script type="text/javascript" src="GraphIssues/caroussel.js"></script>
        
         <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
         <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
         <link rel="stylesheet" type="text/css" href="GraphIssues/slickcarous.css">-->
         
        <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.js"></script>
        <script type="text/javascript" src="GraphIssues/caroussel.js"></script>
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css">
        <link rel="stylesheet" href="GraphIssues/slickcarous.css">
        
        <!-- DATATABLES -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.js"></script>

        <script>
        $(function(){
          $("#nav-content").load("./config/nav.html");
        });
        </script>

            
    </head>
<body>
    <div id="nav-content"></div>
    

    <div class="container">
        <br>
        <br>
        <br>
        <br>
        
            <h2>Welcome to the Moncton IGT Report</h2>
<!--            <p>Please select a Game in the List below.</p>-->
              
        <form>
        <div class="form-group">
          <div id="txtHint"><b>Game List will be listed here ...</b></div>
          <select class="form-control form-control-md" id="projListSelect" >
              <option></option>
            
      
          </select>

            <br>
            
            <input type="button" id="submitProject" class="btn btn-success" value="Submit">
         </form>

        </div> 

            
        

            
         <div id="GraphIssues"  >
<!--             Components Issues, Open vs Reopen vs In progess, Closed accept vs Closed NP vs Closed Pass vs Duplicate ... -->
                       
                                <div class="column small-11 small-centered">
                                        
                                        <div class="slider slider-for">
                                            
                                            <div>
                                                <div class="chartTitle" id="totalDefectTitle">TITLE</div>
                                                <div class="chartTitleGame" id="totalDefectTitleGame">TITLE</div>
                                                <div id="totalDefectChart" style="width: 1110px; height: 510px;"></div>
                                                
                                            </div>
                                                
                                            <div>
                                                <div class="chartTitle" id="GITvsQADefectTitle">TITLE</div>
                                                <div class="chartTitleGame" id="GITvsQADefectTitleGame">TITLE</div>
                                                <div id="GITvsQA" style="width: 1110px; height: 510px;"></div>
                                                    
                                            </div>
                                                
                                            <div>
                                                <div class="chartTitle" id="TestWasteGITQADefectTitle">TITLE</div>
                                                <div class="chartTitleGame" id="TestWasteGITQADefectTitleGame">TITLE</div>
                                                <div id="TestWasteGITQA" style="width: 1110px; height: 510px;"></div>
                                                    
                                            </div>
                                                
                                                <div><h3 style="width: 1110px; height: 510px;">4</h3></div>
                                                <div><h3 style="width: 1110px; height: 510px;">5</h3></div>
                                                <div><h3 style="width: 1110px; height: 510px;">6</h3></div>
                                                <div><h3 style="width: 1110px; height: 510px;">7</h3></div>
                                                <div><h3 style="width: 1110px; height: 510px;">8</h3></div>
                                                <div><h3 style="width: 1110px; height: 510px;">9</h3></div>
                                            
                                        </div>
                                    <div class="slider slider-nav">
                                        <div class="blocksSlick"><span><div id="totalDefectChartSmall" style="width: 321px; height: 245px; " class="img"></div></span></div>
                                        <div class="blocksSlick"><span><div id="GITvsQASmall" style="width: 321px; height: 245px;" class="img"></div></span></div>
                                        <div class="blocksSlick"><span><div id="TestWasteGITQASmall" style="width: 321px; height: 245px;" class="img"></div></span></div>
                                        
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>4</span></h3></div>
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>5</span></h3></div>
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>6</span></h3></div>
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>7</span></h3></div>
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>8</span></h3></div>
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>9</span></h3></div>
                                        <div class="blocksSlick"><h3 style="width: 321px; height: 245px;"><span>10</span></h3></div>
                                    </div>
                                        
                                </div>
                        
            
                        
        </div>



      
    </div>







                    <div id="SideMenu"><!--
                    <!-- Hours Reports per Game and TIR per Games -->
                    </div>



                    <div id="HoursMetrics">
                    <!-- GIT vs Dev GIT vs QA QA vs Dev GIT vs QA vs Dev -->
                    </div>
        
    
    
 
</body>
</html>