
$(window).ready(function(){

    // Whe Submit button gets clicked
    $("#submitProject").click(function(){
        newTotalDefects();
    })

    //First Load of files
     loadTotalDefects();
     loadTotalDefectsSmall();
})
   


function loadTotalDefects(){
    totalDefects = AmCharts.makeChart("totalDefectChart",
        {
            "type": "serial",
            "categoryField": "issuetypename",
            "startDuration": 1,
            "categoryAxis": {
                    "gridPosition": "start"
            },
            "trendLines": [],
            "graphs": [
                    {
                            "fillAlphas": 1,
                            "id": "AmGraph-3",
                            "title": "TIR",
                            "type": "column",
                            "valueField": "count",
                            "xField": "count",
                            "urlField": "url"
                    }
            ],
            "guides": [],
            "valueAxes": [
                    {
                            "axisFrequency": -2,
                            "id": "ValueAxis-1",
                            "zeroGridAlpha": -1,
                            "minHorizontalGap": 73,
                            "title": "Axis title",
                            "titleFontSize": 0
                    }
            ],
            "allLabels": [],
            "balloon": {},
            "legend": {
                    "enabled": true,
                    "useGraphSettings": true
            },
//            "titles": [
//                    {
//                            "id": "Title-1",
//                            "size": 15,
//                            "text": "Chart Title"
//                    }
//            ],
            "dataLoader": {
                "url": "./amcharts/requests/totalDefects.php?projectID=28921",
                "format":"json"
            }
        }
    );
    $("#totalDefectTitle").html("Total Defects for: ");
    
}
    
    
    
function loadTotalDefectsSmall(){
    totalDefectsSmall = AmCharts.makeChart("totalDefectChartSmall",
        {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 13,
            "labelText": "",
            "descriptionField": "issuetypename",
            "labelTickAlpha": 0,
            "maxLabelWidth": 196,
            "tabIndex": -3,
            "titleField": "issuetypename",
            "valueField": "count",
            "fontSize": 14,
            "processCount": 995,
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataLoader": {
                "url": "./amcharts/requests/totalDefects.php?projectID=28921",
                "format":"json"
            }
        }
    );
}
    
function newTotalDefects(){
    var e = document.getElementById("projListSelect");
    var selProjectID = e.options[e.selectedIndex].id;
    var selProjectVal = e.options[e.selectedIndex].value
    //alert (selProjectVal);
    $.ajax({
           url:'./amcharts/requests/totalDefects.php?projectID='+selProjectID,
           dataType:'JSON',
           success:function(data){
               totalDefects.dataProvider = data;
               totalDefects.validateData();
               totalDefectsSmall.dataProvider = data;
               totalDefectsSmall.validateData();
               $("#totalDefectTitle").html("Total Defects per Project Type for:");
               $("#totalDefectTitleGame").html(selProjectVal);
           }
    });
}