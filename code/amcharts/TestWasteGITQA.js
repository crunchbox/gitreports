$(window).ready(function(){

    // Whe Submit button gets clicked
    $("#submitProject").click(function(){
        newTestWasteDefects();
    })

    //First Load of files
     loadTestWasteDefects();
     loadTestWasteDefectsSmall();
})
   


function loadTestWasteDefects(){
    TestWasteGITQADefects = AmCharts.makeChart("TestWasteGITQA",
        {
            "type": "serial",
            "categoryField": "issuetypename",
            "startDuration": 1,
            "categoryAxis": {
                    "gridPosition": "start"
            },
            "trendLines": [],
            "graphs": [
                    {
                            "fillAlphas": 1,
                            "id": "AmGraph-3",
                            "title": "GIT vs QA",
                            "type": "column",
                            "valueField": "count",
                            "xField": "count"
                    }
            ],
            "guides": [],
            "valueAxes": [
                    {
                            "axisFrequency": -2,
                            "id": "ValueAxis-1",
                            "zeroGridAlpha": -1,
                            "minHorizontalGap": 73,
                            "title": "Axis title",
                            "titleFontSize": 0
                    }
            ],
            "allLabels": [],
            "balloon": {},
            "legend": {
                    "enabled": true,
                    "useGraphSettings": true
            },
//            "titles": [
//                    {
//                            "id": "Title-1",
//                            "size": 15,
//                            "text": "Chart Title"
//                    }
//            ],
            "dataLoader": {
                "url": "./amcharts/requests/TestWasteGITQA.php?projectID=28921",
                "format":"json"
            }
        }
    );
    $("#TestWasteGITQADefectTitle").html("Total Waste Defect for: ");
    
}
    
    
    
function loadTestWasteDefectsSmall(){
    TestWasteDefectsSmall = AmCharts.makeChart("TestWasteGITQASmall",
        {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 13,
            "labelText": "",
            "descriptionField": "issuetypename",
            "labelTickAlpha": 0,
            "maxLabelWidth": 196,
            "tabIndex": -3,
            "titleField": "issuetypename",
            "valueField": "count",
            "fontSize": 14,
            "processCount": 995,
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataLoader": {
                "url": "./amcharts/requests/TestWasteGITQA.php?projectID=28921",
                "format":"json"
            }
        }
    );
}
    
function newTestWasteDefects(){
    var e = document.getElementById("projListSelect");
    var selProjectID = e.options[e.selectedIndex].id;
    var selProjectVal = e.options[e.selectedIndex].value
    //alert (selProjectVal);
    $.ajax({
           url:'./amcharts/requests/TestWasteGITQA.php?projectID='+selProjectID,
           dataType:'JSON',
           success:function(data){
               TestWasteGITQADefects.dataProvider = data;
               TestWasteGITQADefects.validateData();
               TestWasteDefectsSmall.dataProvider = data;
               TestWasteDefectsSmall.validateData();
               $("#TestWasteGITQADefectTitle").html("Total Waste Defects GIT vs QA per Project Type for:");
               $("#TestWasteGITQADefectTitleGame").html(selProjectVal);
           }
    });
}



